<?php

/**
 * Created by
 * User: Wisard17
 * Filename: ${FILE_NAME}
 * Date: 18/11/2020
 * Time: 10:44 AM
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this \yii\web\View */
/* @var $model \app\models\ResetPass */

$this->title = 'Change Password';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?= Html::encode($this->title) ?> </h5>
                <div class="ibox-tools">

                </div>
            </div>

            <div class="ibox-content">

                <div class="row">
                    <div class="col-lg-5">

                        <?php $form = ActiveForm::begin(['id' => 'reset-form']); ?>

                        <?= $form->field($model, 'old_password')->passwordInput(['autofocus' => true]) ?>

                        <?= $form->field($model, 'new_password1')->passwordInput()
                            ->hint("Password minimal 4 karakter ") ?>

                        <?= $form->field($model, 'new_password2')->passwordInput() ?>

                        <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                            'template' => '<div class="row"><div class="col-lg-3">{input}</div><div class="col-lg-6">{image}</div></div>',
                        ]) ?>

                        <div class="form-group">
                            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                        </div>

                        <?php ActiveForm::end(); ?>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
