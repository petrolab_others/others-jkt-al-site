<?php

/* @var $this yii\web\View */
use yii\web\View;
use yii\helpers\Html;
use yii\helpers\Url;
use \app\modules\reports\models\DashboardPieChartEvalCodeView as pieChart;

$this->title = 'Reports Applications';
?>
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">
        <article class="col-sm-12 sortable-grid ui-sortable">

            <!-- new widget -->
            <div class="jarviswidget jarviswidget-color-blue jarviswidget-sortable" id="wid-id-5" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false" role="widget">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header role="heading" class="ui-sortable-handle"><div class="jarviswidget-ctrls" role="menu">   <a href="javascript:void(0);" class="button-icon jarviswidget-toggle-btn" rel="tooltip" title="Collapse" data-placement="bottom"><i class="fa fa-minus "></i></a> <a href="javascript:void(0);" class="button-icon jarviswidget-fullscreen-btn" rel="tooltip" title="Fullscreen" data-placement="bottom"><i class="fa fa-expand "></i></a> </div>
                    <!-- <span class="widget-icon"> <i class="glyphicon glyphicon-stats txt-color-darken"></i> </span> -->
                    <h2>Status </h2>

                    <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>

                <!-- widget div-->
                <div role="content">
                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <label>Title:</label>
                        <input type="text">
                    </div>
                    <!-- end widget edit box -->

                    <div class="widget-body no-padding smart-form">

                        <!-- search form -->
                        <form action="<?= Url::to(['site/index']) ?>" class="smart-form" method="get" autocomplete="off">
                                <fieldset>
                                    <section class="col col-2">
                                        <label class="label">Report Date</label>
                                        <label class="input">
                                            <i class="icon-append fa fa-calendar"></i>
                                            <input type="text" id="begin" name="begin" value="<?= @$_GET['begin'] ?>"
                                                   placeholder="Begin">
                                        </label>

                                    </section>
                                    <section class="col col-2">
                                        <label class="label">&nbsp;</label>
                                        <label class="input">
                                            <i class="icon-append fa fa-calendar"></i>
                                            <input type="text" id="end" name="end" value="<?= @$_GET['end'] ?>"
                                                   placeholder="End">
                                        </label>
                                    </section>
                                    <section class="col col-8">
                                        <label class="label">&nbsp;</label>
                                        <?= Html::button('Submit', ['class' => 'btn btn-primary btn-sm', 'type' => "submit"]) . ' 
                                        ' . Html::a('<i class="fa fa-refresh"></i> Cancel', ['/'], ['class' => 'btn btn-default btn-sm', 'type' => "button"]); ?>
                                    </section>
                                </fieldset>
                            </form>
                        <?php 
                                $beginDate = $params['begin'] = !empty($_GET['begin'])? $_GET['begin'] : '';
                                $endDate = $params['end'] = !empty($_GET['end'])? $_GET['end'] : '';
                        ?>
                        <?= pieChart::widget(['beginDate' => $beginDate, 'endDate' => $endDate])?>

                        <!-- end content -->
                    </div>

                </div>
                <!-- end widget div -->
            </div>
            <!-- end widget -->

        </article>
    </div>

    <!-- end row -->

</section>
<?php
$jsScript = <<< JS

// Date Range Picker
   $("#end").datepicker({
       defaultDate: "+1w",
       changeMonth: true,
       numberOfMonths: 3,
       dateFormat: 'yy-mm-dd',
       prevText: '<i class="fa fa-chevron-left"></i>',
       nextText: '<i class="fa fa-chevron-right"></i>',
       onClose: function (selectedDate) {
           $("#begin").datepicker("option", "maxDate", selectedDate);
       }

   });
   $("#begin").datepicker({
       defaultDate: "+1w",
       changeMonth: true,
       numberOfMonths: 3,
       dateFormat: 'yy-mm-dd',
       prevText: '<i class="fa fa-chevron-left"></i>',
       nextText: '<i class="fa fa-chevron-right"></i>',
       onClose: function (selectedDate) {
           $("#end").datepicker("option", "minDate", selectedDate);
       }
   });       

JS;
$this->registerJs($jsScript, View::POS_READY, 'index_login');
?>