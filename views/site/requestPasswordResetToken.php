<?php

/**
 * Created by
 * User: Wisard17
 * Filename: ${FILE_NAME}
 * Date: 18/11/2020
 * Time: 10:50 AM
 */

/* @var $this \yii\web\View */
/* @var $model \app\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Request password reset';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-request-password-reset">

    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?= Html::encode($this->title) ?> </h5>
                <div class="ibox-tools">
                    <a class="collapse-link">

                    </a>
                </div>
            </div>

            <div class="ibox-content">

                <p>Please fill out your email. A link to reset password will be sent there.</p>

                <div class="row">
                    <div class="col-lg-5">
                        <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>

                        <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

                        <div class="form-group">
                            <?= Html::submitButton('Send', ['class' => 'btn btn-primary']) ?>
                        </div>

                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
