<?php
/**
 * Created by
 * User: Wisard17
 * Date: 15/07/2018
 * Time: 06.04 PM
 */

namespace app\assets;


use yii\web\AssetBundle;

/**
 * Class HighChart
 * @package app\assets\highchart
 */
class HighChart extends AssetBundle
{
    public $sourcePath = '@app/assets/plugins/highchart/';

    public $css = [
    ];

    public $js = [
        'highcharts.js',
        'modules/data.js',
        'modules/drilldown.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}