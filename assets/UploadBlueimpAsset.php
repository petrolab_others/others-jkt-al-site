<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use Yii;
use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class UploadBlueimpAsset extends AssetBundle
{

    public $sourcePath = '@vendor/blueimp/jquery-file-upload';

    public $css = [

    ];
    public $js = [
        "js/vendor/jquery.ui.widget.js",
        "js/jquery.iframe-transport.js",
        "js/jquery.fileupload.js",
//        "js/jquery.fileupload-process.js",
//        "js/jquery.fileupload-image.js",
//        "js/jquery.fileupload-audio.js",
//        "js/jquery.fileupload-video.js",
//        "js/jquery.fileupload-validate.js",
//        "js/jquery.fileupload-ui.js",
//        "js/main.js",
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];

    /**
     *
     */
    public function init()
    {
        parent::init();
//        $patch = Yii::getAlias('@vendor/blueimp/jquery-file-upload/server/php');
//        require_once $patch . '/UploadHandler.php';
    }
}
