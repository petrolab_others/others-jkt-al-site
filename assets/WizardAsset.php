<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use Yii;
use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class WizardAsset extends AssetBundle
{

    public $sourcePath = '@app/smartadmin/resources/assets';

    public $css = [
    ];

    public $js = [
        'js/plugin/bootstrap-wizard/jquery.bootstrap.wizard.min.js',
    ];

    public $depends = [
        'app\smartadmin\assets\PaceAsset',
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];


}
