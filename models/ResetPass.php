<?php
/**
 * Created by PhpStorm.
 * User: wisard17
 * Date: 6/13/2017
 * Time: 10:05 AM
 */

namespace app\models;

use Yii;

class ResetPass extends User
{
    public $old_password;

    public $new_password1;

    public $new_password2;

    public $verifyCode;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['old_password', 'new_password1', 'new_password2'], 'required'],
            [['new_password1', 'new_password2'], 'string', 'min' => 4],
            // verifyCode needs to be entered correctly
            ['verifyCode', 'captcha'],
            ['old_password', 'validPassword'],

            ['new_password2', 'compare', 'compareAttribute' => 'new_password1'],
        ]);
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'old_password' => Yii::t('app', 'Old Password'),
            'new_password1' => Yii::t('app', 'New Password'),
            'new_password2' => Yii::t('app', 'Retype New Password'),
        ]);
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validPassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (!$this->validatePassword($this->old_password)) {
                $this->addError($attribute, 'Incorrect password.');
            }
        }
    }

    public function reset()
    {
        if (!$this->validate()) {
            return false;
        }

        $this->setPassword($this->new_password1);
        $this->save(false);

        return true;
    }
}