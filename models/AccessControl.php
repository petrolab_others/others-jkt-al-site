<?php
/**
 * Created by
 * User: Wisard17
 * Date: 20/11/2017
 * Time: 12.36 PM
 */

namespace app\models;


/**
 * Class AccessControl
 * @package app\models
 */
class AccessControl extends \yii\filters\AccessControl
{

    public $listModule = [
        'default' => [
            'site' => ['index', ]
        ],
        'users' => [
            'default' => ['index', ]
        ],
        'reports' => [
            'site' => ['index', ]
        ],
        'master' => [
            'site' => ['index', ]
        ],
        'customers' => [
            'site' => ['index', ]
        ],
        'options' => [
            'site' => ['index', ]
        ],
        'data' => [
            'site' => ['index', ]
        ],
    ];
}