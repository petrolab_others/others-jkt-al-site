<?php
/**
 * Created by
 * User: Wisard17
 * Date: 10/13/2017
 * Time: 8:55 AM
 */

namespace app\models;

use app\modules\data\models\TypeView;
use app\smartadmin\SideBar;
use Yii;
use yii\base\Model;
use yii\helpers\Url;

/**
 * Class Menu
 * @package app\models
 *
 * @property array $listMenu
 */
class Menu extends Model
{

    public static function listMenu()
    {
        $self = new self();
        return $self->listMenu;
    }

    /**
     * @return array|\yii\console\Response|\yii\web\Response
     */
    public function getListMenu()
    {
        if (Yii::$app->user->isGuest)
            return Yii::$app->getResponse()->redirect(array(Url::to(['site/login'], 302)));
        $monMenu = [['label' => 'Used Oil', 'url' => ['/reports/default'],
            'visible' => in_array(Yii::$app->user->identity->ruleAccess, [-1, 2, 3, 4, 5], true)]];
        foreach (TypeView::find()->all() as $value) {
            if ($value->tree == '') {
                $monMenu[] = ['label' => $value->name, 'url' => ['/reports/' . $value->nameController()],
                    'visible' => in_array(Yii::$app->user->identity->ruleAccess, [-1, 2, 3, 4, 5], true)];
            } else {
                $monMenu[$value->tree]['label'] = $value->tree;
                $monMenu[$value->tree]['visible'] = in_array(Yii::$app->user->identity->ruleAccess, [-1, 2, 3, 4, 5], true);
                $monMenu[$value->tree]['items'][] = ['label' => $value->name, 'url' => ['/reports/' . $value->nameController()],
                    'visible' => in_array(Yii::$app->user->identity->ruleAccess, [-1, 2, 3, 4, 5], true)];
            }
        }
        
        return [
            'options' => ['class' => 'sidebar-menu'],
            'items' => [
                ['label' => 'Login', 'icon' => 'fa fa-user', 'url' => ['/site/login'], 'visible' => Yii::$app->user->isGuest],
                ['label' => 'Dashboard', 'icon' => 'fa fa-dashboard', 'url' => ['/site/index'], 'visible' => !Yii::$app->user->isGuest],
                ['label' => 'Monitoring', 'icon' => 'fa fa-desktop', 'visible' => in_array(Yii::$app->user->identity->ruleAccess, [-1, 2, 3, 4, 5], true),
                    'items' => $monMenu,
                ],
                ['label' => 'Master', 'icon' => 'fa fa-cubes', 'visible' => in_array(Yii::$app->user->identity->ruleAccess, [-1, 4, ], true),
                    'items' => [
                        ['label' => 'Used Oil', 'items' => [
                            ['label' => 'Input Data', 'url' => ['/master/report/new', 'input' => 1],
                                'visible' => in_array(Yii::$app->user->identity->ruleAccess, [-1, 4, 5, 6], true)],
                            ['label' => 'Report', 'url' => ['/master/report/index'],
                                'active' => SideBar::isActiveItems(['url' => ['/master/report/view']]) ||
                                    SideBar::isActiveItems(['url' => ['/master/report/index']]) ||
                                    SideBar::isActiveItems(['url' => ['/master/report/edit']]),
                                'visible' => in_array(Yii::$app->user->identity->ruleAccess, [-1, 4, 5, 6], true)],
                            ['label' => 'Upload', 'url' => ['/master/report/upload'],
                                'visible' => in_array(Yii::$app->user->identity->ruleAccess, [-1, 4, 5, 6], true)],
                            ['label' => 'Others Parameter', 'url' => ['/master/parameter/others'],
                                'visible' => in_array(Yii::$app->user->identity->ruleAccess, [-1, 4, 5, 6], true)],
                            ['label' => 'Parameter', 'url' => ['/master/parameter/index'],
                                'visible' => in_array(Yii::$app->user->identity->ruleAccess, [-1, 4, 5, 6], true)],
                            ['label' => 'Lube Oil', 'url' => ['/master/lube-oil'],
                                'visible' => in_array(Yii::$app->user->identity->ruleAccess, [-1, 4, 5, 6], true)],
                            ['label' => 'Matrix', 'icon' => 'fa fa-sliders', 'visible' => in_array(Yii::$app->user->identity->ruleAccess, [-1, 4, 5, 6], true),
                                'items' => [
                                    ['label' => 'Oil', 'url' => ['/master/oil-matrix'],
                                        'visible' => in_array(Yii::$app->user->identity->ruleAccess, [-1, 4, 5, 6], true)],
                                    ['label' => 'Wear', 'url' => ['/master/matrix'],
                                        'visible' => in_array(Yii::$app->user->identity->ruleAccess, [-1, 4, 5, 6], true)],
                                    ['label' => 'Flash Point', 'url' => ['/master/pq-matrix'],
                                        'visible' => in_array(Yii::$app->user->identity->ruleAccess, [-1, 4, 5, 6], true)],
                                ],
                            ],
                        ]],
                        ['label' => 'Others Package', 'items' => [
                            ['label' => 'Input Data', 'url' => ['/data/default/new', 'input' => 1],
                                'visible' => in_array(Yii::$app->user->identity->ruleAccess, [-1, 4, 5, 6], true)],
                            ['label' => 'Report', 'url' => ['/data/default/index'],
                                'active' => SideBar::isActiveItems(['url' => ['/data/default/view']]) ||
                                    SideBar::isActiveItems(['url' => ['/data/default/index']]) ||
                                    SideBar::isActiveItems(['url' => ['/data/default/edit']]),
                                'visible' => in_array(Yii::$app->user->identity->ruleAccess, [-1, 4, 5, 6], true)],
                            ['label' => 'Upload', 'url' => ['/data/default/upload'],
                                'visible' => in_array(Yii::$app->user->identity->ruleAccess, [-1, 4, 5, 6], true)],
                            ['label' => 'Parameter', 'url' => ['/data/parameter'],
                                'visible' => in_array(Yii::$app->user->identity->ruleAccess, [-1, 4, 5, 6], true)],
                            ['label' => 'Report Type', 'url' => ['/data/report-type'],
                                'visible' => in_array(Yii::$app->user->identity->ruleAccess, [-1, 4, 5, 6], true)],
                            ['label' => 'Category Type', 'url' => ['/data/category-type'],
                                'visible' => in_array(Yii::$app->user->identity->ruleAccess, [-1, 4, 5, 6], true)],
                            ['label' => 'Reference', 'url' => ['/data/reference'],
                                'visible' => in_array(Yii::$app->user->identity->ruleAccess, [-1, 4, 5, 6], true)],
                            ['label' => 'Limit',
                                'items' => [
                                    ['label' => 'Matrix', 'url' => ['/data/matrix'],
                                        'visible' => in_array(Yii::$app->user->identity->ruleAccess, [-1, 4, 5, 6], true)],
                                    ['label' => 'Oil Matrix', 'url' => ['/data/oil-matrix'],
                                        'visible' => in_array(Yii::$app->user->identity->ruleAccess, [-1, 4, 5, 6], true)],
                                    ['label' => 'Typical', 'url' => ['/data/spesifikasi'],
                                        'visible' => in_array(Yii::$app->user->identity->ruleAccess, [-1, 4, 5, 6], true)],
                                ]
                            ],

                        ]],
                        ['label' => 'Customer', 'url' => ['/customers'],
                            'visible' => in_array(Yii::$app->user->identity->ruleAccess, [-1, 4, 5, 6], true)],
                        ['label' => 'Unit', 'url' => ['/master/unit'],
                            'visible' => in_array(Yii::$app->user->identity->ruleAccess, [-1, 4, 5, 6], true)],
                    ],
                ],
                ['label' => 'Users', 'icon' => 'fa fa-user', 'url' => ['/users'], 'visible' => in_array(Yii::$app->user->identity->ruleAccess, [-1,  4], true),]
            ],
        ];
    }
}