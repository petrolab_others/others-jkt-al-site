<?php

namespace app\models;

use app\components\ArrayHelper;
use app\modelsDB\Userlevels;
use app\modules\customers\models\Customers;
use app\modules\users\models\AdditionUser;
use app\modules\users\models\UserLog;
use Yii;
use app\modelsDB\Users;
//use yii\base\Object;
use yii\helpers\Json;
use yii\web\IdentityInterface;

/**
 * Class User
 *
 *
 * @package app\models
 *
 * @property string $authKey
 * @property string $password
 * @property string $ruleName
 * @property \yii\db\ActiveQuery|\app\modules\users\models\AdditionUser $additionUser
 * @property string $fullName
 * @property mixed $id
 * @property int $ruleAccess
 * @property null|string $passwordResetToken
 * @property \app\modules\users\models\UserLog $sessionLog
 * @property \yii\db\ActiveQuery|\app\modelsDB\Userlevels $usersLevel
 * @property array $allId
 * @property string $additionRule
 * @property \yii\db\ActiveQuery|\app\modules\customers\models\Customers $customer
 * @property string $username
 */
class User extends Users implements IdentityInterface
{
//    public $id;
//    public $username;
//    public $password;
//    public $authKey;
//    public $accessToken;

//    private static $users = [
//        '100' => [
//            'id' => '100',
//            'username' => 'admin',
//            'password' => 'admin',
//            'authKey' => 'test100key',
//            'accessToken' => '100-token',
//        ],
//        '101' => [
//            'id' => '101',
//            'username' => 'demo',
//            'password' => 'demo',
//            'authKey' => 'test101key',
//            'accessToken' => '101-token',
//        ],
//    ];

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['Username'], 'unique'],
        ]);
    }


    /**
     * @return array
     */
    public static function admin()
    {
        return [
            'Username' => 'admin',
            'Password' => '31e0b6d892f396b9a23ff3c03481a0af',
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        if ($id === 'admin')
            return new static(self::admin());
        return self::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        if ($username === 'admin')
            return new static(self::admin());
        return static::findOne($username);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        $this->authKey;
        return $this->Username;
    }

    /**
     * @inheritdoc
     * @throws \yii\base\Exception
     */
    public function getAuthKey()
    {
        if ($this->Username === 'admin')
            return 's';
        if ($this->additionUser === null) {
            $aduser = AdditionUser::findOne(['username' => $this->Username,]);

            if ($aduser === null) {
                $aduser = new AdditionUser([
                    'username' => $this->Username,
                    'authKey' => Yii::$app->security->generateRandomString(),
                ]);
            }

            return $aduser->save()? $aduser->authKey : 's';
        }

        return $this->additionUser->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->Password === md5($password);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->Password = md5($password);
    }

    public function validateRules($role)
    {
        $addRule = Json::decode($this->additionRule);
        if (isset($addRule['rules']) && is_array($addRule['rules'])) {
            foreach ($addRule['rules'] as $ruleS) {
                return $ruleS == $role;
            }
        }
        return false;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->Username;
    }

    private $_addUser;

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdditionUser()
    {
        if ($this->_addUser == null) {
            $this->_addUser = $this->hasOne(AdditionUser::className(), ['username' => 'Username']);
        }
        return $this->_addUser;
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        if (parent::save($runValidation, $attributeNames)) {
            if ($this->additionUser != null) {
                $this->additionUser->save();
            }
            return true;
        }
        return false;
    }

    /**
     * @return int
     */
    public function getRuleAccess()
    {
        if ($this->Username === 'admin') {
            return -1;
        }
        return $this->User_Level;
    }

    /**
     * @param $ruleId
     */
    public function setRuleAccess($ruleId)
    {
        if ($this->additionUser === null) {
            $this->authKey;
        }

        $this->User_Level = $ruleId;
    }

    /**
     * @return string
     */
    public function getAdditionRule()
    {
        if ($this->additionUser === null) {
            return '';
        }
        return $this->additionUser->addition_rule;
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        if ($this->Username === 'admin')
            return 'Administrator';
        return $this->First_Name . ' ' . $this->Last_Name;
    }

    /**
     * @return string
     */
    public function getRuleName()
    {
        if ($this->Username === 'admin')
            return 'Administrator';
        return $this->User_Level !== null ? $this->usersLevel->User_Level_Name : 'Petrolab';
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        $adU = AdditionUser::findOne(['accessToken' => $token]);

        if ($adU === null)
            return null;

        return static::findOne([
            'Username' => $adU->username,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire = 24 * 1200;
        return $timestamp + $expire >= time();
    }

    /**
     * Generates new password reset token
     * @throws \yii\base\Exception
     */
    public function generatePasswordResetToken()
    {
        if ($this->additionUser === null) {
            $aduser = AdditionUser::findOne(['username' => $this->Username,]);

            if ($aduser === null) {
                $aduser = new AdditionUser([
                    'username' => $this->Username,
                    'accessToken' => Yii::$app->security->generateRandomString() . '_' . time(),
                ]);
            }
        } else {
            $aduser = $this->additionUser;
            $aduser->accessToken = Yii::$app->security->generateRandomString() . '_' . time();
        }
        $aduser->save();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        if ($this->additionUser !== null) {
            $aduser = $this->additionUser;
            $aduser->accessToken = null;
            $aduser->save();
        }
    }

    /**
     * @return null|string
     */
    public function getPasswordResetToken()
    {
        if ($this->additionUser === null) {
            return null;
        } else {
            return $this->additionUser->accessToken;
        }
    }

    /**
     * @return \yii\db\ActiveQuery|Userlevels
     */
    public function getUsersLevel()
    {
        return $this->hasOne(Userlevels::className(), ['User_Level_ID' => 'User_Level']);
    }

    /**
     * @return \yii\db\ActiveQuery|Customers
     */
    public function getCustomer()
    {
        return $this->hasOne(Customers::className(), ['CustomerID' => 'customer_id']);
    }

    public $_temId;

    public function getAllId()
    {
        $out = [];
        if ($this->User_Level == 5 && $this->_temId == null) {
            $ac = Customers::find()->select(['CustomerID'])
                ->where(['branch_no' => $this->customer->branch_no])
                ->asArray()->all();
            $this->_temId = ArrayHelper::getColumn($ac,'CustomerID');
            return $this->_temId;
        }

        $report_to = self::find()->where(['Report_To' => $this->customer_id])->all();
        $out[] = $this->customer_id;
        foreach ($report_to as $item) {
            foreach ($item->getAllId() as $item2) {
                $out[] = $item2;
            }
        }

        return $out;
    }

    public function log($action)
    {
        $log = $this->sessionLog;
        $tmp = $log->log == '' ? [] : Json::decode($log->log);
        $new[] = [
            'do_action' => $action,
            'time' => time(),
        ];
        $log->log = Json::encode(array_merge($new,$tmp));
        $log->save();
    }


    public $_session;

    public function getSessionLog()
    {
        if ($this->_session == null) {
            $parm = ['username' => $this->id, 'ip_login' => Yii::$app->request->userIP, 'session_id' => Yii::$app->session->id];
            $log = UserLog::findOne($parm);
            if ($log == null || $log->date_logout != '') {
                $log = new UserLog($parm);
                $log->date_login = date('Y-m-d H:i:s');
            }
            $this->_session = $log;
        }

        return $this->_session;
    }

    public function logout()
    {
        $log = $this->sessionLog;
        $log->date_logout = date('Y-m-d H:i:s');
        $log->save();
    }
}
