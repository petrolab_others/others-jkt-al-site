<?php

namespace app\controllers;

use app\models\PasswordResetRequestForm;
use app\models\ResetPass;
use app\models\ResetPasswordForm;
use app\modelsDB\TblComponent;
use app\modelsDB\TblTransaction;
use app\modelsDB\TblUnit;
use app\modules\customers\models\Branch;
use app\modules\customers\models\Customers;
use Yii;
use app\models\AccessControl;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'index'],
                'rules' => [
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
                'layout' => 'main_login',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        $this->layout = 'main_login';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * @return string|Response
     */
    public function actionChangePassword()
    {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $this->layout = 'main_login';

        $model = ResetPass::findOne(['Username' => Yii::$app->user->id]);

        if ($model === null) {
            Yii::$app->session->setFlash('error', 'User tidak terdaftar dalam Database');
            return $this->goHome();
        }

        if ($model->load(Yii::$app->request->post()) && $model->reset()) {
            Yii::$app->session->setFlash('success', 'Password Berhasil diganti');
            return $this->goHome();
        }

        return $this->render('reset_pass', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $this->layout = 'main_login';
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', '<p>Password change has been sent! <br>
                            We have sent an email to ' . $model->email . ' with a link to set your password.
                            </p>');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        $this->layout = 'main_login';
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionRepair($id, $chosen)
    {
        if ($id == 'lockopen') {
            ini_set('memory_limit', '-1');
            ini_set('max_execution_time', -1);
            $transaction = Yii::$app->db->beginTransaction();
            try {
                if ($chosen == 'branch') {
                    $branch = Branch::find()->all();
                    foreach ($branch as $b) {
                        $b2 = Branch::find()->where(['branch' => $b->branch, 'address' => $b->address,
                            'telp_fax' => $b->telp_fax])->all();
                        if (count($b2) > 1) {
                            foreach ($b2 as $value) {
                                if ($value->branch_no != $b->branch_no) {
                                    $value->delete();
                                }
                            }
                        }
                    }
                }
                if ($chosen == 'customer') {
                    $branch = Customers::find()->orderBy('CustomerID')->all();
                    foreach ($branch as $b) {
                        $b2 = Customers::find()->where(['Name' => $b->Name, 'Branch' => $b->Branch,
                            'Address' => $b->Address])->all();
                        if (count($b2) > 1) {
                            foreach ($b2 as $value) {
                                if ($value->CustomerID != $b->CustomerID) {
                                    $txCount = TblTransaction::find()->where(['customer_id' => $value->CustomerID,])->count();
                                    for ($bg = 1; $bg <= (floor($txCount/500) +1); $bg++) {
                                        $tx = TblTransaction::find()
                                            ->limit(500)->offset(500 * ($bg-1))
                                            ->where(['customer_id' => $value->CustomerID,])
                                            ->all();
                                        foreach ($tx as $t) {
                                            $t->customer_id = $b->CustomerID;
                                            $t->save();
                                        }
                                    }

                                    $u = TblUnit::find()->where(['CustomerID' => $value->CustomerID,])->all();
                                    foreach ($u as $xu) {
                                        $xu->CustomerID = $b->CustomerID;
                                        $xu->save();
                                    }

                                    $value->delete();
                                }
                            }
                        }
                    }
                }
                if ($chosen == 'unit') {
                    $units = TblUnit::find()->orderBy('UnitID')->all();
                    foreach ($units as $u) {
                        $u2 = TblUnit::find()->where(['UnitNo' => $u->UnitNo, 'Model' => $u->Model,
                            'SerialNo' => $u->SerialNo, 'Brand' => $u->Brand])->all();
                        if (count($u2) > 1) {
                            foreach ($u2 as $value) {
                                if ($value->UnitID != $u->UnitID) {
                                    $txCount = TblTransaction::find()->where(['unit_id' => $value->UnitID,])->count();
                                    for ($bg = 1; $bg <= (floor($txCount/500) +1); $bg++) {
                                        $tx = TblTransaction::find()
                                            ->limit(500)->offset(500 * ($bg-1))
                                            ->where(['unit_id' => $value->UnitID,])
                                            ->all();
                                        foreach ($tx as $t) {
                                            $t->unit_id = $u->UnitID;
                                            $t->save();
                                        }
                                    }

                                    $c = TblComponent::find()->where(['UnitID' => $value->UnitID,])->all();
                                    foreach ($c as $cx) {
                                        $cx->UnitID = $u->UnitID;
                                        $cx->save();
                                    }

                                    $value->delete();
                                }
                            }
                        }
                    }
                }
                $transaction->commit();
                die('ok');
            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            }
        }
        die;
    }
}
