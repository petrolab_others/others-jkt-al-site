<?php
/**
 * Created by
 * User     : Wisard17
 * Date     : 25/06/2021
 * Time     : 02:59 PM
 * File Name: ParameterController.php
 **/

namespace app\commands;


use app\modules\data\models\Parameters;
use yii\console\Controller;

class ParameterController extends Controller
{

    public function actionChangeParentPlace()
    {
        Parameters::migrationParent();
    }
}