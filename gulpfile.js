'use strict';
var gulp=require('gulp');
var sass=require('gulp-sass');
var cleanCSS = require('gulp-clean-css');
var rename = require("gulp-rename");
sass.compiler = require('node-sass');

gulp.task('default', function () {
	return gulp.src('./resources/sass/*.scss')
	.pipe(sass().on('error', sass.logError))
	.pipe(gulp.dest('./web/css'));
});

gulp.task('sass', function () {
	return gulp.src('./resources/sass/*.scss')
	.pipe(sass().on('error', sass.logError))
	.pipe(gulp.dest('./web/css'));
});

gulp.task('watch', function () {
	gulp.watch('./resources/sass/*.scss', ['sass']);
});

function dist(){
	gulp.src('web/css/*.css')
	.pipe(cleanCSS({compatibility: 'ie8'}))
	.pipe(rename({
		extname: ".min.css"
	}))
	.pipe(gulp.dest('web/css/'));
}

gulp.task('dist', () => {
	dist();
});
