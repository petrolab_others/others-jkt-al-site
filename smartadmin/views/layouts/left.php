<?php
/* @var $this \yii\web\View */

use app\models\Menu;
use app\smartadmin\SideBar;

// use yuncms\admin\helpers\MenuHelper;
?>

<!-- Note: This width of the aside area can be adjusted through LESS variables -->
<aside id="left-panel">

    <!-- User info -->
    <div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as it -->
					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                        <img src="<?= $asset->baseUrl; ?>/img1/user.jpg" alt="me" class="online">
						<span> 
							 <?php echo Yii::$app->user->isGuest ? 'Guest' : Yii::$app->user->identity->fullName; ?>
						</span>
                        <i class="fa fa-angle-down"></i>
                    </a>

				</span>
    </div>
    <!-- end user info -->

    <!-- NAVIGATION : This navigation is also responsive-->

    <?php
    SideBar::begin();
    if (!Yii::$app->user->isGuest)
        echo SideBar::widget(Menu::listMenu());
    SideBar::end();
    ?>

    <span class="minifyme" data-action="minifyMenu">
        <i class="fa fa-arrow-circle-left hit"></i>
    </span>

</aside>
