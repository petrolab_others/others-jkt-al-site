<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "tbl_fmc".
 *
 * @property int $no
 * @property string $site
 * @property string $cust
 * @property string $unit_model
 * @property string $unit_sn
 * @property string $unit_code
 * @property string $status_fmc
 * @property string $unit_no
 */
class TblFmc extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_fmc';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['no', 'site', 'cust', 'unit_model', 'unit_sn', 'unit_code', 'status_fmc'], 'required'],
            [['no'], 'integer'],
            [['site', 'cust'], 'string', 'max' => 10],
            [['unit_model', 'unit_sn', 'status_fmc'], 'string', 'max' => 20],
            [['unit_code', 'unit_no'], 'string', 'max' => 25],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'no' => Yii::t('app', 'No'),
            'site' => Yii::t('app', 'Site'),
            'cust' => Yii::t('app', 'Cust'),
            'unit_model' => Yii::t('app', 'Unit Model'),
            'unit_sn' => Yii::t('app', 'Unit Sn'),
            'unit_code' => Yii::t('app', 'Unit Code'),
            'status_fmc' => Yii::t('app', 'Status Fmc'),
            'unit_no' => Yii::t('app', 'Unit No'),
        ];
    }
}
