<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "tbl_degradasi".
 *
 * @property int $no_degradasi
 * @property string $Visc
 * @property string $Visc40
 * @property string $TAN
 * @property string $TBN
 * @property string $OXI
 * @property string $Gejala
 * @property string $Penyebab
 * @property string $Metoda_Inspeksi
 */
class TblDegradasi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_degradasi';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Penyebab', 'Metoda_Inspeksi'], 'required'],
            [['Penyebab', 'Metoda_Inspeksi'], 'string'],
            [['Visc', 'Visc40', 'TAN', 'TBN', 'OXI'], 'string', 'max' => 2],
            [['Gejala'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'no_degradasi' => Yii::t('app', 'No Degradasi'),
            'Visc' => Yii::t('app', 'Visc'),
            'Visc40' => Yii::t('app', 'Visc40'),
            'TAN' => Yii::t('app', 'Tan'),
            'TBN' => Yii::t('app', 'Tbn'),
            'OXI' => Yii::t('app', 'Oxi'),
            'Gejala' => Yii::t('app', 'Gejala'),
            'Penyebab' => Yii::t('app', 'Penyebab'),
            'Metoda_Inspeksi' => Yii::t('app', 'Metoda Inspeksi'),
        ];
    }
}
