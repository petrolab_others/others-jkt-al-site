<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "settings".
 *
 * @property string $Option_ID
 * @property string $Option_Default
 * @property string $Default_Theme
 * @property string $Menu_Horizontal
 * @property int $Vertical_Menu_Width
 * @property string $Show_Border_Layout
 * @property string $Show_Shadow_Layout
 * @property string $Show_Announcement
 * @property string $Demo_Mode
 * @property string $Show_Page_Processing_Time
 * @property string $Allow_User_Preferences
 * @property string $SMTP_Server
 * @property string $SMTP_Server_Port
 * @property string $SMTP_Server_Username
 * @property string $SMTP_Server_Password
 * @property string $Sender_Email
 * @property string $Recipient_Email
 * @property string $Use_Default_Locale
 * @property string $Default_Language
 * @property string $Default_Timezone
 * @property string $Default_Thousands_Separator
 * @property string $Default_Decimal_Point
 * @property string $Default_Currency_Symbol
 * @property string $Default_Money_Thousands_Separator
 * @property string $Default_Money_Decimal_Point
 * @property string $Maintenance_Mode
 * @property string $Maintenance_Finish_DateTime
 * @property string $Auto_Normal_After_Maintenance
 * @property string $Allow_User_To_Register
 * @property string $Suspend_New_User_Account
 * @property string $User_Need_Activation_After_Registered
 * @property string $Show_Captcha_On_Registration_Page
 * @property string $Show_Terms_And_Conditions_On_Registration_Page
 * @property string $Show_Captcha_On_Login_Page
 * @property string $Show_Captcha_On_Forgot_Password_Page
 * @property string $Show_Captcha_On_Change_Password_Page
 * @property string $User_Auto_Login_After_Activation_Or_Registration
 * @property int $User_Auto_Logout_After_Idle_In_Minutes
 * @property int $User_Login_Maximum_Retry
 * @property int $User_Login_Retry_Lockout
 * @property string $Redirect_To_Last_Visited_Page_After_Login
 * @property string $Enable_Password_Expiry
 * @property int $Password_Expiry_In_Days
 * @property string $Show_Entire_Header
 * @property int $Logo_Width
 * @property string $Show_Site_Title_In_Header
 * @property string $Show_Current_User_In_Header
 * @property string $Text_Align_In_Header
 * @property string $Site_Title_Text_Style
 * @property string $Language_Selector_Visibility
 * @property string $Language_Selector_Align
 * @property string $Show_Entire_Footer
 * @property string $Show_Text_In_Footer
 * @property string $Show_Back_To_Top_On_Footer
 * @property string $Show_Terms_And_Conditions_On_Footer
 * @property string $Show_About_Us_On_Footer
 * @property string $Pagination_Position
 * @property string $Pagination_Style
 * @property string $Selectable_Records_Per_Page
 * @property string $Selectable_Groups_Per_Page
 * @property int $Default_Record_Per_Page
 * @property int $Default_Group_Per_Page
 * @property int $Maximum_Selected_Records
 * @property int $Maximum_Selected_Groups
 * @property string $Show_PageNum_If_Record_Not_Over_Pagesize
 * @property string $Table_Width_Style 1 = Scroll, 2 = Normal, 3 = 100%
 * @property int $Scroll_Table_Width
 * @property int $Scroll_Table_Height
 * @property string $Show_Record_Number_On_List_Page
 * @property string $Show_Empty_Table_On_List_Page
 * @property string $Search_Panel_Collapsed
 * @property string $Filter_Panel_Collapsed
 * @property string $Rows_Vertical_Align_Top
 * @property string $Show_Add_Success_Message
 * @property string $Show_Edit_Success_Message
 * @property string $jQuery_Auto_Hide_Success_Message
 * @property string $Show_Record_Number_On_Detail_Preview
 * @property string $Show_Empty_Table_In_Detail_Preview
 * @property int $Detail_Preview_Table_Width
 * @property int $Password_Minimum_Length
 * @property int $Password_Maximum_Length
 * @property string $Password_Must_Comply_With_Minumum_Length
 * @property string $Password_Must_Comply_With_Maximum_Length
 * @property string $Password_Must_Contain_At_Least_One_Lower_Case
 * @property string $Password_Must_Contain_At_Least_One_Upper_Case
 * @property string $Password_Must_Contain_At_Least_One_Numeric
 * @property string $Password_Must_Contain_At_Least_One_Symbol
 * @property string $Password_Must_Be_Difference_Between_Old_And_New
 * @property string $Export_Record_Options
 * @property string $Show_Record_Number_On_Exported_List_Page
 * @property string $Use_Table_Setting_For_Export_Field_Caption
 * @property string $Use_Table_Setting_For_Export_Original_Value
 * @property string $Font_Name
 * @property string $Font_Size
 * @property string $Use_Javascript_Message
 * @property string $Login_Window_Type
 * @property string $Forgot_Password_Window_Type
 * @property string $Change_Password_Window_Type
 * @property string $Registration_Window_Type
 * @property string $Reset_Password_Field_Options
 * @property string $Action_Button_Alignment
 */
class Settings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'settings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Option_Default', 'Menu_Horizontal', 'Show_Border_Layout', 'Show_Shadow_Layout', 'Show_Announcement', 'Demo_Mode', 'Show_Page_Processing_Time', 'Allow_User_Preferences', 'Use_Default_Locale', 'Maintenance_Mode', 'Auto_Normal_After_Maintenance', 'Allow_User_To_Register', 'Suspend_New_User_Account', 'User_Need_Activation_After_Registered', 'Show_Captcha_On_Registration_Page', 'Show_Terms_And_Conditions_On_Registration_Page', 'Show_Captcha_On_Login_Page', 'Show_Captcha_On_Forgot_Password_Page', 'Show_Captcha_On_Change_Password_Page', 'User_Auto_Login_After_Activation_Or_Registration', 'Redirect_To_Last_Visited_Page_After_Login', 'Enable_Password_Expiry', 'Show_Entire_Header', 'Show_Site_Title_In_Header', 'Show_Current_User_In_Header', 'Text_Align_In_Header', 'Site_Title_Text_Style', 'Language_Selector_Visibility', 'Language_Selector_Align', 'Show_Entire_Footer', 'Show_Text_In_Footer', 'Show_Back_To_Top_On_Footer', 'Show_Terms_And_Conditions_On_Footer', 'Show_About_Us_On_Footer', 'Pagination_Position', 'Pagination_Style', 'Show_PageNum_If_Record_Not_Over_Pagesize', 'Table_Width_Style', 'Show_Record_Number_On_List_Page', 'Show_Empty_Table_On_List_Page', 'Search_Panel_Collapsed', 'Filter_Panel_Collapsed', 'Rows_Vertical_Align_Top', 'Show_Add_Success_Message', 'Show_Edit_Success_Message', 'jQuery_Auto_Hide_Success_Message', 'Show_Record_Number_On_Detail_Preview', 'Show_Empty_Table_In_Detail_Preview', 'Password_Must_Comply_With_Minumum_Length', 'Password_Must_Comply_With_Maximum_Length', 'Password_Must_Contain_At_Least_One_Lower_Case', 'Password_Must_Contain_At_Least_One_Upper_Case', 'Password_Must_Contain_At_Least_One_Numeric', 'Password_Must_Contain_At_Least_One_Symbol', 'Password_Must_Be_Difference_Between_Old_And_New', 'Export_Record_Options', 'Show_Record_Number_On_Exported_List_Page', 'Use_Table_Setting_For_Export_Field_Caption', 'Use_Table_Setting_For_Export_Original_Value', 'Use_Javascript_Message', 'Login_Window_Type', 'Forgot_Password_Window_Type', 'Change_Password_Window_Type', 'Registration_Window_Type', 'Reset_Password_Field_Options', 'Action_Button_Alignment'], 'string'],
            [['Vertical_Menu_Width', 'User_Auto_Logout_After_Idle_In_Minutes', 'User_Login_Maximum_Retry', 'User_Login_Retry_Lockout', 'Password_Expiry_In_Days', 'Logo_Width', 'Default_Record_Per_Page', 'Default_Group_Per_Page', 'Maximum_Selected_Records', 'Maximum_Selected_Groups', 'Scroll_Table_Width', 'Scroll_Table_Height', 'Detail_Preview_Table_Width', 'Password_Minimum_Length', 'Password_Maximum_Length'], 'integer'],
            [['Maintenance_Finish_DateTime'], 'safe'],
            [['Default_Theme'], 'string', 'max' => 30],
            [['SMTP_Server', 'SMTP_Server_Username', 'SMTP_Server_Password', 'Sender_Email', 'Recipient_Email', 'Default_Timezone', 'Selectable_Records_Per_Page', 'Selectable_Groups_Per_Page', 'Font_Name'], 'string', 'max' => 50],
            [['SMTP_Server_Port', 'Default_Language', 'Default_Thousands_Separator', 'Default_Decimal_Point', 'Default_Money_Thousands_Separator', 'Default_Money_Decimal_Point'], 'string', 'max' => 5],
            [['Default_Currency_Symbol'], 'string', 'max' => 10],
            [['Font_Size'], 'string', 'max' => 4],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Option_ID' => Yii::t('app', 'Option ID'),
            'Option_Default' => Yii::t('app', 'Option Default'),
            'Default_Theme' => Yii::t('app', 'Default Theme'),
            'Menu_Horizontal' => Yii::t('app', 'Menu Horizontal'),
            'Vertical_Menu_Width' => Yii::t('app', 'Vertical Menu Width'),
            'Show_Border_Layout' => Yii::t('app', 'Show Border Layout'),
            'Show_Shadow_Layout' => Yii::t('app', 'Show Shadow Layout'),
            'Show_Announcement' => Yii::t('app', 'Show Announcement'),
            'Demo_Mode' => Yii::t('app', 'Demo Mode'),
            'Show_Page_Processing_Time' => Yii::t('app', 'Show Page Processing Time'),
            'Allow_User_Preferences' => Yii::t('app', 'Allow User Preferences'),
            'SMTP_Server' => Yii::t('app', 'Smtp Server'),
            'SMTP_Server_Port' => Yii::t('app', 'Smtp Server Port'),
            'SMTP_Server_Username' => Yii::t('app', 'Smtp Server Username'),
            'SMTP_Server_Password' => Yii::t('app', 'Smtp Server Password'),
            'Sender_Email' => Yii::t('app', 'Sender Email'),
            'Recipient_Email' => Yii::t('app', 'Recipient Email'),
            'Use_Default_Locale' => Yii::t('app', 'Use Default Locale'),
            'Default_Language' => Yii::t('app', 'Default Language'),
            'Default_Timezone' => Yii::t('app', 'Default Timezone'),
            'Default_Thousands_Separator' => Yii::t('app', 'Default Thousands Separator'),
            'Default_Decimal_Point' => Yii::t('app', 'Default Decimal Point'),
            'Default_Currency_Symbol' => Yii::t('app', 'Default Currency Symbol'),
            'Default_Money_Thousands_Separator' => Yii::t('app', 'Default Money Thousands Separator'),
            'Default_Money_Decimal_Point' => Yii::t('app', 'Default Money Decimal Point'),
            'Maintenance_Mode' => Yii::t('app', 'Maintenance Mode'),
            'Maintenance_Finish_DateTime' => Yii::t('app', 'Maintenance Finish Date Time'),
            'Auto_Normal_After_Maintenance' => Yii::t('app', 'Auto Normal After Maintenance'),
            'Allow_User_To_Register' => Yii::t('app', 'Allow User To Register'),
            'Suspend_New_User_Account' => Yii::t('app', 'Suspend New User Account'),
            'User_Need_Activation_After_Registered' => Yii::t('app', 'User Need Activation After Registered'),
            'Show_Captcha_On_Registration_Page' => Yii::t('app', 'Show Captcha On Registration Page'),
            'Show_Terms_And_Conditions_On_Registration_Page' => Yii::t('app', 'Show Terms And Conditions On Registration Page'),
            'Show_Captcha_On_Login_Page' => Yii::t('app', 'Show Captcha On Login Page'),
            'Show_Captcha_On_Forgot_Password_Page' => Yii::t('app', 'Show Captcha On Forgot Password Page'),
            'Show_Captcha_On_Change_Password_Page' => Yii::t('app', 'Show Captcha On Change Password Page'),
            'User_Auto_Login_After_Activation_Or_Registration' => Yii::t('app', 'User Auto Login After Activation Or Registration'),
            'User_Auto_Logout_After_Idle_In_Minutes' => Yii::t('app', 'User Auto Logout After Idle In Minutes'),
            'User_Login_Maximum_Retry' => Yii::t('app', 'User Login Maximum Retry'),
            'User_Login_Retry_Lockout' => Yii::t('app', 'User Login Retry Lockout'),
            'Redirect_To_Last_Visited_Page_After_Login' => Yii::t('app', 'Redirect To Last Visited Page After Login'),
            'Enable_Password_Expiry' => Yii::t('app', 'Enable Password Expiry'),
            'Password_Expiry_In_Days' => Yii::t('app', 'Password Expiry In Days'),
            'Show_Entire_Header' => Yii::t('app', 'Show Entire Header'),
            'Logo_Width' => Yii::t('app', 'Logo Width'),
            'Show_Site_Title_In_Header' => Yii::t('app', 'Show Site Title In Header'),
            'Show_Current_User_In_Header' => Yii::t('app', 'Show Current User In Header'),
            'Text_Align_In_Header' => Yii::t('app', 'Text Align In Header'),
            'Site_Title_Text_Style' => Yii::t('app', 'Site Title Text Style'),
            'Language_Selector_Visibility' => Yii::t('app', 'Language Selector Visibility'),
            'Language_Selector_Align' => Yii::t('app', 'Language Selector Align'),
            'Show_Entire_Footer' => Yii::t('app', 'Show Entire Footer'),
            'Show_Text_In_Footer' => Yii::t('app', 'Show Text In Footer'),
            'Show_Back_To_Top_On_Footer' => Yii::t('app', 'Show Back To Top On Footer'),
            'Show_Terms_And_Conditions_On_Footer' => Yii::t('app', 'Show Terms And Conditions On Footer'),
            'Show_About_Us_On_Footer' => Yii::t('app', 'Show About Us On Footer'),
            'Pagination_Position' => Yii::t('app', 'Pagination Position'),
            'Pagination_Style' => Yii::t('app', 'Pagination Style'),
            'Selectable_Records_Per_Page' => Yii::t('app', 'Selectable Records Per Page'),
            'Selectable_Groups_Per_Page' => Yii::t('app', 'Selectable Groups Per Page'),
            'Default_Record_Per_Page' => Yii::t('app', 'Default Record Per Page'),
            'Default_Group_Per_Page' => Yii::t('app', 'Default Group Per Page'),
            'Maximum_Selected_Records' => Yii::t('app', 'Maximum Selected Records'),
            'Maximum_Selected_Groups' => Yii::t('app', 'Maximum Selected Groups'),
            'Show_PageNum_If_Record_Not_Over_Pagesize' => Yii::t('app', 'Show Page Num If Record Not Over Pagesize'),
            'Table_Width_Style' => Yii::t('app', 'Table Width Style'),
            'Scroll_Table_Width' => Yii::t('app', 'Scroll Table Width'),
            'Scroll_Table_Height' => Yii::t('app', 'Scroll Table Height'),
            'Show_Record_Number_On_List_Page' => Yii::t('app', 'Show Record Number On List Page'),
            'Show_Empty_Table_On_List_Page' => Yii::t('app', 'Show Empty Table On List Page'),
            'Search_Panel_Collapsed' => Yii::t('app', 'Search Panel Collapsed'),
            'Filter_Panel_Collapsed' => Yii::t('app', 'Filter Panel Collapsed'),
            'Rows_Vertical_Align_Top' => Yii::t('app', 'Rows Vertical Align Top'),
            'Show_Add_Success_Message' => Yii::t('app', 'Show Add Success Message'),
            'Show_Edit_Success_Message' => Yii::t('app', 'Show Edit Success Message'),
            'jQuery_Auto_Hide_Success_Message' => Yii::t('app', 'J Query Auto Hide Success Message'),
            'Show_Record_Number_On_Detail_Preview' => Yii::t('app', 'Show Record Number On Detail Preview'),
            'Show_Empty_Table_In_Detail_Preview' => Yii::t('app', 'Show Empty Table In Detail Preview'),
            'Detail_Preview_Table_Width' => Yii::t('app', 'Detail Preview Table Width'),
            'Password_Minimum_Length' => Yii::t('app', 'Password Minimum Length'),
            'Password_Maximum_Length' => Yii::t('app', 'Password Maximum Length'),
            'Password_Must_Comply_With_Minumum_Length' => Yii::t('app', 'Password Must Comply With Minumum Length'),
            'Password_Must_Comply_With_Maximum_Length' => Yii::t('app', 'Password Must Comply With Maximum Length'),
            'Password_Must_Contain_At_Least_One_Lower_Case' => Yii::t('app', 'Password Must Contain At Least One Lower Case'),
            'Password_Must_Contain_At_Least_One_Upper_Case' => Yii::t('app', 'Password Must Contain At Least One Upper Case'),
            'Password_Must_Contain_At_Least_One_Numeric' => Yii::t('app', 'Password Must Contain At Least One Numeric'),
            'Password_Must_Contain_At_Least_One_Symbol' => Yii::t('app', 'Password Must Contain At Least One Symbol'),
            'Password_Must_Be_Difference_Between_Old_And_New' => Yii::t('app', 'Password Must Be Difference Between Old And New'),
            'Export_Record_Options' => Yii::t('app', 'Export Record Options'),
            'Show_Record_Number_On_Exported_List_Page' => Yii::t('app', 'Show Record Number On Exported List Page'),
            'Use_Table_Setting_For_Export_Field_Caption' => Yii::t('app', 'Use Table Setting For Export Field Caption'),
            'Use_Table_Setting_For_Export_Original_Value' => Yii::t('app', 'Use Table Setting For Export Original Value'),
            'Font_Name' => Yii::t('app', 'Font Name'),
            'Font_Size' => Yii::t('app', 'Font Size'),
            'Use_Javascript_Message' => Yii::t('app', 'Use Javascript Message'),
            'Login_Window_Type' => Yii::t('app', 'Login Window Type'),
            'Forgot_Password_Window_Type' => Yii::t('app', 'Forgot Password Window Type'),
            'Change_Password_Window_Type' => Yii::t('app', 'Change Password Window Type'),
            'Registration_Window_Type' => Yii::t('app', 'Registration Window Type'),
            'Reset_Password_Field_Options' => Yii::t('app', 'Reset Password Field Options'),
            'Action_Button_Alignment' => Yii::t('app', 'Action Button Alignment'),
        ];
    }
}
