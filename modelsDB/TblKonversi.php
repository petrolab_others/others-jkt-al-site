<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "tbl_konversi".
 *
 * @property string $Lab_No
 * @property string $RECV_DT1
 * @property string $Matrix
 * @property string $MODEL
 * @property string $COMPONENT
 * @property string $HRS_KM_OC
 * @property string $HRS_KM_TOT
 * @property string $oil_change
 * @property double $FE
 * @property double $CU
 * @property double $AL
 * @property double $PB
 * @property double $NA
 * @property double $SI
 * @property double $CR
 * @property double $FE_KONV
 * @property double $CU_KONV
 * @property double $AL_KONV
 * @property double $PB_KONV
 * @property double $NA_KONV
 * @property double $SI_KONV
 * @property double $CR_KONV
 * @property double $FE_RES
 * @property double $CU_RES
 * @property double $AL_RES
 * @property double $PB_RES
 * @property double $NA_RES
 * @property double $SI_RES
 * @property double $CR_RES
 * @property int $calculate
 */
class TblKonversi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_konversi';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Lab_No'], 'required'],
            [['RECV_DT1'], 'safe'],
            [['HRS_KM_OC', 'HRS_KM_TOT', 'FE', 'CU', 'AL', 'PB', 'NA', 'SI', 'CR', 'FE_KONV', 'CU_KONV', 'AL_KONV', 'PB_KONV', 'NA_KONV', 'SI_KONV', 'CR_KONV', 'FE_RES', 'CU_RES', 'AL_RES', 'PB_RES', 'NA_RES', 'SI_RES', 'CR_RES'], 'number'],
            [['calculate'], 'integer'],
            [['Lab_No'], 'string', 'max' => 10],
            [['Matrix', 'MODEL', 'COMPONENT'], 'string', 'max' => 25],
            [['oil_change'], 'string', 'max' => 3],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Lab_No' => Yii::t('app', 'Lab No'),
            'RECV_DT1' => Yii::t('app', 'Recv Dt1'),
            'Matrix' => Yii::t('app', 'Matrix'),
            'MODEL' => Yii::t('app', 'Model'),
            'COMPONENT' => Yii::t('app', 'Component'),
            'HRS_KM_OC' => Yii::t('app', 'Hrs Km Oc'),
            'HRS_KM_TOT' => Yii::t('app', 'Hrs Km Tot'),
            'oil_change' => Yii::t('app', 'Oil Change'),
            'FE' => Yii::t('app', 'Fe'),
            'CU' => Yii::t('app', 'Cu'),
            'AL' => Yii::t('app', 'Al'),
            'PB' => Yii::t('app', 'Pb'),
            'NA' => Yii::t('app', 'Na'),
            'SI' => Yii::t('app', 'Si'),
            'CR' => Yii::t('app', 'Cr'),
            'FE_KONV' => Yii::t('app', 'Fe Konv'),
            'CU_KONV' => Yii::t('app', 'Cu Konv'),
            'AL_KONV' => Yii::t('app', 'Al Konv'),
            'PB_KONV' => Yii::t('app', 'Pb Konv'),
            'NA_KONV' => Yii::t('app', 'Na Konv'),
            'SI_KONV' => Yii::t('app', 'Si Konv'),
            'CR_KONV' => Yii::t('app', 'Cr Konv'),
            'FE_RES' => Yii::t('app', 'Fe Res'),
            'CU_RES' => Yii::t('app', 'Cu Res'),
            'AL_RES' => Yii::t('app', 'Al Res'),
            'PB_RES' => Yii::t('app', 'Pb Res'),
            'NA_RES' => Yii::t('app', 'Na Res'),
            'SI_RES' => Yii::t('app', 'Si Res'),
            'CR_RES' => Yii::t('app', 'Cr Res'),
            'calculate' => Yii::t('app', 'Calculate'),
        ];
    }
}
