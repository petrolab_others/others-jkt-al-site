<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "tbl_customers_copy".
 *
 * @property int $CustomerID
 * @property string $Name
 * @property string $Address
 * @property string $Branch
 * @property string $login
 * @property string $password
 * @property string $entrydate
 * @property string $updatedate
 * @property string $loginpusat
 * @property string $passwordpusat
 * @property string $owner
 * @property int $reports_to
 * @property int $userlevel
 * @property int $ownerid
 */
class TblCustomersCopy extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_customers_copy';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['CustomerID', 'Name', 'Branch', 'entrydate'], 'required'],
            [['CustomerID', 'reports_to', 'userlevel', 'ownerid'], 'integer'],
            [['entrydate', 'updatedate'], 'safe'],
            [['Name', 'Branch'], 'string', 'max' => 60],
            [['Address'], 'string', 'max' => 50],
            [['login', 'password', 'loginpusat', 'passwordpusat', 'owner'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'CustomerID' => Yii::t('app', 'Customer ID'),
            'Name' => Yii::t('app', 'Name'),
            'Address' => Yii::t('app', 'Address'),
            'Branch' => Yii::t('app', 'Branch'),
            'login' => Yii::t('app', 'Login'),
            'password' => Yii::t('app', 'Password'),
            'entrydate' => Yii::t('app', 'Entrydate'),
            'updatedate' => Yii::t('app', 'Updatedate'),
            'loginpusat' => Yii::t('app', 'Loginpusat'),
            'passwordpusat' => Yii::t('app', 'Passwordpusat'),
            'owner' => Yii::t('app', 'Owner'),
            'reports_to' => Yii::t('app', 'Reports To'),
            'userlevel' => Yii::t('app', 'Userlevel'),
            'ownerid' => Yii::t('app', 'Ownerid'),
        ];
    }
}
