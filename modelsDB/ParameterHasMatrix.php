<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "parameter_has_matrix".
 *
 * @property int $parameter_id
 * @property string $matrix
 * @property string $B
 * @property string $C
 * @property string $D
 * @property string $max
 * @property string $min
 *
 * @property Parameter $parameter
 */
class ParameterHasMatrix extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'parameter_has_matrix';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parameter_id', 'matrix'], 'required'],
            [['parameter_id'], 'integer'],
            [['matrix'], 'string', 'max' => 255],
            [['B', 'C', 'D', 'max', 'min'], 'string', 'max' => 45],
            [['parameter_id'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::className(), 'targetAttribute' => ['parameter_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'parameter_id' => Yii::t('app', 'Parameter ID'),
            'matrix' => Yii::t('app', 'Matrix'),
            'B' => Yii::t('app', 'B'),
            'C' => Yii::t('app', 'C'),
            'D' => Yii::t('app', 'D'),
            'max' => Yii::t('app', 'Max'),
            'min' => Yii::t('app', 'Min'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParameter()
    {
        return $this->hasOne(Parameter::className(), ['id' => 'parameter_id']);
    }
}
