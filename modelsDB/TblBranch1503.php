<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "tbl_branch_1503".
 *
 * @property int $branch_no
 * @property string $branch
 * @property string $short_branch
 * @property string $address
 * @property string $telp_fax
 * @property string $login
 * @property string $password
 * @property string $entrydate
 * @property string $updatedate
 * @property string $loginpusat
 * @property string $passwordpusat
 * @property string $reports_to
 * @property int $userlevel
 */
class TblBranch1503 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_branch_1503';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['branch'], 'required'],
            [['address'], 'string'],
            [['entrydate', 'updatedate'], 'safe'],
            [['userlevel'], 'integer'],
            [['branch', 'reports_to'], 'string', 'max' => 100],
            [['short_branch', 'telp_fax', 'login', 'password', 'loginpusat', 'passwordpusat'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'branch_no' => Yii::t('app', 'Branch No'),
            'branch' => Yii::t('app', 'Branch'),
            'short_branch' => Yii::t('app', 'Short Branch'),
            'address' => Yii::t('app', 'Address'),
            'telp_fax' => Yii::t('app', 'Telp Fax'),
            'login' => Yii::t('app', 'Login'),
            'password' => Yii::t('app', 'Password'),
            'entrydate' => Yii::t('app', 'Entrydate'),
            'updatedate' => Yii::t('app', 'Updatedate'),
            'loginpusat' => Yii::t('app', 'Loginpusat'),
            'passwordpusat' => Yii::t('app', 'Passwordpusat'),
            'reports_to' => Yii::t('app', 'Reports To'),
            'userlevel' => Yii::t('app', 'Userlevel'),
        ];
    }
}
