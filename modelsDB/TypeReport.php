<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "type_report".
 *
 * @property int $id
 * @property string $name
 * @property string $lab_no_temp
 * @property string $template
 * @property string $rk_no
 * @property string $draft_rk_no
 * @property string $header_name
 * @property string $deskription
 * @property int $history
 * @property int $reference
 * @property string $limit_type
 * @property int $mpc
 * @property int $blotter_spot_test
 * @property int $ffp
 * @property int $ptest
 * @property int $chart
 * @property int $active
 * @property int $type_view_id
 * @property int $filtrat
 *
 * @property DataAnalisa[] $dataAnalisas
 * @property ParameterHasTypeReport[] $parameterHasTypeReports
 * @property Parameter[] $parameters
 * @property Spesifikasi[] $spesifikasis
 * @property TypeView $typeView
 */
class TypeReport extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'type_report';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['template', 'deskription'], 'string'],
            [['history', 'reference', 'mpc', 'blotter_spot_test', 'ffp', 'ptest', 'chart', 'active', 'type_view_id', 'filtrat'], 'integer'],
            [['name', 'lab_no_temp', 'rk_no', 'draft_rk_no'], 'string', 'max' => 100],
            [['header_name'], 'string', 'max' => 200],
            [['limit_type'], 'string', 'max' => 45],
            [['type_view_id'], 'exist', 'skipOnError' => true, 'targetClass' => TypeView::className(), 'targetAttribute' => ['type_view_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'lab_no_temp' => 'Lab No Temp',
            'template' => 'Template',
            'rk_no' => 'Rk No',
            'draft_rk_no' => 'Draft Rk No',
            'header_name' => 'Header Name',
            'deskription' => 'Deskription',
            'history' => 'History',
            'reference' => 'Reference',
            'limit_type' => 'Limit Type',
            'mpc' => 'Mpc',
            'blotter_spot_test' => 'Blotter Spot Test',
            'ffp' => 'Ffp',
            'ptest' => 'Ptest',
            'chart' => 'Chart',
            'active' => 'Active',
            'type_view_id' => 'Type View ID',
            'filtrat' => 'Filtrat',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDataAnalisas()
    {
        return $this->hasMany(DataAnalisa::className(), ['type_report_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParameterHasTypeReports()
    {
        return $this->hasMany(ParameterHasTypeReport::className(), ['type_report_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParameters()
    {
        return $this->hasMany(Parameter::className(), ['id' => 'parameter_id'])->viaTable('parameter_has_type_report', ['type_report_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpesifikasis()
    {
        return $this->hasMany(Spesifikasi::className(), ['type_report_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTypeView()
    {
        return $this->hasOne(TypeView::className(), ['id' => 'type_view_id']);
    }
}
