<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "languages".
 *
 * @property string $Language_Code
 * @property string $Language_Name
 * @property string $Default
 * @property string $Site_Logo
 * @property string $Site_Title
 * @property string $Default_Thousands_Separator
 * @property string $Default_Decimal_Point
 * @property string $Default_Currency_Symbol
 * @property string $Default_Money_Thousands_Separator
 * @property string $Default_Money_Decimal_Point
 * @property string $Terms_And_Condition_Text
 * @property string $Announcement_Text
 * @property string $About_Text
 */
class Languages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'languages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Language_Code', 'Language_Name', 'Site_Logo', 'Site_Title', 'Terms_And_Condition_Text', 'Announcement_Text', 'About_Text'], 'required'],
            [['Default', 'Terms_And_Condition_Text', 'Announcement_Text', 'About_Text'], 'string'],
            [['Language_Code'], 'string', 'max' => 2],
            [['Language_Name'], 'string', 'max' => 20],
            [['Site_Logo', 'Site_Title'], 'string', 'max' => 100],
            [['Default_Thousands_Separator', 'Default_Decimal_Point', 'Default_Money_Thousands_Separator', 'Default_Money_Decimal_Point'], 'string', 'max' => 5],
            [['Default_Currency_Symbol'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Language_Code' => Yii::t('app', 'Language Code'),
            'Language_Name' => Yii::t('app', 'Language Name'),
            'Default' => Yii::t('app', 'Default'),
            'Site_Logo' => Yii::t('app', 'Site Logo'),
            'Site_Title' => Yii::t('app', 'Site Title'),
            'Default_Thousands_Separator' => Yii::t('app', 'Default Thousands Separator'),
            'Default_Decimal_Point' => Yii::t('app', 'Default Decimal Point'),
            'Default_Currency_Symbol' => Yii::t('app', 'Default Currency Symbol'),
            'Default_Money_Thousands_Separator' => Yii::t('app', 'Default Money Thousands Separator'),
            'Default_Money_Decimal_Point' => Yii::t('app', 'Default Money Decimal Point'),
            'Terms_And_Condition_Text' => Yii::t('app', 'Terms And Condition Text'),
            'Announcement_Text' => Yii::t('app', 'Announcement Text'),
            'About_Text' => Yii::t('app', 'About Text'),
        ];
    }
}
