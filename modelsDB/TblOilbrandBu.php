<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "tbl_oilbrand_bu".
 *
 * @property string $OIL_TYPE
 * @property string $ORIG_VISC
 * @property string $SAE
 * @property string $API
 * @property string $OIL_MATRIX
 * @property double $visc_100
 * @property double $visc_40
 * @property double $T_A_N
 * @property double $T_B_N
 */
class TblOilbrandBu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_oilbrand_bu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['OIL_TYPE'], 'required'],
            [['visc_100', 'visc_40', 'T_A_N', 'T_B_N'], 'number'],
            [['OIL_TYPE'], 'string', 'max' => 25],
            [['ORIG_VISC'], 'string', 'max' => 5],
            [['SAE'], 'string', 'max' => 15],
            [['API'], 'string', 'max' => 4],
            [['OIL_MATRIX'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'OIL_TYPE' => Yii::t('app', 'Oil Type'),
            'ORIG_VISC' => Yii::t('app', 'Orig Visc'),
            'SAE' => Yii::t('app', 'Sae'),
            'API' => Yii::t('app', 'Api'),
            'OIL_MATRIX' => Yii::t('app', 'Oil Matrix'),
            'visc_100' => Yii::t('app', 'Visc 100'),
            'visc_40' => Yii::t('app', 'Visc 40'),
            'T_A_N' => Yii::t('app', 'T A N'),
            'T_B_N' => Yii::t('app', 'T B N'),
        ];
    }
}
