<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "tbl_wear_ftir_matrix".
 *
 * @property string $Matrix
 * @property double $SootB
 * @property double $SootC
 * @property double $SootD
 * @property double $OxiB
 * @property double $OxiC
 * @property double $OxiD
 * @property double $NitB
 * @property double $NitC
 * @property double $NitD
 * @property double $SulB
 * @property double $SulC
 * @property double $SulD
 * @property double $FuelB
 * @property double $FuelC
 * @property double $FuelD
 * @property double $GlycolB
 * @property double $GlycolC
 * @property double $GlycolD
 * @property double $WaterB
 * @property double $WaterC
 * @property double $WaterD
 * @property string $F23
 * @property double $NaB
 * @property double $NaC
 * @property double $NaD
 * @property double $SiB
 * @property double $SiC
 * @property double $SiD
 * @property double $FeB
 * @property double $FeC
 * @property double $FeD
 * @property double $CuB
 * @property double $CuC
 * @property double $CuD
 * @property double $AlB
 * @property double $AlC
 * @property double $AlD
 * @property double $CrB
 * @property double $CrC
 * @property double $CrD
 * @property double $NiB
 * @property double $NiC
 * @property double $NiD
 * @property double $SnB
 * @property double $SnC
 * @property double $SnD
 * @property double $PbB
 * @property double $PbC
 * @property double $PbD
 * @property double $MoB
 * @property double $MoC
 * @property double $MoD
 * @property double $PQB
 * @property double $PQC
 * @property double $PQD
 * @property int $4attention
 * @property int $6attention
 * @property int $14attention
 * @property int $4urgent
 * @property int $6urgent
 * @property int $14urgent
 * @property string $SF
 * @property string $iso4406B
 * @property string $iso4406C
 * @property string $iso4406D
 */
class TblWearFtirMatrix extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_wear_ftir_matrix';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Matrix'], 'required'],
            [['SootB', 'SootC', 'SootD', 'OxiB', 'OxiC', 'OxiD', 'NitB', 'NitC', 'NitD', 'SulB', 'SulC', 'SulD', 'FuelB', 'FuelC', 'FuelD', 'GlycolB', 'GlycolC', 'GlycolD', 'WaterB', 'WaterC', 'WaterD', 'NaB', 'NaC', 'NaD', 'SiB', 'SiC', 'SiD', 'FeB', 'FeC', 'FeD', 'CuB', 'CuC', 'CuD', 'AlB', 'AlC', 'AlD', 'CrB', 'CrC', 'CrD', 'NiB', 'NiC', 'NiD', 'SnB', 'SnC', 'SnD', 'PbB', 'PbC', 'PbD', 'MoB', 'MoC', 'MoD', 'PQB', 'PQC', 'PQD'], 'number'],
            [['F23', 'SF'], 'string'],
            [['4attention', '6attention', '14attention', '4urgent', '6urgent', '14urgent'], 'integer'],
            [['Matrix'], 'string', 'max' => 255],
            [['iso4406B', 'iso4406C', 'iso4406D'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Matrix' => Yii::t('app', 'Matrix'),
            'SootB' => Yii::t('app', 'Soot B'),
            'SootC' => Yii::t('app', 'Soot C'),
            'SootD' => Yii::t('app', 'Soot D'),
            'OxiB' => Yii::t('app', 'Oxi B'),
            'OxiC' => Yii::t('app', 'Oxi C'),
            'OxiD' => Yii::t('app', 'Oxi D'),
            'NitB' => Yii::t('app', 'Nit B'),
            'NitC' => Yii::t('app', 'Nit C'),
            'NitD' => Yii::t('app', 'Nit D'),
            'SulB' => Yii::t('app', 'Sul B'),
            'SulC' => Yii::t('app', 'Sul C'),
            'SulD' => Yii::t('app', 'Sul D'),
            'FuelB' => Yii::t('app', 'Fuel B'),
            'FuelC' => Yii::t('app', 'Fuel C'),
            'FuelD' => Yii::t('app', 'Fuel D'),
            'GlycolB' => Yii::t('app', 'Glycol B'),
            'GlycolC' => Yii::t('app', 'Glycol C'),
            'GlycolD' => Yii::t('app', 'Glycol D'),
            'WaterB' => Yii::t('app', 'Water B'),
            'WaterC' => Yii::t('app', 'Water C'),
            'WaterD' => Yii::t('app', 'Water D'),
            'F23' => Yii::t('app', 'F23'),
            'NaB' => Yii::t('app', 'Na B'),
            'NaC' => Yii::t('app', 'Na C'),
            'NaD' => Yii::t('app', 'Na D'),
            'SiB' => Yii::t('app', 'Si B'),
            'SiC' => Yii::t('app', 'Si C'),
            'SiD' => Yii::t('app', 'Si D'),
            'FeB' => Yii::t('app', 'Fe B'),
            'FeC' => Yii::t('app', 'Fe C'),
            'FeD' => Yii::t('app', 'Fe D'),
            'CuB' => Yii::t('app', 'Cu B'),
            'CuC' => Yii::t('app', 'Cu C'),
            'CuD' => Yii::t('app', 'Cu D'),
            'AlB' => Yii::t('app', 'Al B'),
            'AlC' => Yii::t('app', 'Al C'),
            'AlD' => Yii::t('app', 'Al D'),
            'CrB' => Yii::t('app', 'Cr B'),
            'CrC' => Yii::t('app', 'Cr C'),
            'CrD' => Yii::t('app', 'Cr D'),
            'NiB' => Yii::t('app', 'Ni B'),
            'NiC' => Yii::t('app', 'Ni C'),
            'NiD' => Yii::t('app', 'Ni D'),
            'SnB' => Yii::t('app', 'Sn B'),
            'SnC' => Yii::t('app', 'Sn C'),
            'SnD' => Yii::t('app', 'Sn D'),
            'PbB' => Yii::t('app', 'Pb B'),
            'PbC' => Yii::t('app', 'Pb C'),
            'PbD' => Yii::t('app', 'Pb D'),
            'MoB' => Yii::t('app', 'Mo B'),
            'MoC' => Yii::t('app', 'Mo C'),
            'MoD' => Yii::t('app', 'Mo D'),
            'PQB' => Yii::t('app', 'Pqb'),
            'PQC' => Yii::t('app', 'Pqc'),
            'PQD' => Yii::t('app', 'Pqd'),
            '4attention' => Yii::t('app', '4attention'),
            '6attention' => Yii::t('app', '6attention'),
            '14attention' => Yii::t('app', '14attention'),
            '4urgent' => Yii::t('app', '4urgent'),
            '6urgent' => Yii::t('app', '6urgent'),
            '14urgent' => Yii::t('app', '14urgent'),
            'SF' => Yii::t('app', 'Sf'),
            'iso4406B' => Yii::t('app', 'Iso4406 B'),
            'iso4406C' => Yii::t('app', 'Iso4406 C'),
            'iso4406D' => Yii::t('app', 'Iso4406 D'),
        ];
    }
}
