<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "tbl_others_parameter".
 *
 * @property int $no
 * @property string $parameter
 * @property string $method
 * @property string $unit
 * @property string $matrix_limit
 * @property string $col1
 * @property string $col2
 * @property string $col3
 * @property string $col4
 * @property string $col5
 */
class TblOthersParameter extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_others_parameter';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parameter'], 'required'],
            [['parameter'], 'string', 'max' => 30],
            [['method', 'unit', 'matrix_limit', 'col1', 'col2', 'col3', 'col4', 'col5'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'no' => Yii::t('app', 'No'),
            'parameter' => Yii::t('app', 'Parameter'),
            'method' => Yii::t('app', 'Method'),
            'unit' => Yii::t('app', 'Unit'),
            'matrix_limit' => Yii::t('app', 'Matrix Limit'),
            'col1' => Yii::t('app', 'Col1'),
            'col2' => Yii::t('app', 'Col2'),
            'col3' => Yii::t('app', 'Col3'),
            'col4' => Yii::t('app', 'Col4'),
            'col5' => Yii::t('app', 'Col5'),
        ];
    }
}
