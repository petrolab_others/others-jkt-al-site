<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "stats_hour".
 *
 * @property int $Year
 * @property int $Month
 * @property int $Date
 * @property int $Hour
 * @property int $Hits
 */
class StatsHour extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stats_hour';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Year', 'Month', 'Date', 'Hour'], 'required'],
            [['Year', 'Hits'], 'integer'],
            [['Month', 'Date', 'Hour'], 'string', 'max' => 4],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Year' => Yii::t('app', 'Year'),
            'Month' => Yii::t('app', 'Month'),
            'Date' => Yii::t('app', 'Date'),
            'Hour' => Yii::t('app', 'Hour'),
            'Hits' => Yii::t('app', 'Hits'),
        ];
    }
}
