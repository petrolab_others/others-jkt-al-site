<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "fp_matrix".
 *
 * @property string $matrix
 * @property string $B
 * @property string $C
 * @property string $D
 */
class FpMatrix extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fp_matrix';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['matrix'], 'required'],
            [['matrix'], 'string', 'max' => 255],
            [['B', 'C', 'D'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'matrix' => Yii::t('app', 'Matrix'),
            'B' => Yii::t('app', 'B'),
            'C' => Yii::t('app', 'C'),
            'D' => Yii::t('app', 'D'),
        ];
    }
}
