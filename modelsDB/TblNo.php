<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "tbl_no".
 *
 * @property string $precode
 * @property string $prenumber
 * @property string $number
 * @property string $medcode
 * @property int $year
 */
class TblNo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_no';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['precode', 'prenumber', 'number', 'medcode', 'year'], 'required'],
            [['year'], 'integer'],
            [['precode', 'prenumber', 'medcode'], 'string', 'max' => 2],
            [['number'], 'string', 'max' => 12],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'precode' => Yii::t('app', 'Precode'),
            'prenumber' => Yii::t('app', 'Prenumber'),
            'number' => Yii::t('app', 'Number'),
            'medcode' => Yii::t('app', 'Medcode'),
            'year' => Yii::t('app', 'Year'),
        ];
    }
}
