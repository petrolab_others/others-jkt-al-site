<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "users_addition".
 *
 * @property string $username
 * @property string $accessToken
 * @property string $authKey
 * @property string $addition_rule
 */
class UsersAddition extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users_addition';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username'], 'required'],
            [['addition_rule'], 'string'],
            [['username'], 'string', 'max' => 50],
            [['accessToken', 'authKey'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'username' => Yii::t('app', 'Username'),
            'accessToken' => Yii::t('app', 'Access Token'),
            'authKey' => Yii::t('app', 'Auth Key'),
            'addition_rule' => Yii::t('app', 'Addition Rule'),
        ];
    }
}
