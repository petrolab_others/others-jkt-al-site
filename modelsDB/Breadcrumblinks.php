<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "breadcrumblinks".
 *
 * @property string $Page_Title
 * @property string $Page_URL
 * @property int $Lft
 * @property int $Rgt
 */
class Breadcrumblinks extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'breadcrumblinks';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Page_Title', 'Page_URL', 'Lft', 'Rgt'], 'required'],
            [['Lft', 'Rgt'], 'integer'],
            [['Page_Title', 'Page_URL'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Page_Title' => Yii::t('app', 'Page Title'),
            'Page_URL' => Yii::t('app', 'Page Url'),
            'Lft' => Yii::t('app', 'Lft'),
            'Rgt' => Yii::t('app', 'Rgt'),
        ];
    }
}
