<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "tbl_category_parameter".
 *
 * @property int $id
 * @property string $name
 * @property string $unit
 * @property string $methode
 * @property string $methode2
 * @property string $date_create
 */
class TblCategoryParameter extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_category_parameter';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['date_create'], 'safe'],
            [['name'], 'string', 'max' => 200],
            [['unit'], 'string', 'max' => 10],
            [['methode', 'methode2'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'unit' => Yii::t('app', 'Unit'),
            'methode' => Yii::t('app', 'Methode'),
            'methode2' => Yii::t('app', 'Methode2'),
            'date_create' => Yii::t('app', 'Date Create'),
        ];
    }
}
