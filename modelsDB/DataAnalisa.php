<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "data_analisa".
 *
 * @property int $id
 * @property int $branch_no
 * @property int $CustomerID
 * @property string $lab_number
 * @property int $UnitID
 * @property int $ComponentID
 * @property int $type_report_id
 * @property string $sample_name
 * @property string $sampling_date
 * @property string $received_date
 * @property string $start_analisa
 * @property string $end_analisa
 * @property string $report_date
 * @property string $publish
 * @property string $verify_date
 * @property string $verify_by
 * @property string $eval_code
 * @property string $report_number
 * @property string $req_order
 * @property string $hm_unit
 * @property string $hm_oil
 * @property string $followup
 * @property string $recom1
 * @property string $recom2
 * @property int $spesifikasi_id
 * @property int $reference_id
 * @property string $OIL_MATRIX
 * @property string $matrix
 * @property string $mpc_pic
 * @property string $oil_change
 *
 * @property Spesifikasi $spesifikasi
 * @property Reference $reference
 * @property TypeReport $typeReport
 * @property Users $verifyBy
 * @property DataAnalisaHasParameter[] $dataAnalisaHasParameters
 */
class DataAnalisa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'data_analisa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['branch_no', 'lab_number', 'type_report_id'], 'required'],
            [['branch_no', 'CustomerID', 'UnitID', 'ComponentID', 'type_report_id', 'spesifikasi_id', 'reference_id'], 'integer'],
            [['sample_name', 'followup', 'recom1', 'recom2', 'OIL_MATRIX'], 'string'],
            [['sampling_date', 'received_date', 'start_analisa', 'end_analisa', 'report_date', 'publish', 'verify_date'], 'safe'],
            [['lab_number'], 'string', 'max' => 200],
            [['verify_by'], 'string', 'max' => 50],
            [['eval_code'], 'string', 'max' => 2],
            [['report_number', 'req_order'], 'string', 'max' => 150],
            [['hm_unit', 'hm_oil'], 'string', 'max' => 100],
            [['matrix', 'mpc_pic'], 'string', 'max' => 255],
            [['oil_change'], 'string', 'max' => 45],
            [['spesifikasi_id'], 'exist', 'skipOnError' => true, 'targetClass' => Spesifikasi::className(), 'targetAttribute' => ['spesifikasi_id' => 'id']],
            [['reference_id'], 'exist', 'skipOnError' => true, 'targetClass' => Reference::className(), 'targetAttribute' => ['reference_id' => 'id']],
            [['type_report_id'], 'exist', 'skipOnError' => true, 'targetClass' => TypeReport::className(), 'targetAttribute' => ['type_report_id' => 'id']],
            [['verify_by'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['verify_by' => 'Username']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'branch_no' => 'Branch No',
            'CustomerID' => 'Customer ID',
            'lab_number' => 'Lab Number',
            'UnitID' => 'Unit ID',
            'ComponentID' => 'Component ID',
            'type_report_id' => 'Type Report ID',
            'sample_name' => 'Sample Name',
            'sampling_date' => 'Sampling Date',
            'received_date' => 'Received Date',
            'start_analisa' => 'Start Analisa',
            'end_analisa' => 'End Analisa',
            'report_date' => 'Report Date',
            'publish' => 'Publish',
            'verify_date' => 'Verify Date',
            'verify_by' => 'Verify By',
            'eval_code' => 'Eval Code',
            'report_number' => 'Report Number',
            'req_order' => 'Req Order',
            'hm_unit' => 'Hm Unit',
            'hm_oil' => 'Hm Oil',
            'followup' => 'Followup',
            'recom1' => 'Recom1',
            'recom2' => 'Recom2',
            'spesifikasi_id' => 'Spesifikasi ID',
            'reference_id' => 'Reference ID',
            'OIL_MATRIX' => 'Oil Matrix',
            'matrix' => 'Matrix',
            'mpc_pic' => 'Mpc Pic',
            'oil_change' => 'Oil Change',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpesifikasi()
    {
        return $this->hasOne(Spesifikasi::className(), ['id' => 'spesifikasi_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReference()
    {
        return $this->hasOne(Reference::className(), ['id' => 'reference_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTypeReport()
    {
        return $this->hasOne(TypeReport::className(), ['id' => 'type_report_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVerifyBy()
    {
        return $this->hasOne(Users::className(), ['Username' => 'verify_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDataAnalisaHasParameters()
    {
        return $this->hasMany(DataAnalisaHasParameter::className(), ['data_analisa_id' => 'id']);
    }
}
