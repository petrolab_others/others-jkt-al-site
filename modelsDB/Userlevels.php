<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "userlevels".
 *
 * @property int $User_Level_ID
 * @property string $User_Level_Name
 */
class Userlevels extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'userlevels';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['User_Level_ID', 'User_Level_Name'], 'required'],
            [['User_Level_ID'], 'integer'],
            [['User_Level_Name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'User_Level_ID' => Yii::t('app', 'User Level ID'),
            'User_Level_Name' => Yii::t('app', 'User Level Name'),
        ];
    }
}
