<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "stats_counterlog".
 *
 * @property string $IP_Address
 * @property string $Hostname
 * @property string $First_Visit
 * @property string $Last_Visit
 * @property int $Counter
 */
class StatsCounterlog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stats_counterlog';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['IP_Address', 'First_Visit', 'Last_Visit'], 'required'],
            [['First_Visit', 'Last_Visit'], 'safe'],
            [['Counter'], 'integer'],
            [['IP_Address', 'Hostname'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'IP_Address' => Yii::t('app', 'Ip Address'),
            'Hostname' => Yii::t('app', 'Hostname'),
            'First_Visit' => Yii::t('app', 'First Visit'),
            'Last_Visit' => Yii::t('app', 'Last Visit'),
            'Counter' => Yii::t('app', 'Counter'),
        ];
    }
}
