<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "tbl_sequence".
 *
 * @property int $N1
 * @property int $N2
 * @property int $B1
 * @property int $B2
 * @property int $C1
 * @property int $C2
 */
class TblSequence extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_sequence';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['N1', 'N2', 'B1', 'B2', 'C1', 'C2'], 'required'],
            [['N1', 'N2', 'B1', 'B2', 'C1', 'C2'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'N1' => Yii::t('app', 'N1'),
            'N2' => Yii::t('app', 'N2'),
            'B1' => Yii::t('app', 'B1'),
            'B2' => Yii::t('app', 'B2'),
            'C1' => Yii::t('app', 'C1'),
            'C2' => Yii::t('app', 'C2'),
        ];
    }
}
