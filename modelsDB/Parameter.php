<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "parameter".
 *
 * @property int $id
 * @property string $name
 * @property string $unit
 * @property string $method
 * @property string $col_name
 * @property string $notes
 * @property int $parent
 * @property int $active
 * @property int $matrix_type 1 untuk matrix 2 untuk oil matrix
 *
 * @property ParameterChild[] $parameterChildren
 * @property ParameterChild[] $parameterChildren0
 * @property Parameter[] $children
 * @property Parameter[] $parents
 * @property ParameterHasMatrix[] $parameterHasMatrices
 * @property ParameterHasOilMatrix[] $parameterHasOilMatrices
 * @property ParameterHasReference[] $parameterHasReferences
 * @property Reference[] $references
 * @property ParameterHasTypeReport[] $parameterHasTypeReports
 * @property TypeReport[] $typeReports
 * @property Typical[] $typicals
 */
class Parameter extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'parameter';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['notes'], 'string'],
            [['parent', 'active', 'matrix_type'], 'integer'],
            [['name', 'col_name'], 'string', 'max' => 200],
            [['unit', 'method'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'unit' => 'Unit',
            'method' => 'Method',
            'col_name' => 'Col Name',
            'notes' => 'Notes',
            'parent' => 'Parent',
            'active' => 'Active',
            'matrix_type' => 'Matrix Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParameterChildren()
    {
        return $this->hasMany(ParameterChild::className(), ['parent' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParameterChildren0()
    {
        return $this->hasMany(ParameterChild::className(), ['child' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChildren()
    {
        return $this->hasMany(Parameter::className(), ['id' => 'child'])->viaTable('parameter_child', ['parent' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParents()
    {
        return $this->hasMany(Parameter::className(), ['id' => 'parent'])->viaTable('parameter_child', ['child' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParameterHasMatrices()
    {
        return $this->hasMany(ParameterHasMatrix::className(), ['parameter_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParameterHasOilMatrices()
    {
        return $this->hasMany(ParameterHasOilMatrix::className(), ['parameter_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParameterHasReferences()
    {
        return $this->hasMany(ParameterHasReference::className(), ['parameter_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReferences()
    {
        return $this->hasMany(Reference::className(), ['id' => 'reference_id'])->viaTable('parameter_has_reference', ['parameter_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParameterHasTypeReports()
    {
        return $this->hasMany(ParameterHasTypeReport::className(), ['parameter_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTypeReports()
    {
        return $this->hasMany(TypeReport::className(), ['id' => 'type_report_id'])->viaTable('parameter_has_type_report', ['parameter_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTypicals()
    {
        return $this->hasMany(Typical::className(), ['new_oil_parameter_id' => 'id']);
    }
}
