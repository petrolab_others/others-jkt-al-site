<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "parameter_has_type_report".
 *
 * @property int $type_report_id
 * @property int $parameter_id
 * @property int $order
 *
 * @property Parameter $parameter
 * @property TypeReport $typeReport
 */
class ParameterHasTypeReport extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'parameter_has_type_report';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_report_id', 'parameter_id'], 'required'],
            [['type_report_id', 'parameter_id', 'order'], 'integer'],
            [['parameter_id'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::className(), 'targetAttribute' => ['parameter_id' => 'id']],
            [['type_report_id'], 'exist', 'skipOnError' => true, 'targetClass' => TypeReport::className(), 'targetAttribute' => ['type_report_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'type_report_id' => Yii::t('app', 'Type Report ID'),
            'parameter_id' => Yii::t('app', 'Parameter ID'),
            'order' => Yii::t('app', 'Order'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParameter()
    {
        return $this->hasOne(Parameter::className(), ['id' => 'parameter_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTypeReport()
    {
        return $this->hasOne(TypeReport::className(), ['id' => 'type_report_id']);
    }
}
