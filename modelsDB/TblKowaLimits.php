<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "tbl_kowa_limits".
 *
 * @property string $MATRIX
 * @property string $MODEL
 * @property string $COMPONENT
 * @property double $Fe_B
 * @property double $Fe_C
 * @property double $Al_B
 * @property double $Al_C
 * @property double $Cu_B
 * @property double $Cu_C
 * @property double $Cr_B
 * @property double $Cr_C
 * @property double $Si_B
 * @property double $Si_C
 * @property double $Pb_B
 * @property double $Pb_C
 */
class TblKowaLimits extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_kowa_limits';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['MATRIX', 'MODEL', 'COMPONENT', 'Fe_B', 'Fe_C', 'Al_B', 'Al_C', 'Cu_B', 'Cu_C', 'Cr_B', 'Cr_C', 'Si_B', 'Si_C', 'Pb_B', 'Pb_C'], 'required'],
            [['Fe_B', 'Fe_C', 'Al_B', 'Al_C', 'Cu_B', 'Cu_C', 'Cr_B', 'Cr_C', 'Si_B', 'Si_C', 'Pb_B', 'Pb_C'], 'number'],
            [['MATRIX', 'MODEL', 'COMPONENT'], 'string', 'max' => 25],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'MATRIX' => Yii::t('app', 'Matrix'),
            'MODEL' => Yii::t('app', 'Model'),
            'COMPONENT' => Yii::t('app', 'Component'),
            'Fe_B' => Yii::t('app', 'Fe B'),
            'Fe_C' => Yii::t('app', 'Fe C'),
            'Al_B' => Yii::t('app', 'Al B'),
            'Al_C' => Yii::t('app', 'Al C'),
            'Cu_B' => Yii::t('app', 'Cu B'),
            'Cu_C' => Yii::t('app', 'Cu C'),
            'Cr_B' => Yii::t('app', 'Cr B'),
            'Cr_C' => Yii::t('app', 'Cr C'),
            'Si_B' => Yii::t('app', 'Si B'),
            'Si_C' => Yii::t('app', 'Si C'),
            'Pb_B' => Yii::t('app', 'Pb B'),
            'Pb_C' => Yii::t('app', 'Pb C'),
        ];
    }
}
