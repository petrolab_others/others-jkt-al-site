<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "parameter_has_oil_matrix".
 *
 * @property int $parameter_id
 * @property string $OIL_MATRIX
 * @property string $B
 * @property string $C
 * @property string $D
 * @property string $max
 * @property string $min
 * @property string $Bmin
 * @property string $Cmin
 * @property string $Dmin
 * @property string $Bmax
 * @property string $Cmax
 * @property string $Dmax
 *
 * @property Parameter $parameter
 */
class ParameterHasOilMatrix extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'parameter_has_oil_matrix';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parameter_id', 'OIL_MATRIX'], 'required'],
            [['parameter_id'], 'integer'],
            [['OIL_MATRIX'], 'string', 'max' => 255],
            [['B', 'C', 'D', 'max', 'min', 'Bmin', 'Cmin', 'Dmin', 'Bmax', 'Cmax', 'Dmax'], 'string', 'max' => 45],
            [['parameter_id'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::className(), 'targetAttribute' => ['parameter_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'parameter_id' => Yii::t('app', 'Parameter ID'),
            'OIL_MATRIX' => Yii::t('app', 'Oil Matrix'),
            'B' => Yii::t('app', 'B'),
            'C' => Yii::t('app', 'C'),
            'D' => Yii::t('app', 'D'),
            'max' => Yii::t('app', 'Max'),
            'min' => Yii::t('app', 'Min'),
            'Bmin' => Yii::t('app', 'Bmin'),
            'Cmin' => Yii::t('app', 'Cmin'),
            'Dmin' => Yii::t('app', 'Dmin'),
            'Bmax' => Yii::t('app', 'Bmax'),
            'Cmax' => Yii::t('app', 'Cmax'),
            'Dmax' => Yii::t('app', 'Dmax'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParameter()
    {
        return $this->hasOne(Parameter::className(), ['id' => 'parameter_id']);
    }
}
