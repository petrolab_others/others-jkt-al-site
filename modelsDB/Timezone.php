<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "timezone".
 *
 * @property string $Timezone
 * @property string $Default
 */
class Timezone extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'timezone';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Timezone'], 'required'],
            [['Default'], 'string'],
            [['Timezone'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Timezone' => Yii::t('app', 'Timezone'),
            'Default' => Yii::t('app', 'Default'),
        ];
    }
}
