<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "parameter_has_reference".
 *
 * @property int $parameter_id
 * @property int $reference_id
 * @property string $value
 * @property string $B
 * @property string $C
 * @property string $D
 *
 * @property Parameter $parameter
 * @property Reference $reference
 */
class ParameterHasReference extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'parameter_has_reference';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parameter_id', 'reference_id'], 'required'],
            [['parameter_id', 'reference_id'], 'integer'],
            [['value', 'B', 'C', 'D'], 'string', 'max' => 45],
            [['parameter_id'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::className(), 'targetAttribute' => ['parameter_id' => 'id']],
            [['reference_id'], 'exist', 'skipOnError' => true, 'targetClass' => Reference::className(), 'targetAttribute' => ['reference_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'parameter_id' => Yii::t('app', 'Parameter ID'),
            'reference_id' => Yii::t('app', 'Reference ID'),
            'value' => Yii::t('app', 'Value'),
            'B' => Yii::t('app', 'B'),
            'C' => Yii::t('app', 'C'),
            'D' => Yii::t('app', 'D'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParameter()
    {
        return $this->hasOne(Parameter::className(), ['id' => 'parameter_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReference()
    {
        return $this->hasOne(Reference::className(), ['id' => 'reference_id']);
    }
}
