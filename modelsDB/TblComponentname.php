<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "tbl_componentname".
 *
 * @property string $ComponentCode
 * @property string $Name
 */
class TblComponentname extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_componentname';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ComponentCode', 'Name'], 'required'],
            [['ComponentCode', 'Name'], 'string', 'max' => 40],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ComponentCode' => Yii::t('app', 'Component Code'),
            'Name' => Yii::t('app', 'Name'),
        ];
    }
}
