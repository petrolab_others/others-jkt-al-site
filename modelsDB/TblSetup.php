<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "tbl_setup".
 *
 * @property int $nomor
 * @property string $mutu
 * @property string $teknis
 * @property string $lab_code
 */
class TblSetup extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_setup';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mutu', 'teknis'], 'required'],
            [['mutu', 'teknis'], 'string', 'max' => 30],
            [['lab_code'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'nomor' => Yii::t('app', 'Nomor'),
            'mutu' => Yii::t('app', 'Mutu'),
            'teknis' => Yii::t('app', 'Teknis'),
            'lab_code' => Yii::t('app', 'Lab Code'),
        ];
    }
}
