<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "data_analisa_has_parameter".
 *
 * @property int $data_analisa_id
 * @property string $col_name
 * @property string $value
 * @property string $code
 *
 * @property DataAnalisa $dataAnalisa
 */
class DataAnalisaHasParameter extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'data_analisa_has_parameter';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['data_analisa_id', 'col_name'], 'required'],
            [['data_analisa_id'], 'integer'],
            [['col_name'], 'string', 'max' => 200],
            [['value'], 'string', 'max' => 15],
            [['code'], 'string', 'max' => 5],
            [['data_analisa_id'], 'exist', 'skipOnError' => true, 'targetClass' => DataAnalisa::className(), 'targetAttribute' => ['data_analisa_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'data_analisa_id' => Yii::t('app', 'Data Analisa ID'),
            'col_name' => Yii::t('app', 'Col Name'),
            'value' => Yii::t('app', 'Value'),
            'code' => Yii::t('app', 'Code'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDataAnalisa()
    {
        return $this->hasOne(DataAnalisa::className(), ['id' => 'data_analisa_id']);
    }
}
