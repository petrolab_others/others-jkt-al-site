<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "tbl_alarm".
 *
 * @property string $lab_no
 * @property string $name
 * @property string $branch
 * @property string $model
 * @property string $unit_no
 * @property string $component
 * @property string $recv_dt1
 * @property string $element
 * @property int $urgent
 * @property int $attention
 * @property string $confirm
 */
class TblAlarm extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_alarm';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lab_no', 'branch', 'unit_no', 'element', 'confirm'], 'required'],
            [['recv_dt1'], 'safe'],
            [['urgent', 'attention'], 'integer'],
            [['lab_no'], 'string', 'max' => 9],
            [['name', 'branch'], 'string', 'max' => 50],
            [['model', 'unit_no'], 'string', 'max' => 15],
            [['component', 'element'], 'string', 'max' => 20],
            [['confirm'], 'string', 'max' => 2],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'lab_no' => Yii::t('app', 'Lab No'),
            'name' => Yii::t('app', 'Name'),
            'branch' => Yii::t('app', 'Branch'),
            'model' => Yii::t('app', 'Model'),
            'unit_no' => Yii::t('app', 'Unit No'),
            'component' => Yii::t('app', 'Component'),
            'recv_dt1' => Yii::t('app', 'Recv Dt1'),
            'element' => Yii::t('app', 'Element'),
            'urgent' => Yii::t('app', 'Urgent'),
            'attention' => Yii::t('app', 'Attention'),
            'confirm' => Yii::t('app', 'Confirm'),
        ];
    }
}
