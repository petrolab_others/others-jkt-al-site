<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "tbl_component_control".
 *
 * @property int $ComponentID
 * @property string $Component
 * @property int $UnitID
 * @property string $Lab_No
 * @property string $tgl1
 * @property string $tgl2
 * @property string $tgl3
 * @property string $tgl4
 * @property string $tgl5
 * @property string $ec1
 * @property string $ec2
 * @property string $ec3
 * @property string $ec4
 * @property string $ec5
 */
class TblComponentControl extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_component_control';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ComponentID', 'UnitID', 'Lab_No'], 'required'],
            [['ComponentID', 'UnitID'], 'integer'],
            [['tgl1', 'tgl2', 'tgl3', 'tgl4', 'tgl5'], 'safe'],
            [['Component'], 'string', 'max' => 30],
            [['Lab_No'], 'string', 'max' => 10],
            [['ec1', 'ec2', 'ec3', 'ec4', 'ec5'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ComponentID' => Yii::t('app', 'Component ID'),
            'Component' => Yii::t('app', 'Component'),
            'UnitID' => Yii::t('app', 'Unit ID'),
            'Lab_No' => Yii::t('app', 'Lab No'),
            'tgl1' => Yii::t('app', 'Tgl1'),
            'tgl2' => Yii::t('app', 'Tgl2'),
            'tgl3' => Yii::t('app', 'Tgl3'),
            'tgl4' => Yii::t('app', 'Tgl4'),
            'tgl5' => Yii::t('app', 'Tgl5'),
            'ec1' => Yii::t('app', 'Ec1'),
            'ec2' => Yii::t('app', 'Ec2'),
            'ec3' => Yii::t('app', 'Ec3'),
            'ec4' => Yii::t('app', 'Ec4'),
            'ec5' => Yii::t('app', 'Ec5'),
        ];
    }
}
