<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "com_pc".
 *
 * @property string $comp
 * @property string $min
 * @property string $max
 */
class ComPc extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'com_pc';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['comp', 'min', 'max'], 'required'],
            [['comp'], 'string', 'max' => 100],
            [['min', 'max'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'comp' => Yii::t('app', 'Comp'),
            'min' => Yii::t('app', 'Min'),
            'max' => Yii::t('app', 'Max'),
        ];
    }
}
