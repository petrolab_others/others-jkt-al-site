<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "new_view".
 *
 * @property string $Name_exp_1
 */
class NewView extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'new_view';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Name_exp_1'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Name_exp_1' => Yii::t('app', 'Name Exp 1'),
        ];
    }
}
