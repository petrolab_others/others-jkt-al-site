<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "v_lab_no".
 *
 * @property string $Lab_no
 */
class VLabNo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'v_lab_no';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Lab_no'], 'string', 'max' => 24],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Lab_no' => Yii::t('app', 'Lab No'),
        ];
    }
}
