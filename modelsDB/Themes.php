<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "themes".
 *
 * @property string $Theme_ID
 * @property string $Theme_Name
 * @property string $Default
 */
class Themes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'themes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Theme_ID', 'Theme_Name'], 'required'],
            [['Default'], 'string'],
            [['Theme_ID', 'Theme_Name'], 'string', 'max' => 25],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Theme_ID' => Yii::t('app', 'Theme ID'),
            'Theme_Name' => Yii::t('app', 'Theme Name'),
            'Default' => Yii::t('app', 'Default'),
        ];
    }
}
