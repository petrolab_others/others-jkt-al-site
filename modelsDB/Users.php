<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property string $Username
 * @property int $customer_id
 * @property string $Password
 * @property string $First_Name
 * @property string $Last_Name
 * @property string $Email
 * @property int $User_Level
 * @property int $Report_To
 * @property string $Activated
 * @property string $Locked
 * @property string $Profile
 * @property string $Current_URL
 * @property string $Theme
 * @property string $Menu_Horizontal
 * @property string $Table_Width_Style 1 = Scroll, 2 = Normal, 3 = 100%
 * @property int $Scroll_Table_Width
 * @property int $Scroll_Table_Height
 * @property string $Rows_Vertical_Align_Top
 * @property string $Language
 * @property string $Redirect_To_Last_Visited_Page_After_Login
 * @property string $Font_Name
 * @property string $Font_Size
 *
 * @property DataAnalisa[] $dataAnalisas
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Username', 'customer_id', 'Password'], 'required'],
            [['customer_id', 'User_Level', 'Report_To', 'Scroll_Table_Width', 'Scroll_Table_Height'], 'integer'],
            [['Activated', 'Locked', 'Profile', 'Current_URL', 'Menu_Horizontal', 'Table_Width_Style', 'Rows_Vertical_Align_Top', 'Redirect_To_Last_Visited_Page_After_Login'], 'string'],
            [['Username', 'First_Name', 'Last_Name', 'Font_Name'], 'string', 'max' => 50],
            [['Password'], 'string', 'max' => 64],
            [['Email'], 'string', 'max' => 100],
            [['Theme'], 'string', 'max' => 30],
            [['Language'], 'string', 'max' => 2],
            [['Font_Size'], 'string', 'max' => 4],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Username' => Yii::t('app', 'Username'),
            'customer_id' => Yii::t('app', 'Customer ID'),
            'Password' => Yii::t('app', 'Password'),
            'First_Name' => Yii::t('app', 'First Name'),
            'Last_Name' => Yii::t('app', 'Last Name'),
            'Email' => Yii::t('app', 'Email'),
            'User_Level' => Yii::t('app', 'User Level'),
            'Report_To' => Yii::t('app', 'Report To'),
            'Activated' => Yii::t('app', 'Activated'),
            'Locked' => Yii::t('app', 'Locked'),
            'Profile' => Yii::t('app', 'Profile'),
            'Current_URL' => Yii::t('app', 'Current Url'),
            'Theme' => Yii::t('app', 'Theme'),
            'Menu_Horizontal' => Yii::t('app', 'Menu Horizontal'),
            'Table_Width_Style' => Yii::t('app', 'Table Width Style'),
            'Scroll_Table_Width' => Yii::t('app', 'Scroll Table Width'),
            'Scroll_Table_Height' => Yii::t('app', 'Scroll Table Height'),
            'Rows_Vertical_Align_Top' => Yii::t('app', 'Rows Vertical Align Top'),
            'Language' => Yii::t('app', 'Language'),
            'Redirect_To_Last_Visited_Page_After_Login' => Yii::t('app', 'Redirect To Last Visited Page After Login'),
            'Font_Name' => Yii::t('app', 'Font Name'),
            'Font_Size' => Yii::t('app', 'Font Size'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDataAnalisas()
    {
        return $this->hasMany(DataAnalisa::className(), ['verify_by' => 'Username']);
    }
}
