<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "stats_month".
 *
 * @property int $Year
 * @property int $Month
 * @property string $Hits
 */
class StatsMonth extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stats_month';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Year', 'Month'], 'required'],
            [['Year', 'Hits'], 'integer'],
            [['Month'], 'string', 'max' => 4],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Year' => Yii::t('app', 'Year'),
            'Month' => Yii::t('app', 'Month'),
            'Hits' => Yii::t('app', 'Hits'),
        ];
    }
}
