<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "tbl_oil_matrix_bu".
 *
 * @property string $OIL_MATRIX
 * @property double $V40minB
 * @property double $V40minC
 * @property double $V40minD
 * @property double $V40maxB
 * @property double $V40maxC
 * @property double $V40maxD
 * @property double $V100minB
 * @property double $V100minC
 * @property double $V100minD
 * @property double $V100maxB
 * @property double $V100maxC
 * @property double $V100maxD
 * @property double $TBNB
 * @property double $TBNC
 * @property double $TBND
 * @property double $TANB
 * @property double $TANC
 * @property double $TAND
 */
class TblOilMatrixBu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_oil_matrix_bu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['OIL_MATRIX'], 'required'],
            [['V40minB', 'V40minC', 'V40minD', 'V40maxB', 'V40maxC', 'V40maxD', 'V100minB', 'V100minC', 'V100minD', 'V100maxB', 'V100maxC', 'V100maxD', 'TBNB', 'TBNC', 'TBND', 'TANB', 'TANC', 'TAND'], 'number'],
            [['OIL_MATRIX'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'OIL_MATRIX' => Yii::t('app', 'Oil Matrix'),
            'V40minB' => Yii::t('app', 'V40min B'),
            'V40minC' => Yii::t('app', 'V40min C'),
            'V40minD' => Yii::t('app', 'V40min D'),
            'V40maxB' => Yii::t('app', 'V40max B'),
            'V40maxC' => Yii::t('app', 'V40max C'),
            'V40maxD' => Yii::t('app', 'V40max D'),
            'V100minB' => Yii::t('app', 'V100min B'),
            'V100minC' => Yii::t('app', 'V100min C'),
            'V100minD' => Yii::t('app', 'V100min D'),
            'V100maxB' => Yii::t('app', 'V100max B'),
            'V100maxC' => Yii::t('app', 'V100max C'),
            'V100maxD' => Yii::t('app', 'V100max D'),
            'TBNB' => Yii::t('app', 'Tbnb'),
            'TBNC' => Yii::t('app', 'Tbnc'),
            'TBND' => Yii::t('app', 'Tbnd'),
            'TANB' => Yii::t('app', 'Tanb'),
            'TANC' => Yii::t('app', 'Tanc'),
            'TAND' => Yii::t('app', 'Tand'),
        ];
    }
}
