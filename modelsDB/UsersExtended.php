<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "users_extended".
 *
 * @property string $username
 * @property int $customer_id
 * @property string $Password
 * @property string $First_Name
 * @property string $Last_Name
 * @property string $Email
 * @property string $accessToken
 * @property string $authKey
 * @property string $addition_rule
 */
class UsersExtended extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users_extended';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'customer_id', 'Password'], 'required'],
            [['customer_id'], 'integer'],
            [['addition_rule'], 'string'],
            [['username', 'First_Name', 'Last_Name'], 'string', 'max' => 50],
            [['Password'], 'string', 'max' => 64],
            [['Email'], 'string', 'max' => 100],
            [['accessToken', 'authKey'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'username' => Yii::t('app', 'Username'),
            'customer_id' => Yii::t('app', 'Customer ID'),
            'Password' => Yii::t('app', 'Password'),
            'First_Name' => Yii::t('app', 'First Name'),
            'Last_Name' => Yii::t('app', 'Last Name'),
            'Email' => Yii::t('app', 'Email'),
            'accessToken' => Yii::t('app', 'Access Token'),
            'authKey' => Yii::t('app', 'Auth Key'),
            'addition_rule' => Yii::t('app', 'Addition Rule'),
        ];
    }
}
