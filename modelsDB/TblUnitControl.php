<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "tbl_unit_control".
 *
 * @property int $UnitID
 * @property int $customer_id
 * @property string $address
 * @property string $UnitNo
 * @property string $Model
 * @property string $tgl1
 * @property string $tgl2
 * @property string $tgl3
 * @property string $tgl4
 * @property string $tgl5
 * @property string $ec1
 * @property string $ec2
 * @property string $ec3
 * @property string $ec4
 * @property string $ec5
 * @property string $ln1
 * @property string $ln2
 * @property string $ln3
 * @property string $ln4
 * @property string $ln5
 */
class TblUnitControl extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_unit_control';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id'], 'integer'],
            [['UnitNo'], 'required'],
            [['tgl1', 'tgl2', 'tgl3', 'tgl4', 'tgl5'], 'safe'],
            [['address'], 'string', 'max' => 50],
            [['UnitNo', 'Model'], 'string', 'max' => 25],
            [['ec1', 'ec2', 'ec3', 'ec4', 'ec5'], 'string', 'max' => 1],
            [['ln1', 'ln2', 'ln3', 'ln4', 'ln5'], 'string', 'max' => 11],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'UnitID' => Yii::t('app', 'Unit ID'),
            'customer_id' => Yii::t('app', 'Customer ID'),
            'address' => Yii::t('app', 'Address'),
            'UnitNo' => Yii::t('app', 'Unit No'),
            'Model' => Yii::t('app', 'Model'),
            'tgl1' => Yii::t('app', 'Tgl1'),
            'tgl2' => Yii::t('app', 'Tgl2'),
            'tgl3' => Yii::t('app', 'Tgl3'),
            'tgl4' => Yii::t('app', 'Tgl4'),
            'tgl5' => Yii::t('app', 'Tgl5'),
            'ec1' => Yii::t('app', 'Ec1'),
            'ec2' => Yii::t('app', 'Ec2'),
            'ec3' => Yii::t('app', 'Ec3'),
            'ec4' => Yii::t('app', 'Ec4'),
            'ec5' => Yii::t('app', 'Ec5'),
            'ln1' => Yii::t('app', 'Ln1'),
            'ln2' => Yii::t('app', 'Ln2'),
            'ln3' => Yii::t('app', 'Ln3'),
            'ln4' => Yii::t('app', 'Ln4'),
            'ln5' => Yii::t('app', 'Ln5'),
        ];
    }
}
