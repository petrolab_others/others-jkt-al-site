<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "tbl_newrecommendation".
 *
 * @property int $nomor
 * @property string $component
 * @property string $parameter
 * @property string $val
 * @property string $rec
 * @property string $act
 */
class TblNewrecommendation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_newrecommendation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rec', 'act'], 'string'],
            [['component', 'parameter'], 'string', 'max' => 20],
            [['val'], 'string', 'max' => 2],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'nomor' => Yii::t('app', 'Nomor'),
            'component' => Yii::t('app', 'Component'),
            'parameter' => Yii::t('app', 'Parameter'),
            'val' => Yii::t('app', 'Val'),
            'rec' => Yii::t('app', 'Rec'),
            'act' => Yii::t('app', 'Act'),
        ];
    }
}
