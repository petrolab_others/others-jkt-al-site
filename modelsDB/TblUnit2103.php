<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "tbl_unit_2103".
 *
 * @property int $UnitID
 * @property string $UnitNo
 * @property string $Model
 * @property string $SerialNo
 * @property string $Brand
 * @property int $CustomerID
 * @property string $UnitLocation
 * @property string $updatedate
 * @property string $fmc
 */
class TblUnit2103 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_unit_2103';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['UnitNo', 'Model', 'CustomerID'], 'required'],
            [['CustomerID'], 'integer'],
            [['updatedate'], 'safe'],
            [['UnitNo', 'UnitLocation'], 'string', 'max' => 255],
            [['Model', 'Brand'], 'string', 'max' => 25],
            [['SerialNo'], 'string', 'max' => 20],
            [['fmc'], 'string', 'max' => 15],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'UnitID' => Yii::t('app', 'Unit ID'),
            'UnitNo' => Yii::t('app', 'Unit No'),
            'Model' => Yii::t('app', 'Model'),
            'SerialNo' => Yii::t('app', 'Serial No'),
            'Brand' => Yii::t('app', 'Brand'),
            'CustomerID' => Yii::t('app', 'Customer ID'),
            'UnitLocation' => Yii::t('app', 'Unit Location'),
            'updatedate' => Yii::t('app', 'Updatedate'),
            'fmc' => Yii::t('app', 'Fmc'),
        ];
    }
}
