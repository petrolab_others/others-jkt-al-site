<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "announcement".
 *
 * @property string $Announcement_ID
 * @property string $Is_Active
 * @property string $Topic
 * @property string $Message
 * @property string $Date_LastUpdate
 * @property string $Language
 * @property string $Auto_Publish
 * @property string $Date_Start
 * @property string $Date_End
 * @property string $Date_Created
 * @property string $Created_By
 * @property int $Translated_ID
 */
class Announcement extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'announcement';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Is_Active', 'Message', 'Auto_Publish'], 'string'],
            [['Topic', 'Message'], 'required'],
            [['Date_LastUpdate', 'Date_Start', 'Date_End', 'Date_Created'], 'safe'],
            [['Translated_ID'], 'integer'],
            [['Topic'], 'string', 'max' => 50],
            [['Language'], 'string', 'max' => 2],
            [['Created_By'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Announcement_ID' => Yii::t('app', 'Announcement ID'),
            'Is_Active' => Yii::t('app', 'Is Active'),
            'Topic' => Yii::t('app', 'Topic'),
            'Message' => Yii::t('app', 'Message'),
            'Date_LastUpdate' => Yii::t('app', 'Date Last Update'),
            'Language' => Yii::t('app', 'Language'),
            'Auto_Publish' => Yii::t('app', 'Auto Publish'),
            'Date_Start' => Yii::t('app', 'Date Start'),
            'Date_End' => Yii::t('app', 'Date End'),
            'Date_Created' => Yii::t('app', 'Date Created'),
            'Created_By' => Yii::t('app', 'Created By'),
            'Translated_ID' => Yii::t('app', 'Translated ID'),
        ];
    }
}
