<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "options_param".
 *
 * @property int $id
 * @property string $Lab_No
 * @property string $col_name
 * @property string $methode
 * @property string $unit
 * @property string $attention
 * @property string $urgent
 * @property string $severe
 */
class OptionsParam extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'options_param';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Lab_No'], 'required'],
            [['Lab_No'], 'string', 'max' => 15],
            [['col_name', 'unit'], 'string', 'max' => 100],
            [['methode'], 'string', 'max' => 200],
            [['attention', 'urgent', 'severe'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'Lab_No' => Yii::t('app', 'Lab No'),
            'col_name' => Yii::t('app', 'Col Name'),
            'methode' => Yii::t('app', 'Methode'),
            'unit' => Yii::t('app', 'Unit'),
            'attention' => Yii::t('app', 'Attention'),
            'urgent' => Yii::t('app', 'Urgent'),
            'severe' => Yii::t('app', 'Severe'),
        ];
    }
}
