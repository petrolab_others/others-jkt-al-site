<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "eval_code".
 *
 * @property string $eval_code
 * @property string $code
 */
class EvalCode extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'eval_code';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['eval_code', 'code'], 'required'],
            [['eval_code', 'code'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'eval_code' => Yii::t('app', 'Eval Code'),
            'code' => Yii::t('app', 'Code'),
        ];
    }
}
