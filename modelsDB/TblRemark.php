<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "tbl_remark".
 *
 * @property int $no_remark
 * @property int $wear
 * @property int $kontaminasi
 * @property int $degradasi
 * @property string $ph1
 * @property string $ph2
 * @property string $ph3
 * @property string $ph4
 * @property string $ph5
 * @property string $ph6
 * @property string $remark1
 * @property string $remark2
 */
class TblRemark extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_remark';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['wear', 'kontaminasi', 'degradasi'], 'integer'],
            [['ph1', 'ph2', 'ph3', 'ph4', 'ph5', 'ph6', 'remark1', 'remark2'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'no_remark' => Yii::t('app', 'No Remark'),
            'wear' => Yii::t('app', 'Wear'),
            'kontaminasi' => Yii::t('app', 'Kontaminasi'),
            'degradasi' => Yii::t('app', 'Degradasi'),
            'ph1' => Yii::t('app', 'Ph1'),
            'ph2' => Yii::t('app', 'Ph2'),
            'ph3' => Yii::t('app', 'Ph3'),
            'ph4' => Yii::t('app', 'Ph4'),
            'ph5' => Yii::t('app', 'Ph5'),
            'ph6' => Yii::t('app', 'Ph6'),
            'remark1' => Yii::t('app', 'Remark1'),
            'remark2' => Yii::t('app', 'Remark2'),
        ];
    }
}
