<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "help".
 *
 * @property int $Help_ID
 * @property string $Language
 * @property string $Topic
 * @property string $Description
 * @property int $Category
 * @property int $Order
 * @property string $Display_in_Page
 * @property string $Updated_By
 * @property string $Last_Updated
 */
class Help extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'help';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Help_ID', 'Language', 'Topic', 'Description', 'Category', 'Order', 'Display_in_Page'], 'required'],
            [['Help_ID', 'Category', 'Order'], 'integer'],
            [['Description'], 'string'],
            [['Last_Updated'], 'safe'],
            [['Language'], 'string', 'max' => 2],
            [['Topic'], 'string', 'max' => 255],
            [['Display_in_Page'], 'string', 'max' => 100],
            [['Updated_By'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Help_ID' => Yii::t('app', 'Help ID'),
            'Language' => Yii::t('app', 'Language'),
            'Topic' => Yii::t('app', 'Topic'),
            'Description' => Yii::t('app', 'Description'),
            'Category' => Yii::t('app', 'Category'),
            'Order' => Yii::t('app', 'Order'),
            'Display_in_Page' => Yii::t('app', 'Display In Page'),
            'Updated_By' => Yii::t('app', 'Updated By'),
            'Last_Updated' => Yii::t('app', 'Last Updated'),
        ];
    }
}
