<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "tbl_addition".
 *
 * @property int $id
 * @property string $Lab_No
 * @property int $tbl_parameter_id
 * @property string $value
 * @property string $value_CODE
 * @property string $date_create
 */
class TblAddition extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_addition';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Lab_No', 'tbl_parameter_id'], 'required'],
            [['tbl_parameter_id'], 'integer'],
            [['value'], 'string'],
            [['date_create'], 'safe'],
            [['Lab_No'], 'string', 'max' => 15],
            [['value_CODE'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'Lab_No' => Yii::t('app', 'Lab No'),
            'tbl_parameter_id' => Yii::t('app', 'Tbl Parameter ID'),
            'value' => Yii::t('app', 'Value'),
            'value_CODE' => Yii::t('app', 'Value Code'),
            'date_create' => Yii::t('app', 'Date Create'),
        ];
    }
}
