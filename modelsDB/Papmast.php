<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "papmast".
 *
 * @property string $LAB_NO
 * @property string $NAME
 * @property string $ADDRESS
 * @property string $CUST_RENT
 * @property string $BRANCH_NM
 * @property string $UNIT_NO
 * @property string $UNIT_LOC
 * @property string $BRAND
 * @property string $MODEL
 * @property string $COMPONENT
 * @property string $OIL_TYPE
 * @property string $OIL_MATRIX
 * @property double $ORIG_VISC
 * @property string $SAMPL_DT1
 * @property string $RECV_DT1
 * @property string $RPT_DT1
 * @property double $HRS_KM_OH
 * @property double $HRS_KM_OC
 * @property double $HRS_KM_TOT
 * @property double $VISC_CST
 * @property string $CST_CODE
 * @property double $VISC_SAE
 * @property string $SAE_CODE
 * @property double $DILUTION
 * @property string $DILUT_CODE
 * @property double $DIR_TRANS
 * @property string $TRANS_CODE
 * @property double $OXIDATION
 * @property string $OXID_CODE
 * @property double $NITRATION
 * @property string $NITR_CODE
 * @property double $WATER
 * @property string $WTR_CODE
 * @property double $T_A_N
 * @property string $TAN_CODE
 * @property double $T_B_N
 * @property string $TBN_CODE
 * @property string $ADD_PHYS
 * @property double $ADD_VALUE
 * @property string $ADD_CODE
 * @property double $NICKEL
 * @property string $NI_CODE
 * @property double $SOX
 * @property string $SOX_CODE
 * @property double $GLYCOL
 * @property string $GLY_CODE
 * @property double $SILICON
 * @property string $SI_CODE
 * @property double $IRON
 * @property string $FE_CODE
 * @property double $COPPER
 * @property string $CU_CODE
 * @property double $ALUMUNIUM
 * @property string $AL_CODE
 * @property double $CHROMIUM
 * @property string $CR_CODE
 * @property double $MAGNESIUM
 * @property string $MG_CODE
 * @property double $SILVER
 * @property string $AG_CODE
 * @property double $TIN
 * @property string $SN_CODE
 * @property double $LEAD
 * @property string $PB_CODE
 * @property double $SODIUM
 * @property string $NA_CODE
 * @property double $CALCIUM
 * @property string $CA_CODE
 * @property double $ZINC
 * @property string $ZN_CODE
 * @property string $ADD_MET
 * @property double $ADD_MVAL
 * @property string $ADD_MCODE
 * @property string $EVAL_CODE
 * @property string $UT_EMD
 * @property string $UT_ESN
 * @property string $RECOMM1
 * @property string $RECOMM2
 * @property string $RECOMM3
 * @property string $RECOMM4
 * @property string $MATRIX
 * @property string $Confirm
 * @property bool $Ignore
 * @property string $Email
 * @property string $Action
 * @property double $Molybdenum
 * @property string $Molybdenum_CODE
 * @property double $Boron
 * @property string $Boron_CODE
 * @property double $Potassium
 * @property string $Potassium_CODE
 * @property double $Barium
 * @property string $Barium_CODE
 * @property string $ISO4406
 * @property string $ISO4406_CODE
 * @property double $PQIndex
 * @property string $PQIndex_CODE
 * @property string $colourcode
 * @property double $phosphor
 * @property double $sulphur
 * @property double $visc_100
 * @property string $visc_100_code
 */
class Papmast extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'papmast';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['LAB_NO'], 'required'],
            [['ORIG_VISC', 'HRS_KM_OH', 'HRS_KM_OC', 'HRS_KM_TOT', 'VISC_CST', 'VISC_SAE', 'DILUTION', 'DIR_TRANS', 'OXIDATION', 'NITRATION', 'WATER', 'T_A_N', 'T_B_N', 'ADD_VALUE', 'NICKEL', 'SOX', 'GLYCOL', 'SILICON', 'IRON', 'COPPER', 'ALUMUNIUM', 'CHROMIUM', 'MAGNESIUM', 'SILVER', 'TIN', 'LEAD', 'SODIUM', 'CALCIUM', 'ZINC', 'ADD_MVAL', 'Molybdenum', 'Boron', 'Potassium', 'Barium', 'PQIndex', 'phosphor', 'sulphur', 'visc_100'], 'number'],
            [['SAMPL_DT1', 'RECV_DT1', 'RPT_DT1'], 'safe'],
            [['Ignore'], 'boolean'],
            [['Action'], 'string'],
            [['LAB_NO'], 'string', 'max' => 12],
            [['NAME', 'BRANCH_NM'], 'string', 'max' => 60],
            [['ADDRESS', 'UNIT_LOC', 'OIL_TYPE', 'ADD_PHYS'], 'string', 'max' => 30],
            [['CUST_RENT', 'CST_CODE', 'DILUT_CODE', 'TRANS_CODE', 'OXID_CODE', 'NITR_CODE', 'WTR_CODE', 'TAN_CODE', 'TBN_CODE', 'ADD_CODE', 'NI_CODE', 'SOX_CODE', 'GLY_CODE', 'SI_CODE', 'FE_CODE', 'CU_CODE', 'AL_CODE', 'CR_CODE', 'MG_CODE', 'AG_CODE', 'SN_CODE', 'PB_CODE', 'NA_CODE', 'CA_CODE', 'ZN_CODE', 'ADD_MCODE', 'EVAL_CODE', 'Molybdenum_CODE', 'Boron_CODE', 'Potassium_CODE', 'Barium_CODE', 'ISO4406_CODE', 'PQIndex_CODE', 'visc_100_code'], 'string', 'max' => 1],
            [['UNIT_NO', 'MODEL'], 'string', 'max' => 25],
            [['BRAND', 'ADD_MET', 'UT_EMD', 'UT_ESN'], 'string', 'max' => 15],
            [['COMPONENT', 'OIL_MATRIX', 'SAE_CODE'], 'string', 'max' => 20],
            [['RECOMM1'], 'string', 'max' => 900],
            [['RECOMM2', 'RECOMM3'], 'string', 'max' => 500],
            [['RECOMM4'], 'string', 'max' => 254],
            [['MATRIX', 'ISO4406', 'colourcode'], 'string', 'max' => 10],
            [['Confirm'], 'string', 'max' => 2],
            [['Email'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'LAB_NO' => Yii::t('app', 'Lab No'),
            'NAME' => Yii::t('app', 'Name'),
            'ADDRESS' => Yii::t('app', 'Address'),
            'CUST_RENT' => Yii::t('app', 'Cust Rent'),
            'BRANCH_NM' => Yii::t('app', 'Branch Nm'),
            'UNIT_NO' => Yii::t('app', 'Unit No'),
            'UNIT_LOC' => Yii::t('app', 'Unit Loc'),
            'BRAND' => Yii::t('app', 'Brand'),
            'MODEL' => Yii::t('app', 'Model'),
            'COMPONENT' => Yii::t('app', 'Component'),
            'OIL_TYPE' => Yii::t('app', 'Oil Type'),
            'OIL_MATRIX' => Yii::t('app', 'Oil Matrix'),
            'ORIG_VISC' => Yii::t('app', 'Orig Visc'),
            'SAMPL_DT1' => Yii::t('app', 'Sampl Dt1'),
            'RECV_DT1' => Yii::t('app', 'Recv Dt1'),
            'RPT_DT1' => Yii::t('app', 'Rpt Dt1'),
            'HRS_KM_OH' => Yii::t('app', 'Hrs Km Oh'),
            'HRS_KM_OC' => Yii::t('app', 'Hrs Km Oc'),
            'HRS_KM_TOT' => Yii::t('app', 'Hrs Km Tot'),
            'VISC_CST' => Yii::t('app', 'Visc Cst'),
            'CST_CODE' => Yii::t('app', 'Cst Code'),
            'VISC_SAE' => Yii::t('app', 'Visc Sae'),
            'SAE_CODE' => Yii::t('app', 'Sae Code'),
            'DILUTION' => Yii::t('app', 'Dilution'),
            'DILUT_CODE' => Yii::t('app', 'Dilut Code'),
            'DIR_TRANS' => Yii::t('app', 'Dir Trans'),
            'TRANS_CODE' => Yii::t('app', 'Trans Code'),
            'OXIDATION' => Yii::t('app', 'Oxidation'),
            'OXID_CODE' => Yii::t('app', 'Oxid Code'),
            'NITRATION' => Yii::t('app', 'Nitration'),
            'NITR_CODE' => Yii::t('app', 'Nitr Code'),
            'WATER' => Yii::t('app', 'Water'),
            'WTR_CODE' => Yii::t('app', 'Wtr Code'),
            'T_A_N' => Yii::t('app', 'T A N'),
            'TAN_CODE' => Yii::t('app', 'Tan Code'),
            'T_B_N' => Yii::t('app', 'T B N'),
            'TBN_CODE' => Yii::t('app', 'Tbn Code'),
            'ADD_PHYS' => Yii::t('app', 'Add Phys'),
            'ADD_VALUE' => Yii::t('app', 'Add Value'),
            'ADD_CODE' => Yii::t('app', 'Add Code'),
            'NICKEL' => Yii::t('app', 'Nickel'),
            'NI_CODE' => Yii::t('app', 'Ni Code'),
            'SOX' => Yii::t('app', 'Sox'),
            'SOX_CODE' => Yii::t('app', 'Sox Code'),
            'GLYCOL' => Yii::t('app', 'Glycol'),
            'GLY_CODE' => Yii::t('app', 'Gly Code'),
            'SILICON' => Yii::t('app', 'Silicon'),
            'SI_CODE' => Yii::t('app', 'Si Code'),
            'IRON' => Yii::t('app', 'Iron'),
            'FE_CODE' => Yii::t('app', 'Fe Code'),
            'COPPER' => Yii::t('app', 'Copper'),
            'CU_CODE' => Yii::t('app', 'Cu Code'),
            'ALUMUNIUM' => Yii::t('app', 'Alumunium'),
            'AL_CODE' => Yii::t('app', 'Al Code'),
            'CHROMIUM' => Yii::t('app', 'Chromium'),
            'CR_CODE' => Yii::t('app', 'Cr Code'),
            'MAGNESIUM' => Yii::t('app', 'Magnesium'),
            'MG_CODE' => Yii::t('app', 'Mg Code'),
            'SILVER' => Yii::t('app', 'Silver'),
            'AG_CODE' => Yii::t('app', 'Ag Code'),
            'TIN' => Yii::t('app', 'Tin'),
            'SN_CODE' => Yii::t('app', 'Sn Code'),
            'LEAD' => Yii::t('app', 'Lead'),
            'PB_CODE' => Yii::t('app', 'Pb Code'),
            'SODIUM' => Yii::t('app', 'Sodium'),
            'NA_CODE' => Yii::t('app', 'Na Code'),
            'CALCIUM' => Yii::t('app', 'Calcium'),
            'CA_CODE' => Yii::t('app', 'Ca Code'),
            'ZINC' => Yii::t('app', 'Zinc'),
            'ZN_CODE' => Yii::t('app', 'Zn Code'),
            'ADD_MET' => Yii::t('app', 'Add Met'),
            'ADD_MVAL' => Yii::t('app', 'Add Mval'),
            'ADD_MCODE' => Yii::t('app', 'Add Mcode'),
            'EVAL_CODE' => Yii::t('app', 'Eval Code'),
            'UT_EMD' => Yii::t('app', 'Ut Emd'),
            'UT_ESN' => Yii::t('app', 'Ut Esn'),
            'RECOMM1' => Yii::t('app', 'Recomm1'),
            'RECOMM2' => Yii::t('app', 'Recomm2'),
            'RECOMM3' => Yii::t('app', 'Recomm3'),
            'RECOMM4' => Yii::t('app', 'Recomm4'),
            'MATRIX' => Yii::t('app', 'Matrix'),
            'Confirm' => Yii::t('app', 'Confirm'),
            'Ignore' => Yii::t('app', 'Ignore'),
            'Email' => Yii::t('app', 'Email'),
            'Action' => Yii::t('app', 'Action'),
            'Molybdenum' => Yii::t('app', 'Molybdenum'),
            'Molybdenum_CODE' => Yii::t('app', 'Molybdenum Code'),
            'Boron' => Yii::t('app', 'Boron'),
            'Boron_CODE' => Yii::t('app', 'Boron Code'),
            'Potassium' => Yii::t('app', 'Potassium'),
            'Potassium_CODE' => Yii::t('app', 'Potassium Code'),
            'Barium' => Yii::t('app', 'Barium'),
            'Barium_CODE' => Yii::t('app', 'Barium Code'),
            'ISO4406' => Yii::t('app', 'Iso4406'),
            'ISO4406_CODE' => Yii::t('app', 'Iso4406 Code'),
            'PQIndex' => Yii::t('app', 'Pq Index'),
            'PQIndex_CODE' => Yii::t('app', 'Pq Index Code'),
            'colourcode' => Yii::t('app', 'Colourcode'),
            'phosphor' => Yii::t('app', 'Phosphor'),
            'sulphur' => Yii::t('app', 'Sulphur'),
            'visc_100' => Yii::t('app', 'Visc 100'),
            'visc_100_code' => Yii::t('app', 'Visc 100 Code'),
        ];
    }
}
