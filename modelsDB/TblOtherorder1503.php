<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "tbl_otherorder_1503".
 *
 * @property int $OtherOrderId
 * @property string $Lab_No
 * @property string $unsur
 * @property string $unit
 * @property string $methode
 * @property string $value
 * @property string $value_CODE
 * @property string $attention
 * @property string $urgent
 * @property string $severe
 */
class TblOtherorder1503 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_otherorder_1503';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Lab_No', 'unsur'], 'required'],
            [['Lab_No'], 'string', 'max' => 15],
            [['unsur'], 'string', 'max' => 30],
            [['unit', 'attention'], 'string', 'max' => 10],
            [['methode', 'urgent', 'severe'], 'string', 'max' => 20],
            [['value'], 'string', 'max' => 5],
            [['value_CODE'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'OtherOrderId' => Yii::t('app', 'Other Order ID'),
            'Lab_No' => Yii::t('app', 'Lab No'),
            'unsur' => Yii::t('app', 'Unsur'),
            'unit' => Yii::t('app', 'Unit'),
            'methode' => Yii::t('app', 'Methode'),
            'value' => Yii::t('app', 'Value'),
            'value_CODE' => Yii::t('app', 'Value Code'),
            'attention' => Yii::t('app', 'Attention'),
            'urgent' => Yii::t('app', 'Urgent'),
            'severe' => Yii::t('app', 'Severe'),
        ];
    }
}
