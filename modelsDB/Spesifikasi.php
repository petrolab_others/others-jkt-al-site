<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "spesifikasi".
 *
 * @property int $id
 * @property string $name
 * @property int $type_report_id
 * @property string $note
 *
 * @property DataAnalisa[] $dataAnalisas
 * @property TypeReport $typeReport
 * @property Typical[] $typicals
 */
class Spesifikasi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'spesifikasi';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_report_id'], 'integer'],
            [['note'], 'string'],
            [['name'], 'string', 'max' => 200],
            [['type_report_id'], 'exist', 'skipOnError' => true, 'targetClass' => TypeReport::className(), 'targetAttribute' => ['type_report_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'type_report_id' => Yii::t('app', 'Type Report ID'),
            'note' => Yii::t('app', 'Note'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDataAnalisas()
    {
        return $this->hasMany(DataAnalisa::className(), ['spesifikasi_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTypeReport()
    {
        return $this->hasOne(TypeReport::className(), ['id' => 'type_report_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTypicals()
    {
        return $this->hasMany(Typical::className(), ['spesifikasi_id' => 'id']);
    }
}
