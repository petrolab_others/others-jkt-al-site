<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "stats_counter".
 *
 * @property string $Type
 * @property string $Variable
 * @property string $Counter
 */
class StatsCounter extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stats_counter';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Type', 'Variable'], 'required'],
            [['Counter'], 'integer'],
            [['Type', 'Variable'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Type' => Yii::t('app', 'Type'),
            'Variable' => Yii::t('app', 'Variable'),
            'Counter' => Yii::t('app', 'Counter'),
        ];
    }
}
