<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "type_view".
 *
 * @property int $id
 * @property string $name
 * @property string $tree
 *
 * @property TypeReport[] $typeReports
 */
class TypeView extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'type_view';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 100],
            [['tree'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'tree' => Yii::t('app', 'Tree'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTypeReports()
    {
        return $this->hasMany(TypeReport::className(), ['type_view_id' => 'id']);
    }
}
