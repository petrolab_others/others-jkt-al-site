<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "tbl_model".
 *
 * @property string $Model
 * @property string $Brand
 */
class TblModel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_model';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Model', 'Brand'], 'required'],
            [['Model'], 'string', 'max' => 25],
            [['Brand'], 'string', 'max' => 15],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Model' => Yii::t('app', 'Model'),
            'Brand' => Yii::t('app', 'Brand'),
        ];
    }
}
