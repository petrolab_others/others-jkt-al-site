<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "stats_date".
 *
 * @property int $Year
 * @property int $Month
 * @property int $Date
 * @property string $Hits
 */
class StatsDate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stats_date';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Year', 'Month', 'Date'], 'required'],
            [['Year', 'Hits'], 'integer'],
            [['Month', 'Date'], 'string', 'max' => 4],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Year' => Yii::t('app', 'Year'),
            'Month' => Yii::t('app', 'Month'),
            'Date' => Yii::t('app', 'Date'),
            'Hits' => Yii::t('app', 'Hits'),
        ];
    }
}
