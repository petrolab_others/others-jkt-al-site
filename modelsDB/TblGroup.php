<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "tbl_group".
 *
 * @property string $grouploc
 */
class TblGroup extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['grouploc'], 'required'],
            [['grouploc'], 'string', 'max' => 15],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'grouploc' => Yii::t('app', 'Grouploc'),
        ];
    }
}
