<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "stats_year".
 *
 * @property int $Year
 * @property string $Hits
 */
class StatsYear extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stats_year';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Year'], 'required'],
            [['Year', 'Hits'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Year' => Yii::t('app', 'Year'),
            'Hits' => Yii::t('app', 'Hits'),
        ];
    }
}
