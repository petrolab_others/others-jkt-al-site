<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "reference".
 *
 * @property int $id
 * @property string $name
 *
 * @property DataAnalisa[] $dataAnalisas
 * @property ParameterHasReference[] $parameterHasReferences
 * @property Parameter[] $parameters
 */
class Reference extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reference';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDataAnalisas()
    {
        return $this->hasMany(DataAnalisa::className(), ['reference_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParameterHasReferences()
    {
        return $this->hasMany(ParameterHasReference::className(), ['reference_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParameters()
    {
        return $this->hasMany(Parameter::className(), ['id' => 'parameter_id'])->viaTable('parameter_has_reference', ['reference_id' => 'id']);
    }
}
