<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "tbl_otherorder_lu".
 *
 * @property string $unsur
 * @property string $unit
 * @property string $methode
 * @property string $attention
 * @property string $urgent
 * @property string $matrix_limit
 * @property string $col1
 * @property string $col2
 * @property string $col3
 * @property string $col4
 * @property string $col5
 */
class TblOtherorderLu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_otherorder_lu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['unsur', 'unit', 'methode'], 'required'],
            [['unsur'], 'string', 'max' => 30],
            [['unit'], 'string', 'max' => 10],
            [['methode', 'attention', 'urgent', 'matrix_limit', 'col1', 'col2', 'col3', 'col4', 'col5'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'unsur' => Yii::t('app', 'Unsur'),
            'unit' => Yii::t('app', 'Unit'),
            'methode' => Yii::t('app', 'Methode'),
            'attention' => Yii::t('app', 'Attention'),
            'urgent' => Yii::t('app', 'Urgent'),
            'matrix_limit' => Yii::t('app', 'Matrix Limit'),
            'col1' => Yii::t('app', 'Col1'),
            'col2' => Yii::t('app', 'Col2'),
            'col3' => Yii::t('app', 'Col3'),
            'col4' => Yii::t('app', 'Col4'),
            'col5' => Yii::t('app', 'Col5'),
        ];
    }
}
