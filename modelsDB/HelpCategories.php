<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "help_categories".
 *
 * @property int $Category_ID
 * @property string $Language
 * @property string $Category_Description
 */
class HelpCategories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'help_categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Category_ID', 'Language', 'Category_Description'], 'required'],
            [['Category_ID'], 'integer'],
            [['Language'], 'string', 'max' => 2],
            [['Category_Description'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Category_ID' => Yii::t('app', 'Category ID'),
            'Language' => Yii::t('app', 'Language'),
            'Category_Description' => Yii::t('app', 'Category Description'),
        ];
    }
}
