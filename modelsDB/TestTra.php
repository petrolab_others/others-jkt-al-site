<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "test_tra".
 *
 * @property string $Name
 * @property int $CustomerID
 * @property string $A
 */
class TestTra extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'test_tra';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['CustomerID', 'A'], 'integer'],
            [['Name'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Name' => Yii::t('app', 'Name'),
            'CustomerID' => Yii::t('app', 'Customer ID'),
            'A' => Yii::t('app', 'A'),
        ];
    }
}
