<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "tbl_customers".
 *
 * @property int $CustomerID
 * @property string $Name
 * @property string $Address
 * @property string $Branch
 * @property int $branch_no
 * @property string $login
 * @property string $password
 * @property string $entrydate
 * @property string $updatedate
 * @property string $loginpusat
 * @property string $passwordpusat
 * @property string $owner
 * @property int $reports_to
 * @property int $userlevel
 * @property int $ownerid
 */
class TblCustomers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_customers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['branch_no', 'reports_to', 'userlevel', 'ownerid'], 'integer'],
            [['entrydate', 'updatedate'], 'safe'],
            [['Name'], 'string', 'max' => 100],
            [['Address', 'Branch'], 'string', 'max' => 50],
            [['login', 'password', 'loginpusat', 'passwordpusat', 'owner'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'CustomerID' => Yii::t('app', 'Customer ID'),
            'Name' => Yii::t('app', 'Name'),
            'Address' => Yii::t('app', 'Address'),
            'Branch' => Yii::t('app', 'Branch'),
            'branch_no' => Yii::t('app', 'Branch No'),
            'login' => Yii::t('app', 'Login'),
            'password' => Yii::t('app', 'Password'),
            'entrydate' => Yii::t('app', 'Entrydate'),
            'updatedate' => Yii::t('app', 'Updatedate'),
            'loginpusat' => Yii::t('app', 'Loginpusat'),
            'passwordpusat' => Yii::t('app', 'Passwordpusat'),
            'owner' => Yii::t('app', 'Owner'),
            'reports_to' => Yii::t('app', 'Reports To'),
            'userlevel' => Yii::t('app', 'Userlevel'),
            'ownerid' => Yii::t('app', 'Ownerid'),
        ];
    }
}
