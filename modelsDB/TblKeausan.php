<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "tbl_keausan".
 *
 * @property int $no_keausan
 * @property string $component
 * @property int $Fe
 * @property int $Cr
 * @property int $Cu
 * @property int $Al
 * @property int $Pb
 * @property int $Si
 * @property string $COL 9
 * @property string $Lokasi
 * @property string $Penyebab
 * @property string $Inspeksi
 * @property string $Metoda_Inspeksi
 */
class TblKeausan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_keausan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['component', 'Lokasi', 'Penyebab', 'Inspeksi', 'Metoda_Inspeksi'], 'required'],
            [['Fe', 'Cr', 'Cu', 'Al', 'Pb', 'Si'], 'integer'],
            [['Lokasi', 'Penyebab', 'Inspeksi', 'Metoda_Inspeksi'], 'string'],
            [['component'], 'string', 'max' => 20],
            [['COL 9'], 'string', 'max' => 14],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'no_keausan' => Yii::t('app', 'No Keausan'),
            'component' => Yii::t('app', 'Component'),
            'Fe' => Yii::t('app', 'Fe'),
            'Cr' => Yii::t('app', 'Cr'),
            'Cu' => Yii::t('app', 'Cu'),
            'Al' => Yii::t('app', 'Al'),
            'Pb' => Yii::t('app', 'Pb'),
            'Si' => Yii::t('app', 'Si'),
            'COL 9' => Yii::t('app', 'Col 9'),
            'Lokasi' => Yii::t('app', 'Lokasi'),
            'Penyebab' => Yii::t('app', 'Penyebab'),
            'Inspeksi' => Yii::t('app', 'Inspeksi'),
            'Metoda_Inspeksi' => Yii::t('app', 'Metoda Inspeksi'),
        ];
    }
}
