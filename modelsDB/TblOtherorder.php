<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "tbl_otherorder".
 *
 * @property int $OtherOrderId
 * @property string $Lab_No
 * @property string $unsur
 * @property string $unit
 * @property string $methode
 * @property string $value
 * @property string $value_CODE
 * @property string $attention
 * @property string $urgent
 * @property string $severe
 * @property int $tbl_parameter_id
 */
class TblOtherorder extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_otherorder';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Lab_No', 'unsur'], 'required'],
            [['tbl_parameter_id'], 'integer'],
            [['Lab_No'], 'string', 'max' => 15],
            [['unsur'], 'string', 'max' => 30],
            [['unit', 'value', 'attention'], 'string', 'max' => 10],
            [['methode', 'urgent', 'severe'], 'string', 'max' => 20],
            [['value_CODE'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'OtherOrderId' => Yii::t('app', 'Other Order ID'),
            'Lab_No' => Yii::t('app', 'Lab No'),
            'unsur' => Yii::t('app', 'Unsur'),
            'unit' => Yii::t('app', 'Unit'),
            'methode' => Yii::t('app', 'Methode'),
            'value' => Yii::t('app', 'Value'),
            'value_CODE' => Yii::t('app', 'Value Code'),
            'attention' => Yii::t('app', 'Attention'),
            'urgent' => Yii::t('app', 'Urgent'),
            'severe' => Yii::t('app', 'Severe'),
            'tbl_parameter_id' => Yii::t('app', 'Tbl Parameter ID'),
        ];
    }
}
