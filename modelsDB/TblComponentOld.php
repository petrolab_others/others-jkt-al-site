<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "tbl_component_old".
 *
 * @property int $ComponentID
 * @property string $component
 * @property string $ComponentCode
 * @property string $samplefrom
 * @property int $UnitID
 * @property string $Matrix
 * @property string $updatedate
 */
class TblComponentOld extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_component_old';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['UnitID'], 'integer'],
            [['Matrix'], 'required'],
            [['updatedate'], 'safe'],
            [['component'], 'string', 'max' => 20],
            [['ComponentCode'], 'string', 'max' => 40],
            [['samplefrom'], 'string', 'max' => 50],
            [['Matrix'], 'string', 'max' => 15],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ComponentID' => Yii::t('app', 'Component ID'),
            'component' => Yii::t('app', 'Component'),
            'ComponentCode' => Yii::t('app', 'Component Code'),
            'samplefrom' => Yii::t('app', 'Samplefrom'),
            'UnitID' => Yii::t('app', 'Unit ID'),
            'Matrix' => Yii::t('app', 'Matrix'),
            'updatedate' => Yii::t('app', 'Updatedate'),
        ];
    }
}
