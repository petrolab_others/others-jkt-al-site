<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "tbl_kontaminan".
 *
 * @property int $no_kontaminan
 * @property string $component
 * @property int $water
 * @property int $dilution
 * @property int $silicon
 * @property string $Inspeksi
 * @property string $Metoda_Inspeksi
 * @property string $Penyebab_kontaminan
 */
class TblKontaminan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_kontaminan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['component', 'Inspeksi', 'Metoda_Inspeksi'], 'required'],
            [['water', 'dilution', 'silicon'], 'integer'],
            [['Inspeksi', 'Metoda_Inspeksi', 'Penyebab_kontaminan'], 'string'],
            [['component'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'no_kontaminan' => Yii::t('app', 'No Kontaminan'),
            'component' => Yii::t('app', 'Component'),
            'water' => Yii::t('app', 'Water'),
            'dilution' => Yii::t('app', 'Dilution'),
            'silicon' => Yii::t('app', 'Silicon'),
            'Inspeksi' => Yii::t('app', 'Inspeksi'),
            'Metoda_Inspeksi' => Yii::t('app', 'Metoda Inspeksi'),
            'Penyebab_kontaminan' => Yii::t('app', 'Penyebab Kontaminan'),
        ];
    }
}
