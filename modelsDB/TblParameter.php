<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "tbl_parameter".
 *
 * @property int $id
 * @property string $unsur
 * @property string $unit
 * @property string $methode
 * @property string $methode2
 * @property string $attention
 * @property string $urgent
 * @property string $severe
 * @property string $col_name
 * @property string $col_matrix
 * @property string $date_create
 * @property int $tbl_category_parameter_id
 * @property string $type type : other, param, 
 */
class TblParameter extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_parameter';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['unsur'], 'required'],
            [['date_create'], 'safe'],
            [['tbl_category_parameter_id'], 'integer'],
            [['unsur'], 'string', 'max' => 30],
            [['unit', 'type'], 'string', 'max' => 10],
            [['methode', 'methode2'], 'string', 'max' => 20],
            [['attention', 'urgent', 'severe'], 'string', 'max' => 45],
            [['col_name', 'col_matrix'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'unsur' => Yii::t('app', 'Unsur'),
            'unit' => Yii::t('app', 'Unit'),
            'methode' => Yii::t('app', 'Methode'),
            'methode2' => Yii::t('app', 'Methode2'),
            'attention' => Yii::t('app', 'Attention'),
            'urgent' => Yii::t('app', 'Urgent'),
            'severe' => Yii::t('app', 'Severe'),
            'col_name' => Yii::t('app', 'Col Name'),
            'col_matrix' => Yii::t('app', 'Col Matrix'),
            'date_create' => Yii::t('app', 'Date Create'),
            'tbl_category_parameter_id' => Yii::t('app', 'Tbl Category Parameter ID'),
            'type' => Yii::t('app', 'Type'),
        ];
    }
}
