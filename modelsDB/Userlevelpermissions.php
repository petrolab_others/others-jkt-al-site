<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the model class for table "userlevelpermissions".
 *
 * @property int $User_Level_ID
 * @property string $Table_Name
 * @property int $Permission
 */
class Userlevelpermissions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'userlevelpermissions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['User_Level_ID', 'Table_Name', 'Permission'], 'required'],
            [['User_Level_ID', 'Permission'], 'integer'],
            [['Table_Name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'User_Level_ID' => Yii::t('app', 'User Level ID'),
            'Table_Name' => Yii::t('app', 'Table Name'),
            'Permission' => Yii::t('app', 'Permission'),
        ];
    }
}
