<?php
/**
 * Created by
 * User: Wisard17
 * Date: 11/01/2018
 * Time: 09.33 AM
 */

namespace app\components;


/**
 * Class Formatter
 * @package app\components
 */
class Formatter extends \yii\i18n\Formatter
{
    public function asCurrency($value, $currency = null, $options = [], $textOptions = [])
    {
        if ($value === null) {
            return $this->nullDisplay;
        }
        $value = $this->normalizeNumericValue($value);

        return $currency . ' ' . $this->asDecimal($value, 2, $options, $textOptions);
    }

    public function asIndoDate($value, $format = 'D, M d, Y')
    {
        $BulanIndo = [
            'Jan' => 'Januari',
            'Feb' => 'Februari',
            'Mar' => 'Maret',
            'Apr' => 'April',
            'May' => 'Mei',
            'Jun' => 'Juni',
            'Jul' => 'Juli',
            'Aug' => 'Agustus',
            'Sep' => 'September',
            'Oct' => 'Oktober',
            'Nov' => 'November',
            'Dec' => 'Desember',
        ];
        $HariIndo = ['Mon' => "Senin", 'Tue' => "Selasa", 'Wed' => "Rabu", 'Thu' => "Kamis", 'Fri' => "Jumat", 'Sat' => "Sabtu", 'Sun' => "Minggu"];
        $date = date($format, strtotime($value));
        foreach ($HariIndo as $d => $day) {
            $date = str_replace($d, $day, $date);
        }
        foreach ($BulanIndo as $m => $mouth) {
            $date = str_replace($m, $mouth, $date);
        }
        return $date;
    }

    /**
     * @param int $number
     * @return string
     */
    public static function numberToRomanRepresentation($number) {
        $map = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400, 'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40, 'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);
        $returnValue = '';
        while ($number > 0) {
            foreach ($map as $roman => $int) {
                if($number >= $int) {
                    $number -= $int;
                    $returnValue .= $roman;
                    break;
                }
            }
        }
        return $returnValue;
    }
}