<?php
/**
 * Created by
 * User: Wisard17
 * Date: 14/11/2017
 * Time: 01.17 PM
 */

namespace app\components;


use yii\base\Component;

/**
 * Class Converter
 * @package app\models
 */
class Converter extends Component
{
    /**
     * @param $num
     * @return string
     */
    public static function numToWord($num)
    {
        $self = new self();
        return $self->nToWord($num);
    }

    /**
     * @param $number
     * @return string
     */
    public function nToWord($number)
    {
        $num = ( string )(( int )($number));
//        $num = 101000;

        if (( int )($num) && ctype_digit($num)) {
            $words = array();

            $num = str_replace(array(',', ' '), '', trim($num));

            $list1 = array('', 'satu', 'dua', 'tiga', 'empat', 'lima', 'enam', 'tujuh',
                'delapan', 'sembilan', 'sepuluh', 'sebelas', 'dua belas', 'tiga belas', 'empat belas',
                'lima belas', 'enam belas', 'tujuh belas', 'delapan belas', 'sembilan belas');

            $list2 = array('', 'sepuluh', 'dua puluh', 'tiga puluh', 'empat puluh', 'lima puluh', 'enam puluh',
                'tujuh puluh', 'delapan puluh', 'sembilan puluh', 'seratus');

            $list3 = array('', 'ribu', 'juta', 'miliar', 'triliun', 'kuadriliun');

            $num_length = strlen($num);
            $levels = ( int )(($num_length + 2) / 3);
            $max_length = $levels * 3;
            $num = substr('00' . $num, -$max_length);
            $num_levels = str_split($num, 3);

            foreach ($num_levels as $num_part) {
                $levels--;
                $hundreds = ( int )($num_part / 100);
                $hundreds = ($hundreds ? ' ' . ($hundreds == 1 ? 'Seratus' : $list1[$hundreds] . ' Ratus') . ($hundreds == 1 ? '' : '') . ' ' : '');
                $tens = ( int )($num_part % 100);
                $singles = '';

                if ($tens < 20) {
                    $tens = ($tens ? ' ' . $list1[$tens] . ' ' : '');
                } else {
                    $tens = ( int )($tens / 10);
                    $tens = ' ' . $list2[$tens] . ' ';
                    $singles = ( int )($num_part % 10);
                    $singles = ' ' . $list1[$singles] . ' ';
                }
                $words[] = $hundreds . (($levels && ( int )($num_part)) ? ' ' . ((( int )($num_part / 100) < 1 && $levels == 1 && ( int )($num_part)) == 1 ? 'Seribu' : $tens . $singles . $list3[$levels] . ' ') . ' ' : $tens . $singles);
            }

            $commas = count($words);

            if ($commas > 1) {
                $commas = $commas - 1;
            }

            $words = implode(', ', $words);

            //Some Finishing Touch
            //Replacing multiples of spaces with one space
            $words = trim(str_replace(' ,', '', $this->trim_all(ucwords($words))), ', ');
            if ($commas) {
                $words = $this->str_replace_last(',', '', $words);
            }

            return $words . ' Rupiah';
        } else if (!(( int )$num)) {
            return 'Zero';
        }
        return '';
    }

    /**
     * @param $search
     * @param $replace
     * @param $str
     * @return mixed
     */
    public function str_replace_last($search, $replace, $str)
    {
        if (($pos = strrpos($str, $search)) !== false) {
            $search_length = strlen($search);
            $str = substr_replace($str, $replace, $pos, $search_length);
        }
        return $str;
    }

    /**
     * @param $str
     * @param null $what
     * @param string $with
     * @return string
     */
    public function trim_all($str, $what = NULL, $with = ' ')
    {
        if ($what === NULL) {
            //  Character      Decimal      Use
            //  "\0"            0           Null Character
            //  "\t"            9           Tab
            //  "\n"           10           New line
            //  "\x0B"         11           Vertical Tab
            //  "\r"           13           New Line in Mac
            //  " "            32           Space

            $what = "\\x00-\\x20";    //all white-spaces and control chars
        }

        return trim(preg_replace("/[" . $what . "]+/", $with, $str), $what);
    }
}