<?php
/**
 * Created by
 * User     : Wisard17
 * Date     : 15/08/2019
 * Time     : 01.03 PM
 * File Name: DataTableAjax.php
 **/

namespace app\components;


use Yii;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\Json;

/**
 * Class DataTableAjax
 * @package app\modules\pcb\models
 */
class DataTableAjax extends Model
{
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['query', 'param', 'allData', 'currentData'], 'safe']
        ]);
    }

    /** @var Query */
    public $query;

    public $param;

    /** @var ActiveRecord */
    public $model;

    /**
     * @throws InvalidConfigException
     */
    public function init()
    {
        parent::init();

        if ($this->query == null) {
            throw new InvalidConfigException('The "query" property must be set.');
        }

        if (!($this->query instanceof Query)) {
            throw new InvalidConfigException('The "query" must instance of yii\db\query.');
        }

        if ($this->model == null) {
            $this->model = new $this->query->modelClass;
        }

        if ($this->param == null) {
            $this->param = Yii::$app->request->queryParams;
        }
    }

    /**
     * @param $params
     * @return string
     */
    public function ordering()
    {
        if (isset($this->param['order'])) {
            foreach ($this->param['order'] as $value) {
                $col = isset($this->param['columns'][$value['column']]['data']) ?
                    $this->param['columns'][$value['column']]['data'] : '';
                if ($this->model->hasAttribute($col)) {
                    $this->query->addOrderBy("$col $value[dir]");
                }
            }
            return '';
        }
        return '';
    }


    public $allData;

    public $currentData;

    /**
     * @return int
     */
    public function calcAllData()
    {
        return $this->allData == null ? $this->query->distinct()->count() : $this->allData;
    }

    /**
     * @param $params
     * @param bool $export
     * @param bool $join
     * @return $this|\yii\db\ActiveQuery|Query
     */
    public function searchData($export = false)
    {
        $this->ordering();

        $start = isset($this->param['start']) ? $this->param['start'] : 0;
        $lang = isset($this->param['length']) ? $this->param['length'] : 10;

        $this->allData = $this->calcAllData();

        if (isset($this->param['columns']))
            foreach ($this->param['columns'] as $col) {
                $m = $this->model;
                if (isset($this->param['search']['value']) && $this->param['search']['value'] != '') {
                    if ($this->model->hasAttribute($col['data'])) {
                        $this->query->orFilterWhere(['like', $m::tableName() . '.' . $col['data'], $this->param['search']['value']]);
                    }
                }
                if ($col['searchable'] == 'true' && $col['search']['value'] != '') {
                    if ($this->model->hasAttribute($col['data'])) {
                        $this->query->andFilterWhere(['like', $m::tableName() . '.' . $col['data'], $col['search']['value']]);
                    }
                }
            }


        if ($export) {
            return $this->query;
        }

        $this->currentData = $this->query->count();

        $this->query->limit($lang)->offset($start);

        return $this;
    }

    /**
     * @return mixed
     */
    public function searchDataTable()
    {
        return Json::encode([
            'draw' => isset ($this->param['draw']) ? (int)$this->param['draw'] : 0,
            'data' => $this->searchData()->renderData(),
            'recordsTotal' => (int)$this->allData,
            'recordsFiltered' => (int)$this->currentData,
        ]);
    }

    /**
     * @param $query Query
     * @param $params
     * @return array
     */

    public function renderData()
    {
        $out = [];

        /** @var self $model */
        foreach ($this->query->all() as $model) {
            $add = [];
            foreach ($this->param['columns'] as $item) {
                $com = $item['data'];
                $add[$com] = $model->$com;
            }
            $out[] = array_merge($model->toArray(), $add);
        }
        return $out;
    }

    public static function renderAjax($query, $data = [])
    {
        $model = new self(['query' => $query]);
        if ($data != []) {
            $model->load(['data' => $data], 'data');
        }
        return $model->searchDataTable();
    }
}