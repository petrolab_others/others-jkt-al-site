<?php
/**
 * Created by
 * User: Wisard17
 * Date: 09/08/2018
 * Time: 09.01 AM
 */

namespace app\components;

use PHPExcel_IOFactory;
use PHPExcel;

/**
 * Class Excel
 * @package app\components
 */
class Excel extends PHPExcel
{
    /**
     * @param $array
     * @param string $startCell
     * @param string $titleSheet
     * @param bool $setHeader
     * @return Excel
     * @throws \PHPExcel_Exception
     */
    public function genExcelByArray($array, $startCell = 'A1', $titleSheet = 'sheet', $setHeader = false)
    {

        if (PHP_SAPI === 'cli') {
            die('This example should only be run from a Web Browser');
        }

        $objPHPExcel = new self();

        $objPHPExcel->getProperties()->setCreator("Petrolab Services")
            ->setLastModifiedBy("Petrolab Services")
            ->setTitle("Petrolab Services ")
            ->setSubject("Petrolab Services")
            ->setDescription("Petrolab Services.")
            ->setKeywords("Petrolab Services")
            ->setCategory("Petrolab Services ");

        if ($setHeader) {
            $header = [];
            if (isset($array[0])) {
                foreach ($array[0] as $idx => $value) {
                    $header[0][] = $idx;
                }
            }
            $array = array_merge($header,$array);
        }

        $objPHPExcel->setActiveSheetIndex(0)->fromArray($array, null, $startCell);
        $objPHPExcel->getActiveSheet()->setTitle($titleSheet);
        $objPHPExcel->setActiveSheetIndex(0);

        return $objPHPExcel;
    }

    /**
     * @param $objPHPExcel
     * @param $fileName
     * @param bool $office2007
     * @throws \PHPExcel_Reader_Exception
     * @throws \PHPExcel_Writer_Exception
     */
    public function download($objPHPExcel, $fileName, $office2007 = false)
    {
        $v = 'Excel5';
        $ext = '.xls';
        $h1 = 'Content-Type: application/vnd.ms-excel';
        $h2 = 'Expires: Mon, 26 Jul 1997 05:00:00 GMT';

        if ($office2007) {
            $v = 'Excel2007';
            $ext = '.xlsx';
            $h1 = 'Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
            $h2 = 'Expires: Mon, 26 Jul 1997 05:00:00 GMT';
        }

        header($h1);
        header('Content-Disposition: attachment;filename="' . $fileName . $ext . '"');
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');
        header($h2); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, $v);
        $objWriter->save('php://output');
        exit;
    }

    /**
     * @param $objPHPExcel
     * @param $fileName
     * @throws \PHPExcel_Reader_Exception
     * @throws \PHPExcel_Writer_Exception
     */
    public function downloadCsv($objPHPExcel, $fileName)
    {

        $h1 = 'Content-Type: text/csv';
        $h2 = 'Expires: Mon, 26 Jul 1997 05:00:00 GMT';

        header($h1);
        header('Content-Disposition: attachment;filename="'.$fileName.'.csv"');
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');
        header($h2); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
        $objWriter->save('php://output');
        exit;
    }

    /**
     * @param $objPHPExcel
     * @param $fileName
     * @param $separator
     * @return void
     * @throws \PHPExcel_Reader_Exception
     * @throws \PHPExcel_Writer_Exception
     */
    public function downloadCustom($objPHPExcel, $fileName, $separator = ',')
    {
//        $h1 = 'Content-Type: text/csv';
//        $h2 = 'Expires: Mon, 26 Jul 1997 05:00:00 GMT';
//
//        header($h1);
//        header('Content-Disposition: attachment;filename="'.$fileName.'.csv"');
//        header('Cache-Control: max-age=0');
//        header('Cache-Control: max-age=1');
//        header($h2); // Date in the past
//        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
//        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
//        header('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
        $objWriter->setSheetIndex(0);
        $objWriter->setDelimiter($separator);
        $objWriter->save('assets/' . $fileName);

        return;
    }

    /**
     * @param $fileName
     * @param null $type
     * @return \PHPExcel
     * @throws \PHPExcel_Reader_Exception
     */
    public static function renderData($fileName, $type = null)
    {
        if ($type == null) {
            return PHPExcel_IOFactory::load($fileName);
        }
        try {
            $inputFileType = PHPExcel_IOFactory::identify($fileName);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        } catch (\Exception $e) {
            die($e->getMessage());
        }
        return $objReader->load($fileName);
    }

    /**
     * @return PHPExcel
     */
    public function newExcel()
    {
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setCreator("Petrolab Services")
            ->setLastModifiedBy("Petrolab Services")
            ->setTitle("Petrolab Services ")
            ->setSubject("Petrolab Services")
            ->setDescription("Petrolab Services.")
            ->setKeywords("Petrolab Services")
            ->setCategory("Petrolab Services ");
        return $objPHPExcel;
    }
}