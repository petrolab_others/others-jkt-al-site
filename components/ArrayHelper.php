<?php
/**
 * Created by
 * User: Wisard17
 * Date: 27/04/2018
 * Time: 08.36 AM
 */

namespace app\components;

/**
 * Class ArrayHelper
 * @package app\components
 */
class ArrayHelper extends \yii\helpers\ArrayHelper
{
    /**
     * @param array $array
     * @return string
     */
    public static function toString($array)
    {
        $out = '(';
        if (is_array($array)) {
            foreach ($array as $idx => $value) {
                if (is_array($value))
                    $out .= " $idx => " . self::toString($value);
                else
                    $out .= " $idx => " . $value;
            }
        } else
            $out .= $array;
        return "$out)";
    }
}