<?php
/**
 * Created by
 * User: Wisard17
 * Date: 25/02/2019
 * Time: 10.22 AM
 */

namespace app\components;


use Yii;
use yii\db\mysql\ColumnSchemaBuilder;

/**
 * Class Migration
 * @package app\components
 */
class Migration extends \yii\db\Migration
{
    /**
     * @param $tableName
     * @return bool
     */
    public function tableExists($tableName)
    {
        return in_array($tableName, Yii::$app->db->schema->tableNames, true);
    }

    /**
     * @param string $table
     * @param array $columns
     * @param null $options
     * @return null|void
     */
    public function createTable($table, $columns, $options = null)
    {
        if ($this->tableExists($table)) {
            $this->completeColumns($table, $columns);
            return null;
        }
        parent::createTable($table, $columns, $options);
    }

    /**
     * @param $table
     * @param $columns ColumnSchemaBuilder[]
     * @return null
     */
    public function completeColumns($table, $columns)
    {
        if (!$this->tableExists($table)) {
            return null;
        }
        $schema = $this->getDb()->getTableSchema($table);
        /* @var $column string */
        foreach ($columns as $column => $type) {
            if (is_int($column)) {
                continue;
            }
            if ($schema->getColumn($column) === null) {
                $this->addColumn($table, $column, $type);
            }
        }
        return null;
    }

    public function addColumn($table, $column, $type)
    {
        if (!$this->tableExists($table)) {
            return null;
        }
        $schema = $this->getDb()->getTableSchema($table);
        if ($schema->getColumn($column) === null) {
            parent::addColumn($table, $column, $type);
        }
    }

    public function columnExists($table, $column)
    {
        $schema = $this->getDb()->getTableSchema($table);
        return $schema->getColumn($column) !== null;
    }

    public function clean($string) {
        $string = str_replace(' ', '_', $string); // Replaces all spaces with hyphens.
        return preg_replace('/[^A-Za-z0-9\_]/', '', $string); // Removes special chars.
    }
}