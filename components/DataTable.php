<?php
/**
 * Created by
 * User     : Wisard17
 * Date     : 15/08/2019
 * Time     : 12.02 PM
 * File Name: DataTable.php
 **/

namespace app\components;


use app\smartadmin\assets\plugins\DataTableAsset;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\base\Widget;
use yii\db\ActiveRecord;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;

/**
 * Class DataTable
 * @package app\modules\pcb\models
 */
class DataTable extends Widget
{
    /**
     * @var array the HTML attributes for the container tag of the list view.
     * The "tag" element specifies the tag name of the container element and defaults to "div".
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $options = [];

    public $columns;

    /** @var  ActiveRecord */
    public $model;

    public $request;

    /**
     * Initializes the view.
     * @throws InvalidConfigException
     */
    public function init()
    {
        parent::init();

        if ($this->columns === null) {
            throw new InvalidConfigException('The "columns" property must be set.');
        }

        if ($this->model === null) {
            throw new InvalidConfigException('The "model" property must be set.');
        }

        if (!($this->model instanceof ActiveRecord)) {
            throw new InvalidConfigException('The "model" property are not intense of ActiveRecord');
        }

        if (!isset($this->options['id'])) {
            $this->options['id'] = $this->getId();
        }
    }

    /**
     * Runs the widget.
     */
    public function run()
    {
        $assetTable = DataTableAsset::register($this->view);
        echo $this->loadTable();
        $this->runJs();
    }

    /**
     * @return string
     */
    public function loadTable()
    {
        $content = '';

        return Html::tag('table', "",
            ['class' => 'table table-responsive', 'width' => "100%", 'id' => $this->options['id']]);
    }


    /**
     * @return array
     */
    protected function loadColumnHeader()
    {
        $out = [];
        foreach ($this->columns as $column) {
            if (is_array($column)) {
                if (isset($column['data']) && !isset($column['title'])) {
                    $column['title'] = $this->model->getAttributeLabel($column['data']);
                }
                $column['name'] = $column['title'];
                $out[] = $column;
            } else {
                $out[] = [
                    'title' => $this->model->getAttributeLabel($column),
                    'name' => $this->model->getAttributeLabel($column),
                    'data' => (string)$column,
                ];
            }
        }

        return $out;
    }

    protected static function putNameIn($name, $string, $addition)
    {
        if (stripos($string, $name) !== false) {
            return str_replace($name, "$name $addition", $string);
        }
        return $string;
    }

    /**
     * @return string
     */
    protected function loadColumnFilter()
    {
        $out = '<tr>';
        foreach ($this->columns as $column) {
            if (isset($column['searchable']) && !$column['searchable']) {
                $out .= '<th></th>';
                continue;
            }
            if (is_array($column)) {
                $col = isset($column['data']) ? $column['data'] : '';
                if (isset($column['input'])) {
                    $intp = self::putNameIn('<select', $column['input'], "name='filter_$col'");
                    $intp = self::putNameIn('<input', $intp, "name='filter_$col'");
                    $out .= "<th class='hasinput' >$intp</th>";
                } elseif ($this->model->hasAttribute($col)) {
                    $out .= "<th class='hasinput' ><input type='text' class='' placeholder=' Filter " .
                        $this->model->getAttributeLabel($col) . "' name='filter_$col' /></th>";
                } else {
                    $out .= '<th></th>';
                }
            } elseif ($this->model->hasAttribute($column)) {
                $out .= "<th class='hasinput' ><input type='text' class='' placeholder=' Filter " .
                    $this->model->getAttributeLabel($column) . "' name='filter_$column'/></th>";
            } else {
                $out .= '<th></th>';
            }
        }

        return $out . '</tr>';
    }

    public $jsAfterLoadTable = '';

    /**
     * All JS
     */
    protected function runJs()
    {
        $csrf = Yii::$app->request->getCsrfToken();
        $loadingHtml = '<div align="center"><i class="fa fa-spinner fa-pulse fa-4x fa-fw margin-bottom"></i></div>';
        $idTable = $this->options['id'];
        $columns = Json::encode($this->loadColumnHeader());
        $columnsFilter =  $this->loadColumnFilter();
        $detailUrl = Url::to(['detail']);
        $order = isset($this->options['order']) ? $this->options['order'] : "[0, 'desc']";
        $urlExportExcel = isset($this->options['excel-export']) ? $this->options['excel-export'] : Url::toRoute(['/pcb/monitoring/download-excel']);
        $jsScript = <<< JS
var delay2 = (function () {
    var timer = 0;
    return function (callback, ms) {
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
    };
})();
var renderdata = (function () {
    // var dataSet = ;
    var domElement = $('#$idTable');

    var domOuttable = domElement.parent().parent().parent().parent();

    var newHeader = $("$columnsFilter");

    var table = domElement.DataTable({
        sDom: "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'bar-export col-sm-3 col-xs-12 hidden-xs'><'col-sm-3 pull-right col-xs-12 hidden-xs'l>r>" +
            "t" + "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
        scrollX: true,
        // rowId: "",
        processing: true,
        serverSide: true,
        stateSave: true,
        ajax: '',
        columns: $columns,
        order:[$order],
        createdRow: function (row, data, index) {
            if (data.classRow !== '')
                $(row).addClass(data.classRow);
        },
        initComplete: function () {
            delay2(function () {
               
            }, 1000);
            var state = table.state.loaded();
            if (state) {
                table.columns().eq(0).each(function (colIdx) {
                    var colname = table.column(colIdx).dataSrc();
                    var colSearch = state.columns[colIdx].search;
                    if (colSearch.search) {
                        newHeader.find('[name=filter_' + colname + ']').val(colSearch.search);
                    }
                });
                
            }
            domOuttable.find('thead').eq(0).append(newHeader);
            $this->jsAfterLoadTable
        }
    })

    domOuttable.find('thead').eq(0).delegate('th input[type=text]', 'keyup change', function () {
        var self = this;
        delay2(function () {
            table
                .column($(self).parent().index() + ':visible')
                .search(self.value)
                .draw();
        }, 1000);

    });

    domOuttable.find('thead').eq(0).delegate('th select', 'change', function () {
        var self = this;
        delay2(function () {
            table
                .column($(self).parent().index() + ':visible')
                .search(self.value)
                .draw();
        }, 1000);

    });

    domOuttable.delegate('[data-action=update_status]', 'click', function (a) {
        a.preventDefault();
        a.stopPropagation();
        var elm = $(this);
        var td = elm.closest('td');
        var cll = table.cell(td);

        $.ajax({
            url: elm.attr('href'),
            method: 'post',
            data: {},
            error: function (response) {
                alert_notif({status: "error", message: 'tidak bisa save'});
            },
            success: function (response) {
                cll.data(response.button);
                alert_notif(response);
            },

        });

    });

    var modalvar = $('#modal-temp');

    modalvar.delegate('[data-action=export_excel]', 'click', function (a) {
        a.preventDefault();
        a.stopPropagation();
        var elm = $(this);

        var form = modalvar.find('form');
        var data = $(form).serializeArray();

        var param = table.ajax.params();
        param._csrf = '$csrf';
        var urlparm = $.param(param);

        $.fileDownload($(form).attr('action'), {
            preparingMessageHtml: "We are preparing your report, please wait...",
            failMessageHtml: "There was a problem generating your report, the report is to big, filter first and please try again.",
            httpMethod: $(form).attr('method'),
            data: data
        });
        modalvar.modal('hide');
        return false;
    });

    $('[data-action=admin_excel]').click(function (a) {
        a.preventDefault();
        a.stopPropagation();
        var elm = $(this);

        modalvar.removeData('bs.modal');
        modalvar.modal().find('.modal-content').load(elm.attr('href'), function () {
            var modal = $(this);
            modal.find('.select2param').select2({width: "100%",});
        });
        modalvar.modal('show');
    });


    function alert_notif(res) {
        $.sound_on = false;
        var color = res.status === 'success' ? '#659265' : '#C46A69';
        var icon = res.status === 'success' ? 'fa-check' : 'fa-times';
        $.smallBox({
            title: res.status,
            content: res.message,
            color: color,
            iconSmall: "fa " + icon + " fa-2x fadeInRight animated",
            timeout: 4000
        });
    }

    return {
        "test": function () {
            alert('daa');
        },
        table: table
    };
})();        

window.tbl_0004 = renderdata.table;

JS;


        $this->view->registerJs($jsScript, 3, 'runfor_' . $this->options['id']);
    }
}