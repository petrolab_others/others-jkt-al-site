<?php

require_once(__DIR__.'/phpchartdir.php');
$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'name' => 'Other Report',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'users', 'data', 'customers', 'reports'],
    'timeZone' => 'Asia/Jakarta',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
        '@uploaddir' => '@app/uploaded',
        '@logfile' => '@app/logfile',
        '@chartImg' => '@app/web/image/reports/streaming/chart',
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'FV6vI92FnPXF8hnTnHu-sP2oBqhEoDaD',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        /* */
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '/' => 'site/index',
                '<action:(login|logout|about|contact|register|profile|change-password|reset-password|request-password-reset)>'
                => 'site/<action>',
                '<controller>/v/<id:\d+>' => '<controller>/view',
                '<controller>/u/<id:\d+>' => '<controller>/update',
                '<controller>/<action>' => '<controller>/<action>',
                '<controller:[\w\-]+>/<action:[\w\-]+>/<id:\d+>' => '<controller>/<action>',
                '<controller:[\w\-]+>/<action:[\w\-]+>' => '<controller>/<action>',
                '<module>/<controller>/v/<id:\d+>' => '<module>/<controller>/view',
                '<module>/<controller>/u/<id:\d+>' => '<module>/<controller>/update',
                '<module>/<controller:[\w\-]+>/<action:[\w\-]+>/<id:\d+>' => '<module>/<controller>/<action>',
                '<module>/<controller:[\w\-]+>/<action:[\w\-]+>' => '<module>/<controller>/<action>',
            ],
        ],
        'assetManager' => [
            'linkAssets' => true,
        ],

        'view' => [
            'theme' => [
                'pathMap' => [
                    '@app/views' => '@app/smartadmin/views'
                ],
            ],
        ],
        'session' => [
            'name' => '_identity-petrolab-others-jkt',
        ],
        'formatter' => [
            'class' => 'app\components\Formatter',
            'dateFormat' => 'yyyy-MM-dd',
            'decimalSeparator' => ',',
            'thousandSeparator' => ' ',
            'currencyCode' => 'IDR',
            'nullDisplay' => '',
        ],

    ],

    'modules' => [
        'users' => [
            'class' => 'app\modules\users\Module',
        ],
        'reports' => [
            'class' => 'app\modules\reports\Module',
        ],
        'master' => [
            'class' => 'app\modules\master\Module',
        ],
        'customers' => [
            'class' => 'app\modules\customers\Module',
        ],
        'options' => [
            'class' => 'app\modules\options\Module',
        ],
        'data' => [
            'class' => 'app\modules\data\Module',
        ],
        'api' => [
            'class' => 'app\modules\api\Module',
        ],
    ],

    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
