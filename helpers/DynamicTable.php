<?php

namespace app\helpers;

use Yii;
use yii\helpers\Url;

/**
 *
 */
class DynamicTable
{
    public static function get($trx)
    {
        $total = count($trx);
        $aa = 0;
        if ($total == 0) {
            $aa = 5;
        } elseif ($total == 1) {
            $aa = 4;
        } elseif ($total == 2) {
            $aa = 3;
        } elseif ($total == 3) {
            $aa = 2;
        } elseif ($total == 4) {
            $aa = 1;
        }
        if ($total < 5) {
            for ($bb = 0; $bb < $aa; $bb++) {
                echo "<td class='value'></td>";
            }
        }
    }

    public static function getOther($trx)
    {
        $total = count($trx);
        $aa = 0;
        if ($total == 0) {
            $aa = 5;
        } elseif ($total == 1) {
            $aa = 4;
        } elseif ($total == 2) {
            $aa = 3;
        } elseif ($total == 3) {
            $aa = 2;
        } elseif ($total == 4) {
            $aa = 1;
        }
        if ($total < 5) {
            for ($bb = 0; $bb < $aa; $bb++) {
                echo "<td class='value'></td>";
            }
        }
    }

    /**
     * @param $code
     * @throws \yii\base\InvalidConfigException
     */
    public static function evalCode($code)
    {
        $cd = strtoupper($code);
        $base = Url::home(true);
        if ($cd == 'A') {
            echo '<img width="50" src="./image/biru.jpg">
			<section class="td_code">Normal</section>';
        } elseif ($cd == 'N') {
            echo '<img width="50" src="./image/biru.jpg">
			<section class="td_code">Normal</section>';
        } elseif ($cd == '') {
            echo '<section></section>';
        } elseif ($cd == 'B') {
            echo '<img width="50" src="./image/kuning.jpg">
			<section class="td_code">Attention</section>';
        } elseif ($cd == 'C') {
            echo '<img width="50" src="./image/merah.jpg">
			<section class="td_code">Urgent</section>';
        } elseif ($cd == 'D') {
            echo '<img width="50" src="./image/merah.jpg">
			<section class="td_code">Severe</section>';
        }
    }

    public static function color($code = '')
    {
        if ($code == 'B') {
            return 'yellow';
        } elseif ($code == 'C') {
            return 'red';
        } elseif ($code == 'D') {
            return 'red';
        }
        return '';
    }

    public static function code($code = '')
    {
        if ($code == 'B') {
            return '/ B';
        } elseif ($code == 'C') {
            return '/ C';
        } elseif ($code == 'D') {
            return '/ D';
        }
        return '';
    }

    public static function tdValue($code, $value)
    {
        $n = $code;
        $code = strlen($code) > 1 ? $code[0] : $code;
        $color = DynamicTable::color($code);
        $add_code = DynamicTable::code($code);
        $c = $add_code == '' ? '' : "/ $n";
        return "<td class='value $color' style='font-size: 11px;'>$value $c</td>";
    }

    public static function wear_metal($code, $value)
    {
        $color = DynamicTable::color($code);
        $add_code = DynamicTable::code($code);
        $new_value = "$value" == "0" ? "<1" : $value;
        return "<td class='value $color'>$new_value $add_code</td>";
    }

    public static function ftir($code, $value)
    {
        $color = DynamicTable::color($code);
        $add_code = DynamicTable::code($code);
        $new_value = "$value" == "0" ? "<0,02" : $value;
        return "<td class='value $color'>$new_value $add_code</td>";
    }


}

?>