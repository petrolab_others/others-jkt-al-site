<?php

namespace app\helpers;

use app\components\XYChart;
use Yii;
use yii\base\Model;

/**
 * Class ChartDir
 * @package app\helpers
 *
 * @property string $dirBerkas
 */
class ChartDir extends Model
{

    public $dir_chart;
    public $title_size = 12;

    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->dir_chart = $this->getDirBerkas();
    }


    /**
     * @return string
     */
    public function getDirBerkas()
    {
        $dir = Yii::getAlias('@app/web/image/reports');
        if (!file_exists($dir . '/streaming') && !mkdir($concurrentDirectory = $dir . '/streaming', 0777, true) && !is_dir($concurrentDirectory)) {
            throw new \RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
        }
        $dirFile = $dir . '/streaming/chart';
        if (!file_exists($dirFile) && !mkdir($dirFile, 0777, true) && !is_dir($dirFile)) {
            throw new \RuntimeException(sprintf('Directory "%s" was not created', $dirFile));
        }
        foreach (['wear_trending', 'tan_tbn', 'contaminant', 'visco'] as $item) {
            $dirCart = $dirFile . '/' . $item;
            if (!file_exists($dirCart) && !mkdir($dirCart, 0777, true) && !is_dir($dirCart)) {
                throw new \RuntimeException(sprintf('Directory "%s" was not created', $dirCart));
            }
        }
        return $dirFile;
    }

    public function wear_trending($c, $datas)
    {
        $smbl = 10;

        $labels = [];
        $fe = [];
        $cu = [];
        $al = [];
        $cr = [];
        $ni = [];
        $sn = [];
        $pb = [];
        $pq = [];
        foreach ($datas as $key => $value) {
            $labels[] = date('d-M-y', strtotime($value['RPT_DT1']));
            $fe[] = $value['IRON_others'];
            $cu[] = $value['COPPER_others'];
            $al[] = $value['ALUMINIUM_others'];
            $cr[] = $value['CHROMIUM_others'];
            $ni[] = $value['NICKEL_others'];
            $sn[] = $value['TIN_others'];
            $pb[] = $value['LEAD_others'];
            $pq[] = $value['PQIndex'];
        }
        $title = "WEAR TRENDING";
        $c->setPlotArea(25, 40, 670, 125, 0xffffff, -1, -1, 0xc0c0c0, -1);
        $c->addTitle2(Top, $title, "arialbd.ttf", $this->title_size, 0xffffff, 0x31319c);
        $c->xAxis->setLabels($labels);
        $c->xAxis->setLabelStyle("Arial Bold", 8, "0xcc6600");
        $c->yAxis->setLabelStyle("Arial Bold", 8, "0xcc6600");
        $layer = $c->addLineLayer();

        $legendObj = $c->addLegend(30, 14, false, '4', $smbl);
        $legendObj->setBackground(Transparent);

        $dataSetObj = $layer->addDataSet($fe, 0xff0000, "FE");
        $dataSetObj->setDataSymbol('1', $smbl);

        $dataSetObj = $layer->addDataSet($cu, 0xffff00, "Cu");
        $dataSetObj->setDataSymbol('2', $smbl);

        $dataSetObj = $layer->addDataSet($al, 0x009900, "Al");
        $dataSetObj->setDataSymbol('3', $smbl);

        $dataSetObj = $layer->addDataSet($cr, 0x3333cc, "Cr");
        $dataSetObj->setDataSymbol('4', $smbl);

        $dataSetObj = $layer->addDataSet($ni, 0x990033, "NI");
        $dataSetObj->setDataSymbol('5', $smbl);

        $dataSetObj = $layer->addDataSet($sn, 0x990033, "Sn");
        $dataSetObj->setDataSymbol('6', $smbl);

        $dataSetObj = $layer->addDataSet($pb, 0x990033, "Pb");
        $dataSetObj->setDataSymbol('7', $smbl);

        $dataSetObj = $layer->addDataSet($pq, 0x990033, "Pq");
        $dataSetObj->setDataSymbol('8', $smbl);

        $c->makeChart($this->dir_chart . '/wear_trending/chart.png');
        $this->crop_img($this->dir_chart . '/wear_trending/chart.png');

    }

    /**
     * @param XYChart $c
     * @param $datas
     */
    public function tan_tbn($c, $datas)
    {
        $smbl = 10;
        $size = 10;
        $labels = [];
        $tan = [];
        $tbn = [];
        foreach ($datas as $key => $value) {
            $labels[] = date('d-M-y', strtotime($value['RPT_DT1']));
            $tan[] = (string)$value['T_A_N'] === '' ? NoValue : (float)$value['T_A_N'];
            $tbn[] = (string)$value['T_B_N'] === '' ? NoValue : (float)$value['T_B_N'];
        }

        # The data for the chart
        $data0 = $tan;
        $data1 = $tbn;

        $title = "TAN - TBN";
        $c->setPlotArea(50, 40, 610, 125, 0xffffff, -1, -1, 0xc0c0c0, -1);
        $c->addTitle2(Top, $title, "arialbd.ttf", $this->title_size, 0xffffff, 0x31319c);
        $c->xAxis->setLabels($labels);
        $c->xAxis->setLabelStyle("Arial Bold", $size, "0xcc6600");
        $c->yAxis->setLabelStyle("Arial Bold", $size, "0xcc6600");
        $layer = $c->addLineLayer();
        $layer2 = $c->addLineLayer();
# Set the labels on the x axis.
        $c->xAxis->setLabels($labels);

# Add a title to the primary (left) y axis
        $c->yAxis->setTitle("TAN");

# Set the axis, label and title colors for the primary y axis to red (0xc00000) to match the first
# data set
        $c->yAxis->setColors(0xc00000, 0xc00000, 0xc00000);

# Add a title to the secondary (right) y axis
        $c->yAxis2->setTitle("TBN");

# set the axis, label and title colors for the primary y axis to green (0x008000) to match the
# second data set
        $c->yAxis2->setColors(0x008000, 0x008000, 0x008000);

# Add a line layer to for the first data set using red (0xc00000) color with a line width to 3
# pixels
        $lineLayerObj = $layer->addDataSet($data0, 0xc00000);
        $lineLayerObj->setDataSymbol('1', $smbl);
        $lineLayerObj->setLineWidth(3);

# Add a bar layer to for the second data set using green (0x00C000) color. Bind the second data set
# to the secondary (right) y axis
        $barLayerObj = $layer2->addDataSet($data1, 0x00c000);
        $barLayerObj->setDataSymbol('2', $smbl);
        $barLayerObj->setUseYAxis2();

        $c->makeChart($this->dir_chart . '/tan_tbn/chart.png');
        $this->crop_img($this->dir_chart . '/tan_tbn/chart.png');
    }

    public function contaminant($c, $datas)
    {
        $smbl = 10;
        $size = 10;
        $labels = [];
        $soot = [];
        $water = [];
        foreach ($datas as $key => $value) {
            $labels[] = date('d-M-y', strtotime($value['RPT_DT1']));
            $soot[] = (string)$value['DIR_TRANS'] === '' ? NoValue : (float)$value['DIR_TRANS'];
            $water[] = (string)$value['WATER'] === '' ? NoValue : (float)$value['WATER'];
        }

        # The data for the chart
        $data0 = $soot;
        $data1 = $water;

        $title = "SOOT - WATER";
        $c->setPlotArea(50, 40, 610, 125, 0xffffff, -1, -1, 0xc0c0c0, -1);
        $c->addTitle2(Top, $title, "arialbd.ttf", $this->title_size, 0xffffff, 0x31319c);
        $c->xAxis->setLabels($labels);
        $c->xAxis->setLabelStyle("Arial Bold", $size, "0xcc6600");
        $c->yAxis->setLabelStyle("Arial Bold", $size, "0xcc6600");
        $layer = $c->addLineLayer();
        $layer2 = $c->addLineLayer();

# Set the labels on the x axis.
        $c->xAxis->setLabels($labels);

# Add a title to the primary (left) y axis
        $c->yAxis->setTitle("Soot");

# Set the axis, label and title colors for the primary y axis to red (0xc00000) to match the first
# data set
        $c->yAxis->setColors(0xc00000, 0xc00000, 0xc00000);

# Add a title to the secondary (right) y axis
        $c->yAxis2->setTitle("Water");

# set the axis, label and title colors for the primary y axis to green (0x008000) to match the
# second data set
        $c->yAxis2->setColors(0x008000, 0x008000, 0x008000);

# Add a line layer to for the first data set using red (0xc00000) color with a line width to 3
# pixels
        $lineLayerObj = $layer->addDataSet($data0, 0xc00000);
        $lineLayerObj->setDataSymbol('1', $smbl);
        $lineLayerObj->setLineWidth(3);

# Add a bar layer to for the second data set using green (0x00C000) color. Bind the second data set
# to the secondary (right) y axis
        $barLayerObj = $layer2->addDataSet($data1, 0x00c000);
        $barLayerObj->setDataSymbol('2', $smbl);
        $barLayerObj->setUseYAxis2();

        $c->makeChart($this->dir_chart . '/contaminant/chart.png');
        $this->crop_img($this->dir_chart . '/contaminant/chart.png');
    }

    public function visco($c, $datas)
    {
        $smbl = 10;
        $size = 10;
        $labels = [];
        $visc_40 = [];
        $visc_100 = [];
        foreach ($datas as $key => $value) {
            $labels[] = date('d-M-y', strtotime($value['RPT_DT1']));
            $visc_40[] = (string)$value['visc_40'] === '' ? NoValue : (float)$value['visc_40'];
            $visc_100[] = (string)$value['VISC_CST'] === '' ? NoValue : (float)$value['VISC_CST'];
        }

        # The data for the chart
        $data0 = $visc_40;
        $data1 = $visc_100;

        $title = "VISCOSITY";
        $c->setPlotArea(50, 40, 610, 125, 0xffffff, -1, -1, 0xc0c0c0, -1);
        $c->addTitle2(Top, $title, "arialbd.ttf", $this->title_size, 0xffffff, 0x31319c);
        $c->xAxis->setLabels($labels);
        $c->xAxis->setLabelStyle("Arial Bold", $size, "0xcc6600");
        $c->yAxis->setLabelStyle("Arial Bold", $size, "0xcc6600");
        $layer = $c->addLineLayer();
        $layer2 = $c->addLineLayer();

# Set the labels on the x axis.
        $c->xAxis->setLabels($labels);

# Add a title to the primary (left) y axis
        $c->yAxis->setTitle("Visc 40");

# Set the axis, label and title colors for the primary y axis to red (0xc00000) to match the first
# data set
        $c->yAxis->setColors(0xc00000, 0xc00000, 0xc00000);

# Add a title to the secondary (right) y axis
        $c->yAxis2->setTitle("Visc 100");

# set the axis, label and title colors for the primary y axis to green (0x008000) to match the
# second data set
        $c->yAxis2->setColors(0x008000, 0x008000, 0x008000);

# Add a line layer to for the first data set using red (0xc00000) color with a line width to 3
# pixels
        $lineLayerObj = $layer->addDataSet($data0, 0xc00000);
        $lineLayerObj->setDataSymbol('1', $smbl);
        $lineLayerObj->setLineWidth(3);

# Add a bar layer to for the second data set using green (0x00C000) color. Bind the second data set
# to the secondary (right) y axis
        $barLayerObj = $layer2->addDataSet($data1, 0x00c000);
        $barLayerObj->setDataSymbol('2', $smbl);
        $barLayerObj->setUseYAxis2();

        $c->makeChart2('PNG');
        $c->makeChart($this->dir_chart . '/visco/chart.png');

        $this->crop_img($this->dir_chart . '/visco/chart.png');
    }

    /**
     * delete all image
     */
    public function deleteAllChart($dir)
    {
        $file = glob($this->dir_chart . '/' . $dir . "/*");
        if ($file) {
            foreach ($file as $key => $value) {
                unlink($value);
            }
        }
    }

//crop image
    public function crop_img($img)
    {
        $im = imagecreatefrompng($img);
        $im2 = imagecrop($im, ['x' => 0, 'y' => 0, 'width' => imagesx($im), 'height' => (imagesy($im) - 50)]);
        if ($im2 !== FALSE) {
            imagepng($im2, $img);
            imagedestroy($im2);
        }
        imagedestroy($im);
    }

}

?>