<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user \app\models\User */
$name =  $user->fullName;

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->passwordResetToken]);
?>
Name : <?= $name ?>
Username : <?= Html::encode($user->Username) ?>

Follow the link below to reset your password:

<?= $resetLink ?>
