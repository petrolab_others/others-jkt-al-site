<?php

namespace app\modules\customers\controllers;


use app\modules\customers\models\Customers;
use app\modules\customers\models\FormCustomer;
use Yii;
use app\models\AccessControl;
use app\modules\customers\models\Branch;
use app\modules\customers\models\search\CustomerSearch;
use conquer\select2\Select2Action;
use yii\base\Model;
use yii\db\ActiveQuery;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Default controller for the `customers` module
 */
class DefaultController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['detail', 'index', 'add', 'edit'],
                'rules' => [
                    [
                        'actions' => ['detail', 'index', 'add', 'edit'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'del' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'list-customer' => [
                'class' => Select2Action::className(),
                'dataCallback' => [$this, 'dataCallback'],
            ],
            'list-branch' => [
                'class' => Select2Action::className(),
                'dataCallback' => [$this, 'dataBranch'],
            ],
        ];
    }

    /**
     * @param $q
     * @return array
     */
    public function dataCallback($q)
    {
        $query = new ActiveQuery(Customers::className());
        $get = Yii::$app->request->get('param');
        $add = '';
        $out = [];
        if (isset($get['col'])) {
            $add = "$get[col] = '$get[filter]'";
            if ($get['filter'] == '') {
                /** @var Model $model */
                $model = new $get['model']();
                $n = isset($model->attributeLabels()[$get['fromcol']]) ? $model->attributeLabels()[$get['fromcol']] : $get['fromcol'];
                $out[] = [
                    'id' => '',
                    'text' => $n . ' select first',
                ];
            }
        }
        $out = array_merge($out, $query->select([
            'CustomerID as id',
            'Name as text',
            'Branch'
        ])->filterWhere(['like', 'Name', $q])->andWhere($add)
            ->asArray()
            ->limit(100)
            ->all());
        if ($out == []) {
            $out[] = [
                'id' => 0,
                'text' => 'New Attention',
                'Branch' => '',
            ];
        }
        return [
            'results' => $out,
        ];
    }

    /**
     * @param $q
     * @return array
     */
    public function dataBranch($q)
    {
        $query = new ActiveQuery(Branch::className());
        $get = Yii::$app->request->get('param');
        $fromId = 'branch';
        if (isset($get['fromId']) && $get['fromId'] != '') {
            $fromId = $get['fromId'];
        }
        $out = $query->select([
            $fromId . ' as id',
            'branch as text',
            'address',
        ])->filterWhere(['like', 'branch', $q])
            ->asArray()
            ->limit(20)
            ->all();
        if ($out == []) {
            $out[] = [
                'id' => 0,
                'text' => 'New Customer',
                'address' => '',
            ];
        }
        return [
            'results' => $out,
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new CustomerSearch();
        $request = Yii::$app->request->queryParams;

        if (Yii::$app->request->isAjax) {
            return $searchModel->searchDataTable($request);
        }

        return $this->render('index');
    }

    /**
     * @return array|string|Response
     * @throws \Exception
     */
    public function actionNew()
    {
        $model = new FormCustomer();

        $post = Yii::$app->request->post();

        if (Yii::$app->request->isAjax && isset($post['reg_new_row'])) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $model::renderRowAjax($post['reg_new_row']);
        }

        if ($model->load($post) && $model->save()) {
            return $this->redirect(Url::toRoute(['/customers/default/index', 'id' => $model->branch_no]));
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return array|string|Response
     * @throws NotFoundHttpException
     */
    public function actionEdit($id)
    {
        $model = $this->findModel($id);

        $post = Yii::$app->request->post();

        if (Yii::$app->request->isAjax && isset($post['reg_new_row'])) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $model::renderRowAjax($post['reg_new_row']);
        }

        if ($model->load($post) && $model->save()) {
            return $this->redirect(Url::toRoute(['/customers/default/index', 'id' => $model->branch_no]));
        }

        return $this->render('edit', [
            'model' => $model,
        ]);
    }


    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     */
    public function actionDel($id)
    {
        $model = $this->findModel($id);
        $model->delete();
//        $model->active = 0;
//        $model->save();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Orders model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FormCustomer the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FormCustomer::find()->where("branch_no = '$id'")->one()) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
