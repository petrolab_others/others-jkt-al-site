<?php
/**
 * Created by
 * User: Wisard17
 * Date: 08/09/2018
 * Time: 09.41 AM
 */

namespace app\modules\customers\models;


use app\modelsDB\TblCustomers;
use yii\helpers\Html;

/**
 * Class Customers
 * @package app\modules\customers\models
 *
 * @property string $action
 * @property string $idName
 * @property \yii\db\ActiveQuery|\app\modules\customers\models\Branch $branch
 */
class Customers extends TblCustomers
{
    /**
     * @return \yii\db\ActiveQuery|Branch
     */
    public function getBranch()
    {
        return $this->hasOne(Branch::className(), ['branch' => 'Branch']);
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return Html::tag('div',
            Html::tag('a', 'Action <span class="caret"></span>', [
                'class' => 'btn btn-default dropdown-toggle', 'data-toggle' => 'dropdown', 'aria-expanded' => 'false'
            ]) . Html::tag('ul',
                Html::tag('li', '<a data-action="delate_row">Delete</a>')
                , ['class' => 'dropdown-menu']), ['class' => 'btn-group']);
    }

    /** @var string */
    public $_idName;

    /**
     * @return string
     */
    public function getIdName()
    {
        return $this->_idName;
    }

    /**
     * @param string $idName
     */
    public function setIdName($idName)
    {
        $this->_idName = $idName;
    }

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function formName()
    {
        if ($this->_idName == '') {
            return parent::formName() . '[new]';
        }
        return parent::formName() . "[$this->idName]";
    }

    /** @var  string|int */
    public $actionId;
}