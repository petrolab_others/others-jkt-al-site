<?php
/**
 * Created by
 * User: Wisard17
 * Date: 02/11/2018
 * Time: 01.52 PM
 */

namespace app\modules\customers\models;


use app\modelsDB\TblBranch;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class Branch
 * @package app\modules\customers\models
 *
 * @property \yii\db\ActiveQuery|\app\modules\customers\models\Customers[] $customer
 * @property string $listAttention
 * @property string $actions
 */
class Branch extends TblBranch
{

    public function rules()
    {
        return array_merge(parent::rules(), [
            ['branch', 'unique'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'branch_no' => 'ID',
            'branch' => 'Customer Name',
            'short_branch' => 'Short Name/Prefix',
            'address' => 'Address',
            'telp_fax' => 'Telp Fax',
            'login' => 'Login',
            'password' => 'Password',

        ];
    }

    /**
     * @return string
     */
    public function getActions()
    {
        return Html::tag('div',
            Html::tag('a', 'Action <span class="caret"></span>', [
                'class' => 'btn btn-default dropdown-toggle', 'data-toggle' => 'dropdown', 'aria-expanded' => 'false'
            ]) . Html::tag('ul',
                Html::tag('li', Html::tag('a', '<i class="fa fa-caret-square-o-right"></i> view', [
                    'href' => Url::toRoute(['/customers/default/view', 'id' => $this->branch_no])])) .
                Html::tag('li', Html::tag('a', '<i class="fa fa-edit"></i> Edit', [
                    'href' => Url::toRoute(['/customers/default/edit', 'id' => $this->branch_no])])) .
                Html::tag('li', Html::tag('a', '<i class="fa fa-times"></i> Delete',
                    ['href' => Url::toRoute(['/customers/default/del', 'id' => $this->branch_no]),
                        'data-method' => 'post',
                        'data-confirm' => 'Are you absolutely sure ? You will lose all the information about this user with this action.',
                    ]))
                , ['class' => 'dropdown-menu']), ['class' => 'btn-group']);
    }

    /**
     * @return \yii\db\ActiveQuery|Customers[]
     */
    public function getCustomer()
    {
        return $this->hasMany(Customers::className(), ['branch_no' => 'branch_no']);
    }

    public function getListAttention()
    {
        $out = '';
        $i = 1;
        foreach ($this->customer as $customers) {
            $out .= Html::tag('li', "$customers->CustomerID - " . $customers->Name);
            if ($i == 10) {
                $out .= Html::tag('li', Html::tag('a', 'See more...', [
                    'href' => Url::toRoute(['/customers/default/view', 'id' => $this->branch_no])]));
                break;
            }
            $i++;
        }
        return Html::tag('ul', $out);
    }
}