<?php
/**
 * Created by
 * User: Wisard17
 * Date: 23/03/2019
 * Time: 01.21 PM
 */

namespace app\modules\customers\models;


use app\components\ArrayHelper;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/**
 * Class FormCustomer
 * @package app\modules\customers\models
 *
 * @property \yii\db\ActiveQuery|array|\app\modules\customers\models\Customers[] $childList
 */
class FormCustomer extends Branch
{

    /**
     * @param bool $runValidation
     * @param null $attributeNames
     * @return bool
     * @throws \Exception
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            if (parent::save($runValidation, $attributeNames)) {
                foreach ($this->childList as $item) {
                    if ($item->actionId == 'del') {
//                        $item->active = 0;
                    }
                    $item->branch_no = $this->branch_no;
                    $item->Branch = $this->branch;
                    if (!$item->save()) {
                        $this->addError('customer', 'kesalahan pada child :' . ArrayHelper::toString($item->errors));
                        $transaction->rollBack();
                        return false;
                    }
                }
                $transaction->commit();
                return true;
            }
            $transaction->rollBack();
            return false;
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    /** @var array Customers[] */
    public $_childList = [];

    /**
     * @return Customers[]|array|\yii\db\ActiveQuery
     */
    public function getChildList()
    {
        if ($this->_childList == null) {
            $this->_childList = $this->customer;
            $c =Customers::find()->where(['Branch' => $this->branch])->andWhere('branch_no is null')->all();
            foreach ($c == null ? [] : $c as $value) {
                $this->_childList[] = $value;
            }
        }
        return $this->_childList;
    }

    /**
     * @param array $data
     * @param null|string $formName
     * @return bool
     */
    public function load($data, $formName = null)
    {
        $data = $data == null ? [] : $data;
        $ld = parent::load($data, $formName);
        if (isset($data['Customers'])) {
            foreach ($data['Customers'] as $idx => $datum) {
                $item = Customers::findOne(['CustomerID' => $idx]);
                if ($item == null) {
                    $item = new Customers($datum);
                }
                $item->load(['Data' => $datum], 'Data');
                $item->actionId = isset($datum['actionId']) ? $datum['actionId'] : null;
                $this->_childList[$idx] = $item;
            }
        }
        return $ld;
    }

    /**
     * @param \yii\widgets\ActiveForm $form
     * @return string
     */
    public function renderDetail($form)
    {
        $out = '<thead><tr>
                    <th class="text-center">action</th>
                    <th class="text-center" >Name</th>
                </tr></thead><tbody>';
        foreach ($this->childList as $idx => $child) {
            $child->idName = $child->CustomerID == null ? 'newrow_' . $idx : $child->CustomerID;
            $out .= Html::tag('tr',
                Html::tag('td', $child->action) .
                Html::tag('td', $form->field($child, 'Name', ['template' => '<label class="">{input}</label>{error}']))

                , ['data-id' => $child->idName]
            );
        }

        return Html::tag('table', $out . '</tbody>', ['class' => 'table']);
    }

    /**
     * @param int $rowTo
     * @return array
     */
    public static function renderRowAjax($rowTo = 0)
    {
        $form = ActiveForm::begin();
        $child = new Customers();
        $child->idName = 'newrow_' . $rowTo;

        $out = Html::tag('tr',
            Html::tag('td', $child->action) .
            Html::tag('td', $form->field($child, 'Name', ['template' => '<label class="">{input}</label>{error}']))

            , ['data-id' => $child->idName]
        );

        return [
            'data' => $out,
            'jsAdd' => ArrayHelper::toArray($form->attributes),
        ];
    }
}