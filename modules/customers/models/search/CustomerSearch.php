<?php
/**
 * Created by
 * User: Wisard17
 * Date: 02/11/2018
 * Time: 10.34 AM
 */

namespace app\modules\customers\models\search;


use app\components\ArrayHelper;
use app\modules\customers\models\Branch;
use Yii;
use app\modules\customers\models\Customers;
use yii\db\Query;
use yii\helpers\Json;

/**
 * Class CustomerSearch
 * @package app\modules\customers\models\search
 */
class CustomerSearch extends Branch
{
    /**
     * @var array
     */
    public $allField = [
        'branch_no' => 'branch_no',
        'branch' => 'branch',
        'short_branch' => 'short_branch',
        'address' => 'address',
        'telp_fax' => 'telp_fax',
        'login' => 'login',
        'entrydate' => 'entrydate',
        'updatedate' => 'updatedate',
        'loginpusat' => 'loginpusat',
        'reports_to' => 'reports_to',
        'userlevel' => 'userlevel',

        'customerName' => '',
        'listAttention' => 'branch',
    ];

    /**
     * @param $params
     * @return string
     */
    public function ordering($params)
    {
        $ncol = isset($params['order'][0]['column']) ? $params['order'][0]['column'] : 0;
        $col = (isset($params['columns'][$ncol]) && array_key_exists($params['columns'][$ncol]['data'], $this->allField)) ?
            $this->allField[$params['columns'][$ncol]['data']] : '';
        $argg = isset($params['order'][0]['dir']) ? $params['order'][0]['dir'] : 'asc';
        $table = self::tableName();

        if (isset($params['columns'][$ncol]['data']) && $params['columns'][$ncol]['data'] === 'customerName') {
            $col = 'Name';
            $table = 'tbl_customers';
        }

        return $col !== '' ? "$table.$col $argg " : '';
    }


    public $allData;

    public $currentData;

    /**
     * @param Query $query
     * @return int
     */
    public function calcAllData($query)
    {
        return $this->allData === null ? $query->count('distinct branch_no') : $this->allData;
    }

    /**
     * @param Query $query
     * @param $params
     * @return Query
     */
    public static function defaultFilterByUser($query, $params = '')
    {
        $user = Yii::$app->user->identity;

        if ($user->ruleAccess === -1) {
            return $query;
        }

        if ($user->ruleAccess === 2) {

        }

        if ($user->ruleAccess === 3) {

        }

//        $query->andWhere('publish is not null');

        return $query;
    }

    /**
     * @param $params
     * @param bool $export
     * @param bool $join
     * @return $this|\yii\db\ActiveQuery|Query
     */
    public function searchData($params, $export = false, $join = true)
    {

        $odr = $this->ordering($params);

        $query = self::find();

        if (!$join) {
            $query = self::find();
        }

        $query = self::defaultFilterByUser($query, $params);

        $query->orderBy($odr);

        $start = isset($params['start']) ? $params['start'] : 0;
        $lang = isset($params['length']) ? $params['length'] : 10;

        $this->allData = $this->calcAllData($query);

        $fltr = '';
        if (isset($params['columns']))
            foreach ($params['columns'] as $col) {
                if (isset($params['search']) && $params['search']['value'] !== '') {
                    $lst[] = $col['data'];
                    if (array_key_exists($col['data'], $this->allField) &&
                        !array_key_exists($col['data'], $lst) && $this->allField[$col['data']] != '') {
                        $fltr .= $fltr !== '' ? ' or ' : '';
                        $fltr .= ' `' . self::tableName() . '`.' . $this->allField[$col['data']] . " like '%" . $params['search']['value'] . "%' ";
                    }
                    if ($col['data'] == 'listAttention') {
                        $c = Customers::find()->where("Name like '%" . $params['search']['value'] . "%'")->distinct()->select(['Branch'])->all();
                        $fltr .= $fltr !== '' ? ' or ' : '';
                        $fltr .= ' `' . self::tableName() . "`.branch IN ('" . join("','", ArrayHelper::getColumn($c, 'Branch')) . "') ";
                    }
                }
                if ($col['searchable'] === 'true' && $col['search']['value'] !== '' &&
                    array_key_exists($col['data'], $this->allField) && $this->allField[$col['data']] != '') {
                    if ($col['data'] === 'customerName') {
                        $query->andFilterWhere(['like', '`tbl_customers`.Name', $col['search']['value']]);
                    } elseif ($col['data'] == 'listAttention') {
                        $c = Customers::find()->where("Name like '%" . $col['search']['value'] . "%'")->distinct()->select(['Branch'])->all();
                        $query->andWhere(' `' . self::tableName() . "`.branch IN ('" . join("','", ArrayHelper::getColumn($c, 'Branch')) . "')");
                    } else {
                        $query->andFilterWhere(['like', '`' . self::tableName() . '`.' . $this->allField[$col['data']], $col['search']['value']]);
                    }
                }
            }

        $query->andWhere($fltr);

        $this->load($params);

        if ($export) {
            return $query;
        }

        $this->currentData = $query->count('distinct branch_no');

        $query->limit($lang)->offset($start);
        return $query;
    }

    /**
     * @param $params
     * @return mixed
     */
    public function searchDataTable($params)
    {
        $data = $this->searchData($params);
        return Json::encode([
            'draw' => isset ($params['draw']) ? intval($params['draw']) : 0,
            'recordsTotal' => intval($this->allData),
            'recordsFiltered' => intval($this->currentData),
            'data' => $this->renderData($data, $params),
        ]);
    }

    /**
     * @param $query Query
     * @param $params
     * @return array
     */
    public function renderData($query, $params)
    {
        $out = [];

        /** @var self $model */
        foreach ($query->all() as $model) {
            $out[] = array_merge(ArrayHelper::toArray($model), [
                'actions' => $model->actions,
                'listAttention' => $model->listAttention,
            ]);

        }
        return $out;
    }
}