<?php
/**
 * Created by
 * User     : Wisard17
 * Date     : 14/03/2022
 * Time     : 11:20 AM
 * File Name: PDFReport.php
 **/

namespace app\modules\reports;

use app\modules\reports\models\search\SearchReport;

class PDFReport extends SearchReport
{
    public function attributeUnits()
    {
        $ftirUgmt = 'Abs/01mm';
        if ($this->branch == 'PT. GLOBAL MAKARA TEKNIK') {
            $ftirUgmt = 'Abs/cm';
        }
        return array_merge(parent::attributeUnits(), [
            'DIR_TRANS' => $ftirUgmt,
            'OXIDATION' => $ftirUgmt,
            'NITRATION' => $ftirUgmt,
            'SOX' => $ftirUgmt,
        ]);
    }

    public function getLimit($type, $attribute)
    {
        if (parent::__get('branch') == 'PT. GLOBAL MAKARA TEKNIK') {
            $p = ['DIR_TRANS', 'OXIDATION' , 'NITRATION' , 'SOX'];
            if (in_array($attribute, $p, true)) {
                return parent::getLimit($type, $attribute) * 100;
            }
        }

        return parent::getLimit($type, $attribute);
    }

    public function __get($name)
    {
        $catP =$this->parameterByCategory();

        if (parent::__get('branch') == 'PT. GLOBAL MAKARA TEKNIK') {
            $p = ['DIR_TRANS', 'OXIDATION' , 'NITRATION' , 'SOX'];
            if (in_array($name, $p, true)) {
                if (isset($catP['FTIR'][$name]) && (string)parent::__get($name) == '0') {
                    return '< 1';
                }
                return (float)parent::__get($name) * 100;
            }
        }

        if ($this->getMethod($name) == 'ASTM D5185-13e1') {
            $value = parent::__get($name);
            return (string)$value === '0' ? '< 1' : $value;
        }

        if (isset($catP['FTIR'][$name]) && (string)parent::__get($name) === '0') {
            return '< 0.02';
        }

        return parent::__get($name);
    }
}