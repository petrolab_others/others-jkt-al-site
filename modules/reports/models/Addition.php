<?php
/**
 * Created
 * User: Wisard17
 * Date: 07/09/2018
 * Time: 05.05 PM
 */

namespace app\modules\reports\models;


use Yii;
use app\modelsDB\TblAddition;
use yii\helpers\Html;

/**
 * Class Addition
 * @package app\modules\reports\models
 *
 * @property string $urlFile
 * @property string $pathFile
 * @property string $dirBerkas
 * @property null|string $viewFile
 * @property \app\modules\reports\models\Parameter|\yii\db\ActiveQuery $parameter
 */
class Addition extends TblAddition
{

    /**
     * @return \yii\db\ActiveQuery|Parameter
     */
    public function getParameter()
    {
        return $this->hasOne(Parameter::className(), ['id' => 'tbl_parameter_id']);
    }

    public function getDirBerkas()
    {
        $dir = Yii::getAlias('@uploaddir');
        if (!file_exists($dir . '/report') && !mkdir($concurrentDirectory = $dir . '/report', 0777, true) && !is_dir($concurrentDirectory)) {
            throw new \RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
        }
        if ($this->tbl_parameter_id == 19) {
            $dirFile = $dir . '/report/' . 'mpc';
        } else {
            $dirFile = $dir . '/report/' . ($this->tbl_parameter_id == 23 ? 'patch_test' : 'no_cat');
        }
        if (!file_exists($dirFile) && !mkdir($dirFile, 0777, true) && !is_dir($dirFile)) {
            throw new \RuntimeException(sprintf('Directory "%s" was not created', $dirFile));
        }

        return $dirFile;
    }

    /**
     * @return string
     */
    public function getPathFile()
    {
        return $this->dirBerkas . '/' . $this->value;
    }

    /**
     * @return string
     */
    public function getUrlFile()
    {
        Yii::$app->assetManager->publish($this->dirBerkas);
        return Yii::$app->assetManager->getPublishedUrl($this->dirBerkas) . '/' . $this->value;
    }

    /**
     * @return null|string
     */
    public function getViewFile()
    {
        if ($this->urlFile == null)
            return null;

        return Html::a('Lihat File', $this->urlFile, ['class' => 'btn btn-default ', 'target' => '_blank']);
    }
}