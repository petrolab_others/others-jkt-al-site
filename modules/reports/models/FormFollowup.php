<?php
/**
 * Created by
 * User: Wisard17
 * Date: 06/08/2018
 * Time: 05.11 PM
 */

namespace app\modules\reports\models;


use app\components\ArrayHelper;

class FormFollowup extends Report
{

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['Notes'], 'string'],
        ];
    }

    /**
     * @param bool $runValidation
     * @param null $attributeNames
     * @return bool
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        $this->Notes = $this->Notes === '' ? null : $this->Notes;
        if (parent::save($runValidation = true, $attributeNames = null)) {
            $this->status = 'success';
            $this->message = 'Berhasil Disimpan';
            return true;
        }
        $this->status = 'error';
        $this->message = 'Data Tidak disimpan, Coba lagi.<br>' . ArrayHelper::toString($this->errors);
        return false;
    }

    public $message = '';

    public $status = '';

}