<?php
/**
 * Created by
 * User: Wisard17
 * Date: 02/08/2018
 * Time: 05.36 PM
 */

namespace app\modules\reports\models;


use app\components\ArrayHelper;
use app\modules\master\models\Component;
use app\modules\master\models\Matrix;
use app\modules\master\models\OilMatrix;
use app\modules\master\models\OthersOrder;
use app\modules\master\models\Unit;
use Yii;
use app\modelsDB\TblTransaction;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class Report
 * @package app\modules\reports\models
 *
 * @property string $labNumber
 * @property string $unitNumber
 * @property string $receiveDate
 * @property string $customerName
 * @property string $serialNo
 * @property string $followup
 * @property string $component
 * @property string $oilChg
 * @property string $reportDate
 * @property string $sampleDate
 * @property string $model
 * @property string $actions
 * @property string $status
 * @property \yii\db\ActiveQuery|\app\modules\reports\models\Customer $customer
 * @property string $group
 *
 * addition
 * @property \yii\db\ActiveQuery|\app\modules\master\models\Unit $unit
 * @property \app\modules\master\models\Component|\yii\db\ActiveQuery $component1
 * @property string $branch2
 * @property string $unitSn
 * @property \app\modules\master\models\OthersOrder[]|\yii\db\ActiveQuery $others
 * @property string $otherForm
 * @property \app\modules\reports\models\Report[] $history
 * @property int $numHistory
 * @property int $maxPage
 * @property string $brand2
 * @property array $otherHis
 * @property int $titanium
 * @property  string $particle4
 * @property  string $particle6
 * @property  string $particle14
 * @property  string $iso4406
 * @property  string $particle5
 * @property  string $particle15
 * @property  string $particle25
 * @property  string $particle50
 * @property  string $particle100
 * @property  string $nasclass1638
 * @property  string $mpc
 * @property  string $patch_test
 * @property  string $v_sample
 * @property array $footNote
 * @property string $menegerTtd
 * @property string|mixed $addressLogo
 * @property string $oilHourL
 * @property string $unitHourL
 * @property \app\modules\reports\models\Parameter[] $paramAll
 * @property mixed $oilHour
 * @property mixed $unitHour
 * @property mixed $rowId
 * @property \yii\db\ActiveQuery|\app\modules\master\models\OilMatrix $wearOilMatrix
 * @property \yii\db\ActiveQuery|\app\modules\master\models\Matrix $wearMatrix
 * @property  string $magnification
 *
 */
class Report extends TblTransaction
{

    protected $additionAttribute = [];

    public function attributes()
    {
        if ($this->additionAttribute == []) {
            foreach (array_keys($this->_additionParameter) as $key) {
                $this->additionAttribute[] = $key;
                $this->additionAttribute[] = $key . '_code';
            }
        }
        return array_merge(parent::attributes(), $this->additionAttribute);
    }

    private $_additionParameter = [
        'titanium' => [
            'parameter_id' => 7,
            'value' => null,
            'value_code' => null,
            'model' => null,
        ],
        'particle4' => [
            'parameter_id' => 8,
            'value' => null,
            'value_code' => null,
            'model' => null,
        ],
        'particle6' => [
            'parameter_id' => 9,
            'value' => null,
            'value_code' => null,
            'model' => null,
        ],
        'particle14' => [
            'parameter_id' => 10,
            'value' => null,
            'value_code' => null,
            'model' => null,
        ],
        'iso4406' => [
            'parameter_id' => 11,
            'value' => null,
            'value_code' => null,
            'model' => null,
        ],
        'particle5' => [
            'parameter_id' => 12,
            'value' => null,
            'value_code' => null,
            'model' => null,
        ],
        'particle15' => [
            'parameter_id' => 13,
            'value' => null,
            'value_code' => null,
            'model' => null,
        ],
        'particle25' => [
            'parameter_id' => 14,
            'value' => null,
            'value_code' => null,
            'model' => null,
        ],
        'particle50' => [
            'parameter_id' => 15,
            'value' => null,
            'value_code' => null,
            'model' => null,
        ],
        'particle100' => [
            'parameter_id' => 16,
            'value' => null,
            'value_code' => null,
            'model' => null,
        ],
        'nasclass1638' => [
            'parameter_id' => 17,
            'value' => null,
            'value_code' => null,
            'model' => null,
        ],
        'mpc' => [
            'parameter_id' => 18,
            'value' => null,
            'value_code' => null,
            'model' => null,
        ],
        'patch_test' => [
            'parameter_id' => 20,
            'value' => null,
            'value_code' => null,
            'model' => null,
        ],
        'v_sample' => [
            'parameter_id' => 21,
            'value' => null,
            'value_code' => null,
            'model' => null,
        ],
        'magnification' => [
            'parameter_id' => 22,
            'value' => null,
            'value_code' => null,
            'model' => null,
        ],
        'famedu' => [
            'parameter_id' => 92,
            'value' => null,
            'value_code' => null,
            'model' => null,
        ],

    ];

    public $draft;

    public function getMenegerTtd()
    {
        $site = strlen($this->Lab_No) > 2 && isset($this->Lab_No[1]) ? $this->Lab_No[1] : '';
        $listSite = [
            'J' => 'Endriastuti',
            'B' => 'Reko Sayogo',
        ];
        return isset($listSite[$site]) ? $listSite[$site] : '';
    }

    public function getAddressLogo()
    {
        $site = strlen($this->Lab_No) > 2 && isset($this->Lab_No[1]) ? $this->Lab_No[1] : '';
        $listSite = [
            'J' => 'Pisangan Lama III no.28 Pisangan Timur - Jakarta Timur 13230<br>
                    Telp +62 21 2968 8694; Fax +62 21 2968 8693; petrolab@cbn.net.id;www.petrolab.co.id',
            'B' => 'Jl. Jend Sudirman No. 874 Komp. UT Stalkuda Balikpapan 76114 <br>
                    Telp 0542-762873 fax. 0542-765019; petrolab@cbn.net.id; www.petrolab.co.id',
        ];
        return isset($listSite[$site]) ? $listSite[$site] : '';
    }

    public function rules()
    {
        return array_merge(parent::rules(), [
            [['Lab_No'], 'unique'],
            [['titanium', 'particle4', 'particle6', 'particle14', 'particle5', 'particle15', 'particle25',
                'particle50', 'particle100', 'nasclass1638',], 'number'],
            [['iso4406', 'patch_test', 'v_sample', 'magnification', 'famedu', 'famedu_code'], 'string'],
            [['mpc'], 'string']
        ]);
    }

    /** @var OthersOrder[] */
    public $_others = [];

    /**
     * @return OthersOrder[]|\yii\db\ActiveQuery
     */
    public function getOthers()
    {
        if ($this->_others == null)
            $this->_others = $this->hasMany(OthersOrder::className(), ['Lab_No' => 'Lab_No'])->orderBy('OtherOrderId');
        return $this->_others;
    }

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function getOtherForm()
    {
        return (new OthersOrder())->formName();
    }

    /**
     * @param bool $runValidation
     * @param null $attributeNames
     * @return bool
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        $this->SODIUM = $this->SODIUM_others;
        $this->SILICON = $this->SILICON_others;
        $this->IRON = $this->IRON_others;
        $this->COPPER = $this->COPPER_others;
        $this->ALUMINIUM = $this->ALUMINIUM_others;
        $this->CHROMIUM = $this->CHROMIUM_others;
        $this->NICKEL = $this->NICKEL_others;
        $this->TIN = $this->TIN_others;
        $this->LEAD = $this->LEAD_others;
        if (parent::save($runValidation, $attributeNames)) {
            $result = true;
            foreach ($this->_additionParameter as $name => $value) {
                $par = ['Lab_No' => $this->Lab_No, 'tbl_parameter_id' => $value['parameter_id']];
                $t = Addition::findOne($par);
                if ($t == null) {
                    $t = new Addition($par);
                }
                $t->value = $value['value'];
                $t->value_CODE = $value['value_code'];
                if (!$t->save()) {
                    $this->addError($name, $t->parameter->unsur . ' not save');
                    $result = false;
                }
            }
            return $result;
        }
        return false;
    }

    /**
     * @return array
     */
    public function attributeUnits()
    {
        $ftirUgmt = 'Abs/01mm';
        if ($this->branch == 'PT. GLOBAL MAKARA TEKNIK') {
            $ftirUgmt = 'Abs/cm';
        }

        return [
            'visc_40' => 'cSt',
            'VISC_CST' => 'cSt',
            'visc_index' => '-',
            'T_A_N' => 'mg KOH/g',
            'T_B_N' => 'mg KOH/g',

            'MAGNESIUM' => 'ppm',
            'CALCIUM' => 'ppm',
            'ZINC' => 'ppm',
            'Molybdenum' => 'ppm',
            'Boron' => 'ppm',
            'phosphor' => 'ppm',
            'Barium'  => 'ppm',

            'SODIUM_others' => 'ppm',
            'SILICON_others' => 'ppm',
            'DILUTION' => '%v',
            'famedu' => '%v',
            'WATER' => '%v',
            'GLYCOL' => '%v',

            'IRON_others' => 'ppm',
            'COPPER_others' => 'ppm',
            'ALUMINIUM_others' => 'ppm',
            'CHROMIUM_others' => 'ppm',
            'NICKEL_others' => 'ppm',
            'TIN_others' => 'ppm',
            'LEAD_others' => 'ppm',
            'PQIndex' => '-',

            'DIR_TRANS' => $ftirUgmt,
            'OXIDATION' => $ftirUgmt,
            'NITRATION' => $ftirUgmt,
            'SOX' => $ftirUgmt,
        ];
    }

    public function getMethod($name)
    {
        $p = [
            'MAGNESIUM' => 'ASTM D5185-13e1',
            'CALCIUM' => 'ASTM D5185-13e1',
            'ZINC' => 'ASTM D5185-13e1',
            'Molybdenum' => 'ASTM D5185-13e1',
            'Boron' => 'ASTM D5185-13e1',
            'phosphor' => 'ASTM D5185-13e1',

            'SODIUM_others' => 'ASTM D5185-13e1',
            'SILICON_others' => 'ASTM D5185-13e1',
            'DILUTION' => 'ASTM E2412-10',
            'WATER' => 'ASTM E2412-10',
            'GLYCOL' => 'ASTM E2412-10',

            'IRON_others' => 'ASTM D5185-13e1',
            'COPPER_others' => 'ASTM D5185-13e1',
            'ALUMINIUM_others' => 'ASTM D5185-13e1',
            'CHROMIUM_others' => 'ASTM D5185-13e1',
            'NICKEL_others' => 'ASTM D5185-13e1',
            'TIN_others' => 'ASTM D5185-13e1',
            'LEAD_others' => 'ASTM D5185-13e1',
        ];
        return isset($p[$name]) ? $p[$name] : '';
    }

    public function attributeMethods()
    {
        $v = strlen($this->Lab_No) > 2 && isset($this->Lab_No[1]) && $this->Lab_No[1] == 'B' ? 'ASTM D7279' : 'ASTM D445-21e2';
        return [
            'visc_40' => $v,
            'VISC_CST' => $v,
            'visc_index' => 'ASTM D2270-10 (Reapproved 2016)',
            'T_A_N' => 'ASTMD 974-22',
            'T_B_N' => 'ASTM D2896-21',

            'MAGNESIUM' => 'ASTM D5185-18',
            'CALCIUM' => 'ASTM D5185-18',
            'ZINC' => 'ASTM D5185-18',
            'Molybdenum' => 'ASTM D5185-18',
            'Boron' => 'ASTM D5185-18',
            'phosphor' => 'ASTM D5185-18',
            'Barium'  => 'ASTM D5185-18',

            'SODIUM_others' => 'ASTM D5185-18',
            'SILICON_others' => 'ASTM D5185-18',
            'DILUTION' => 'ASTM E2412-10',
            'famedu' => 'In House Method',
            'WATER' => 'ASTM E2412-10',
            'GLYCOL' => 'ASTM E2412-10',

            'IRON_others' => 'ASTM D5185-18',
            'COPPER_others' => 'ASTM D5185-18',
            'ALUMINIUM_others' => 'ASTM D5185-18',
            'CHROMIUM_others' => 'ASTM D5185-18',
            'NICKEL_others' => 'ASTM D5185-18',
            'TIN_others' => 'ASTM D5185-18',
            'LEAD_others' => 'ASTM D5185-18',
            'PQIndex' => 'ASTM D8184-18e1',

            'DIR_TRANS' => 'ASTM E2412-10 (R18)',
            'OXIDATION' => 'ASTM E2412-10 (R18)',
            'NITRATION' => 'ASTM E2412-10 (R18)',
            'SOX' => 'ASTM E2412-10 (R18)',
        ];
    }

    /** @var OilMatrix */
    public $oil_limit;

    /** @var Matrix */
    public $wear_limit;

    /** @var Parameter */
    public $model_param;

    /**
     * @param $type
     * @param $attribute
     * @return string
     */
    public function getLimit($type, $attribute)
    {
        if ($this->oil_limit == null) {
            $this->oil_limit = $this->getWearOilMatrix()->one();
        }

        if ($this->wear_limit == null) {
            $this->wear_limit = $this->getWearMatrix()->one();
        }

        if ($this->oil_limit == null) {
            $this->oil_limit = new OilMatrix();
        }

        if ($this->wear_limit == null) {
            $this->wear_limit = new Matrix();
        }

        if (isset($this->_additionParameter[$attribute])) {
            $this->model_param[$attribute] = Parameter::findOne(['id' => $this->_additionParameter[$attribute]['parameter_id']]);
        }

        $c = isset($this->attributeColumnLimit()[$attribute]) ? $this->attributeColumnLimit()[$attribute] : '';

        if ($c != '' || (isset($this->model_param[$attribute]) && $this->model_param[$attribute] != null && $this->model_param[$attribute]->col_matrix != '')) {
            if ($type == 'attention') {
                $cn = $c . 'B';
                switch ($attribute) {
                    case 'visc_40' :
                        return $this->oil_limit->V40minB . ' | ' . $this->oil_limit->V40maxB;
                        break;
                    case 'VISC_CST' :
                        return $this->oil_limit->V100minB . ' | ' . $this->oil_limit->V100maxB;
                        break;
                    case 'T_A_N' :
                    case 'T_B_N' :
                        if ($attribute == 'T_B_N' && $this->oil_limit->hasAttribute($cn) && $this->oil_limit->OIL_MATRIX == 'SHELL ALEXIA 70 (CYLINDER CMM)'){
                            $pisah = explode('.',$this->oil_limit->TBNB);
                            return $pisah[0] . ' | ' .  $pisah[1];
                        }elseif ($this->oil_limit->hasAttribute($cn)) {
                            return $this->oil_limit->$cn;
                        }
                        break;
                    case 'titanium' :
                    case 'particle4' :
                    case 'particle6' :
                    case 'particle14' :
                    case 'iso4406' :
                    case 'particle5' :
                    case 'particle15' :
                    case 'particle25' :
                    case 'particle50' :
                    case 'particle100' :
                    case 'nasclass1638' :
                    case 'mpc' :
                    case 'patch_test' :
                    case 'v_sample' :
                    case 'magnification' :
                        $col = $this->model_param[$attribute]->col_matrix . 'B';
                        if ($this->wear_limit->hasAttribute($col)) {
                            return $this->wear_limit->$col;
                        }
                        break;
                    default :
                        if ($this->wear_limit->hasAttribute($cn)) {
                            return $this->wear_limit->$cn;
                        }
                }
            }

            if ($type == 'urgent') {
                $cn = $c . 'C';
                switch ($attribute) {
                    case 'visc_40' :
                        return $this->oil_limit->V40minC . ' | ' . $this->oil_limit->V40maxC;
                        break;
                    case 'VISC_CST' :
                        return $this->oil_limit->V100minC . ' | ' . $this->oil_limit->V100maxC;
                        break;
                    case 'T_A_N' :
                    case 'T_B_N' :
                        if ($this->oil_limit->hasAttribute($cn)) {
                            return $this->oil_limit->$cn;
                        }
                        break;
                    case 'titanium':
                    case 'particle4' :
                    case 'particle6' :
                    case 'particle14' :
                    case 'iso4406' :
                    case 'particle5' :
                    case 'particle15' :
                    case 'particle25' :
                    case 'particle50' :
                    case 'particle100' :
                    case 'nasclass1638' :
                    case 'mpc' :
                    case 'patch_test' :
                    case 'v_sample' :
                    case 'magnification' :
                        $col = $this->model_param[$attribute]->col_matrix . 'C';
                        if ($this->wear_limit->hasAttribute($col)) {
                            return $this->wear_limit->$col;
                        }
                        break;
                    default :
                        if ($this->wear_limit->hasAttribute($cn)) {
                            return $this->wear_limit->$cn;
                        }
                }
            }
            if ($type == 'severe') {
                $cn = $c . 'D';
                switch ($attribute) {
                    case 'visc_40' :
                        return $this->oil_limit->V40minD . ' | ' . $this->oil_limit->V40maxD;
                        break;
                    case 'VISC_CST' :
                        return $this->oil_limit->V100minD . ' | ' . $this->oil_limit->V100maxD;
                        break;
                    case 'T_A_N' :
                    case 'T_B_N' :
                        if ($this->oil_limit->hasAttribute($cn)) {
                            return $this->oil_limit->$cn;
                        }
                        break;
                    case 'titanium':
                    case 'particle4' :
                    case 'particle6' :
                    case 'particle14' :
                    case 'iso4406' :
                    case 'particle5' :
                    case 'particle15' :
                    case 'particle25' :
                    case 'particle50' :
                    case 'particle100' :
                    case 'nasclass1638' :
                    case 'mpc' :
                    case 'patch_test' :
                    case 'v_sample' :
                    case 'magnification' :
                        $col = $this->model_param[$attribute]->col_matrix . 'D';
                        if ($this->wear_limit->hasAttribute($col)) {
                            return $this->wear_limit->$col;
                        }
                        break;
                    default :
                        if ($this->wear_limit->hasAttribute($cn)) {
                            return $this->wear_limit->$cn;
                        }
                }
            }
        }

        return '';
    }

    public function attributeColumnLimit()
    {
        return [
            'visc_40' => 'V40',
            'VISC_CST' => 'V100',
            'visc_index' => '',
            'T_A_N' => 'TAN',
            'T_B_N' => 'TBN',

            'MAGNESIUM' => '',
            'CALCIUM' => '',
            'ZINC' => '',
            'Molybdenum' => 'Mo',
            'Boron' => '',
            'phosphor' => '',

            'SODIUM_others' => 'Na',
            'SILICON_others' => 'Si',
            'DILUTION' => 'Fuel',
            'WATER' => 'Water',
            'GLYCOL' => 'Glycol',

            'IRON_others' => 'Fe',
            'COPPER_others' => 'Cu',
            'ALUMINIUM_others' => 'Al',
            'CHROMIUM_others' => 'Cr',
            'NICKEL_others' => 'Ni',
            'TIN_others' => 'Sn',
            'LEAD_others' => 'Pb',
            'PQIndex' => 'PQ',

            'DIR_TRANS' => 'Soot',
            'OXIDATION' => 'Oxi',
            'NITRATION' => 'Nit',
            'SOX' => 'Sul',
        ];
    }

    public function getAttributeUnit($attribute)
    {
        $units = $this->attributeUnits();
        if (isset($units[$attribute])) {
            return $units[$attribute];
        }
        return $this->generateAttributeLabel($attribute . '_unit');
    }

    public function getAttributeMethod($attribute)
    {
        $methods = $this->attributeMethods();
        if (isset($methods[$attribute])) {
            return $methods[$attribute];
        }
        return $this->generateAttributeLabel($attribute . '_method');
    }

    public function getAttributeLabel($attribute)
    {
        $footNote = '';
        foreach ($this->getFootNote() as $key => $items) {
            if (in_array($attribute, $items, true)) {
                $footNote = $key;
            }
        }
        return parent::getAttributeLabel($attribute) . " $footNote";
    }

    /** @var Parameter[] */
    public $_param_all;

    /**
     * @return Parameter[]
     */
    public function getParamAll()
    {
        if ($this->_param_all == null) {
            $this->_param_all = Parameter::find()->indexBy('id')->all();
        }
        return $this->_param_all;
    }

    public function attributeLabels()
    {
        $param = $this->paramAll;
        $addMrg = [];
        foreach ($this->_additionParameter as $name => $value) {
            $addMrg[$name] = isset($param[$value['parameter_id']]) ? $param[$value['parameter_id']]->unsur : $name;
        }
        $o_label = strtolower($this->oilHourL) == 'km' ? 'Km on Oil' : 'Hours on Oil';
        $u_label = strtolower($this->unitHourL) == 'km' ? 'Km on Unit' : 'Hours on Unit';
        return array_merge(parent::attributeLabels(), [
            'SODIUM_others' => 'Natrium (Na)',
            'SILICON_others' => 'Silicon (Si) ',
            'DIR_TRANS' => 'Soot',
            'branch' => 'Customer Name',
            'name' => 'For Customer',
            'customer_id' => 'For Customer',
//            'address' => 'Address',
            'MODEL' => 'Unit Model',
            'UNIT_NO' => 'Unit Number',
//            'COMPONENT' => '',
            'MATRIX' => 'Component Matrix',
//            'OIL_MATRIX' => '',
            'OIL_TYPE' => 'Lube Oil Name',
            'UNIT_LOC' => 'Location',
            'Lab_No' => 'Lab. Number',
            'HRS_KM_TOT' => $u_label,
            'HRS_KM_OC' => $o_label,
            'SAMPL_DT1' => 'Sampling Date',
            'RECV_DT1' => 'Received Date',
            'RPT_DT1' => 'Report Date',
            'visc_40' => 'Kin Visc at 40<sup>o</sup>C',
            'VISC_CST' => 'Kin Visc at 100<sup>o</sup>C',
            'visc_index' => 'Visccosity Index',
            'MAGNESIUM' => 'Magnesium (Mg)',
            'CALCIUM' => 'Calcium (Ca)',
            'ZINC' => 'Zinc (Zn)',
            'Molybdenum' => 'Molybdenum (Mo)',
            'Boron' => 'Boron (B)',
            'phosphor' => 'Phosphor (P)',
            'Barium' => 'Barium (Ba)',
            'IRON_others' => 'Iron (Fe)',
            'COPPER_others' => 'Copper (Cu)',
            'ALUMINIUM_others' => 'Alumunium (Al)',
            'CHROMIUM_others' => 'Chromium (Cr)',
            'NICKEL_others' => 'Nickel (Ni)',
            'TIN_others' => 'Tin (Sn)',
            'LEAD_others' => 'Lead (Pb)',
            'SILVER' => 'Silver (Ag)',
            'PQIndex' => 'PQ Index',
            'DILUTION' => 'Fuel Dilution',
            'WATER' => 'Water Content',
            'SOX' => 'Sulfation',
            'ADD_PHYS' => 'Report Number',
            'ADD_VALUE' => 'SPB Number',

            'customerName' => 'For Customer',
            'brand2' => 'Unit Brand',
            'unitHour' => $u_label,
            'oilHour' => $o_label,
        ], $addMrg);
    }


    /**
     * @return array
     */
    public function parameterByCategory2()
    {
        return [
            'Physical Test' => [
                'visc_40' => 'visc_40_code',
                'VISC_CST' => 'CST_CODE',
                'visc_index' => 'visc_index_code',
                'T_B_N' => 'TBN_CODE',
                'T_A_N' => 'TAN_CODE',
            ],
            'Metal Additive' => [
                'MAGNESIUM' => 'MG_CODE',
                'CALCIUM' => 'CA_CODE',
                'ZINC' => 'ZN_CODE',
                'Molybdenum' => 'Molybdenum_CODE',
                'Boron' => 'Boron_CODE',
                'phosphor' => 'phosphor_code',
                'Barium' => 'Barium_CODE'
            ],
            'Contaminant' => [
                'SODIUM_others' => 'NA_CODE',
                'SILICON_others' => 'SI_CODE',
                'DILUTION' => 'DILUT_CODE',
                'famedu' => 'famedu_code',
                'WATER' => 'WTR_CODE',
                'GLYCOL' => 'GLY_CODE',
            ],
            'Wear Metal' => [
                'IRON_others' => 'FE_CODE',
                'COPPER_others' => 'CU_CODE',
                'ALUMINIUM_others' => 'AL_CODE',
                'CHROMIUM_others' => 'CR_CODE',
                'NICKEL_others' => 'NI_CODE',
                'TIN_others' => 'SN_CODE',
                'LEAD_others' => 'PB_CODE',
                'PQIndex' => 'PQIndex_CODE',
                'SILVER' => 'AG_CODE',
                'titanium' => 'titanium_code',
            ],
            'FTIR' => [
                'DIR_TRANS' => 'TRANS_CODE',
                'OXIDATION' => 'OXID_CODE',
                'NITRATION' => 'NITR_CODE',
                'SOX' => 'SOX_CODE',
            ],
            'Contamination 1' => [
                'particle4' => 'particle4_code',
                'particle6' => 'particle6_code',
                'particle14' => 'particle14_code',
                'iso4406' => 'iso4406_code',
            ],
            'Contamination 2' => [
                'particle5' => 'particle5_code',
                'particle15' => 'particle15_code',
                'particle25' => 'particle25_code',
                'particle50' => 'particle50_code',
                'particle100' => 'particle100_code',
                'nasclass1638' => 'nasclass1638_code',
            ],
            'MPC' => [
                'mpc' => 'mpc_code',
                'uploadMpc' => 'file',
            ],
            'Patch Test' => [
                'patch_test' => 'patch_test_code',
                'v_sample' => 'v_sample_code',
                'magnification' => 'magnification_code',
                'uploadPatch' => 'file',
            ],
        ];
    }

    /**
     * @return array
     *
     *
     */
    public function parameterByCategory()
    {
        return [
            'Physical Test' => [
                'visc_40' => 'visc_40_code',
                'VISC_CST' => 'CST_CODE',
                'visc_index' => 'visc_index_code',
                'T_B_N' => 'TBN_CODE',
                'T_A_N' => 'TAN_CODE',
            ],
            'Metal Additive' => [
                'MAGNESIUM' => 'MG_CODE',
                'CALCIUM' => 'CA_CODE',
                'ZINC' => 'ZN_CODE',
                'Molybdenum' => 'Molybdenum_CODE',
                'Boron' => 'Boron_CODE',
                'phosphor' => 'phosphor_code',
                'Barium' => 'Barium_CODE'
            ],
            'Contaminant' => [
                'SODIUM_others' => 'NA_CODE',
                'SILICON_others' => 'SI_CODE',
                'DILUTION' => 'DILUT_CODE',
                'famedu' => 'famedu_code',
                'WATER' => 'WTR_CODE',
                'GLYCOL' => 'GLY_CODE',
            ],
            'Wear Metal' => [
                'IRON_others' => 'FE_CODE',
                'COPPER_others' => 'CU_CODE',
                'ALUMINIUM_others' => 'AL_CODE',
                'CHROMIUM_others' => 'CR_CODE',
                'NICKEL_others' => 'NI_CODE',
                'TIN_others' => 'SN_CODE',
                'LEAD_others' => 'PB_CODE',
                'PQIndex' => 'PQIndex_CODE',
            ],
            'FTIR' => [
                'DIR_TRANS' => 'TRANS_CODE',
                'OXIDATION' => 'OXID_CODE',
                'NITRATION' => 'NITR_CODE',
                'SOX' => 'SOX_CODE',
            ],
        ];
    }

    public $_category_state;

    /**
     * @param $category
     * @return array|null
     */
    public function checkCategoryShow($category)
    {
        if (!isset($this->_category_state[$category])) {
            foreach ($this->parameterByCategory()[$category] as $parameter => $code) {
                foreach ($this->history as $report) {
                    if ($report->hasAttribute($parameter)) {
                        if ($report->$parameter !== null) {
                            $this->_category_state[$category][] = $parameter;
                        }
                    }
                }
            }
        }
        return isset($this->_category_state[$category]) ? $this->_category_state[$category] : null;
    }

    /**
     * @return \yii\db\ActiveQuery|Customer
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['CustomerID' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery|Unit
     */
    public function getUnit()
    {
        return $this->hasOne(Unit::className(), ['UnitID' => 'unit_id']);
    }

    /**
     * @return \yii\db\ActiveQuery|Component
     */
    public function getComponent1()
    {
        return $this->hasOne(Component::className(), ['ComponentID' => 'ComponentID']);
    }

    /**
     * @return \yii\db\ActiveQuery|Matrix
     */
    public function getWearMatrix()
    {
        return $this->hasOne(Matrix::className(), ['Matrix' => 'MATRIX']);
    }

    /**
     * @return \yii\db\ActiveQuery|OilMatrix
     */
    public function getWearOilMatrix()
    {
        return $this->hasOne(OilMatrix::className(), ['OIL_MATRIX' => 'OIL_MATRIX']);
    }

    /**
     * @return string
     */
    public function getActions()
    {
        return
            Html::tag('a', '<i class="fa fa-external-link"></i> View', ['class' => 'btn btn-default btn-xs',
                'title' => 'View Report', 'href' => Url::to(['/reports/default/view', 'id' => $this->Lab_No]),]) .
            Html::tag('a', '<i class="fa fa-bar-chart-o"></i> Chart', ['class' => 'btn btn-default btn-xs',
                'title' => 'View Chart', 'href' => Url::to(['/reports/default/chart', 'labNo' => $this->Lab_No]), 'target' => '_blank']) .
            Html::tag('a', '<i class="fa fa-file-pdf-o"></i> PDF', ['class' => 'btn btn-default btn-xs',
                'title' => 'Download Report', 'href' => Url::to(['/reports/default/show-pdf', 'labNumber' => $this->Lab_No]), 'target' => '_blank'])
//           . '<a class="btn btn-default btn-xs" title="View History" href='.Url::to(["/master/transaction/view/$this->Lab_No","page"=>1]).' style=""><i class="fa fa-eye"></i></a>'
            // Html::tag('a','<i class="fa fa-search"></i>', ['class'=>'btn btn-primary btn-xs','title'=>'View Detail']).' '
            // Html::tag('a','<i class="fa fa-share"></i>', ['class'=>'btn btn-primary btn-xs','title'=>'Follow Up','onclick'=>"openFollow('$this->Lab_No')"])
            ;
    }

    /**
     * @return string
     */
    public function getFollowup()
    {
        $button = "<i class='fa fa-square-o'></i>";
        if ($this->Notes != '') {
            $button = "<i class='fa fa-check-square-o'></i>";
        }
        return "<a class='btn btn-default btn-xs' title='Follow Up' data-action='update_followup' >$button</a>";
    }

    public function getRowId()
    {
        return str_replace('/', '_', $this->Lab_No);
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        $code = $this->EVAL_CODE;
        $status = $this->getTextEvalCode($code);
        $label = '';
        if (($code == 'C') || ($code == 'U') || $code == 'D') {
            $label = 'label-danger';
        }
        if ($code == 'B' || $code == 'A') {
            $label = 'label-warning';
        }
        if ($code == 'N') {
            $label = 'label-success';
        }
        return $this->EVAL_CODE == '' ? '' : Html::tag('span', $status, ['class' => 'label ' . $label]);
    }

    public function getTextEvalCode($code)
    {
        $code = strtoupper($code);
        $eval = 'NORMAL';
        if ($code == 'D') {
            $eval = 'SEVERE';
        }
        if (($code == 'C') || ($code == 'U')) {
            $eval = 'URGENT';
        }
        if ($code == 'B' || $code == 'A') {
            $eval = 'ATTENTION';
        }
        if ($code == 'N') {
            $eval = 'NORMAL';
        }
        return $eval;
    }

    /**
     * @return string
     */
    public function getGroup()
    {
        return $this->grouploc;
    }

    /**
     * @return string
     */
    public function getCustomerName()
    {
        return $this->customer === null ? '' : $this->customer->Name;
    }

    /**
     * @return string
     */
    public function getLabNumber()
    {
        return $this->Lab_No;
    }

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function getSampleDate()
    {
        return Yii::$app->formatter->asDate($this->SAMPL_DT1);
    }

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function getReceiveDate()
    {
        return Yii::$app->formatter->asDate($this->RECV_DT1);
    }

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function getReportDate()
    {
        return Yii::$app->formatter->asDate($this->RPT_DT1);
    }

    /**
     * @return string
     */
    public function getUnitNumber()
    {
        return $this->UNIT_NO;
    }

    /**
     * @return string
     */
    public function getComponent()
    {
        return $this->COMPONENT;
    }

    /**
     * @return string
     */
    public function getModel()
    {
        return $this->MODEL;
    }

    /**
     * @return string
     */
    public function getSerialNo()
    {
        return $this->unitSn;
    }

    public $_unit_sn;

    /**
     * @return string
     */
    public function getUnitSn()
    {
        if ($this->_unit_sn == null) {
//            $this->_unit_sn = $this->ADD_MET;
            if ($this->_unit_sn == null) {
                $this->_unit_sn = $this->unit == null ? '' : $this->unit->SerialNo;
            }
        }
        return $this->_unit_sn;
    }

    /**
     * @param string $unit_sn
     */
    public function setUnitSn($unit_sn)
    {
        $this->ADD_MET = $unit_sn;
        $this->_unit_sn = $unit_sn;
    }

    public $_brand;

    /**
     * @return string
     */
    public function getBrand2()
    {
        if ($this->_brand == null) {
            $this->_brand = $this->BRAND;
            if ($this->_brand == null) {
                $this->_brand = $this->unit == null ? '' : $this->unit->Brand;
            }
        }
        return $this->_brand;
    }

    /**
     * @param string $brand
     */
    public function setBrand2($brand)
    {
        $this->BRAND = $brand;
        $this->_brand = $brand;
    }

    /**
     * @return string
     */
    public function getOilChg()
    {
        return $this->oil_change;
    }

    public static function allStatus()
    {
        $trans = [
            'N' => 'N',
            'B' => 'B',
            'C' => 'C',
            'D' => 'D',
            'A' => 'B',
            'U' => 'C',
            'S' => 'D',
        ];
        $label = [
            'N' => 'NORMAL',
            'B' => 'ATTENTION',
            'C' => 'URGENT',
            'D' => 'SEVERE',
        ];
        $color = [
            'N' => '#00B500',
            'B' => '#FFED00',
            'C' => '#DF6240',
            'D' => '#ff2828',
        ];
        return ['trans' => $trans, 'label' => $label, 'color' => $color];
    }

//    public function __construct(array $config = [])
//    {
//        parent::__construct($config);
//        $additon = Addition::find()->where("Lab_No = '$this->Lab_No'")->all();
//        foreach ($additon as $record) {
//            foreach ($this->_additionParameter as $name => $value) {
//                if ($record->tbl_parameter_id == $value['parameter_id']) {
//                    $this->_additionParameter[$name]['value'] = $record->value;
//                    $this->_additionParameter[$name]['value_code'] = $record->value_CODE;
//                }
//            }
//        }
//    }


    public $_branch2;

    /**
     * @return string
     */
    public function getBranch2()
    {
        if ($this->_branch2 == null) {
            $this->_branch2 = $this->branch;
        }
        return $this->_branch2;
    }

    /**
     * @param string $branch2
     */
    public function setBranch2($branch2)
    {
        $this->_branch2 = $branch2;
    }


    /**
     * {@inheritdoc}
     */
    public function __get($name)
    {
        $pca = explode('_', $name);
        $name2 = is_array($pca) && count($pca) > 1 ? $pca[0] : '';
        if (array_key_exists($name, $this->_additionParameter) || array_key_exists($name2, $this->_additionParameter)) {
            if (isset($pca[1]) && strtolower($pca[1]) == 'code') {
                if ($this->_additionParameter[$name2]['model'] == null) {
                    $param = ['Lab_No' => $this->Lab_No, 'tbl_parameter_id' => $this->_additionParameter[$name2]['parameter_id']];
                    $a = Addition::findOne($param);
                    if ($a == null) {
                        $a = new Addition($param);
                    }
                    $this->_additionParameter[$name2]['model'] = $a;
                }
                if ($this->_additionParameter[$name2]['value_code'] === null) {
                    $this->_additionParameter[$name2]['value_code'] = $this->_additionParameter[$name2]['model']->value_CODE;
                }
                return $this->_additionParameter[$name2]['value_code'];
            }

            if ($this->_additionParameter[$name]['model'] == null) {
                $param = ['Lab_No' => $this->Lab_No, 'tbl_parameter_id' => $this->_additionParameter[$name]['parameter_id']];
                $a = Addition::findOne($param);
                if ($a == null) {
                    $a = new Addition($param);
                }
                $this->_additionParameter[$name]['model'] = $a;
            }
            if ($this->_additionParameter[$name]['value'] === null) {
                $this->_additionParameter[$name]['value'] = $this->_additionParameter[$name]['model']->value;
            }
            return $this->_additionParameter[$name]['value'];
        }

        return parent::__get($name);
    }

    /**
     * {@inheritdoc}
     */
    public function __set($name, $value)
    {
        $pca = explode('_', $name);
        $name2 = is_array($pca) && count($pca) > 1 ? $pca[0] : '';
        if (array_key_exists($name, $this->_additionParameter) || array_key_exists($name2, $this->_additionParameter)) {
            if (isset($pca[1]) && strtolower($pca[1]) == 'code') {
                $this->_additionParameter[$name2]['value_code'] = $value;
            } else {
                $this->_additionParameter[$name]['value'] = $value;
            }
        } else {
            parent::__set($name, $value);
        }
    }

    public static function generateLabNumber($type)
    {
        $p = self::find()->select(['MID(Lab_No,3,5) as urut', 'Lab_No'])
            ->where(" Lab_No like 'O$type" . '_____/UO/' . substr(date('Y'), 2, 2) . "'")
            ->andWhere(['=', 'YEAR(RECV_DT1)', date('Y')])
            ->orderBy('urut DESC')->one();
        if (empty($p))
            return "O$type" . '00001/UO/' . substr(date('Y'), 2, 2);
        return self::plusOne($p->Lab_No, $type);
    }

    public static function plusOne($value, $type)
    {
        $int = (int)substr($value, 2, 5) + 1;
        return 'O' . $type . str_pad($int, 5, '0', STR_PAD_LEFT) . '/UO/' . substr(date('Y'), 2, 2);
    }

    public $_history;

    public $offset = 0;
    public $limit = 5;

    public $page = 1;
    public $_numHistory;

    /**
     * @return self[]
     */
    public function getHistory()
    {
        if ($this->_history == null) {
            if ($this->page > 1) {
                $this->offset = $this->limit * ($this->page - 1);
            }
            $filter = self::find()->orderBy('SAMPL_DT1 DESC')->select(['Lab_No'])
                ->where(['unit_id' => $this->unit_id, 'ComponentID' => $this->ComponentID])
                ->andWhere( "SAMPL_DT1 <= '$this->SAMPL_DT1'")
                ->limit($this->limit)->offset($this->offset)->all();
            $filter = $filter == null || $filter == [] ? [['Lab_No' => $this->Lab_No]] : $filter;
            $filter = ArrayHelper::getColumn($filter, 'Lab_No');
            $query = self::find();
            $query->joinWith('others');
            $query->orderBy(self::tableName() . '.SAMPL_DT1 ASC');
            $query->where(self::tableName() . ".Lab_No IN ('" . join("','", $filter) . "')");
            $this->_history = $query->all();
        }
        return $this->_history;
    }

    /**
     * @return int
     */
    public function getNumHistory()
    {
        if ($this->_numHistory == null) {
            $this->_numHistory = self::find()->orderBy('SAMPL_DT1 DESC')->select(['Lab_No'])
                ->where(['unit_id' => $this->unit_id, 'ComponentID' => $this->ComponentID])
                ->andWhere("SAMPL_DT1 <= '$this->SAMPL_DT1'")->count();
        }
        return $this->_numHistory;
    }

    /**
     * @return float|int
     */
    public function getMaxPage()
    {
        return floor($this->numHistory / $this->limit) + 1;
    }

    public $_otherHis;

    /**
     * @return array
     */
    public function getOtherHis()
    {
        if ($this->_otherHis == null) {
            foreach ($this->others as $other) {
                $this->_otherHis[$other->unsur] = [
                    'value' => $other->value,
                    'code' => $other->value_CODE,
                ];
            }
        }
        return $this->_otherHis;
    }

    public function getFootNote()
    {
        return [
            '(*)' => [
                'visc_40',
                'VISC_CST',
            ],
            '*)' => [
                'Molybdenum',
                'Boron',
                'phosphor',
                'DILUTION',
                'WATER',
                'GLYCOL',
                'PQIndex',
                'SILVER',
                'titanium',
            ],
        ];
    }

    public $_unit_hour;
    public $_unit_hour_l;
    public $_oil_hour;
    public $_oil_hour_l;

    /**
     * @return mixed
     */
    public function getUnitHour()
    {
        if ($this->_unit_hour === null) {
            $t = explode(' ', $this->HRS_KM_TOT);
            if (is_array($t)) {
                $this->_unit_hour = isset($t[0]) ? $t[0] : '';
            }
        }
        return $this->_unit_hour;
    }

    /**
     * @param mixed $unit_hour
     */
    public function setUnitHour($unit_hour)
    {
        $this->_unit_hour = str_replace(' ', '', $unit_hour);
    }

    /**
     * @return mixed
     */
    public function getUnitHourL()
    {
        if ($this->_unit_hour_l === null) {
            $t = explode(' ', $this->HRS_KM_TOT);
            if (is_array($t)) {
                $this->_unit_hour_l = isset($t[1]) ? $t[1] : '';
            }
        }
        $this->_unit_hour_l = $this->_unit_hour_l == 'Jam' ? 'Hour' : $this->_unit_hour_l;
        $this->_unit_hour_l = $this->_unit_hour_l == '' ? '01' : $this->_unit_hour_l;
        return $this->_unit_hour_l;
    }

    /**
     * @param mixed $unit_hour_l
     */
    public function setUnitHourL($unit_hour_l)
    {
        if ($unit_hour_l === '01') {
            $unit_hour_l = '';
        }
        $this->_unit_hour_l = $unit_hour_l;
    }

    /**
     * @return mixed
     */
    public function getOilHour()
    {
        if ($this->_oil_hour === null) {
            $t = explode(' ', $this->HRS_KM_OC);
            if (is_array($t)) {
                $this->_oil_hour = isset($t[0]) ? $t[0] : '';
            }
            if (str_replace(' ', '', $this->HRS_KM_OC) === 'NEWOIL') {
                $this->_oil_hour = '';
            }
        }
        return $this->_oil_hour;
    }

    /**
     * @param mixed $oil_hour
     */
    public function setOilHour($oil_hour)
    {
        $this->_oil_hour = str_replace(' ', '', $oil_hour);
    }

    /**
     * @return mixed
     */
    public function getOilHourL()
    {
        if ($this->_oil_hour_l === null) {
            $t = explode(' ', $this->HRS_KM_OC);
            if (is_array($t)) {
                $t2 = isset($t[2]) ? " $t[2]" : '';
                $this->_oil_hour_l = isset($t[1]) ? $t[1] . $t2 : '';
            }
            if (str_replace(' ', '', $this->HRS_KM_OC) === 'NEWOIL') {
                $this->_oil_hour_l = 'NEW OIL';
            }
            $this->_oil_hour_l = $this->_oil_hour_l == 'Jam' ? 'Hour' : $this->_oil_hour_l;
            $this->_oil_hour_l = $this->_oil_hour_l == '' ? '01' : $this->_oil_hour_l;
        }
        return $this->_oil_hour_l;
    }

    /**
     * @param mixed $oil_hour_l
     */
    public function setOilHourL($oil_hour_l)
    {
        if ($oil_hour_l === '01') {
            $oil_hour_l = '';
        }
        $this->_oil_hour_l = $oil_hour_l;
    }
}
