<?php

namespace app\modules\reports\models;


/**
 *
 *
 * @property mixed $oil_mtrx
 * @property mixed $wear_mtrx
 */
class Transaction extends Report
{

    public function getOil_mtrx()
    {
        return $this->hasOne(\app\modelsDB\TblOilMatrix::class, ['OIL_MATRIX' => 'OIL_MATRIX']);
    }

    public function getWear_mtrx()
    {
        return $this->hasOne(\app\modelsDB\TblWearFtirMatrix::class, ['Matrix' => 'MATRIX']);
    }

    public function color($code = '')
    {
        if ($code == 'B') {
            return 'yellow';
        } elseif ($code == 'C') {
            return 'red';
        } elseif ($code == 'D') {
            return 'red';
        }
    }

    public function code($code = '')
    {
        if ($code == 'B') {
            return '/B';
        } elseif ($code == 'C') {
            return '/C';
        } elseif ($code == 'D') {
            return '/D';
        }
    }


}

?>