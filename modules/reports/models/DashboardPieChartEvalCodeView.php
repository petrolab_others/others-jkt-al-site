<?php
/**
 * Created by
 * User: Wisard17
 * Date: 02/08/2018
 * Time: 05.31 PM
 */

namespace app\modules\reports\models;


use app\assets\HighChart;
use app\modules\reports\models\search\SearchData;
use app\modules\reports\models\search\SearchReport;
use Yii;
use yii\bootstrap\Widget;
use yii\helpers\Html;
use yii\helpers\Json;

/**
 * Class DashboardPieChartEvalCodeView
 * @package app\modules\reports\models
 */
class DashboardPieChartEvalCodeView extends Widget
{
    /**
     * @var array the HTML attributes for the container tag of the list view.
     * The "tag" element specifies the tag name of the container element and defaults to "div".
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $beginDate;
    public $endDate;
    public $options = [];
    public $title = 'ALL STATUS';



    /**
     * Initializes the view.
     */
    public function init()
    {
        parent::init();

        if (!isset($this->options['id'])) {
            $this->options['id'] = $this->getId();
        }
    }

    /**
     * Runs the widget.
     */
    public function run()
    {
        $assetTable = HighChart::register($this->view);
        echo $this->loadContainer();
        $this->runJs();
    }

    /**
     * @return string
     */
    public function loadContainer()
    {
        $content = '';
        return Html::tag('div', "",
            ['class' => "", 'width' => "100%", 'id' => $this->options['id']]);
    }

    public function loadData($beginDate = null, $endDate = null)
    {
        $out = [];
        $query = SearchReport::find();
        $query = SearchReport::defaultFilterByUser($query);
        if (!empty($beginDate) && !empty($endDate)){
            $allData = $query->select('EVAL_CODE, count(*) as num')->where(['between', 'RECV_DT1', $beginDate, $endDate])->andWhere(['not', ['EVAL_CODE' => '']])->groupBy('EVAL_CODE')->createCommand()->queryAll();
        } else if (empty($beginDate) && empty($endDate)){
            // 60 days before now
            $endDate = date('Y-m-d', time());
            $beginDate = date('Y-m-d', strtotime($endDate. ' - 60 days'));
            $allData = $query->select('EVAL_CODE, count(*) as num')->where(['between', 'RECV_DT1', $beginDate, $endDate])->andWhere(['not', ['EVAL_CODE' => '']])->groupBy('EVAL_CODE')->createCommand()->queryAll();
        } else {
            // return empty
            $allData = [];
        }
        $trans = SearchReport::allStatus()['trans'];
        $label = SearchReport::allStatus()['label'];
        $color = SearchReport::allStatus()['color'];

        foreach ($allData as $allDatum) {
            $ecode = $allDatum['EVAL_CODE'];
            $ecode = isset($trans[$ecode]) ? $trans[$ecode] : $ecode;
            $l  = isset($label[$ecode]) ? $label[$ecode] : ($ecode === ''? 'blank' : $ecode);
            $c  = isset($color[$ecode]) ? $color[$ecode] : '#000000';
            if (isset($out[$ecode])) {
                $out[$ecode] = [
                    'name' => $l,
                    'y' => (float)$out[$ecode]['y'] + (float)$allDatum['num'],
                    'color' => $c,
                ];
            } else {
                $out[$ecode] = [
                    'name' => $l,
                    'y' => (float)$allDatum['num'],
                    'color' => $c,
                ];
            }
        }

        $queryOthers = SearchData::find();
        $queryOthers = SearchData::filterByUser($queryOthers);
        if (!empty($beginDate) && !empty($endDate)){
            $otherData = $queryOthers->select('eval_code, count(*) as num')->where(['between', 'received_date', $beginDate, $endDate])->andWhere(['not', ['eval_code' => '']])->groupBy('eval_code')->createCommand()->queryAll();
        } else if (empty($beginDate) && empty($endDate)){
            // 60 days before now
            $endDate = date('Y-m-d', time());
            $beginDate = date('Y-m-d', strtotime($endDate. ' - 60 days'));
            $otherData = $queryOthers->select('eval_code, count(*) as num')->where(['between', 'received_date', $beginDate, $endDate])->andWhere(['not', ['eval_code' => '']])->groupBy('eval_code')->createCommand()->queryAll();
        } else {
            // return empty
            $otherData = [];
        }
        foreach ($otherData as $allDatum) {
            $ecode = $allDatum['eval_code'];
            $ecode = isset($trans[$ecode]) ? $trans[$ecode] : $ecode;
            $l  = isset($label[$ecode]) ? $label[$ecode] : ($ecode === ''? 'blank' : $ecode);
            $c  = isset($color[$ecode]) ? $color[$ecode] : '#000000';
            if (isset($out[$ecode])) {
                $out[$ecode] = [
                    'name' => $l,
                    'y' => (float)$out[$ecode]['y'] + (float)$allDatum['num'],
                    'color' => $c,
                ];
            } else {
                $out[$ecode] = [
                    'name' => $l,
                    'y' => (float)$allDatum['num'],
                    'color' => $c,
                ];
            }
        }
        return array_values($out);
    }

    /**
     * All JS
     */
    protected function runJs()
    {

        $csrf = Yii::$app->request->getCsrfToken();

        $loadingHtml = '<div align="center"><i class="fa fa-spinner fa-pulse fa-4x fa-fw margin-bottom"></i></div>';

        $idChart = $this->options['id'];

        $beginDate = $this->beginDate;
        $endDate = $this->endDate;

        $data = Json::encode($this->loadData($beginDate, $endDate));

        $jsScript = <<< JS
var delay2 = (function () {
    var timer = 0;
    return function (callback, ms) {
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
    };
})();

var chartshow = (function () {
    Highcharts.chart('$idChart', {

    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    credits: {
            enabled: false
    },
    title: {
        text: '$this->title'
    },
    tooltip: {
        pointFormat: 'Jumlah <b>{point.y:.0f}</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: false
            },
            showInLegend: true
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: $data
    }]
});
    
    
    function alert_notif(res) {
        $.sound_on = false;
        var color = res.status === 'success' ? '#659265' : '#C46A69';
        var icon = res.status === 'success' ? 'fa-check' : 'fa-times';
        $.smallBox({
            title: res.status,
            content: res.message,
            color: color,
            iconSmall: "fa " + icon + " fa-2x fadeInRight animated",
            timeout: 4000
        });
    }

    return {
        "test": function () {
            alert('daa');
        }
    };
})();        

JS;

        $this->view->registerJs($jsScript, 4, 'runfor_' . $this->options['id']);
    }

}