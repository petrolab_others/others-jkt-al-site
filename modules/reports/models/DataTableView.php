<?php
/**
 * Created by
 * User: Wisard17
 * Date: 02/08/2018
 * Time: 05.31 PM
 */

namespace app\modules\reports\models;


use app\smartadmin\assets\plugins\FileDownloadAssets;
use conquer\select2\Select2Asset;
use Yii;
use app\smartadmin\assets\plugins\DataTableAsset;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\bootstrap\Widget;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;

/**
 * Class DataTableView
 * @package app\modules\reports\models
 */
class DataTableView extends Widget
{
    /**
     * @var array the HTML attributes for the container tag of the list view.
     * The "tag" element specifies the tag name of the container element and defaults to "div".
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $options = [];

    public $columns;

    /** @var  Model */
    public $model;

    public $request;

    public $ajaxUrl;


    /**
     * Initializes the view.
     * @throws InvalidConfigException
     */
    public function init()
    {
        parent::init();

        if ($this->columns === null) {
            throw new InvalidConfigException('The "columns" property must be set.');
        }

        if ($this->model === null) {
            throw new InvalidConfigException('The "model" property must be set.');
        }

        if (!isset($this->options['id'])) {
            $this->options['id'] = $this->getId();
        }
    }

    /**
     * Runs the widget.
     */
    public function run()
    {
        $assetTable = DataTableAsset::register($this->view);
        $asserJSDownload = FileDownloadAssets::register($this->view);
        echo $this->loadTable();
        $this->runJs();
    }

    /**
     * @return string
     */
    public function loadTable()
    {
        $content = '';

        return Html::tag('table', "",
            ['class' => "table table-responsive", 'width' => "100%", 'id' => $this->options['id']]);
    }


    /**
     * @return array
     */
    protected function loadColumnHeader()
    {
        $out = [];
        foreach ($this->columns as $column) {
            $showD = $column == '' ? 'details-control' : '';
            $out[] = [
                'title' => $this->model->getAttributeLabel($column),
                'data' => (string)$column,
                'name' => (string)$column,
                'className' => $showD,
                'orderable' => !in_array($column, ['actions', 'followup', 'status', 'statusPublish']),
            ];
        }

        return $out;
    }

    /**
     * @return string
     */
    protected function loadColumnFilter()
    {
        $out = '';
        foreach ($this->columns as $column) {
            $ipt = '';
            if (!in_array($column, ['actions', 'followup', ])) {
                if (in_array($column, ['sampleDate', 'receiveDate', 'reportDate',], true)) {
                    $ipt = Html::input('text', "filter_$column", '', [
                        'placeholder' => ' Filter ' . $this->model->getAttributeLabel($column),
                        'class' => 'form-control datejuifilter',
                        'style' => 'padding: 1px;height: 100%;width: 100%;',
                    ]);
                } elseif ($column === 'status') {
                    $ipt = '<select name="filter_' . $column . '"><option value="" selected>Select All</option><option value="N">Normal</option><option value="B">Attention</option><option value="C">Urgent</option><option value="D">Severe</option></select>';
                } else {
                    $ipt = Html::input('text', "filter_$column", '', [
                        'placeholder' => ' Filter ' . $this->model->getAttributeLabel($column),
                        'class' => 'form-control',
                        'style' => 'padding: 1px;height: 100%;width: 100%;',
                    ]);
                }
            }

            $out .= Html::tag('td', $ipt);
        }

        return Html::tag('tr', $out);
    }

    public $url_ajax;

    /**
     * All JS
     */
    protected function runJs()
    {

        $csrf = Yii::$app->request->getCsrfToken();

        $loadingHtml = '<div align="center"><i class="fa fa-spinner fa-pulse fa-4x fa-fw margin-bottom"></i></div>';

        $ajaxUrl = Url::to([$this->url_ajax == null ? 'index' : $this->url_ajax,
            '_csrf' => $csrf,
        ]);

        $idTable = $this->options['id'];

        $columns = Json::encode($this->loadColumnHeader());

        $colFilter = $this->loadColumnFilter();

        $detailUrl = Url::to(['detail']);

        $urlFollowup = Url::toRoute(['/reports/default/followup']);
        $urlExportExcel = isset($this->options['excel-export']) ? $this->options['excel-export'] : Url::toRoute(['/reports/default/export-excel']);

        $order = isset($this->options['order']) ? $this->options['order'] : "[0, 'desc']";

        Select2Asset::register($this->view);

        $jsScript = <<< JS
var delay2 = (function () {
    var timer = 0;
    return function (callback, ms) {
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
    };
})();

var renderdata = (function () {
    
    function alert_notif(res) {
        $.sound_on = false;
        var color = res.status === 'success' ? '#659265' : '#C46A69';
        var icon = res.status === 'success' ? 'fa-check' : 'fa-times';
        if (res.status === 'warning') {
            color = '#f0ad4e';
            icon = 'fa-warning'; 
        }
        $.smallBox({
            title: res.status,
            content: res.message,
            color: color,
            iconSmall: "fa " + icon + " fadeInRight animated",
            timeout: res.status === 'success' ? 4000 : 15000
        });
    }
    
    // var dataSet = ;
    var domElement = $('#$idTable');
    var detailurl = "$detailUrl";
    var domOuttable = domElement.parent().parent().parent();
    let ftir = $('#fltr-data');
    if (ftir.length > 0) {
        ftir.find('#generate').click(function () {
            table.ajax.reload();
        });
    }
    
    var table = domElement.DataTable({
        "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'bar-export col-sm-3 col-xs-12 hidden-xs'><'col-sm-3 pull-right col-xs-12 hidden-xs'l>r>" +
            "t" + "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
        scrollX: true,
        //"scrollY": "400px",
        // "scrollCollapse": true,
        stateSave: true,
        rowId: 'rowId',
        processing: true,
        serverSide: true,
        ajax: {
            url:"$ajaxUrl",
            data: function (d) {
                if (ftir.length > 0) {
                    d.r_start = ftir.find('#filter-r_start').val();
                    d.r_end = ftir.find('#filter-r_end').val();
                }
            }
        },
        columns: $columns,
        order: [$order],
        createdRow: function (row, data, index) {
            if (data.classRow !== '')
                $(row).addClass(data.classRow);
        },
        initComplete: function () {
            delay2(function () {
                var colfilter = $('$colFilter');
                colfilter.find('.datejuifilter').datepicker({
                    showButtonPanel: true,
                    altField: "yyyy-mm-dd",
                    dateFormat: "yy-mm-dd",
                    beforeShow: function (input) {
                        setTimeout(function () {
                            var buttonPane = $(input)
                                .datepicker("widget")
                                .find(".ui-datepicker-buttonpane");
                            $("<button>", {
                                text: "Reset",
                                click: function () {
                                    $.datepicker._clearDate(input);
                                }
                            }).appendTo(buttonPane).addClass("ui-datepicker-clear ui-state-default ui-priority-primary ui-corner-all");
                        }, 1);
                    },
                });
                // Restore state
                var state = table.state.loaded();
                if (state) {
                    table.columns().eq(0).each(function (colIdx) {
                        var colname = table.column(colIdx).dataSrc();
                        var colSearch = state.columns[colIdx].search;
                        if (colSearch.search) {
                            console.log(colname);
                            console.log(colSearch.search);
                            colfilter.find('[name=filter_' + colname + ']').val(colSearch.search);
                        }
                    });
                }
                domOuttable.find('thead').eq(0).append(colfilter);
                //domOuttable.find('.dataTables_scrollBody').attr('style', 'position: relative; width: 100%;');
            }, 1000);
        }

    });
    
    $('.bar-export').html("<a class='btn btn-default' title='Export Excel' data-action='exportexcel'><i class='fa fa-file-excel-o'></i></a>");
    
    $('[data-action=exportexcel]').click(function (e) {
       
        e.preventDefault();
        
        var param = table.ajax.params();
        
        param._csrf = '$csrf';
        
        var urlparm = $.param(param);
        
        var urlDw = '$urlExportExcel';
        
        // $.ajax({
        //     url: urlDw,
        //     data: urlparm,
        // });
        $.fileDownload(urlDw, {
            preparingMessageHtml: "We are preparing your report, please wait...",
            failMessageHtml: "There was a problem generating your report, the report is to big, filter first and please try again.",    
            //httpMethod: "POST",
            data: urlparm
        });
        return false;
    });

    
    
    domOuttable.delegate('[data-action=update_status]', 'click', function (a) {
        a.preventDefault();
        a.stopPropagation();
        var elm = $(this);
        var td = elm.closest('td');
        var cll = table.cell(td);

        $.ajax({
            url: elm.attr('href'),
            method: 'post',
            data: {},
            error: function (response) {
                alert_notif({status: "error", message: 'tidak bisa save'});
            },
            success: function (response) {
                console.log(response);
                cll.data(response.button);
                alert_notif(response);
            },

        });

    });
    domOuttable.delegate('[data-action=publish]', 'click', function (a) {
        a.preventDefault();
        a.stopPropagation();
        var elm = $(this);
        var td = elm.closest('td');
        var cll = table.cell(td);

        $.ajax({
            url: elm.attr('href'),
            method: 'post',
            data: {},
            error: function (response) {
                alert_notif({status: "error", message: 'tidak bisa save'});
            },
            success: function (response) {
                console.log(response);
                // cll.data(response.button);
                alert_notif(response);
            },

        });

    });
    domOuttable.find('thead').eq(0).delegate('td input[type=text]', 'change', function () {
        var self = this;
        delay2(function () {
            table
                .column($(self).parent().index() + ':visible')
                .search(self.value)
                .draw();
        }, 1000);
    });

    domOuttable.find('thead').eq(0).delegate('td select', 'change', function () {
        var self = this;
        delay2(function () {
            table
                .column($(self).parent().index() + ':visible')
                .search(self.value)
                .draw();
        }, 1000);
    });

    domElement.on('click', 'tbody .details-control', function () {

        var tr = $(this).closest('tr');
        tr.toggleClass('selected');

        var row = table.row(tr);

        var orderNo = row.data().Order_No;

        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
            if (row.child() === undefined || row.child().find('div').length < 2) {
                row.child('$loadingHtml');
                $.get(detailurl, {
                    "orderno": orderNo,
                    "_csrf": '$csrf'

                }, function (dataresponse, status, xhr) {
                    if (status === "success")
                        row.child(dataresponse);
                    if (status === "error")
                        alert("Error: " + xhr.status + ": " + xhr.statusText);
                });
            }
            row.child.show();

            tr.addClass('shown');
        }
    });

    var modalvar = $('#modal-temp');
    var td = null;
    domElement.on('click', 'tbody [data-action=update_followup]', function (a) {
        a.preventDefault();
        var elm = $(this);
        var tr = elm.closest('tr');
        td = elm.closest('td');

        var row = table.row(tr);
        var labNo = row.data().Lab_No;

        modalvar.removeData('bs.modal');
        modalvar.modal({remote: '$urlFollowup' + '?labNo=' + labNo});
        modalvar.modal('show');
    });

    modalvar.delegate('[data-action=submit_update_followup]', 'click', function (a) {
        a.preventDefault();
        a.stopPropagation();
        var elm = $(this);

        var form = modalvar.find('form');

        var cll = table.cell(td);
        var data = $(form).serializeArray();

        $.ajax({
            url: $(form).attr('action'),
            method: $(form).attr('method'),
            data: data,
            error: function (response) {
                alert_notif({status: "error", message: 'tidak bisa save'});
                modalvar.modal('hide');
            },
            success: function (response) {
                console.log(response);
                cll.data(response.button);
                alert_notif(response);
                if (response.status === 'success') {
                    modalvar.modal('hide');
                }
            },

        });

    });
    
    modalvar.delegate('[data-action=export_excel]', 'click', function (a) {
        a.preventDefault();
        a.stopPropagation();
        var elm = $(this);

        var form = modalvar.find('form');
        var data = $(form).serializeArray();

        var param = table.ajax.params();
        param._csrf = '$csrf';
        var urlparm = $.param(param);
        
        $.fileDownload($(form).attr('action') + '?' + urlparm, {
            preparingMessageHtml: "We are preparing your report, please wait...",
            failMessageHtml: "There was a problem generating your report, the report is to big, filter first and please try again.",
            httpMethod: $(form).attr('method'),
            data: data
        });
        modalvar.modal('hide');
        return false;
    });
    
    $('[data-action=txt_file]').click(function (a) {
        a.preventDefault();
        a.stopPropagation();
        var elm = $(this);
        
        var param = table.ajax.params();
        param._csrf = '$csrf';
        var urlparm = $.param(param);
        
        $.fileDownload(elm.attr('href') + '?' + urlparm, {
            preparingMessageHtml: "We are preparing your report, please wait...",
            failMessageHtml: "There was a problem generating your report, the report is to big, filter first and please try again.",
            httpMethod: 'GET',
            // data: {}
        });
    });

    $('[data-action=admin_excel]').click(function (a) {
        a.preventDefault();
        a.stopPropagation();
        var elm = $(this);
        
        modalvar.removeData('bs.modal');
        modalvar.modal().find('.modal-content').load(elm.attr('href'), function () {
            var modal = $(this);
            modal.find('.select2param').select2({width: "100%",});
        });
        modalvar.modal('show');
    });

     domOuttable.on('click', 'tbody [data-action=update_code]', function (e) {
        e.preventDefault();
        e.stopPropagation();
        
        var elm = $(this);
        update_evalCode(elm);
    });
    
     $('[data-action=update_code]').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        
        var elm = $(this);
        update_evalCode(elm);
    });
     
    function update_evalCode(elm) {
        var param = table.ajax.params();
        param._csrf = '$csrf';
        var urlparm = $.param(param);
        var icon = elm.find('i');
        icon.addClass('fa-spin');
        $.ajax({
            url: elm.attr('href'),
            method: 'post',
            data: param,
            error: function (response) {
                alert_notif({status: "error", message: 'tidak bisa diupdate data terlalu besar filter kembali dan coba lagi. '});
                icon.removeClass('fa-spin');
            },
            success: function (response) {
                console.log(response);
                $.each(response.data, function(i, v) {
                    var row = table.row('#' + i);
                    
                    if (row.data() !== undefined) {
                        row.data().status = v;
                        row.invalidate();
                    }
                });
                icon.removeClass('fa-spin');
                alert_notif(response);
            },

        });
        return false;
    } 
    
     $('[data-action=update_data_pama]').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        
        var elm = $(this);
         update_data_Pama(elm);
    });
    
      function update_data_Pama(elm) {
        var param = table.ajax.params();
        param._csrf = '$csrf';
        var urlparm = $.param(param);
        var icon = elm.find('i');
        icon.addClass('fa-spin');
        $.ajax({
            url: elm.attr('href'),
            method: 'post',
            data: param,
            error: function (response) {
                alert_notif({status: "error", message: 'tidak bisa diupdate data terlalu besar filter kembali dan coba lagi. '});
                icon.removeClass('fa-spin');
            },
            success: function (response) {
                console.log(response);
                // $.each(response.data, function(i, v) {
                //     var row = table.row('#' + i);
                //    
                //     if (row.data() !== undefined) {
                //         row.data().status = v;
                //         row.invalidate();
                //     }
                // });
                icon.removeClass('fa-spin');
                alert_notif(response);
            },

        });
        return false;
    } 
    
    $('[data-action=publish]').click(function(e){
         e.preventDefault();
        e.stopPropagation();
        var elm = $(this);
         publish(elm);
    })
    
      function publish(elm) {
        var param = table.ajax.params();
        param._csrf = '$csrf';
        var urlparm = $.param(param);
        var icon = elm.find('i');
        icon.addClass('fa-spin');
        $.ajax({
            url: elm.attr('href'),
            method: 'post',
            data: param,
            error: function (response) {
                // alert_notif({status: "error", message: 'tidak bisa diupdate data terlalu besar filter kembali dan coba lagi. '});
                alert_notif(response);
                icon.removeClass('fa-spin');
            },
            success: function (response) {
                console.log(response);
                // $.each(response.data, function(i, v) {
                //     var row = table.row('#' + i);
                //    
                //     if (row.data() !== undefined) {
                //         row.data().status = v;
                //         row.invalidate();
                //     }
                // });
                icon.removeClass('fa-spin');
                alert_notif(response);
            },

        });
        return false;
    } 
    return {
        "test": function () {
            alert('daa');
        },
        table: table
    };
})();        

JS;


        $this->view->registerJs($jsScript, 4, 'runfor_' . $this->options['id']);
    }

}