<?php
/**
 * Created by
 * User: Wisard17
 * Date: 04/08/2018
 * Time: 08.27 PM
 */

namespace app\modules\reports\models;


use app\components\FPDF;

/**
 * Class RenderPDF
 * @package app\modules\reports\models
 */
class RenderPDF extends FPDF
{

    function WriteHTML($html)
    {
        //HTML parser
        $html = str_replace("\n", ' ', $html);
        $a = preg_split('/<(.*)>/U', $html, -1, PREG_SPLIT_DELIM_CAPTURE);
        foreach ($a as $i => $e) {
            if ($i % 2 == 0) {
                //Text
                if ($this->HREF)
                    $this->PutLink($this->HREF, $e);
                else
                    $this->Write(5, $e);
            } else {
                //Tag
                if ($e{0} == '/')
                    $this->CloseTag(strtoupper(substr($e, 1)));
                else {
                    //Extract attributes
                    $a2 = explode(' ', $e);
                    $tag = strtoupper(array_shift($a2));
                    $attr = array();
                    foreach ($a2 as $v)
                        if (ereg('^([^=]*)=["\']?([^"\']*)["\']?$', $v, $a3))
                            $attr[strtoupper($a3[1])] = $a3[2];
                    $this->OpenTag($tag, $attr);
                }
            }
        }
    }

    function OpenTag($tag, $attr)
    {
        //Opening tag
        if ($tag == 'B' or $tag == 'I' or $tag == 'U')
            $this->SetStyle($tag, true);
        if ($tag == 'A')
            $this->HREF = $attr['HREF'];
        if ($tag == 'BR')
            $this->Ln(5);
    }

    function CloseTag($tag)
    {
        //Closing tag
        if ($tag == 'B' or $tag == 'I' or $tag == 'U')
            $this->SetStyle($tag, false);
        if ($tag == 'A')
            $this->HREF = '';
    }

    function SetStyle($tag, $enable)
    {
        //Modify style and select corresponding font
        $this->$tag += ($enable ? 1 : -1);
        $style = '';
        foreach (array('B', 'I', 'U') as $s)
            if ($this->$s > 0)
                $style .= $s;
        $this->SetFont('', $style);
    }

    function PutLink($URL, $txt)
    {
        //Put a hyperlink
        $this->SetTextColor(0, 0, 255);
        $this->SetStyle('U', true);
        $this->Write(5, $txt, $URL);
        $this->SetStyle('U', false);
        $this->SetTextColor(0);
    }

    function ImprovedTable($header, $data, $format, $col = 5)
    {
        if ($col == 5) {
            $this->ImprovedTableHis5($header, $data, $format);
        } else {
            $this->ImprovedTableHis4($header, $data, $format);
        }
    }

    function ImprovedTableHis5($header, $data, $format)
    {
        $this->SetTextColor(0);
        $this->SetDrawColor(200, 200, 200);
        $this->SetFont('Arial', 'B', 8);
        $this->SetLineWidth(0.03);

        $w = array(
            2.8,
            1.45,
            2.1,
            2.15,
            2.15,
            2.15,
            2.15,
            2.15,
            1.60,
            1.60
        );

        for ($i = 0, $iMax = count($header); $i < $iMax; $i++) {
            switch ($i) {
                case 0:
                    $this->Cell($w[$i], 0.5, $header[$i], 'LTB', 0, 'L');
                    break;
                case 7:
                    $this->Cell($w[$i], 0.5, $header[$i], 'TBR', 0, 'L');
                    break;
                case 8:
                    if ($format == 1) {
                        $this->Cell($w[$i], 0.5, $header[$i], 'TL', 0, 'L');
                    } else if ($format == 2) {
                        $this->Cell($w[$i], 0.5, $header[$i], 1, 0, 'C');
                    } else {
                        $this->SetTextColor(200);
                        $this->Cell($w[$i], 0.5, $header[$i], 'LR', 0, 'L');
                        $this->SetTextColor(0);
                    }
                    break;
                case 9:
                    if ($format == 1) {
                        $this->Cell($w[$i], 0.5, $header[$i], 'TR', 0, 'L');
                    } else if ($format == 2) {
                        $this->Cell($w[$i], 0.5, $header[$i], 1, 0, 'C');
                    } else {
                        $this->SetTextColor(200);
                        $this->Cell($w[$i], 0.5, $header[$i], 'LR', 0, 'L');
                        $this->SetTextColor(0);
                    }
                    break;
                default:
                    $this->Cell($w[$i], 0.5, $header[$i], 'TB', 0, 'C');
                    break;
            }
        }

        $this->Ln();
        $this->SetFont('Arial', '', 7);

        $fill = 0;
        $width = 0.345;

        foreach ($data as $row) {
            $this->Cell($w[0], $width, $row[0], 'L', 0, 'L', $fill);
            if ($format == 1) {
                $this->Cell($w[1], $width, $row[1], '', 0, 'C', $fill);
                $this->SetFont('Arial', '', 6);
                $this->Cell($w[2], $width, $row[2], 'R', 0, 'C', $fill);
            } else {
                $this->Cell($w[1], $width, $row[1], 'LR', 0, 'C', $fill);
                $this->SetFont('Arial', '', 6);
                $this->Cell($w[2], $width, $row[2], 'LR', 0, 'C', $fill);
            }

            $this->SetFont('Arial', '', 7);

            if ($row[0] == 'Lube Oil Name')
                $this->SetFont('Arial', '', 5.5);

            list($R, $G, $B, $d) = $this->color($row[3]);
            $this->SetTextColor($R, $G, $B);
            $this->Cell($w[3], $width, $row[3], 'LR', 0, 'C', $fill);
            list($R, $G, $B) = $this->color($row[4]);
            $this->SetTextColor($R, $G, $B);
            $this->Cell($w[4], $width, $row[4], 'LR', 0, 'C', $fill);
            list($R, $G, $B) = $this->color($row[5]);
            $this->SetTextColor($R, $G, $B);
            $this->Cell($w[5], $width, $row[5], 'LR', 0, 'C', $fill);
            list($R, $G, $B) = $this->color($row[6]);
            $this->SetTextColor($R, $G, $B);
            $this->Cell($w[6], $width, $row[6], 'LR', 0, 'C', $fill);
            list($R, $G, $B) = $this->color($row[7]);
            $this->SetTextColor($R, $G, $B);
            $this->Cell($w[7], $width, $row[7], 'LR', 0, 'C', $fill);
            $this->SetTextColor(0, 0, 0);
            $this->SetFont('Arial', '', 7);

            if ($format == 1) {
                $this->Cell($w[8], $width, $row[8], 'L', 0, 'C', $fill);
                $this->Cell($w[9], $width, $row[9], 'R', 0, 'C', $fill);
            } else if ($format == 4) {
                $this->Cell($w[8], $width, $row[8], 'LRB', 0, 'C', $fill);
                $this->Cell($w[9], $width, $row[9], 'LRB', 0, 'C', $fill);
            } else {
                $this->Cell($w[8], $width, $row[8], 'LR', 0, 'C', $fill);
                $this->Cell($w[9], $width, $row[9], 'LR', 0, 'C', $fill);
            }

            $this->Ln();
        }

        $this->Cell(2.9 + 1.5 + 1.95 + 2.15 + 2.15 + 2.15 + 2.15 + 2.15, 0, '', 'T');
    }

    function ImprovedTableHis4($header, $data, $format)
    {
        $this->SetTextColor(0);
        $this->SetDrawColor(200, 200, 200);
        $this->SetFont('Arial', 'B', 8);
        $this->SetLineWidth(0.03);

        $w = array(
            2.8,
            1.45,
            2.1,
            2.6875,
            2.6875,
            2.6875,
            2.6875,
            1.60,
            1.60
        );

        for ($i = 0, $iMax = count($header); $i < $iMax; $i++) {
            switch ($i) {
                case 0:
                    $this->Cell($w[$i], 0.5, $header[$i], 'LTB', 0, 'L');
                    break;
                case 6:
                    $this->Cell($w[$i], 0.5, $header[$i], 'TBR', 0, 'L');
                    break;
                case 7:
                    if ($format == 1) {
                        $this->Cell($w[$i], 0.5, $header[$i], 'TL', 0, 'L');
                    } else if ($format == 2) {
                        $this->Cell($w[$i], 0.5, $header[$i], 1, 0, 'C');
                    } else {
                        $this->SetTextColor(200);
                        $this->Cell($w[$i], 0.5, $header[$i], 'LR', 0, 'L');
                        $this->SetTextColor(0);
                    }
                    break;
                case 8:
                    if ($format == 1) {
                        $this->Cell($w[$i], 0.5, $header[$i], 'TR', 0, 'L');
                    } else if ($format == 2) {
                        $this->Cell($w[$i], 0.5, $header[$i], 1, 0, 'C');
                    } else {
                        $this->SetTextColor(200);
                        $this->Cell($w[$i], 0.5, $header[$i], 'LR', 0, 'L');
                        $this->SetTextColor(0);
                    }
                    break;
                default:
                    $this->Cell($w[$i], 0.5, $header[$i], 'TB', 0, 'C');
                    break;
            }
        }

        $this->Ln();
        $this->SetFont('Arial', '', 7);

        $fill = 0;
        $width = 0.345;

        foreach ($data as $row) {
            $this->Cell($w[0], $width, $row[0], 'L', 0, 'L', $fill);
            if ($format == 1) {
                $this->Cell($w[1], $width, $row[1], '', 0, 'C', $fill);
                $this->SetFont('Arial', '', 6);
                $this->Cell($w[2], $width, $row[2], 'R', 0, 'C', $fill);
            } else {
                $this->Cell($w[1], $width, $row[1], 'LR', 0, 'C', $fill);
                $this->SetFont('Arial', '', 6);
                $this->Cell($w[2], $width, $row[2], 'LR', 0, 'C', $fill);
            }

            $this->SetFont('Arial', '', 7);

            if ($row[0] == 'Lube Oil Name')
                $this->SetFont('Arial', '', 5.5);

            list($R, $G, $B, $d) = $this->color($row[3]);
            $this->SetTextColor($R, $G, $B);
            $this->Cell($w[3], $width, $row[3], 'LR', 0, 'C', $fill);
            list($R, $G, $B) = $this->color($row[4]);
            $this->SetTextColor($R, $G, $B);
            $this->Cell($w[4], $width, $row[4], 'LR', 0, 'C', $fill);
            list($R, $G, $B) = $this->color($row[5]);
            $this->SetTextColor($R, $G, $B);
            $this->Cell($w[5], $width, $row[5], 'LR', 0, 'C', $fill);
            list($R, $G, $B) = $this->color($row[6]);
            $this->SetTextColor($R, $G, $B);
            $this->Cell($w[6], $width, $row[6], 'LR', 0, 'C', $fill);
            // list($R, $G, $B) = $this->color($row[7]);
            // $this->SetTextColor($R, $G, $B);
            // $this->Cell($w[7], $width, $row[7], 'LR', 0, 'C', $fill);
            // $this->SetTextColor(0, 0, 0);
            // $this->SetFont('Arial', '', 7);

            if ($format == 1) {
                $this->Cell($w[7], $width, $row[7], 'L', 0, 'C', $fill);
                $this->Cell($w[8], $width, $row[8], 'R', 0, 'C', $fill);
            } else if ($format == 4) {
                $this->Cell($w[7], $width, $row[7], 'LRB', 0, 'C', $fill);
                $this->Cell($w[8], $width, $row[8], 'LRB', 0, 'C', $fill);
            } else {
                $this->Cell($w[7], $width, $row[7], 'LR', 0, 'C', $fill);
                $this->Cell($w[8], $width, $row[8], 'LR', 0, 'C', $fill);
            }

            $this->Ln();
        }

        $this->Cell(2.9 + 1.5 + 1.95 + 2.6875 + 2.6875 + 2.6875 + 2.6875, 0, '', 'T');
    }

    function color($data)
    {
        if (substr($data, 0, 2) == "N/") {
            $R = 0;
            $G = 0;
            $B = 0;
            $data = substr($data, 2, 10);
        } else if (substr($data, 0, 2) == "C/") {
            //$R  = 255;
            $R = 0;
            $G = 0;
            $B = 0;
            $data = substr($data, 2, 10);
            $data = $data . " / C";
        } else if (substr($data, 0, 2) == "B/") {
            // $R  = 255;
            $R = 0;
            //$G  = 220;
            $G = 0;
            $B = 0;
            $data = substr($data, 2, 10);
            $data = $data . " / B";
        } else if (substr($data, 0, 2) == "D/") {
            // $R  = 255;
            $R = 0;
            $G = 0;
            $B = 0;
            $data = substr($data, 2, 10);
            $data = $data . " / D";
        } else if ((substr($data, 0, 3) == "BL/") || (substr($data, 0, 3) == "BH/")) {
            //$R  = 255;
            //$G  = 220;
            $R = 0;
            $G = 0;
            $B = 0;
            $data = substr($data, 3, 10);
            $data = $data . " / B";
        } else if ((substr($data, 0, 3) == "CL/") || (substr($data, 0, 3) == "CH/")) {
            //$R  = 255;
            $R = 0;
            $G = 0;
            $B = 0;
            $data = substr($data, 3, 10);
            $data = $data . " / C";
        } else if ((substr($data, 0, 3) == "DL/") || (substr($data, 0, 3) == "DH/")) {
            //$R  = 255;
            $R = 0;
            $G = 0;
            $B = 0;
            $data = substr($data, 3, 10);
            $data = $data . " / D";
        } else {
            $R = 0;
            $G = 0;
            $B = 0;
        }
        return [$R, $G, $B, $data];
    }

    function Footer()
    {
    }

    function RoundedRect($x, $y, $w, $h, $r, $style = '')
    {
        $k = $this->k;
        $hp = $this->h;

        if ($style == 'F')
            $op = 'f';
        elseif ($style == 'FD' or $style == 'DF')
            $op = 'B';
        else
            $op = 'S';

        $MyArc = 4 / 3 * (sqrt(2) - 1);
        $this->_out(sprintf('%.2f %.2f m', ($x + $r) * $k, ($hp - $y) * $k));
        $xc = $x + $w - $r;
        $yc = $y + $r;
        $this->_out(sprintf('%.2f %.2f l', $xc * $k, ($hp - $y) * $k));
        $this->_Arc($xc + $r * $MyArc, $yc - $r, $xc + $r, $yc - $r * $MyArc, $xc + $r, $yc);
        $xc = $x + $w - $r;
        $yc = $y + $h - $r;
        $this->_out(sprintf('%.2f %.2f l', ($x + $w) * $k, ($hp - $yc) * $k));
        $this->_Arc($xc + $r, $yc + $r * $MyArc, $xc + $r * $MyArc, $yc + $r, $xc, $yc + $r);
        $xc = $x + $r;
        $yc = $y + $h - $r;
        $this->_out(sprintf('%.2f %.2f l', $xc * $k, ($hp - ($y + $h)) * $k));
        $this->_Arc($xc - $r * $MyArc, $yc + $r, $xc - $r, $yc + $r * $MyArc, $xc - $r, $yc);
        $xc = $x + $r;
        $yc = $y + $r;
        $this->_out(sprintf('%.2f %.2f l', ($x) * $k, ($hp - $yc) * $k));
        $this->_Arc($xc - $r, $yc - $r * $MyArc, $xc - $r * $MyArc, $yc - $r, $xc, $yc - $r);
        $this->_out($op);
    }

    function _Arc($x1, $y1, $x2, $y2, $x3, $y3)
    {
        $h = $this->h;
        $this->_out(sprintf('%.2f %.2f %.2f %.2f %.2f %.2f c ', $x1 * $this->k, ($h - $y1) * $this->k, $x2 * $this->k, ($h - $y2) * $this->k, $x3 * $this->k, ($h - $y3) * $this->k));
    }
}