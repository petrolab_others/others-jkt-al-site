<?php

namespace app\modules\reports\models;

use app\modelsDB\TblOtherorder;
use app\helpers\DynamicTable;

/**
 *
 */
class OtherParams extends TblOtherorder
{

    public function getWear_mtrx()
    {
        return $this->hasOne(\app\modelsDB\TblWearFtirMatrix::class, ['Matrix' => 'MATRIX']);
    }

    public function param($history, $unsur)
    {
        $total = count($history);
        $aa = 0;
        $data = "";
        foreach ($history as $key => $value) {
            $otherData = $this->find()
                ->where(['Lab_No' => $value->Lab_No])->andWhere(['unsur' => $unsur])->one();
            $val_param = $otherData['value'];
            $code_param = $otherData['value_CODE'];
            $data .= DynamicTable::tdValue($code_param, $val_param);
        }
        if ($total == 0) {
            $aa = 5;
        } elseif ($total == 1) {
            $aa = 4;
        } elseif ($total == 2) {
            $aa = 3;
        } elseif ($total == 3) {
            $aa = 2;
        } elseif ($total == 4) {
            $aa = 1;
        }
        if ($total < 5) {
            for ($bb = 0; $bb < $aa; $bb++) {
                $data .= "<td class='value'></td>";
            }
        }
        return $data;
    }


}

?>