<?php
/**
 * Created by
 * User     : Wisard17
 * Date     : 20/06/2019
 * Time     : 01.13 PM
 * File Name: SearchData.php
 **/


namespace app\modules\reports\models\search;

use app\components\ArrayHelper;
use Yii;
use app\modules\data\models\TypeView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

/**
 * Class SearchData
 * @package app\modules\reports\models\search
 */
class SearchData extends \app\modules\data\models\SearchData
{

    /** @var TypeView */
    public $viewType;

    public function getAction()
    {
        $rm = Yii::$app->controller->id;
        return
//            Html::tag('a', '<i class="fa fa-external-link"></i> View', ['class' => 'btn btn-default btn-xs',
//                'title' => 'View Report', 'href' => Url::to(['/reports/'. $rm .'/view', 'id' => $this->lab_number]),]) .
//            Html::tag('a', '<i class="fa fa-bar-chart-o"></i> Chart', ['class' => 'btn btn-default btn-xs',
//                'title' => 'View Chart', 'href' => Url::to(['/reports/'. $rm .'/chart', 'labNo' => $this->lab_number]), 'target' => '_blank']) .
            Html::tag('a', '<i class="fa fa-file-pdf-o"></i> PDF', ['class' => 'btn btn-default btn-xs',
                'title' => 'Download Report', 'href' => Url::to(['/data/report/pdf', 'lab_number' => $this->lab_number]), 'target' => '_blank'])
            ;
    }

    public function searchData($params, $export = false, $join = true)
    {
        return parent::searchData($params, $export, $join);
    }

    /**
     * @param \yii\db\Query $query
     * @param string $params
     * @return \yii\db\Query
     * @throws NotFoundHttpException
     */
    public function defaultFilterByUser($query, $params = '')
    {
        $query = parent::defaultFilterByUser($query, $params);
        $viewTypeName = str_replace('-', ' ', Yii::$app->controller->id);
        $viewType = TypeView::find()->where("LOWER(name) = '$viewTypeName'")->one();
        if ($viewType == null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $listID = ArrayHelper::getColumn($viewType->typeReports, 'id');
        $query->andWhere(['in', 'type_report_id', $listID]);

        $query->andWhere('publish is not null');

        return $query;
    }

    /**
     * @param \yii\db\Query $query
     * @param string $params
     * @return \yii\db\Query
     */
    public static function filterByUser($query, $params = '')
    {
        $query->andWhere('publish is not null');
        return (new parent())->defaultFilterByUser($query, $params);
    }
}