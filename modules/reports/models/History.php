<?php

namespace app\modules\reports\models;


/**
 *
 */
class History
{

    public $lab_number;

    public function __construct($lab_number)
    {
        $this->lab_number = $lab_number;
    }

    public function getData()
    {
        $lab_number = $this->lab_number;
        $trx = Transaction::find()->where(['Lab_No' => $lab_number])->one();
        $alldata = Transaction::find();
        $where = "name='$trx->name' and COMPONENT='$trx->COMPONENT' and unit_id='$trx->unit_id' and SAMPL_DT1<='$trx->SAMPL_DT1'";
        $alldata->where($where);
        $alldata->orderBy(['SAMPL_DT1' => SORT_DESC, 'Lab_No' => SORT_DESC]);
        $alldata->limit(5);
        return array_reverse($alldata->all());
    }

}

?>