<?php

namespace app\modules\reports\models;


class DynamicParameter
{
    public function parameter($history)
    {
        $lab_numbers = [];
        foreach ($history as $key => $value) {
            array_push($lab_numbers, $value->Lab_No);
        }
        $other_param = OtherParams::find()->where(['in', 'Lab_No', $lab_numbers])->groupBy('unsur')->all();
        return $other_param;
    }
}

?>