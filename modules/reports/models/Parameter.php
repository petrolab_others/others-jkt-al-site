<?php
/**
 * Created by
 * User: Wisard17
 * Date: 07/09/2018
 * Time: 05.03 PM
 */

namespace app\modules\reports\models;


use app\modelsDB\TblParameter;

/**
 * Class Parameter
 * @package app\modules\reports\models
 */
class Parameter extends TblParameter
{
    public function rules()
    {
        return array_merge(parent::rules(), [
//            ['unsur', 'unique'],
        ]);
    }

    /**
     * @param $type
     * @param \app\modules\master\models\Matrix $wear
     * @return string
     */
    public function getLimit($type, $wear)
    {
        if ($type == 'attention') {
            if ($this->col_matrix != null && $wear != null) {
                $col = $this->col_matrix . 'B';
                if ($wear->hasAttribute($col)) {
                    return $wear->$col;
                }
            }
            return $this->attention;
        }
        if ($type == 'urgent') {
            if ($this->col_matrix != null && $wear != null) {
                $col = $this->col_matrix . 'C';
                if ($wear->hasAttribute($col)) {
                    return $wear->$col;
                }
            }
            return $this->urgent;
        }
        if ($type == 'severe') {
            if ($this->col_matrix != null && $wear != null) {
                $col = $this->col_matrix . 'D';
                if ($wear->hasAttribute($col)) {
                    return $wear->$col;
                }
            }
            return $this->severe;
        }
        return '';
    }
}