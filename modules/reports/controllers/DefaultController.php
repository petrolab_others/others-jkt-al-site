<?php

namespace app\modules\reports\controllers;


use app\components\ArrayHelper;
use app\components\Excel;
use app\models\AccessControl;
use app\modules\reports\models\FormFollowup;
use app\modules\reports\models\search\SearchReport;
use app\modules\master\models\Transaksi;
use app\modules\reports\PDFReport;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

use Ghunti\HighchartsPHP\Highchart;
use Ghunti\HighchartsPHP\HighchartJsExpr;
use kartik\mpdf\Pdf;
use app\modules\reports\models\Transaction;
use app\modules\reports\models\History;

/**
 * Default controller for the `reports` module
 */
class DefaultController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['detail', 'index', 'add', 'edit'],
                'rules' => [
                    [
                        'actions' => ['detail', 'index', 'add', 'edit'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'del' => ['post'],
                ],
            ],
        ];
    }


    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new SearchReport();
        $request = Yii::$app->request->queryParams;

        if (Yii::$app->request->isAjax) {
            return $searchModel->searchDataTable($request);
        }

        return $this->render('index');
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $get = Yii::$app->request->get();
        if (isset($get['page'])) {
            $model->page = $get['page'];
        }

        return $this->render('@app/modules/master/views/report/view', [
            'model' => $model,
            'breadcrumbs' => [
                ['label' => 'Reports', 'url' => Url::toRoute(['/reports/default/index'])],
            ],
        ]);
    }

    /**
     * @param $labNumber
     * @return mixed|string
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionShowPdf($labNumber)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        // Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
//        $history = new History($labNumber);
        $request = Yii::$app->request->queryParams;
        if (isset($request['report2'])) {
            return $this->renderPartial('report2', ['labNumber' => $labNumber]);
        }
        if (isset($request['report1'])) {
            return $this->renderPartial('report', ['labNumber' => $labNumber]);
        }

        //return var_dump($data);
        // get your HTML raw content without any layouts or scripts
//        $trx = Transaction::find()
//            ->where(['Lab_No' => $labNumber])
//            ->with(['oil_mtrx', 'wear_mtrx'])->one();

        $trx = $this->findModel($labNumber);
        if (isset($request['draft'])) {
            $trx->draft = $request['draft'];
        }
        $data['trx'] = $trx;
        $data['history'] = $trx->history;
        $data['two_page'] = isset($request['two_page']) ? $request['two_page'] : 0;
        $data['bp'] = isset($request['bp']) ? $request['bp'] : 0;

//        $other_params = new \app\modules\reports\models\DynamicParameter;
//        $data['other_params'] = $other_params->parameter($data['history']);

        $content = $this->renderPartial('pdf_edit_wisard', $data);
        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::MODE_CORE,
            // A4 paper format
            'format' => [215, 330],
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => [
                '@vendor/bower-asset/bootstrap/dist/css/bootstrap.min.css',
                '@app/web/css/pdf.css',
                '@app/assets/css/pdf.css',
            ],
            // any css to be embedded if required
            // 'cssInline' => '.kv-heading-1{font-size:18px}',
            // set mPDF properties on the fly
            'options' => [
                // 'title' => 'Krajee Report Title'
                'watermarkAngle' => 45,
                'showWatermarkText' => true,
                'watermarkTextAlpha' => 0.1,
            ],
            // call mPDF methods on the fly
            'methods' => [
                'RoundedRect' => [0, 7, 70, 13, 3],
                'SetWatermarkText' => $trx->draft == 1 ? 'DRAFT' : '',
                'SetTitle' => 'Report PDF',
                'SetAuthor' => 'PT. Petrolab Services',
                // 'SetHeader'=>['Krajee Report Header'],
                'SetHTMLFooter' => ['<table style="font-size: 10px;width: 100%"><tr>
                            <td width="33%" align="left"></td><td width="33%" align="center">Page {PAGENO} of {nb}</td>
                            <td width="33%" align="right">' . ($trx->draft == 1 ? /*'RK/7.8/01'*/ 'PS/Formulir/45-01' : /*'RK/7.8/02'*/'PS/Formulir/45-02') . '</td></tr></table>

                        '],
            ]
        ]);
        $pdf->marginTop = 24;
        $pdf->marginRight = 5;
        $pdf->marginLeft = 5;
        $pdf->marginBottom = 15;
        $pdf->marginHeader = 5;
        $pdf->marginFooter = 5;
        $pdf->filename = $trx->branch . '_' . $trx->Lab_No . '.pdf';
        if ($trx->branch == 'PT. GLOBAL MAKARA TEKNIK') {
            $pdf->filename = $trx->UNIT_NO . '_'. $trx->COMPONENT . '_' . $trx->RECV_DT1 . '_' . $trx->Lab_No . '.pdf';
        }
        if ($trx->branch == 'Cakrawala Maju Mapan') {
            $pdf->filename = $trx->branch . '_' . $trx->Lab_No . '_' . $trx->UNIT_LOC . '.pdf';;
        }

        return $pdf->render();

    }

    /**
     * @param $labNo
     * @return array|string
     */
    public function actionFollowup($labNo)
    {
        $report = FormFollowup::findOne(['Lab_No', $labNo]);
        $post = Yii::$app->request->post();

        if (!empty($report) && Yii::$app->request->isAjax && $post) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $valid = ActiveForm::validate($report);
            if (sizeof($valid) > 0) {
                return array_merge($valid, [
                    'status' => $report->save() ? 'success' : 'error',
                    'message' => $report->message,
                    'button' => $report->followup,
                    'modelClose' => 'no',
                ]);
            }

            if ($report->load($post)) {
                return [
                    'status' => $report->save() ? 'success' : 'error',
                    'message' => $report->message,
                    'button' => $report->followup,
                ];
            }
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('followup_form', [
                'model' => $report,
            ]);
        }
        return '';
    }

    /**
     * Finds the Orders model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SearchReport the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PDFReport::find()->where("Lab_No = '$id'")->one()) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     * @throws \PHPExcel_Writer_Exception
     */
    public function actionExportExcel()
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 300);

        $searchModel = new SearchReport();
        $request = Yii::$app->request->get();

        $query = $searchModel->searchData($request, true, true);

        if ($request === null) {
            $query->limit(1000);
        }
        $data = $query->select(
            'tbl_transaction.name,tbl_transaction.EVAL_CODE,tbl_transaction.Lab_No,tbl_transaction.branch,tbl_transaction.address,
            tbl_transaction.MODEL,tbl_transaction.UNIT_NO,tbl_transaction.OIL_TYPE,tbl_transaction.OIL_MATRIX,
            tbl_transaction.UNIT_LOC,tbl_transaction.ComponentID,tbl_transaction.COMPONENT,tbl_transaction.RECOMM1,
            tbl_transaction.RECOMM2,tbl_transaction.ADD_PHYS,tbl_transaction.SAMPL_DT1,
            tbl_transaction.RECV_DT1,tbl_transaction.RPT_DT1,tbl_transaction.HRS_KM_OC,
            tbl_transaction.HRS_KM_TOT,tbl_transaction.oil_change,tbl_transaction.visc_40,
            tbl_transaction.VISC_CST,tbl_transaction.T_A_N,tbl_transaction.T_B_N,tbl_transaction.MAGNESIUM,
            tbl_transaction.CALCIUM,tbl_transaction.ZINC,tbl_transaction.SODIUM_others,
            tbl_transaction.SILICON_others,tbl_transaction.IRON_others,
            tbl_transaction.COPPER_others,tbl_transaction.ALUMINIUM_others,
            tbl_transaction.CHROMIUM_others,tbl_transaction.NICKEL_others,
            tbl_transaction.TIN_others,tbl_transaction.LEAD_others,tbl_transaction.DIR_TRANS,
            tbl_transaction.OXIDATION,tbl_transaction.NITRATION,tbl_transaction.SOX,tbl_transaction.DILUTION,
            tbl_transaction.WATER,tbl_transaction.GLYCOL,tbl_transaction.4um,tbl_transaction.6um,tbl_transaction.15um,
            tbl_transaction.ISO4406,tbl_transaction.seq_I,tbl_transaction.seq_II,tbl_transaction.seq_III,
            tbl_transaction.matrix,tbl_transaction.customer_id')->distinct()->all();

        $toExcel = [];
        foreach ($data as $i => $report) {
            $d = ArrayHelper::toArray($report);
            unset($d['customer_id'], $d['customer'], $d['EVAL_CODE']);
            $oth = '';
            foreach ($report->others as $other) {
                $oth .= "$other->unsur=$other->value; \n";
            }
            $d = ArrayHelper::merge(['No' => $i + 1,
                'Eval Code' => $report->getTextEvalCode($report->EVAL_CODE)], $d);
            $d['Other Analysis'] = $oth;
            $toExcel[] = $d;
        }

        $e = new Excel();

        $p = $e->genExcelByArray($toExcel, 'A1', 'sheet', true);

        $fileName = 'export_excel_' . date('d-m-Y_H-i-s');
        header('Set-Cookie: fileDownload=true; path=/');
        header('Cache-Control: max-age=60, must-revalidate');
        $e->download($p, $fileName);
    }

    /**
     * @param $labNo
     * @return array
     */
    public function actionChart($labNo)
    {

        $param = ['IRON_others', 'COPPER_others', 'ALUMINIUM_others', 'CHROMIUM_others', 'NICKEL_others', 'TIN_others', 'LEAD_others'];

        $unit_id = Transaksi::findOne($labNo)->unit_id;
        $combine = [];
        $data = Transaksi::find()->where(['unit_id' => $unit_id])->orderBy('SAMPL_DT1 DESC')->limit(5)->all();

        foreach (array_reverse($data) as $value) {
            $categories[] = $value->SAMPL_DT1;
        }


        $chart = new Highchart();

        $chart->chart = array(
            'renderTo' => 'container',
            'type' => 'line'
        );
        $chart->title = array(
            'text' => 'WEAR TRENDING'
        );
        // $chart4->subtitle->text = 'Source: WorldClimate.com';
        $chart->xAxis->categories = $categories;
        $chart->yAxis->title->text = 'ppm';
        $chart->xAxis->title->text = 'Sampling Date';
        $chart->tooltip->enabled = true;
        $chart->tooltip->formatter = new HighchartJsExpr(
            "function() {
                return '<b>' + this.series.name + '</b><br/>' +
                this.x + ' (' + this.y +' ppm)';
            }");
        $chart->plotOptions->line->dataLabels->enabled = true;
        $chart->plotOptions->line->enableMouseTracking = true;

        $chart->credits->enabled = false;
        $chart->exporting->enabled = false;

        for ($i = 0; $i <= 6; $i++) {
            $name = explode('_', $param[$i]);
            foreach (array_reverse($data) as $key => $value) {
                $column = $param[$i];
                $val['data'][$key] = ($value->$column != '< 1') ? (int)$value->$column : (int)0;
            }
            array_push($combine, $val);
            $chart->series[] = array(
                'name' => $name[0],
                'data' => $val['data']
            );
        }

        return $this->render('chart', ['chart' => $chart]);
    }

}
