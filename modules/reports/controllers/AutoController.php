<?php
/**
 * Created by
 * User     : Wisard17
 * Date     : 20/06/2019
 * Time     : 12.32 PM
 * File Name: AutoController.php
 **/


namespace app\modules\reports\controllers;


use app\components\ArrayHelper;
use app\components\Excel;
use app\models\AccessControl;
use app\modules\data\models\TypeView;
use app\modules\reports\models\search\SearchData;
use Ghunti\HighchartsPHP\Highchart;
use Ghunti\HighchartsPHP\HighchartJsExpr;
use kartik\mpdf\Pdf;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Class AutoController
 * @package app\modules\reports\controllers
 */
class AutoController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['detail', 'index', 'add', 'edit'],
                'rules' => [
                    [
                        'actions' => ['detail', 'index', 'add', 'edit'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'del' => ['post'],
                ],
            ],
        ];
    }


    /**
     * @return mixed|string
     * @throws NotFoundHttpException
     */
    public function actionIndex()
    {
        $viewTypeName = str_replace('-', ' ', $this->id);
        $viewType = TypeView::find()->where("LOWER(name) = '$viewTypeName'")->one();
        if ($viewType == null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $searchModel = new SearchData();
        $request = Yii::$app->request->queryParams;

        if (Yii::$app->request->isAjax) {
            return $searchModel->searchDataTable($request);
        }

        return $this->render('@app/modules/reports/views/auto/index', ['viewType' => $viewType]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $get = Yii::$app->request->get();
        if (isset($get['page'])) {
            $model->page = $get['page'];
        }

        return $this->render('@app/modules/master/views/report/view', [
            'model' => $model,
            'breadcrumbs' => [
                ['label' => 'Reports', 'url' => Url::toRoute(['/reports/default/index'])],
            ],
        ]);
    }


    /**
     * @param $labNo
     * @return array|string
     */
    public function actionFollowup($labNo)
    {
        $report = FormFollowup::findOne(['Lab_No', $labNo]);
        $post = Yii::$app->request->post();

        if (!empty($report) && Yii::$app->request->isAjax && $post) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $valid = ActiveForm::validate($report);
            if (sizeof($valid) > 0) {
                return array_merge($valid, [
                    'status' => $report->save() ? 'success' : 'error',
                    'message' => $report->message,
                    'button' => $report->followup,
                    'modelClose' => 'no',
                ]);
            }

            if ($report->load($post)) {
                return [
                    'status' => $report->save() ? 'success' : 'error',
                    'message' => $report->message,
                    'button' => $report->followup,
                ];
            }
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('followup_form', [
                'model' => $report,
            ]);
        }
        return '';
    }

    /**
     * Finds the Orders model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SearchReport the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SearchReport::find()->where("Lab_No = '$id'")->one()) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     * @throws \PHPExcel_Writer_Exception
     */
    public function actionExportExcel()
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 300);

        $searchModel = new SearchReport();
        $request = Yii::$app->request->get();

        $query = $searchModel->searchData($request, true, true);

        if ($request === null) {
            $query->limit(1000);
        }
        $data = $query->select(
            'tbl_transaction.name,tbl_transaction.EVAL_CODE,tbl_transaction.Lab_No,tbl_transaction.branch,tbl_transaction.address,
            tbl_transaction.MODEL,tbl_transaction.UNIT_NO,tbl_transaction.OIL_TYPE,tbl_transaction.OIL_MATRIX,
            tbl_transaction.UNIT_LOC,tbl_transaction.ComponentID,tbl_transaction.COMPONENT,tbl_transaction.RECOMM1,
            tbl_transaction.RECOMM2,tbl_transaction.ADD_PHYS,tbl_transaction.SAMPL_DT1,
            tbl_transaction.RECV_DT1,tbl_transaction.RPT_DT1,tbl_transaction.HRS_KM_OC,
            tbl_transaction.HRS_KM_TOT,tbl_transaction.oil_change,tbl_transaction.visc_40,
            tbl_transaction.VISC_CST,tbl_transaction.T_A_N,tbl_transaction.T_B_N,tbl_transaction.MAGNESIUM,
            tbl_transaction.CALCIUM,tbl_transaction.ZINC,tbl_transaction.SODIUM_others,
            tbl_transaction.SILICON_others,tbl_transaction.IRON_others,
            tbl_transaction.COPPER_others,tbl_transaction.ALUMINIUM_others,
            tbl_transaction.CHROMIUM_others,tbl_transaction.NICKEL_others,
            tbl_transaction.TIN_others,tbl_transaction.LEAD_others,tbl_transaction.DIR_TRANS,
            tbl_transaction.OXIDATION,tbl_transaction.NITRATION,tbl_transaction.SOX,tbl_transaction.DILUTION,
            tbl_transaction.WATER,tbl_transaction.GLYCOL,tbl_transaction.4um,tbl_transaction.6um,tbl_transaction.15um,
            tbl_transaction.ISO4406,tbl_transaction.seq_I,tbl_transaction.seq_II,tbl_transaction.seq_III,
            tbl_transaction.matrix,tbl_transaction.customer_id')->distinct()->all();

        $toExcel = [];
        foreach ($data as $i => $report) {
            $dom = new \DOMDocument();
            $d = ArrayHelper::toArray($report);
            unset($d['customer_id'], $d['customer'], $d['EVAL_CODE']);
            $dom->loadHTML($report->status);
            $oth = '';
            foreach ($report->others as $other) {
                $oth .= "$other->unsur=$other->value; \n";
            }
            $d = ArrayHelper::merge(['No' => $i + 1,
                'Eval Code' => $dom->getElementsByTagName('span')->item(0)->nodeValue], $d);
            $d['Other Analysis'] = $oth;
            $toExcel[] = $d;
        }

        $e = new Excel();

        $p = $e->genExcelByArray($toExcel, 'A1', 'sheet', true);

        $fileName = 'export_excel_' . date('d-m-Y_H-i-s');
        header('Set-Cookie: fileDownload=true; path=/');
        header('Cache-Control: max-age=60, must-revalidate');
        $e->download($p, $fileName);
    }

    /**
     * @param $labNo
     * @return array
     */
    public function actionChart($labNo)
    {

        $param = ['IRON_others', 'COPPER_others', 'ALUMINIUM_others', 'CHROMIUM_others', 'NICKEL_others', 'TIN_others', 'LEAD_others'];

        $unit_id = Transaksi::findOne($labNo)->unit_id;
        $combine = [];
        $data = Transaksi::find()->where(['unit_id' => $unit_id])->orderBy('SAMPL_DT1 DESC')->limit(5)->all();

        foreach (array_reverse($data) as $value) {
            $categories[] = $value->SAMPL_DT1;
        }


        $chart = new Highchart();

        $chart->chart = array(
            'renderTo' => 'container',
            'type' => 'line'
        );
        $chart->title = array(
            'text' => 'WEAR TRENDING'
        );
        // $chart4->subtitle->text = 'Source: WorldClimate.com';
        $chart->xAxis->categories = $categories;
        $chart->yAxis->title->text = 'ppm';
        $chart->xAxis->title->text = 'Sampling Date';
        $chart->tooltip->enabled = true;
        $chart->tooltip->formatter = new HighchartJsExpr(
            "function() {
                return '<b>' + this.series.name + '</b><br/>' +
                this.x + ' (' + this.y +' ppm)';
            }");
        $chart->plotOptions->line->dataLabels->enabled = true;
        $chart->plotOptions->line->enableMouseTracking = true;

        $chart->credits->enabled = false;
        $chart->exporting->enabled = false;

        for ($i = 0; $i <= 6; $i++) {
            $name = explode('_', $param[$i]);
            foreach (array_reverse($data) as $key => $value) {
                $column = $param[$i];
                $val['data'][$key] = ($value->$column != '< 1') ? (int)$value->$column : (int)0;
            }
            array_push($combine, $val);
            $chart->series[] = array(
                'name' => $name[0],
                'data' => $val['data']
            );
        }

        return $this->render('chart', ['chart' => $chart]);
    }
}