<?php

namespace app\modules\reports;


use app\models\AccessControl;
use app\modules\data\models\TypeView;
use yii\base\Application;
use yii\base\BootstrapInterface;

/**
 * reports module definition class
 */
class Module extends \yii\base\Module implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\reports\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->controllerMap = array_merge($this->controllerMap, $this->renderController());
    }

    public function renderController()
    {
        $out = [];
        foreach (TypeView::find()->all() as $item) {
            $name = $item->nameController();
            $out[$name] = 'app\modules\reports\controllers\AutoController';
        }
        return $out;
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Bootstrap method to be called during application bootstrap stage.
     * @param Application $app the application currently running
     */
    public function bootstrap($app)
    {
        if ($app instanceof Application) {
            $app->getUrlManager()->addRules([
                ['class' => 'yii\web\UrlRule', 'pattern' => $this->id . '/used-oil', 'route' => $this->id . '/default'],
                ['class' => 'yii\web\UrlRule', 'pattern' => $this->id . '/used-oil' . '/<id:\d+>/<action:(edit|del|print)>', 'route' => $this->id . '/default/<action>'],
                ['class' => 'yii\web\UrlRule', 'pattern' => $this->id . '/used-oil' . '/<id:\d+>', 'route' => $this->id . '/default/view'],
                ['class' => 'yii\web\UrlRule', 'pattern' => $this->id . '/used-oil' . '/<action:[\w\-]+>', 'route' => $this->id . '/default/<action>'],
                ['class' => 'yii\web\UrlRule', 'pattern' => $this->id . '/<controller:[\w\-]+>', 'route' => $this->id . '/<controller>/index'],
                ['class' => 'yii\web\UrlRule', 'pattern' => $this->id . '/<controller:[\w\-]+>' . '/<id:\d+>', 'route' => $this->id . '/<controller>/view'],
                ['class' => 'yii\web\UrlRule', 'pattern' => $this->id . '/<controller:[\w\-]+>' . '/<id:\d+>/<action:(edit|del|print)>', 'route' => $this->id . '/<controller>/<action>'],
                ['class' => 'yii\web\UrlRule', 'pattern' => $this->id . '/<controller:[\w\-]+>/<action:[\w\-]+>', 'route' => $this->id . '/<controller>/<action>'],
            ], false);
        }
    }
}
