<?php

/**
 * Created by PhpStorm.
 * User: Wisard17
 * Date: 21/11/2018
 * Time: 11.30 AM
 */

/* @var $this \yii\web\View */
/* @var $model \app\modules\master\models\FormTransaksi|\yii\db\ActiveRecord */
/* @var $other \app\modules\master\models\OthersOrder */
/* @var $spaceTable void */

use app\helpers\DynamicTable;
?>

<tr class="tr_body">
    <td class="td_title"><?= $other->unsur ?></td>
    <td class="text-center"><?= $other->unit ?></td>
    <td class="text-center"><?= $other->methode ?></td>
    <?php foreach ($model->history as $tr) {
        $ot = isset($tr->otherHis[$other->unsur]) ? $tr->otherHis[$other->unsur] : ['value' => '', 'code' => ''];
        echo DynamicTable::tdValue($ot['code'], $ot['value']);
    } ?>
    <?= $spaceTable ?>
    <td class="text-center"><?= $other->getLimit('attention') ?></td>
    <td class="text-center"><?= $other->getLimit('urgent') ?></td>
</tr>
