<?php

use app\modules\master\models\Matrix;
use app\modules\reports\models\Addition;
use app\modules\reports\models\RenderPDF;
use app\modules\reports\models\Report;
use app\components\XYChart;

/**
 * Created by
 * User: Wisard17
 * Date: 03/08/2018
 * Time: 01.22 PM
 */

/* @var $this \yii\web\View */

/**
 * @param $a
 * @param $j
 * @param $cellTitle
 * @return string
 */
function process($a, $j, $cellTitle)
{
    $cpt = $j;
    $find = 'false';

    while (($cpt > 0) and ($find === 'false')) {
        if ($cellTitle[22] === 'IRON') {
            $find = 'true';
        }
        --$cpt;
    }

    if ($find === 'true') {
        return $a . '/C';
    } else {
        return $a . 'z';
    }
}

function alarm($value, $code)
{
    if ($code == '') {
        return $value;
    } else {
        return $value . ' / ' . $code;
    }
}

function chartbaru($vdatamat, $vlabel, $vfile, $vtitle, $vtoptitle, $jml)
{

    $labels = $vlabel;
    $k = 0;

    while ($k < $jml) {
        $data1[$k] = @$vdatamat[1][$k];
        $data2[$k] = @$vdatamat[2][$k];
        $data3[$k] = @$vdatamat[3][$k];
        $data4[$k] = @$vdatamat[4][$k];
        $data5[$k] = @$vdatamat[5][$k];
        $data6[$k] = @$vdatamat[6][$k];
        $data7[$k] = @$vdatamat[7][$k];
        $k++;
    }

    $c = new XYChart(700, 230, 0xffffff, 0x000000, 0);
    $c->setPlotArea(50, 50, 610, 160, 0xffffff, -1, -1, 0xc0c0c0, -1);
    $legendObj = $c->addLegend(30, 14, false, "", 9);
    $legendObj->setBackground(Transparent);
    $c->addTitle2(Top, $vtoptitle, 'arialbd.ttf', 12, 0xffffff, 0x31319c);
    $c->xAxis->setLabels($labels);
    $layer = $c->addLineLayer();

    $dataSetObj = $layer->addDataSet($data1, 0x0033FF, @$vtitle[1]);
    $dataSetObj->setDataSymbol('1', 9);
    $dataSetObj = $layer->addDataSet($data2, 0x00CC66, @$vtitle[2]);
    $dataSetObj->setDataSymbol('1', 9);
    $dataSetObj = $layer->addDataSet($data3, 0x993300, @$vtitle[3]);
    $dataSetObj->setDataSymbol('1', 9);
    $dataSetObj = $layer->addDataSet($data4, 0xFFCC00, @$vtitle[4]);
    $dataSetObj->setDataSymbol('1', 9);
    $dataSetObj = $layer->addDataSet($data5, 0xFF0000, @$vtitle[5]);
    $dataSetObj->setDataSymbol('1', 9);
    $dataSetObj = $layer->addDataSet($data6, 0x000000, @$vtitle[6]);
    $dataSetObj->setDataSymbol('1', 9);
    $dataSetObj = $layer->addDataSet($data7, 0xFF00FF, @$vtitle[7]);
    $dataSetObj->setDataSymbol('1', 9);

    $c->makeChart2('PNG');

    $c->makeChart($vfile);
    $im = imagecreatefrompng($vfile);
    $im2 = imagecrop($im, ['x' => 0, 'y' => 0, 'width' => imagesx($im), 'height' => (imagesy($im) - 8.5)]);
    if ($im2 !== FALSE) {
        imagepng($im2, $vfile);
        imagedestroy($im2);
    }
    imagedestroy($im);
}

function fSeq_I($seq_I)
{
    $panjang = strlen($seq_I);
    $i = 0;
    $find = 0;

    while (($i < $panjang) && ($find == 0)) {
        $vchar = substr($seq_I, $i, 1);
        if ($vchar == '/') {
            $find = 1;
        } else {
            $i++;
        }
    }

    $vchar = substr($seq_I, 0, $i);
    return $vchar;
}

function hitung_others($col1, $col2, $col3, $col4, $col5, $lab_no)
{
    $conn = Yii::$app->db;

    $colomn[1] = $col1;
    $colomn[2] = $col2;
    $colomn[3] = $col3;
    $colomn[4] = $col4;
    $colomn[5] = $col5;

    $colomnname[1] = 'col1';
    $colomnname[2] = 'col2';
    $colomnname[3] = 'col3';
    $colomnname[4] = 'col4';
    $colomnname[5] = 'col5';

    $sqlupdate = "update tbl_otherorder_lu set col1='',col2='',col3='',col4='',col5=''";
    $hasil = $conn->createCommand($sqlupdate)->query();

    $i = 1;
    while ($i <= 5) {

//  $sqlstr = "select unsur,unit,methode,value,attention,urgent from tbl_otherorder  where Lab_No='$colomn[$i]'";
        $sqlstr = "select unsur,unit,methode,value,value_CODE,attention,urgent,severe from tbl_otherorder  where Lab_No='$lab_no'";
        $hasil = $conn->createCommand($sqlstr)->queryAll();

        if (empty($hasil))
            return ['', '', '', '', '', '', '', ''];
        foreach ($hasil as $rowresume) {
            //  list($unsur,$unit,$methode,$value,$attention,$urgent) = $rowresume;
//            list($unsur, $unit, $methode, $value, $value_CODE, $attention, $urgent, $severe) = $rowresume;
            $sqlupdate = "update tbl_otherorder_lu set $colomnname[$i]='$rowresume[value]' where unsur='$rowresume[unsur]'";
            $conn->createCommand($sqlupdate)->query();
        }
        $i++;
        return ['', '', '', '', '', '', '', ''];
//        return [$unsur, $unit, $methode, $value, $value_CODE, $attention, $urgent, $severe];
    }

}


function Cari_Other1($data9, $lab_no, $Others, $Jml, $ke)
{
//   include("ewcfg12.php");
    global $EW_CONN;
    $dbserver = $EW_CONN["DB"]["host"];
    $dbuser = $EW_CONN["DB"]["user"];
    $dbpassword = $EW_CONN["DB"]["pass"];
    $dbdatabase = $EW_CONN["DB"]["db"];


    $conn = mysqli_Connect($dbserver . ':3306', $dbuser, $dbpassword, $dbdatabase);

//  $pilih     = mysqli_select_db($dbdatabase);

// $sqlstr = "select unsur,unit,methode,value,attention,urgent from tbl_otherorder  where Lab_No='$lab_no'";
    $sqlstr = "select unsur,unit,methode,value,value_CODE,attention,urgent,severe from tbl_otherorder  where Lab_No='$lab_no'";

    $hasilresume = mysqli_query($conn, $sqlstr);
    $i = 900;
    $Temp = 0;
    while ($rowresume = mysqli_fetch_row($hasilresume)) {
        $i++;
        // list($unsur,$unit,$methode,$value,$attention,$urgent) = $rowresume;
        list($unsur, $unit, $methode, $value, $value_CODE, $attention, $urgent, $severe) = $rowresume;
        $Temp = 1;
        if ($ke == 1)
//      $data9[$i] = array($unsur,$unit,$methode,$value,'','','','',$attention,$urgent);
            $data9[$i] = array($unsur, $unit, $methode, $value . '/ ' . $value_CODE, '', '', '', '', $attention, $urgent);

        if ($ke == 2)
            // $data9[$i] = array($unsur,$unit,$methode,'',$value,'','','',$attention,$urgent);
            $data9[$i] = array($unsur, $unit, $methode, '', $value . '/ ' . $value_CODE, '', '', '', $attention, $urgent);

        if ($ke == 3)
            //$data9[$i] = array($unsur,$unit,$methode,'','',$value,'','',$attention,$urgent);
            $data9[$i] = array($unsur, $unit, $methode, '', '', $value . '/ ' . $value_CODE, '', '', $attention, $urgent);

        if ($ke == 4)
            // $data9[$i] = array($unsur,$unit,$methode,'','','',$value,'',$attention,$urgent);
            $data9[$i] = array($unsur, $unit, $methode, '', '', '', $value . '/ ' . $value_CODE, '', $attention, $urgent);

        if ($ke == 5)
            //$data9[$i] = array($unsur,$unit,$methode,'','','','',$value,$attention,$urgent);
            $data9[$i] = array($unsur, $unit, $methode, '', '', '', '', $value . '/ ' . $value_CODE, $attention, $urgent);

        if ($ke == 6)
            // $data9[$i] = array($unsur,$unit,$methode,'','','','',$value,$attention,$urgent);
            $data9[$i] = array($unsur, $unit, $methode, '', '', '', '', $value . '/ ' . $value_CODE, $attention, $urgent);

        if ($ke == 7)
            //   $data9[$i] = array($unsur,$unit,$methode,'','','','',$value,$attention,$urgent);
            $data9[$i] = array($unsur, $unit, $methode, '', '', '', '', $value . '/ ' . $value_CODE, $attention, $urgent);
    }

    while ($i < 905) {
        $i++;
        $data9[$i] = array('', '', '', '', '', '', '', '', '', '');
    }
    $Others = $Temp;
    $Jml = $i - 900;
}

/**
 * @param $bundle_lab_no
 * @return mixed
 * @throws \yii\db\Exception
 */
function beta_others($bundle_lab_no)
{


    $conn = Yii::$app->db;
    $data9 = [];
//  $pilih     = mysqli_select_db($dbdatabase);
    $where_in = join("','", $bundle_lab_no);
    $sqlstr = "select * from tbl_otherorder where Lab_no in ('$where_in') group by unsur";
    $hasilresume = $conn->createCommand($sqlstr);
    $i = 1;
    $unsur_loop = [];
    foreach ($hasilresume->queryAll() as $unsur_row) {
        if ($i < 10) {
            $data9[$i] = array($unsur_row['unsur'], $unsur_row['unit'], $unsur_row['methode']);
            $unsur_loop[] = $unsur_row['unsur'];
        }
        $i++;
    }

    $i = 1;
    foreach ($unsur_loop as $value_unsur) {

        $attention = "";
        $urgent = "";
        $index_data = 3;
        foreach ($bundle_lab_no as $value_labno) {
            $sqlstr = "select * from tbl_otherorder where Lab_no ='$value_labno' and unsur='$value_unsur'";
            $query = $conn->createCommand($sqlstr);
            $mysqli_array = $query->queryOne();
            //$mysqli_row = sizeof($mysqli_array);
            $mysqli_row = ($mysqli_array != NULL)? sizeof($mysqli_array) : 0 ;
            if ($mysqli_row > 0) {
                if (!empty($mysqli_array['value_CODE'])) {
                    $data9[$i][$index_data] = $mysqli_array['value'] . ' / ' . $mysqli_array['value_CODE'];
                } else {
                    $data9[$i][$index_data] = $mysqli_array['value'];
                }
                if (!empty($mysqli_array['attention'])) {
                    $attention = $mysqli_array['attention'];
                }
                if (!empty($mysqli_array['urgent'])) {
                    $urgent = $mysqli_array['urgent'];
                }
            } else {
                $data9[$i][$index_data] = "-";
            }
            $index_data++;
        }
        for ($index_data; $index_data < 10; $index_data++) {
            $data9[$i][$index_data] = "";
        }
        $data9[$i][8] = $attention;
        $data9[$i][9] = $urgent;

        $i++;
    }
    while ($i < 10) {
        $data9[$i] = array('', '', '', '', '', '', '', '', '', '');
        $i++;
    }

//HILMI
    return $data9;
}

function Cari_Other($data9, $lab_no, $Others, $Jml, $ke)
{

    $conn = Yii::$app->db;

//  $pilih     = mysqli_select_db($dbdatabase);

    //$sqlstr = "select unsur,unit,methode,attention,urgent, col1,col2,col3,col4,col5,attention,urgent from tbl_otherorder_lu  where col1<>'' or col2<>'' or col3<>'' or col4<>'' or col5<>''";
    $sqlstr = "select unsur,unit,methode,value,value_CODE,attention,urgent,severe from tbl_otherorder  where Lab_No='$lab_no'";

    $hasilresume = $conn->createCommand($sqlstr);
    $data9[901] = array("", "", "", "", "", "", "", "", "", "");
    $data9[902] = array("", "", "", "", "", "", "", "", "", "");
    $data9[903] = array("", "", "", "", "", "", "", "", "", "");
    $data9[904] = array("", "", "", "", "", "", "", "", "", "");
    $data9[905] = array("", "", "", "", "", "", "", "", "", "");
    $data9[906] = array("", "", "", "", "", "", "", "", "", "");
    $data9[907] = array("", "", "", "", "", "", "", "", "", "");

    $i = 900;
    $Temp = 0;

    foreach ($hasilresume as $rowresume) {
        $i++;
        /*    list($unsur,$unit,$methode,$attention,$urgent,$col1,$col2,$col3,$col4,$col5,$attention,$urgent) = $rowresume;
           $data9[$i] = array($unsur,$unit,$methode,$col1,$col2,$col3,$col4,$col5,$attention,$urgent);	*/
        list($unsur, $unit, $methode, $value, $value_CODE, $attention, $urgent, $severe) = array_values($rowresume);
        /*   $data9[$i] = array($unsur,$unit,$methode,$value.' / '.$value_CODE,$attention,$urgent);	*/
///tdk  $data9[$i] = array($unsur,$unit,$methode,$col1,$col2,$col3,$col4,$col5,5,6);
        $Temp = 1;
        if ($value_CODE != '') {
            if ($ke == 1)
//      $data9[$i] = array($unsur,$unit,$methode,$value,'','','','',$attention,$urgent);
                $data9[$i] = array($unsur, $unit, $methode, $value . ' / ' . $value_CODE, '', '', '', '', $attention, $urgent);

            if ($ke == 2)
                // $data9[$i] = array($unsur,$unit,$methode,'',$value,'','','',$attention,$urgent);
                $data9[$i] = array($unsur, $unit, $methode, '', $value . ' / ' . $value_CODE, '', '', '', $attention, $urgent);

            if ($ke == 3)
                //$data9[$i] = array($unsur,$unit,$methode,'','',$value,'','',$attention,$urgent);
                $data9[$i] = array($unsur, $unit, $methode, '', '', $value . ' / ' . $value_CODE, '', '', $attention, $urgent);

            if ($ke == 4)
                // $data9[$i] = array($unsur,$unit,$methode,'','','',$value,'',$attention,$urgent);
                $data9[$i] = array($unsur, $unit, $methode, '', '', '', $value . ' / ' . $value_CODE, '', $attention, $urgent);

            if ($ke == 5)
                //$data9[$i] = array($unsur,$unit,$methode,'','','','',$value,$attention,$urgent);
                $data9[$i] = array($unsur, $unit, $methode, '', '', '', '', $value . ' / ' . $value_CODE, $attention, $urgent);

            if ($ke == 6)
                // $data9[$i] = array($unsur,$unit,$methode,'','','','',$value,$attention,$urgent);
                $data9[$i] = array($unsur, $unit, $methode, '', '', '', '', $value . ' / ' . $value_CODE, $attention, $urgent);

            if ($ke == 7)
                //   $data9[$i] = array($unsur,$unit,$methode,'','','','',$value,$attention,$urgent);
                $data9[$i] = array($unsur, $unit, $methode, '', '', '', '', $value . ' / ' . $value_CODE, $attention, $urgent);
        } else {

            if ($ke == 1)
//      $data9[$i] = array($unsur,$unit,$methode,$value,'','','','',$attention,$urgent);
                ///	$data9[$i] = array($unsur,$unit,$methode,$value.' / '.$value_CODE,'','','','',$attention,$urgent);
                $data9[$i] = array($unsur, $unit, $methode, $value, '', '', '', '', $attention, $urgent);

            if ($ke == 2)
                // $data9[$i] = array($unsur,$unit,$methode,'',$value,'','','',$attention,$urgent);
                ///   $data9[$i] = array($unsur,$unit,$methode,'',$value.' / '.$value_CODE,'','','',$attention,$urgent);
                $data9[$i] = array($unsur, $unit, $methode, '', $value, '', '', '', $attention, $urgent);

            if ($ke == 3)
                //$data9[$i] = array($unsur,$unit,$methode,'','',$value,'','',$attention,$urgent);
                ///   $data9[$i] = array($unsur,$unit,$methode,'','',$value.' / '.$value_CODE,'','',$attention,$urgent);
                $data9[$i] = array($unsur, $unit, $methode, '', '', $value, '', '', $attention, $urgent);

            if ($ke == 4)
                // $data9[$i] = array($unsur,$unit,$methode,'','','',$value,'',$attention,$urgent);
                ///  $data9[$i] = array($unsur,$unit,$methode,'','','',$value.' / '.$value_CODE,'',$attention,$urgent);
                $data9[$i] = array($unsur, $unit, $methode, '', '', '', $value, '', $attention, $urgent);

            if ($ke == 5)
                //$data9[$i] = array($unsur,$unit,$methode,'','','','',$value,$attention,$urgent);
                ///  $data9[$i] = array($unsur,$unit,$methode,'','','','',$value.' / '.$value_CODE,$attention,$urgent);
                $data9[$i] = array($unsur, $unit, $methode, '', '', '', '', $value, $attention, $urgent);

            if ($ke == 6)
                // $data9[$i] = array($unsur,$unit,$methode,'','','','',$value,$attention,$urgent);
                ///   $data9[$i] = array($unsur,$unit,$methode,'','','','',$value.' / '.$value_CODE,$attention,$urgent);
                $data9[$i] = array($unsur, $unit, $methode, '', '', '', '', $value, $attention, $urgent);

            if ($ke == 7)
                //   $data9[$i] = array($unsur,$unit,$methode,'','','','',$value,$attention,$urgent);
                ///   $data9[$i] = array($unsur,$unit,$methode,'','','','',$value.' / '.$value_CODE,$attention,$urgent);
                $data9[$i] = array($unsur, $unit, $methode, '', '', '', '', $value, $attention, $urgent);
        }

    }


//    $Others = $Temp;
    //$Jml=$i-900;
    //$Jml=7;

    while ($i < 905) {
        $i++;
        $data9[$i] = array('', '', '', '', '', '', '', '', '', '');
    }
    $Others = $Temp;
    $Jml = $i - 900;
}

/**
 * @param $pdf RenderPDF
 * @param $noreport
 * @param $branch_nm
 * @param $component
 * @param $name
 * @param $matrik
 * @param $vaddress_nm
 * @param $oil_matrix
 * @param $model
 * @param $oil_type
 * @param $unit_no
 * @param $unit_loc
 * @param $evalcode
 */
function callheaderreport($pdf, $noreport,
                          $branch_nm,
                          $component,
                          $name,
                          $matrik,
                          $vaddress_nm,
                          $oil_matrix,
                          $model,
                          $oil_type,
                          $unit_no,
                          $unit_loc,
                          $evalcode, $showeval = 1)
{
    $pdf->SetY(1.7);
    $pdf->Image('image/logo.png', 0.5, 1, 12);
    $pdf->SetLineWidth(0.025);
    $pdf->SetFillColor(225);
    $pdf->RoundedRect(14.65, 1.5, 6.18, 0.9, 0.15, 'DF');
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Cell(1.87, 0.4, '', 0, 0, 'L');
    $pdf->Cell(9.35, -0.3, '', 0, 0, 'L');

    if ($noreport <> '') {
        $pdf->SetFont('Arial', 'UB', 12);
        $pdf->Cell(11, 0.3, 'OIL ANALYSIS REPORT', 0, 0, 'C');
    } else {
        $pdf->SetFont('Arial', 'B', 12);
        $pdf->Cell(11, 0.5, 'OIL ANALYSIS REPORT', 0, 0, 'C');
    }

    $pdf->Ln(0);
    $pdf->Cell(1.87, 0.4, '', 0, 0, 'L');
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Cell(2.4);
    $pdf->Cell(6.18, 0.5, '', 0, 1, 'C');
    $pdf->SetFont('Arial', 'B', 12);
    $pdf->Ln(-0.24);
    $pdf->Cell(1.87, 0.4, '', 0, 0, 'L');
    $pdf->SetFont('Arial', 'B', 6.25);
    $pdf->Ln(0);
    $pdf->Cell(1.87, 0.4, '', 0, 0, 'L');
    $pdf->SetFont('Arial', 'B', 6.25);
    $pdf->SetFont('Arial', '', 7);
    $pdf->Cell(30, 0.5, $noreport, 0, 0, 'C');
    $pdf->SetFont('Arial', '', 7);
    $pdf->SetLineWidth(0.05);
    //$pdf->Line(20.80, 2, 1, 2);
    //$pdf->Line(20.80, 3.35, 1, 3.35);
    $pdf->Line(20.80, 2.67, 1, 2.67);
    $pdf->Line(20.80, 4.3, 1, 4.3);
    $pdf->Ln(0.93);

    //line 1
    $pdf->Cell(0.2, 0.13, '', 0, 0, 'L');
    $pdf->Cell(2.4, 0.13, 'Customer Name', 0, 0, 'L');
    $pdf->Cell(9.2, 0.13, ' :  ' . $branch_nm, 0, 0, 'L');
    $pdf->Cell(2.4, 0.13, 'Component', 0, 0, 'L');
    $pdf->Cell(4.2, 0.13, ' :  ' . $component, 0, 0, 'L');

    // $pdf->Cell(2.4, 0.13, 'Viscosity@100\B0C', 0, 0, 'L');
    // $pdf->Cell(2.4, 0.13, ' :  ' . $visc_100, 0, 0, 'L');

    //line 2
    $pdf->Ln(0.27);
    $pdf->Cell(0.2, 0.13, '', 0, 0, 'L');
    $pdf->Cell(2.4, 0.13, 'For Attention', 0, 0, 'L');
    $pdf->Cell(9.2, 0.13, ' :  ' . $name, 0, 0, 'L');
    $pdf->Cell(2.4, 0.13, 'Component Matrix', 0, 0, 'L');
    $pdf->Cell(4.2, 0.13, ' :  ' . $matrik, 0, 0, 'L');

    //  $pdf->Cell(2.4, 0.13, 'Viscosity@40\B0C', 0, 0, 'L');
    //  $pdf->Cell(2.4, 0.13, ' :  ' . $visc_40, 0, 0, 'L');

    //line 3
    $pdf->Ln(0.27);
    $pdf->Cell(0.2, 0.3, '', 0, 0, 'L');
    $pdf->Cell(2.4, 0.13, 'Address');
    $pdf->Cell(9.2, 0.13, ' :  ' . $vaddress_nm);
    $pdf->Cell(2.4, 0.13, 'Oil Matrix', 0, 0, 'L');
    $pdf->Cell(2.4, 0.13, ' :  ' . $oil_matrix, 0, 0, 'L');
    //  $pdf->Cell(2.4, 0.13, $vcaption, 0, 0, 'L');
    //  $pdf->Cell(2.4, 0.13, ' :  ' . $vvalue, 0, 0, 'L');

    //line 4
    $pdf->Ln(0.27);
    $pdf->Cell(0.2, 0.13, '', 0, 0, 'L');
    $pdf->Cell(2.4, 0.13, 'Unit Model', 0, 0, 'L');
    $pdf->Cell(9.2, 0.13, ' :  ' . $model, 0, 0, 'L');
    $pdf->Cell(2.4, 0.13, 'Lube Oil Name', 0, 0, 'L');
    $pdf->Cell(2.4, 0.13, ' :  ' . $oil_type, 0, 0, 'L');

    //line 5
    $pdf->Ln(0.27);
    $pdf->Cell(0.2, 0.13, '', 0, 0, 'L');
    $pdf->Cell(2.4, 0.13, 'Unit Number', 0, 0, 'L');
    $pdf->Cell(9.2, 0.13, ' :  ' . $unit_no, 0, 0, 'L');
    $pdf->Cell(2.4, 0.13, 'Location', 0, 0, 'L');
    $pdf->Cell(2.4, 0.13, ' :  ' . $unit_loc, 0, 0, 'L');
    $pdf->Ln(0.43);

    $x = $pdf->GetY();
    $pdf->SetY($x);
    $pdf->SetFont('Arial', 'B', 8);
    $j = 1;
    $pdf->Cell(17);
    $pdf->SetFont('Arial', 'B', 6);
    if ($showeval == 1) {
        $pdf->Cell(3, 0.5, 'Overall Analysis Result:', 0, 0, 'C');

        if ($evalcode == "D") {
            $eval = "SEVERE";
            $pdf->Ln(0);
            $pdf->Image('image/merah.jpg', 18.85, 4.85, 1.15);
            $pdf->SetFont('Arial', 'B', 9);
            $pdf->Ln(0.3);
            $pdf->Cell(17);
            $pdf->Cell(3, 4.8, 'SEVERE', 0, 0, 'C');
            $pdf->Ln(-0.6);
        }
        if (($evalcode == "C") or ($evalcode == "U")) {
            $eval = "URGENT";
            $pdf->Ln(0);
            $pdf->Image('image/merah.jpg', 18.85, 4.85, 1.15);
            $pdf->SetFont('Arial', 'B', 9);
            $pdf->Ln(0.3);
            $pdf->Cell(17);
            $pdf->Cell(3, 4.8, 'URGENT', 0, 0, 'C');
            $pdf->Ln(-0.6);
        }
        if ($evalcode == "B" || $evalcode == 'A') {
            $eval = "ATTENTION";
            $pdf->Ln(0);
            $pdf->Image('image/kuning.jpg', 18.85, 4.85, 1.15);
            $pdf->SetFont('Arial', 'B', 9);
            $pdf->Ln(0.3);
            $pdf->Cell(17);
            $pdf->Cell(3, 4.8, 'ATTENTION', 0, 0, 'C');
            $pdf->Ln(-0.6);
        }
        if ($evalcode == "N") {
            $eval = "NORMAL";
            $pdf->Ln(0);
            $pdf->Image('image/biru.jpg', 19, 4.85, 1.15);
            $pdf->SetFont('Arial', 'B', 9);
            $pdf->Ln(0.3);
            $pdf->Cell(17);
            $pdf->Cell(3, 4.8, 'NORMAL', 0, 0, 'C');
            $pdf->Ln(-0.6);
        }
        if ($evalcode == "") {
            $eval = "NORMAL";
            $pdf->Ln(0);
            $pdf->Image('image/biru.jpg', 18.85, 4.85, 1.15);
            $pdf->SetFont('Arial', 'B', 9);
            $pdf->Ln(0.3);
            $pdf->Cell(17);
            $pdf->Cell(3, 4.8, 'NORMAL', 0, 0, 'C');
            $pdf->Ln(-0.6);
        }
    } else {
        $pdf->Ln(0);

        $pdf->SetFont('Arial', 'B', 9);
        $pdf->Ln(0.3);
        $pdf->Cell(17);
        $pdf->Cell(3, 4.8, '', 0, 0, 'C');
        $pdf->Ln(-0.6);
    }
    $pdf->Ln(-4.5);
    $lebar = 0.30;
    $pdf->Ln();
    $data1 = NULL;
}

function show_rpt_5($lab_no, $vtgl_1, $vtgl_2, $vgroup, $vcust_id, $get_true, $draft)
{

    $bintang = "";
    if (empty($get_true)) {
        $bintang = " *)";
    } else {
        $bintang = " *)";
    }

    $dbdirgraph = Yii::getAlias('@app/web') . '/assets/';
    $kertas = 'f4';//[21.5, 33];

    $pdf = new RenderPDF('P', 'cm', $kertas);
    $pdf->SetTitle('Report PDF');
//    $pdf->Open();
//    $pdf->AddPage();


    if ($vcust_id != '') {
        $cond_cust_id = " and customer_id=" . $vcust_id . " ";
    } else {
        $cond_cust_id = " ";
    }

    if (($vgroup != '') && ($vtgl_1 != '') && ($vtgl_2 != ''))
        $conditionresume = "  RPT_DT1 >= '" . $vtgl_1 . "' and RPT_DT1 <='" . $vtgl_2 . "'" . $cond_cust_id . " and grouploc like '%" . $vgroup . "%' order by grouploc , lab_no ";
    else if (($vtgl_1 != '') && ($vtgl_2 != ''))
        $conditionresume = "  RPT_DT1 >= '" . $vtgl_1 . "' and RPT_DT1 <='" . $vtgl_2 . "'" . $cond_cust_id . "order by grouploc , lab_no ";
    else
        $conditionresume = " Lab_No ='" . $lab_no . "'";

    $rpt = Report::find()->select(['grouploc', 'Lab_No', 'SAMPL_DT1'])->where($conditionresume)->all();
    $ii = 0;
    $conn = Yii::$app->db;

    foreach ($rpt as $rowresume) {
        $data1 = NULL;
        $data2 = NULL;
        $data3 = NULL;
        $data4 = NULL;
        $data5 = NULL;
        $data6 = NULL;
        $data7 = NULL;
        $data8 = NULL;
        $cell = NULL;
        $cell_c = NULL;
        $vdatawearmat = NULL;
        $vdataftirmat = NULL;
        $vdataTAN_TBN = NULL;
        $vdataVISC = NULL;
        $matrix = NULL;
        $minmatrix = NULL;
        $oil_matrix = '';
        $cellTitle = NULL;
        $vlabel = NULL;
        $cellmethod = NULL;
        $coeff = NULL;
        $i = NULL;
        $j = NULL;
        $jml = NULL;
        list($grouploc, $lab_no1, $Sampl_dt1) = [$rowresume->grouploc, $rowresume->Lab_No, $rowresume->SAMPL_DT1];
        $lab_no = $lab_no1;
        $pdf->AddPage();
        $ii++;

        list($vcode1, $vcode2, $vcode3, $vowner, $vcompany) = find_rec($lab_no);
        $condition = "";

        if (true) {


            $condition = " where name='" . $vcompany . "' and component='" . $vcode1 . "' and unit_id='" . $vcode2 . "' and Sampl_dt1<='" . $Sampl_dt1 . "' order by Sampl_dt1 desc, lab_no desc ";
            $sqlstr = "select name,branch,address, matrix, model,unit_no,oil_type,oil_matrix,UNIT_LOC,ComponentID,component, recomm1, recomm2,add_phys,Lab_no,Date(Sampl_dt1),Date(recv_dt1),Date(rpt_dt1),HRS_KM_OC,HRS_KM_TOT,oil_change,visc_40,Visc_cst,T_A_N,T_B_N,MAGNESIUM,CALCIUM,ZINC,sodium_others,Silicon_others,Iron_others,Copper_others,Aluminium_others,Chromium_others,nickel_others,TIN_others,lead_others,dir_trans,oxidation,nitration,sox,dilution,water,glycol,4um,6um,15um,ISO4406,seq_I,seq_II,seq_III,eval_code, matrix as ps, visc_index,Molybdenum,Boron,phosphor,Barium,SILVER  from tbl_transaction " . $condition;

            $hasil = $conn->createCommand($sqlstr)->queryAll();
            $hasil = ($hasil != NULL)? $hasil : 0;
            $jml = sizeof($hasil) >= 4 ? 4 : sizeof($hasil);
            $i = $jml;
            foreach ($hasil as $row) {
                list($vname, $vbranch_nm, $vaddress_nm, $vmatrix, $vmodel, $vunit_no, $voil_type, $voil_matrix, $vunit_loc, $ComponentID, $vcomponent1, $vrecomm1, $vrecomm2, $add_phys, $cell[$i][101], $cell[$i][102], $cell[$i][103], $cell[$i][104], $cell[$i][105], $cell[$i][106], $cell[$i][107], $cell[$i][201], $cell[$i][202], $cell[$i][203], $cell[$i][204], $cell[$i][301], $cell[$i][302], $cell[$i][303], $cell[$i][401], $cell[$i][402], $cell[$i][501], $cell[$i][502], $cell[$i][503], $cell[$i][504], $cell[$i][505], $cell[$i][506], $cell[$i][507], $cell[$i][601], $cell[$i][602], $cell[$i][603], $cell[$i][604], $cell[$i][605], $cell[$i][606], $cell[$i][607], $cell[$i][701], $cell[$i][702], $cell[$i][703], $cell[$i][704], $cell[$i][801], $cell[$i][802], $cell[$i][803], $cell[$i][44], $matrixcode, $cell[$i][205], $cell[$i][304], $cell[$i][305], $cell[$i][306], $cell[$i][307], $cell[$i][508]) = array_values($row);
                if ($i == $jml) {
                    $name = $vname;
                    $branch_nm = $vbranch_nm;
                    $address_nm = $vaddress_nm;
                    $matrik = $vmatrix;
                    $model = $vmodel;
                    $unit_no = $vunit_no;
                    $oil_type = $voil_type;
                    $oil_matrix = $voil_matrix;
                    $unit_loc = $vunit_loc;
                    $component = $vcomponent1;
                    $recomm1 = $vrecomm1;
                    $recomm2 = $vrecomm2;
                    $eval = $cell[$i][44];
                    $lab = $cell[$i][101];
                    $seq_I = $cell[$i][801];

//                    $sqlstr_tblbrand = "select visc_100,visc_40,T_A_N,T_B_N  from tbl_oilbrand where oil_type='" . $oil_type . "'";
//                    $hasil_tblbrand = mysqli_query($conn, $sqlstr_tblbrand);
//
//                    if ($row = mysqli_fetch_row($hasil_tblbrand))
//                        list($visc_100, $visc_40, $T_A_N, $T_B_N) = $row;
//
//                    /*        if ($component != 'ENGINE') {
//                               $vcaption = "T A N";
//                               $vvalue   = $T_A_N;
//                             } else {
//                               $vcaption = "T B N";
//                               $vvalue   = $T_B_N;
//                             }
//                   */
//                    if ((strtoupper($component) != 'ENGINE') and (strtoupper($component) != 'ENGINE UPPER') and (strtoupper($component) != 'ENGINE LOWER') and (strtoupper($component) != 'New Oil B')) {
//                        $vcaption = "T A N";
//                        $vvalue = $T_A_N;
//                    } else {
//                        $vcaption = "T B N";
//                        $vvalue = $T_B_N;
//                    }
//
//
//                    $noreport = "";

                    if ($add_phys == "") {
                        $noreport = $add_phys;
                    } else {
                        $noreport = 'No. ' . $add_phys;
                    }
                }
                $i--;
            }

            hitung_others($cell[1][101], @$cell[2][101], @$cell[3][101], @$cell[4][101], @$cell[5][101], $lab_no);

            $sqlstr = "select '' as aa,'' as ab,'' as ac,'' as ad,'' as ae,'' as af,'' as ag,VISC_40_CODE,CST_CODE,TAN_CODE,TBN_CODE,MG_CODE,CA_CODE,ZN_CODE,NA_CODE,SI_CODE,FE_CODE,CU_CODE,AL_CODE,CR_CODE,NI_CODE,SN_CODE,PB_CODE,TRANS_CODE,OXID_CODE,NITR_CODE,SOX_CODE,DILUT_CODE, WTR_CODE, GLY_CODE,4um_code,6um_code,15um_code,ISO4406_CODE,seq_I_code,seq_II_code,seq_III_code,visc_index_code,Molybdenum_CODE,Boron_CODE,phosphor_code,Barium_CODE,AG_CODE  from tbl_transaction " . $condition;
            $hasil = $conn->createCommand($sqlstr)->queryAll();
            $i = $jml;
            foreach ($hasil as $row) {
                list($cell_c[$i][101], $cell_c[$i][102], $cell_c[$i][103], $cell_c[$i][104], $cell_c[$i][105], $cell_c[$i][106], $cell_c[$i][107], $cell_c[$i][201], $cell_c[$i][202], $cell_c[$i][203], $cell_c[$i][204], $cell_c[$i][301], $cell_c[$i][302], $cell_c[$i][303], $cell_c[$i][401], $cell_c[$i][402], $cell_c[$i][501], $cell_c[$i][502], $cell_c[$i][503], $cell_c[$i][504], $cell_c[$i][505], $cell_c[$i][506], $cell_c[$i][507], $cell_c[$i][601], $cell_c[$i][602], $cell_c[$i][603], $cell_c[$i][604], $cell_c[$i][605], $cell_c[$i][606], $cell_c[$i][607], $cell_c[$i][701], $cell_c[$i][702], $cell_c[$i][703], $cell_c[$i][704], $cell_c[$i][801], $cell_c[$i][802], $cell_c[$i][803], $cell_c[$i][205], $cell_c[$i][304], $cell_c[$i][305], $cell_c[$i][306], $cell_c[$i][307], $cell_c[$i][508]) = array_values($row);
                //list($cell_c[$i][101], $cell_c[$i][102], $cell_c[$i][103], $cell_c[$i][104], $cell_c[$i][105], $cell_c[$i][106], $cell_c[$i][107], $cell_c[$i][108], $cell_c[$i][201], $cell_c[$i][202], $cell_c[$i][203], $cell_c[$i][204], $cell_c[$i][301], $cell_c[$i][302], $cell_c[$i][303], $cell_c[$i][304], $cell_c[$i][305], $cell_c[$i][401], $cell_c[$i][402], $cell_c[$i][403], $cell_c[$i][404], $cell_c[$i][405], $cell_c[$i][501], $cell_c[$i][502], $cell_c[$i][503], $cell_c[$i][504], $cell_c[$i][505], $cell_c[$i][506], $cell_c[$i][507], $cell_c[$i][508], $cell_c[$i][509], $cell_c[$i][601], $cell_c[$i][602], $cell_c[$i][603], $cell_c[$i][604], $cell_c[$i][701], $cell_c[$i][702], $cell_c[$i][703], $cell_c[$i][704], $cell_c[$i][801], $cell_c[$i][802], $cell_c[$i][803]) = $row;
                $i--;
            }

            $sqlstr = "select element,urgent,attention from tbl_alarm where lab_no='" . $lab_no . "'";
            $hasil = $conn->createCommand($sqlstr)->queryAll();
            $listurgent = '';
            $listattention = '';

            foreach ($hasil as $row) {
                list($element, $urgent, $attention) = $row;
                if ($urgent == 1) {
                    $listurgent = $listurgent . " " . $element;
                } else {
                    $listattention = $listattention . " " . $element;
                }
            }

            if ($jml > 1) {
                $matrix[301] = $cell[$jml - 1][301] * 1.5;
                $matrix[302] = $cell[$jml - 1][302] * 1.5;
                $matrix[303] = $cell[$jml - 1][303] * 1.5;
            }

            $sqlstr = "select V40minB,V40maxB,V40minC,V40maxC,V100minB,V100maxB,V100minC,V100maxC,TANB,TANC,TBNB,TBNC from tbl_oil_matrix where oil_matrix='" . $oil_matrix . "'";
            $hasil = $conn->createCommand($sqlstr)->queryAll();

            foreach ($hasil as $row) {
                list($minBmatrix[201], $maxBmatrix[201], $minCmatrix[201], $maxCmatrix[201], $minBmatrix[202], $maxBmatrix[202], $minCmatrix[202], $maxCmatrix[202], $minmatrix[203], $matrix[203], $minmatrix[204], $matrix[204]) = array_values($row);
                $minmatrix[201] = "" . $minBmatrix[201] . "/" . $maxBmatrix[201];
                $matrix[201] = "" . $minCmatrix[201] . "/" . $maxCmatrix[201];
                $minmatrix[202] = "" . $minBmatrix[202] . "/" . $maxBmatrix[202];
                $matrix[202] = "" . $minCmatrix[202] . "/" . $maxCmatrix[202];
            }

            $sqlstr = "select NaB,SiB,FeB,CuB,AlB,CrB,NiB,SnB,PbB,SootB,OxiB,NitB,SulB,FuelB,WaterB,GlycolB from tbl_wear_ftir_matrix where matrix='" . $matrik . "'";
            $hasil = $conn->createCommand($sqlstr)->queryAll();

            foreach ($hasil as $row) {
                list($minmatrix[401], $minmatrix[402], $minmatrix[501], $minmatrix[502], $minmatrix[503], $minmatrix[504], $minmatrix[505], $minmatrix[506], $minmatrix[507], $minmatrix[601], $minmatrix[602], $minmatrix[603], $minmatrix[604], $minmatrix[605], $minmatrix[606], $minmatrix[607]) = array_values($row);
            }

            $sqlstr = "select NaC,SiC,FeC,CuC,AlC,CrC,NiC,SnC,PbC,SootC,OxiC,NitC,SulC,FuelC,WaterC,GlycolC from tbl_wear_ftir_matrix where matrix='" . $matrik . "'";
            $hasil = $conn->createCommand($sqlstr)->queryAll();

            foreach ($hasil as $row) {
                list($matrix[401], $matrix[402], $matrix[501], $matrix[502], $matrix[503], $matrix[504], $matrix[505], $matrix[506], $matrix[507], $matrix[601], $matrix[602], $matrix[603], $matrix[604], $matrix[605], $matrix[606], $matrix[607]) = array_values($row);
            }


//            $sqlstrcolor = "select B, C from  tbl_color";
//            $hasilcolor = $conn->createCommand($sqlstrcolor);
//            $rowcol = $hasilcolor->queryOne();

//            list($B, $C) = $rowcol;
//            $minmatrix[509] = $B;
//            $matrix[509] = $C;


            $sqlstrseq = "select B1,B2,C1,C2 from  tbl_sequence";
            $hasilseq = $conn->createCommand($sqlstrseq);
            $rowseq = $hasilseq->queryOne();
            list($B1, $B2, $C1, $C2) = array_values($rowseq);
            $minmatrix[801] = $B1 . '/' . $B2;
            $matrix[801] = $C1 . '/' . $C2;

            $condition1 = " where Matrix='" . $matrik . "'";
            $sqlstr = "select 6attention, 14attention, 6urgent, 14urgent from  tbl_wear_ftir_matrix " . $condition1;
            $hasil = $conn->createCommand($sqlstr)->queryOne();

            if (!empty($hasil)) {
                list($attention6, $attention14, $urgent6, $urgent14) = array_values($hasil);
                $minmatrix[704] = $attention6 . "/" . $attention14;
                $matrix[704] = $urgent6 . "/" . $urgent14;
            }
        }

        $vdatawearmat = array();
        $vdataftirmat = array();
        $vdataTAN_TBN = array();
        $vdataVISC = array();
        $k = 0;

        while ($k < $jml) {
            $vdatawearmat[1][$k] = $cell[$k + 1][501];
            /*iron*/
            $vdatawearmat[2][$k] = $cell[$k + 1][502];
            /*cooper*/
            $vdatawearmat[3][$k] = $cell[$k + 1][503];
            /*alumunium*/
            $vdatawearmat[4][$k] = $cell[$k + 1][504];
            /*Chromium*/
            $vdatawearmat[5][$k] = $cell[$k + 1][505];
            /*Nickel*/
            $vdatawearmat[6][$k] = $cell[$k + 1][506];
            /*Tin*/
            $vdatawearmat[7][$k] = $cell[$k + 1][507];
            /*Pb*/
            $vdataftirmat[1][$k] = $cell[$k + 1][601];
            /*soot*/
            $vdataftirmat[2][$k] = $cell[$k + 1][602];
            /*oxid*/
            $vdataftirmat[3][$k] = $cell[$k + 1][603];
            /*nit*/
            $vdataftirmat[4][$k] = $cell[$k + 1][604];
            /*sulf*/
            $vdatawearmat[8][$k] = $cell[$k + 1][605];
            /*fuel dilution*/
            $vdatawearmat[9][$k] = $cell[$k + 1][606];
            /*Water*/
            $vdatawearmat[10][$k] = $cell[$k + 1][607];
            /*glycol*/
            $vdataftirmat[5][$k] = '';//$cell[$k + 1][403];
            /*fuel*/
            $vdataftirmat[6][$k] = '';//$cell[$k + 1][404];
            /*water*/
            $vdataftirmat[7][$k] = '';//$cell[$k + 1][405];
            /*glycol*/
            $vdataVISC[1][$k] = $cell[$k + 1][201];
            /*Visc 40*/
            $vdataVISC[2][$k] = $cell[$k + 1][202];
            /*Visc 100*/
            $vdataTAN_TBN[1][$k] = $cell[$k + 1][203];
            /*TAN*/
            $vdataTAN_TBN[2][$k] = $cell[$k + 1][204];
            /*TBN*/
            $k++;
        }

        $titlewear[1] = "Fe";
        $titlewear[2] = "Cu";
        $titlewear[3] = "Al";
        $titlewear[4] = "Cr";
        $titlewear[5] = "Ni";
        $titlewear[6] = "Sn";
        $titlewear[7] = "Pb";
        $titleftir[1] = "soot";
        $titleftir[2] = "oxid";
        $titleftir[3] = "nit";
        $titleftir[4] = "sulf";
        $titleftir[5] = "fuel";
        $titleftir[6] = "water";
        $titleftir[7] = "glycol";
        $titleTAN_TBN[1] = "TAN";
        $titleTAN_TBN[2] = "TBN";
        $titleVISC[1] = "Visc 40";
        $titleVISC[2] = "Visc 100";
        $vlabel = array();
        $vlabel[0] = @$cell[1][3];
        $vlabel[1] = @$cell[2][3];
        $vlabel[2] = @$cell[3][3];
        $vlabel[3] = @$cell[4][3];
        $vlabel[4] = @$cell[5][3];

        $graph1 = "graph01" . $ii . ".png";
        $graph2 = "graph02" . $ii . ".png";
        $graph3 = "graph03" . $ii . ".png";
        $graph4 = "graph04" . $ii . ".png";

        chartbaru($vdatawearmat, $vlabel, $dbdirgraph . $graph1, $titlewear, "WEAR TRENDING", $jml);
        chartbaru($vdataftirmat, $vlabel, $dbdirgraph . $graph2, $titleftir, "CONTAMINANT - FTIR ANALYSIS", $jml);
        chartbaru($vdataTAN_TBN, $vlabel, $dbdirgraph . $graph3, $titleTAN_TBN, "TBN - TAN", $jml);
        chartbaru($vdataVISC, $vlabel, $dbdirgraph . $graph4, $titleVISC, "Viscosity", $jml);

        $cellTitle[101] = "Lab. Number";
        $cellTitle[102] = "Sampling Date";
        $cellTitle[103] = "Received Date";
        $cellTitle[104] = "Report Date";
        $cellTitle[105] = "Hours on Oil";
        $cellTitle[106] = "Hours On Unit";
        //$cellTitle[107] = "Lube Oil Name";
        // $cellTitle[107] = "Oil Change";
        $cellTitle[201] = "Visc@40C (*)";
        $cellTitle[202] = "Visc@100C (*)";
        $cellTitle[203] = "TAN";
        $cellTitle[204] = "TBN";
        $cellTitle[205] = "Visccosity Index";

        $cellTitle[301] = "Magnesium (Mg)";
        $cellTitle[302] = "Calcium (Ca)";
        $cellTitle[303] = "Zinc (Zn)";
        $cellTitle[304] = "Molybdenum (Mo)" . $bintang;
        $cellTitle[305] = "Boron (B)" . $bintang;
        $cellTitle[306] = "Phosphor (P)" . $bintang;
        $cellTitle[307] = "Barium (Ba)" . $bintang;
        $cellTitle[401] = "Natrium (Na)";
        $cellTitle[402] = "Silicon (Si)";
        //$cellTitle[403] = "Fuel Dilution";
        //$cellTitle[404] = "Water Content";
        //$cellTitle[405] = "Glycol";

        $cellTitle[501] = "Iron (Fe)";
        $cellTitle[502] = "Copper (Cu)";
        $cellTitle[503] = "Alumunium (Al)";
        $cellTitle[504] = "Chromium (Cr)";
        $cellTitle[505] = "Nickel (Ni)";
        $cellTitle[506] = "Tin (Sn)";
        $cellTitle[507] = "Lead (Pb)";
        //$cellTitle[508] = "PQ Index";
        //$cellTitle[509] = "Color";
        $cellTitle[508] = "Silver (Ag)" . $bintang;
        $cellTitle[509] = "Titanium (Ti)" . $bintang;
        $cellunit[508] = "ppm";
        $cellunit[509] = "ppm";
        $cellmethod[508] = "ASTM D 5185-13e1";
        $cellmethod[509] = "ASTM D 5185-13e1";

        $cellTitle[601] = "Soot";
        $cellTitle[602] = "Oxidation";
        $cellTitle[603] = "Nitration";
        $cellTitle[604] = "Sulfation";
        $cellTitle[605] = "Fuel Dilution";
        $cellTitle[606] = "Water Content";
        $cellTitle[607] = "Glycol";
        $cellTitle[701] = "4 um";
        $cellTitle[702] = "6 um";
        $cellTitle[703] = "14 um";
        $cellTitle[704] = "ISO CODE";
        $cellTitle[801] = "Seq.I";
        $cellTitle[802] = "Seq.II";
        $cellTitle[803] = "Seq.III";

        $cellTitle[804] = "MPC";
        $cellTitle[805] = "Image of MPC";
        $cellunit[804] = "Rating";

        $cellTitle[811] = "Patch Test";
        $cellTitle[812] = "Volume Sample";
        $cellTitle[813] = "Magnification";
        $cellTitle[814] = "Image of Patch Test";
        $cellunit[812] = "mL";
        $cellmethod[811] = "ISO 4407-02";

        $cellTitle[821] = "Particle >4 microns";
        $cellTitle[822] = "Particle >6 microns";
        $cellTitle[823] = "Particle >14 microns";
        $cellTitle[824] = "ISO 4406";
        $cellunit[821] = "Counts/mL";
        $cellunit[822] = "Counts/mL";
        $cellunit[823] = "Counts/mL";
        $cellmethod[824] = "ISO 4406-1999";

        $cellTitle[825] = "Particle  5-15 microns";
        $cellTitle[826] = "Particle  15-25 microns";
        $cellTitle[827] = "Particle  25-50 microns";
        $cellTitle[828] = "Particle  50-100 microns";
        $cellTitle[829] = "Particle >100 microns";
        $cellTitle[830] = "NAS Class 1638";
        $cellunit[825] = "Counts/mL";
        $cellunit[826] = "Counts/mL";
        $cellunit[827] = "Counts/mL";
        $cellunit[828] = "Counts/mL";
        $cellunit[829] = "Counts/mL";
        $cellunit[830] = "Counts/mL";
        $cellmethod[830] = "NAS 1638";

        $cellunit[201] = "cSt";
        $cellunit[202] = "cSt";
        $cellunit[203] = "mg KOH/g";
        $cellunit[204] = "mg KOH/g";
        $cellunit[205] = "-";
        $cellunit[301] = "ppm";
        $cellunit[302] = "ppm";
        $cellunit[303] = "ppm";
        $cellunit[304] = "ppm";
        $cellunit[305] = "ppm";
        $cellunit[306] = "ppm";
        $cellunit[307] = "ppm";
        $cellunit[401] = "ppm";
        $cellunit[402] = "ppm";
        $cellunit[403] = "%v";
        $cellunit[404] = "%v";
        $cellunit[405] = "%v";
        $cellunit[501] = "ppm";
        $cellunit[502] = "ppm";
        $cellunit[503] = "ppm";
        $cellunit[504] = "ppm";
        $cellunit[505] = "ppm";
        $cellunit[506] = "ppm";
        $cellunit[507] = "ppm";
        //$cellunit[508] = "";
        $cellunit[510] = "ppm";
        $cellunit[511] = "ppm";
        $cellunit[601] = "Abs/cm";
        $cellunit[602] = "Abs/cm";
        $cellunit[603] = "Abs/cm";
        $cellunit[604] = "Abs/cm";
        $cellunit[605] = "%";
        $cellunit[606] = "%";
        $cellunit[607] = "%";
        $cellunit[701] = "Particles/ml";
        $cellunit[702] = "Particles/ml";
        $cellunit[703] = "Particles/ml";
        $cellunit[704] = "-";
        $cellunit[801] = "ml/ml";
        $cellunit[802] = "ml/ml";
        $cellunit[803] = "ml/ml";


        $cellunit[31] = "-";
        $cellmethod[201] = "ASTM D445-12";
        $cellmethod[202] = "ASTM D445-12";
        $cellmethod[203] = "ASTM D974-12";
        $cellmethod[204] = "ASTM D2896-11";
        $cellmethod[205] = "ASTM D2270-10";
        $cellmethod[301] = "ASTM D 5185-13e1";
        $cellmethod[302] = "ASTM D 5185-13e1";
        $cellmethod[303] = "ASTM D 5185-13e1";
        $cellmethod[304] = "ASTM D 5185-13e1";
        $cellmethod[305] = "ASTM D 5185-13e1";
        $cellmethod[306] = "ASTM D 5185-13e1";
        $cellmethod[307] = "ASTM D 5185-13e1";
        $cellmethod[401] = "ASTM D 5185-13e1";
        $cellmethod[402] = "ASTM D 5185-13e1";

        $cellmethod[403] = "ASTM D 5185-13e1";
        $cellmethod[404] = "ASTM D 5185-13e1";
        $cellmethod[405] = "ASTM D 5185-13e1";
        $cellmethod[501] = "ASTM D 5185-13e1";
        $cellmethod[502] = "ASTM D 5185-13e1";
        $cellmethod[503] = "ASTM D 5185-13e1";
        $cellmethod[504] = "ASTM D 5185-13e1";
        $cellmethod[505] = "ASTM D 5185-13e1";
        $cellmethod[506] = "ASTM D 5185-13e1";
        $cellmethod[507] = "ASTM D 5185-13e1";
        //$cellmethod[508] = "";
        //$cellmethod[509] = "";
        $cellmethod[510] = "ASTM D 5185-13e1";
        $cellmethod[511] = "ASTM D 5185-13e1";
        $cellmethod[601] = "ASTM E2412-10";
        $cellmethod[602] = "ASTM E2412-10";
        $cellmethod[603] = "ASTM E2412-10";
        $cellmethod[604] = "ASTM E2412-10";
        $cellmethod[605] = "ASTM E2412-10";
        $cellmethod[606] = "ASTM E2412-10";
        $cellmethod[607] = "ASTM E2412-10";
        $cellmethod[701] = "";
        $cellmethod[702] = "";
        $cellmethod[703] = "";
        $cellmethod[704] = "ISO 4406";
        $cellmethod[801] = "ASTM D892";
        $cellmethod[802] = "ASTM D892";
        $cellmethod[803] = "ASTM D892";

        $evalcode = $cell[$jml][44];

        for ($j = 1; $j <= 5; $j++) {
            if (!empty($cell[$j][101])) {
                $bundle_lab_no[$j] = $cell[$j][101];
            }
        }

        $tran = [
            1 => 205,
            2 => 304,
            3 => 305,
            4 => 306,
            5 => 307,
            6 => 508,
            7 => 509,
            8 => 821,
            9 => 822,
            10 => 823,
            11 => 824,
            12 => 825,
            13 => 826,
            14 => 827,
            15 => 828,
            16 => 829,
            17 => 830,
            18 => 804,
            19 => 805,
            20 => 811,
            21 => 812,
            22 => 813,
            23 => 814,
        ];
        $matrixss = Matrix::findOne(['Matrix' => $matrik]);
        foreach ($bundle_lab_no as $idx => $item) {
            $additon = Addition::find()->where("Lab_No = '$item'")->all();
            foreach ($additon as $record) {
                if (isset($tran[$record->tbl_parameter_id]) && $dt = $tran[$record->tbl_parameter_id]) {
                    $cell[$idx][$dt] = $record->value;
                    if (in_array((int)$record->tbl_parameter_id, [19, 23], true)) {
                        $cell[$idx][$dt] = $record->pathFile;
                    }
                    $cellTitle[$dt] = $record->parameter->unsur;
                    $cellunit[$dt] = $record->parameter->unit;
                    $cellmethod[$dt] = $record->parameter->methode;
                    $minmatrix[$dt] = $record->parameter->getLimit('attention', $matrixss);
                    $matrix[$dt] = $record->parameter->getLimit('urgent', $matrixss);
                }
//                print_r($dt . '<br>');
            }
        }
//        print_r($cell);
//        die;
        callheaderreport($pdf, $noreport,
            $branch_nm,
            $component,
            $name,
            $matrik,
            $vaddress_nm,
            $oil_matrix,
            $model,
            $oil_type,
            $unit_no,
            $unit_loc,
            $evalcode);

        for ($j = 101; $j <= 107; $j++) {
            $data1[$j] = array(
                @$cellTitle[$j],
                @$cellunit[$j],
                @$cellmethod[$j],
                alarm(@$cell[1][$j], @$cell_c[1][$j]),
                alarm(@$cell[2][$j], @$cell_c[2][$j]),
                alarm(@$cell[3][$j], @$cell_c[3][$j]),
                alarm(@$cell[4][$j], @$cell_c[4][$j]),
                alarm(@$cell[5][$j], @$cell_c[5][$j]),
                @$matrix[$j],
                @$matrix[$j]
            );
        }
        $header1 = array(
            'TEST DETAILS',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            ''
        );
//print_r($data1);die;
        $pdf->ImprovedTable($header1, $data1, 1, 4);
        $data2 = NULL;
        $pdf->Ln();

        for ($j = 201; $j <= 202; $j++) {
            $data2[$j - 200] = array(
                @$cellTitle[$j],
                @$cellunit[$j],
                @$cellmethod[$j],
                alarm(@$cell[1][$j], @$cell_c[1][$j]),
                alarm(@$cell[2][$j], @$cell_c[2][$j]),
                alarm(@$cell[3][$j], @$cell_c[3][$j]),
                alarm(@$cell[4][$j], @$cell_c[4][$j]),
                @$minmatrix[$j],
                @$matrix[$j]
            );
        }
        $j = 205;

        //   if (((strtoupper($component) == "ENGINE") or (strtoupper($component) == "ENGINE UPPER") or (strtoupper($component) == "ENGINE LOWER") or (strtoupper($component) == "NEW OIL B")) and ($j == 204)) {
        $data2[3] = array(
            @$cellTitle[$j],
            @$cellunit[$j],
            @$cellmethod[$j],
            alarm(@$cell[1][$j], @$cell_c[1][$j]),
            alarm(@$cell[2][$j], @$cell_c[2][$j]),
            alarm(@$cell[3][$j], @$cell_c[3][$j]),
            alarm(@$cell[4][$j], @$cell_c[4][$j]),
            @$minmatrix[$j],
            @$matrix[$j]
        );

//	  if (((strtoupper($component) <> "ENGINE") and (strtoupper($component) <> "ENGINE UPPER") and (strtoupper($component) <> "ENGINE LOWER") and (strtoupper($component) <> "NEW OIL B")) and ($j == 203)) {
        for ($j = 203; $j <= 204; $j++) {
            $data2[$j - 199] = array(
                @$cellTitle[$j],
                @$cellunit[$j],
                @$cellmethod[$j],
                alarm(@$cell[1][$j], @$cell_c[1][$j]),
                alarm(@$cell[2][$j], @$cell_c[2][$j]),
                alarm(@$cell[3][$j], @$cell_c[3][$j]),
                alarm(@$cell[4][$j], @$cell_c[4][$j]),
                @$minmatrix[$j],
                @$matrix[$j]
            );
        }
        //   }

        //   }
        $header2 = array(
            'Physical Test',
            'Unit',
            'Method',
            '',
            '',
            'Test Value',
            '',
            'Attention',
            'Urgent'
        );
        $pdf->SetFillColor(235);
        $pdf->Cell(17.1, 0.5, '', 0, 1, 'C', 1);
        $pdf->Ln(-0.5);
        $pdf->ImprovedTable($header2, $data2, 2, 4);
        $data3 = NULL;
        $pdf->Ln();

        for ($j = 301; $j <= 307; $j++) {
            $cell[1][$j] = (string)@$cell[1][$j] === '0' ? '< 1' : @$cell[1][$j];
            $cell[2][$j] = (string)@$cell[2][$j] === '0' ? '< 1' : @$cell[2][$j];
            $cell[3][$j] = (string)@$cell[3][$j] === '0' ? '< 1' : @$cell[3][$j];
            $cell[4][$j] = (string)@$cell[4][$j] === '0' ? '< 1' : @$cell[4][$j];
            $data3[$j - 300] = array(
                @$cellTitle[$j],
                @$cellunit[$j],
                @$cellmethod[$j],
                alarm(@$cell[1][$j], @$cell_c[1][$j]),
                alarm(@$cell[2][$j], @$cell_c[2][$j]),
                alarm(@$cell[3][$j], @$cell_c[3][$j]),
                alarm(@$cell[4][$j], @$cell_c[4][$j]),
                null,
                null
            );

        }
        $header3 = array(
            'Metal Additive',
            '',
            '',
            '',
            '',
            '',
            '',
            'Warning',
            'Limit'
        );
        $pdf->SetFillColor(235);
        $pdf->Cell(17.1, 0.5, '', 0, 1, 'C', 1);
        $pdf->Ln(-0.5);
        $pdf->ImprovedTable($header3, $data3, 3, 4);
        $data4 = NULL;
        $pdf->Ln();

        for ($j = 401; $j <= 402; $j++) {
            $cell[1][$j] = (string)@$cell[1][$j] === '0' ? '< 1' : @$cell[1][$j];
            $cell[2][$j] = (string)@$cell[2][$j] === '0' ? '< 1' : @$cell[2][$j];
            $cell[3][$j] = (string)@$cell[3][$j] === '0' ? '< 1' : @$cell[3][$j];
            $cell[4][$j] = (string)@$cell[4][$j] === '0' ? '< 1' : @$cell[4][$j];
            $data4[$j - 400] = array(
                @$cellTitle[$j],
                @$cellunit[$j],
                @$cellmethod[$j],
                alarm(@$cell[1][$j], @$cell_c[1][$j]),
                alarm(@$cell[2][$j], @$cell_c[2][$j]),
                alarm(@$cell[3][$j], @$cell_c[3][$j]),
                alarm(@$cell[4][$j], @$cell_c[4][$j]),
                @$minmatrix[$j],
                @$matrix[$j]
            );
        }
        $header4 = array(
            'Contaminant',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            ''
        );
        $pdf->SetFillColor(235);
        $pdf->Cell(17.1, 0.5, '', 0, 1, 'C', 1);
        $pdf->Ln(-0.5);
        $pdf->ImprovedTable($header4, $data4, 3, 4);
        $data5 = NULL;
        $pdf->Ln();

        for ($j = 501; $j <= 509; $j++) {
            $cell[1][$j] = (string)@$cell[1][$j] === '0' ? '< 1' : @$cell[1][$j];
            $cell[2][$j] = (string)@$cell[2][$j] === '0' ? '< 1' : @$cell[2][$j];
            $cell[3][$j] = (string)@$cell[3][$j] === '0' ? '< 1' : @$cell[3][$j];
            $cell[4][$j] = (string)@$cell[4][$j] === '0' ? '< 1' : @$cell[4][$j];
            $data5[$j - 500] = array(
                @$cellTitle[$j],
                @$cellunit[$j],
                @$cellmethod[$j],
                alarm(@$cell[1][$j], @$cell_c[1][$j]),
                alarm(@$cell[2][$j], @$cell_c[2][$j]),
                alarm(@$cell[3][$j], @$cell_c[3][$j]),
                alarm(@$cell[4][$j], @$cell_c[4][$j]),
                @$minmatrix[$j],
                @$matrix[$j]
            );
        }
        $header5 = array(
            'Wear Metal',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            ''
        );
        $pdf->SetFillColor(235);
        $pdf->Cell(17.1, 0.5, '', 0, 1, 'C', 1);
        $pdf->Ln(-0.5);
        $pdf->ImprovedTable($header5, $data5, 3, 4);
        $data6 = NULL;
        $pdf->Ln();

        if ((strtoupper($component) == "ENGINE") || (strtoupper($component) != "ENGINE UPPER")) {
            for ($j = 601; $j <= 607; $j++) {
                if ($j < 605) {
                    $cell[1][$j] = (string)@$cell[1][$j] === '0' ? '< 0.02' : @$cell[1][$j];
                    $cell[2][$j] = (string)@$cell[2][$j] === '0' ? '< 0.02' : @$cell[2][$j];
                    $cell[3][$j] = (string)@$cell[3][$j] === '0' ? '< 0.02' : @$cell[3][$j];
                    $cell[4][$j] = (string)@$cell[4][$j] === '0' ? '< 0.02' : @$cell[4][$j];
                }
                $data6[$j - 600] = array(
                    @$cellTitle[$j],
                    @$cellunit[$j],
                    @$cellmethod[$j],
                    alarm(@$cell[1][$j], @$cell_c[1][$j]),
                    alarm(@$cell[2][$j], @$cell_c[2][$j]),
                    alarm(@$cell[3][$j], @$cell_c[3][$j]),
                    alarm(@$cell[4][$j], @$cell_c[4][$j]),
                    @$minmatrix[$j],
                    @$matrix[$j]
                );
            }
        } else {
            $j = 602;
            $cell[1][$j] = (string)@$cell[1][$j] === '0' ? '< 0.02' : @$cell[1][$j];
            $cell[2][$j] = (string)@$cell[2][$j] === '0' ? '< 0.02' : @$cell[2][$j];
            $cell[3][$j] = (string)@$cell[3][$j] === '0' ? '< 0.02' : @$cell[3][$j];
            $cell[4][$j] = (string)@$cell[4][$j] === '0' ? '< 0.02' : @$cell[4][$j];
            $data6[1] = array(
                @$cellTitle[$j],
                @$cellunit[$j],
                @$cellmethod[$j],
                alarm(@$cell[1][$j], @$cell_c[1][$j]),
                alarm(@$cell[2][$j], @$cell_c[2][$j]),
                alarm(@$cell[3][$j], @$cell_c[3][$j]),
                alarm(@$cell[4][$j], @$cell_c[4][$j]),
                @$minmatrix[$j],
                @$matrix[$j]
            );
        }

        $header6 = array(
            'FTIR *).',
            '',
            '',
            '',
            '',
            '',
            '',
            'Warning',
            'Limit'
        );
        $pdf->SetFillColor(235);
        $pdf->Cell(17.1, 0.5, '', 0, 1, 'C', 1);
        $pdf->Ln(-0.5);
        $pdf->ImprovedTable($header6, $data6, 3, 4);
        $pdf->Ln();
        $coeff = 0;


        if ((strtoupper($component) == "ENGINE") or (strtoupper($component) == "ENGINE UPPER") or (strtoupper($component) == "ENGINE LOWER")) {
            for ($j = 704; $j <= 704; $j++) {
                $data7[$j - 704] = array(
                    @$cellTitle[$j],
                    @$cellunit[$j],
                    @$cellmethod[$j],
                    alarm(@$cell[1][$j], @$cell_c[1][$j]),
                    alarm(@$cell[2][$j], @$cell_c[2][$j]),
                    alarm(@$cell[3][$j], @$cell_c[3][$j]),
                    alarm(@$cell[4][$j], @$cell_c[4][$j]),
                    @$minmatrix[$j],
                    @$matrix[$j]
                );
            }
            $header7 = array(
                'Other Analysis',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                ''
            );
        } else {
            $coeff = 2.5;

            for ($j = 701; $j <= 704; $j++) {
                $data7[$j - 700] = array(
                    @$cellTitle[$j],
                    @$cellunit[$j],
                    @$cellmethod[$j],
                    alarm(@$cell[1][$j], @$cell_c[1][$j]),
                    alarm(@$cell[2][$j], @$cell_c[2][$j]),
                    alarm(@$cell[3][$j], @$cell_c[3][$j]),
                    alarm(@$cell[4][$j], @$cell_c[4][$j]),
                    @$minmatrix[$j],
                    @$matrix[$j]
                );
            }
            $header7 = array(
                'Cleanliness',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                ''
            );

            for ($j = 801; $j <= 803; $j++) {
                $data8[$j - 800] = array(
                    @$cellTitle[$j],
                    @$cellunit[$j],
                    @$cellmethod[$j],
                    alarm(@$cell[1][$j], @$cell_c[1][$j]),
                    alarm(@$cell[2][$j], @$cell_c[2][$j]),
                    alarm(@$cell[3][$j], @$cell_c[3][$j]),
                    alarm(@$cell[4][$j], @$cell_c[4][$j]),
                    @$minmatrix[$j],
                    @$matrix[$j]
                );
            }
            $header8 = array(
                'Foaming Characteristic',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                ''
            );

        }

        $header9 = array(
            'OTHERS',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
        );

        // $data9[1] = $bundle_lab_no;
        $data9 = beta_others($bundle_lab_no);
        $pdf->Ln();
        $pdf->SetFillColor(235);
        $pdf->Cell(17.1, 0.5, '', 0, 1, 'C', 1);
        $pdf->Ln(-0.5);
        $pdf->ImprovedTable($header9, $data9, 5, 4);

        $coeff = 0;

        $pdf->SetDrawColor(0);
        $pdf->SetLineWidth(0.05);
        // $coeff = $coeff + 21.75;
        //$pdf->Line(25.80, $coeff , 1, $coeff );
        $coeff = $coeff + 23.77;
        $pdf->Line(21.27, $coeff, 1, $coeff);

        $pdf->Image($dbdirgraph . $graph1, 1, $coeff + 0.1, 9.2);
        $pdf->Image($dbdirgraph . $graph2, 11.3, $coeff + 0.1, 9.2);
        // $pdf->Image('image/' . $graph3, 1, $coeff + 22.2, 9.2);
        //  $pdf->Image('image/' . $graph4, 11.3, $coeff + 22.2, 9.2);
        $pdf->Ln(6);

        $pdf->Ln();
        $pdf->Cell(0, 0.8, 'Page 1 of 2', 0, 0, 'C');

        $pdf->AddPage();
        callheaderreport($pdf, $noreport,
            $branch_nm,
            $component,
            $name,
            $matrik,
            $vaddress_nm,
            $oil_matrix,
            $model,
            $oil_type,
            $unit_no,
            $unit_loc,
            $evalcode, 0);

        for ($j = 101; $j <= 107; $j++) {
            $data1[$j] = array(
                @$cellTitle[$j],
                @$cellunit[$j],
                @$cellmethod[$j],
                alarm(@$cell[1][$j], @$cell_c[1][$j]),
                alarm(@$cell[2][$j], @$cell_c[2][$j]),
                alarm(@$cell[3][$j], @$cell_c[3][$j]),
                alarm(@$cell[4][$j], @$cell_c[4][$j]),
                @$matrix[$j],
                @$matrix[$j]
            );
        }
        $header1 = array(
            'TEST DETAILS',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            ''
        );
//print_r($data1);die;
        $pdf->ImprovedTable($header1, $data1, 1, 4);
        $data2 = NULL;
        $pdf->Ln();

        for ($j = 201; $j <= 202; $j++) {
            $data2[$j - 200] = array(
                @$cellTitle[$j],
                @$cellunit[$j],
                @$cellmethod[$j],
                alarm(@$cell[1][$j], @$cell_c[1][$j]),
                alarm(@$cell[2][$j], @$cell_c[2][$j]),
                alarm(@$cell[3][$j], @$cell_c[3][$j]),
                alarm(@$cell[4][$j], @$cell_c[4][$j]),
                @$minmatrix[$j],
                @$matrix[$j]
            );
        }
        $j = 205;

        //   if (((strtoupper($component) == "ENGINE") or (strtoupper($component) == "ENGINE UPPER") or (strtoupper($component) == "ENGINE LOWER") or (strtoupper($component) == "NEW OIL B")) and ($j == 204)) {
        $data2[3] = array(
            @$cellTitle[$j],
            @$cellunit[$j],
            @$cellmethod[$j],
            alarm(@$cell[1][$j], @$cell_c[1][$j]),
            alarm(@$cell[2][$j], @$cell_c[2][$j]),
            alarm(@$cell[3][$j], @$cell_c[3][$j]),
            alarm(@$cell[4][$j], @$cell_c[4][$j]),
            @$minmatrix[$j],
            @$matrix[$j]
        );

//	  if (((strtoupper($component) <> "ENGINE") and (strtoupper($component) <> "ENGINE UPPER") and (strtoupper($component) <> "ENGINE LOWER") and (strtoupper($component) <> "NEW OIL B")) and ($j == 203)) {
        for ($j = 821; $j <= 824; $j++) {
            $data10[$j - 823] = array(
                @$cellTitle[$j],
                @$cellunit[$j],
                @$cellmethod[$j],
                alarm(@$cell[1][$j], @$cell_c[1][$j]),
                alarm(@$cell[2][$j], @$cell_c[2][$j]),
                alarm(@$cell[3][$j], @$cell_c[3][$j]),
                alarm(@$cell[4][$j], @$cell_c[4][$j]),
                @$minmatrix[$j],
                @$matrix[$j]
            );
        }
        //   }

        //   }

        /*CONTAMINATION 1*/
        $header10 = array(
            'Contamination 1',
            'Unit',
            'Method',
            '',
            '',
            'Test Value',
            '',
            'Attention',
            'Urgent'
        );
        $pdf->SetFillColor(235);
        $pdf->Cell(17.1, 0.5, '', 0, 1, 'C', 1);
        $pdf->Ln(-0.5);
        $pdf->ImprovedTable($header10, $data10, 2, 4);
        $pdf->Ln();
        /*CONTAMINATION 1*/

        /*CONTAMINATION 2*/
        for ($j = 825; $j <= 830; $j++) {
            $data11[$j - 829] = array(
                @$cellTitle[$j],
                @$cellunit[$j],
                @$cellmethod[$j],
                alarm(@$cell[1][$j], @$cell_c[1][$j]),
                alarm(@$cell[2][$j], @$cell_c[2][$j]),
                alarm(@$cell[3][$j], @$cell_c[3][$j]),
                alarm(@$cell[4][$j], @$cell_c[4][$j]),
                @$minmatrix[$j],
                @$matrix[$j]
            );
        }
        $header11 = array(
            'Contamination 2',
            'Unit',
            'Method',
            '',
            '',
            'Test Value',
            '',
            '',
            ''
        );
        $pdf->SetFillColor(235);
        $pdf->Cell(17.1, 0.5, '', 0, 1, 'C', 1);
        $pdf->Ln(-0.5);
        $pdf->ImprovedTable($header11, $data11, 3, 4);
        $pdf->Ln();
        /*CONTAMINATION 2*/

        /*MPC*/
        $data12 = NULL;
        for ($j = 804; $j <= 810; $j++) {
            if ($j == 805) {
                $data12[$j - 809] = array(
                    @$cellTitle[$j],
                    @$cellunit[$j],
                    @$cellmethod[$j],
                    '', '', '', '',
                    @$minmatrix[$j],
                    @$matrix[$j]
                );
            } else {
                $data12[$j - 809] = array(
                    @$cellTitle[$j],
                    @$cellunit[$j],
                    @$cellmethod[$j],
                    alarm(@$cell[1][$j], @$cell_c[1][$j]),
                    alarm(@$cell[2][$j], @$cell_c[2][$j]),
                    alarm(@$cell[3][$j], @$cell_c[3][$j]),
                    alarm(@$cell[4][$j], @$cell_c[4][$j]),
                    @$minmatrix[$j],
                    @$matrix[$j]
                );
            }

        }
        $pdf->Image(clearPicture(@$cell[1][805]), 7.75, 12.7, 1.9, 1.9);
        $pdf->Image(clearPicture(@$cell[2][805]), 10.45, 12.7, 1.9, 1.9);
        $pdf->Image(clearPicture(@$cell[3][805]), 13.15, 12.7, 1.9, 1.9);
        $pdf->Image(clearPicture(@$cell[4][805]), 15.8, 12.7, 1.9, 1.9);
        $header12 = array(
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            ''
        );
        $pdf->SetFillColor(235);
        $pdf->Cell(17.1, 0.5, '', 0, 1, 'C', 1);
        $pdf->Ln(-0.5);
        $pdf->ImprovedTable($header12, $data12, 3, 4);
        $pdf->Ln();
        /*MPC*/

        /*PATCH TEST*/
        $data13 = NULL;
        for ($j = 811; $j <= 820; $j++) {
            if ($j == 814) {
                $data13[$j - 819] = array(
                    @$cellTitle[$j],
                    @$cellunit[$j],
                    @$cellmethod[$j],
                    '', '', '', '',
                    @$minmatrix[$j],
                    @$matrix[$j]
                );
            } else {
                $data13[$j - 819] = array(
                    @$cellTitle[$j],
                    @$cellunit[$j],
                    @$cellmethod[$j],
                    alarm(@$cell[1][$j], @$cell_c[1][$j]),
                    alarm(@$cell[2][$j], @$cell_c[2][$j]),
                    alarm(@$cell[3][$j], @$cell_c[3][$j]),
                    alarm(@$cell[4][$j], @$cell_c[4][$j]),
                    null,
                    null
                );
            }
        }
        $pdf->Image(clearPicture(@$cell[1][814]), 7.5, 16.2, 2.4, 2.4);
        $pdf->Image(clearPicture(@$cell[2][814]), 10.15, 16.2, 2.4, 2.4);
        $pdf->Image(clearPicture(@$cell[3][814]), 12.85, 16.2, 2.4, 2.4);
        $pdf->Image(clearPicture(@$cell[4][814]), 15.55, 16.2, 2.4, 2.4);
        $header13 = array(
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            ''
        );
        $pdf->SetFillColor(235);
        $pdf->Cell(17.1, 0.5, '', 0, 1, 'C', 1);
        $pdf->Ln(-0.5);
        $pdf->ImprovedTable($header13, $data13, 3, 4);
        $pdf->Ln();
        /*PATCH TEST*/

        $coeff = 0;

        $pdf->SetDrawColor(0);
        $pdf->SetLineWidth(0.05);
        // $coeff = $coeff + 21.75;
        //$pdf->Line(25.80, $coeff , 1, $coeff );
        $coeff = $coeff + 18.60;
        $pdf->Line(21.27, $coeff, 1, $coeff);
        $pdf->Ln(-6);
        $mcfile = 'upload/gambar/MC_' . $lab_no . '.jpg';
        $edit = 'http://' . $_SERVER['HTTP_HOST'] . 'tbl_transactionedit.php?Lab_No=' . $lab_no;
        if (file_exists($mcfile)) {
            /*
            $pdf->addpage();
            $pdf->Image($mcfile,6.7,5,8);
            $text = 'Micro Analysis';
            $pdf->SetFont('Arial','B',10);
            $pdf->Ln(10);
            $pdf->Cell(19.5,1,$text,0,0,'C');
            */
            $pdf->SetFont('Arial', 'BU', 7.5);
            $pdf->Ln(6);
            $pdf->Cell(3, 0.8, 'Source of Abnormality', 0, 0, 'L', '', $_SERVER['HTTP_HOST'] . '/pap2/tbl_transactionedit.php?Lab_No=' . $lab_no);
            $pdf->Ln(0.6);
            $pdf->SetFont('Arial', '', 7);
            $pdf->multiCell(13.6, 0.3, $recomm1, 0, 1, 'L');
            $pdf->Ln(-0.15);
            $pdf->SetFont('Arial', 'BU', 7.5);
            $pdf->Cell(3, 0.8, 'Action to be taken', 0, 0, 'L', '', $_SERVER['HTTP_HOST'] . '/pap2/tbl_transactionedit.php?Lab_No=' . $lab_no);
            $pdf->Ln(0.6);
            $pdf->SetFont('Arial', '', 7);
            $pdf->multiCell(13.6, 0.3, $recomm2, 0, 1, 'L');
            $pdf->Image($mcfile, 14.5, 28.4, 6);
        } else {
            $pdf->SetFont('Arial', 'BU', 7.5);
            $pdf->Ln(6);
            $pdf->Cell(5, 0.8, 'Source of Abnormality', 0, 0, 'L', '', $_SERVER['HTTP_HOST'] . '/pap2/tbl_transactionedit.php?Lab_No=' . $lab_no);
            $pdf->Ln(0.6);
            $pdf->SetFont('Arial', '', 7);
            $pdf->multiCell(19.5, 0.3, $recomm1, 0, 1, 'L');
            $pdf->Ln(-0.15);
            $pdf->SetFont('Arial', 'BU', 7.5);
            $pdf->Cell(5, 0.8, 'Action to be taken', 0, 0, 'L', '', $_SERVER['HTTP_HOST'] . '/pap2/tbl_transactionedit.php?Lab_No=' . $lab_no);
            $pdf->Ln(0.6);
            $pdf->SetFont('Arial', '', 7);
            $pdf->multiCell(19.5, 0.3, $recomm2, 0, 1, 'L');
        }
        if (empty($draft)) {
            $pdf->SetFont('Arial', 'BU', 7.5);
            $pdf->Cell(15, 0.8, '', 0, 0, 'L');
            $pdf->Cell(10, 0.8, 'Manager Teknis', 0, 0, 'L');
            $pdf->Ln(2.2);
            $pdf->Cell(15, 0.8, '', 0, 0, 'L');
            $pdf->Cell(10, 0.8, 'Endriastuti', 0, 0, 'L');
        } else {
            $pdf->SetFont('Arial', 'BU', 7.5);
            $pdf->Cell(8.5, 0.8, 'Dibuat,', 0, 0, 'L');
            $pdf->Cell(7.5, 0.8, 'Diperiksa,', 0, 0, 'L');
            $pdf->Cell(9, 0.8, 'Disetujui,', 0, 0, 'L');
            $pdf->Ln(2.2);
            $pdf->Cell(8.5, 0.8, 'Pembuat Laporan', 0, 0, 'L');
            $pdf->Cell(7.5, 0.8, 'Deputy Mgr. Teknis', 0, 0, 'L');
            $pdf->Cell(9, 0.8, 'Mgr. Teknis', 0, 0, 'L');
        }
        $pdf->Ln(0.8);

        $pdf->SetFont('Arial', '', 7);
        $pdf->Cell(25, 0.8, '(*) Berdasarkan ISO VG Grade dan SAE Viscocity Grade', 0, 0, 'L');
        $pdf->Ln(0.3);
        $pdf->Cell(25, 0.8, '     Catatan : Data analisa hanya berlaku untuk sample yang diuji di laboratorium PT. Petrolab Services', 0, 0, 'L');
        $pdf->Ln(0.3);
        $pdf->Cell(10, 0.8, '                    Pengaduan tidak dilayani setelah 30 hari dari tanggal report di terbitkan ', 0, 0, 'L');
        $pdf->Ln(0.3);
        $pdf->Cell(25, 0.8, '*) Tidak Termasuk Ruang Lingkup Akreditasi', 0, 0, 'L');

        $pdf->Cell(0, 0.8, 'RK/5.10/01/01/01                           ', 0, 0, 'R');
        $pdf->Ln(1);
        // if (empty($draft)) {
        //     $pdf->Cell(25, 0.8, '                                                                                                             Page 2 Of 2                                                                                                               RK/5.10/01/01/02', 0, 0, 'L');
        // } else {
        //     $pdf->Cell(25, 0.8, '                                                                                                             Page 1 Of 1                                                                                                                RK/5.10/01/01/01', 0, 0, 'L');
        // }

        $pdf->Ln(1);
        $pdf->Cell(0, 0.8, 'Page 2 of 2', 0, 0, 'C');

        $pdf->Footer();

        //Page number
        //  $pdf->Cell(2,0.5,'Page '.$pdf->PageNo().' Of {nb}',0,0,'C');

        /*
        $vfile = 'gambar/' . $lab_no . '.jpg';
        if (file_exists($vfile)) {
          $pdf->addpage();
          $pdf->Image($vfile, 6.7, 5, 8);
          $text = 'Foaming Tendency pada seq 1 = ' . fSeq_I($seq_I) . ' ml';
          $pdf->SetFont('Arial', 'B', 10);
          $pdf->Ln(10);
          $pdf->Cell(19.5, 1, $text, 0, 0, 'C');
        }
        */

        /*
        $pdf->addpage();
        $text = 'Magnetic Plug and Filter Cutting';
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Cell(19.5, 1, $text, 0, 0, 'C');
        $pdf->Ln();
        $pdf->Cell(5, 1, "Rating", "L T B", 0, "L", 1);
        $pdf->Cell(6, 1, "Picture", "T B", 0, "L", 1);
        $pdf->Cell(8.5, 1, "Notes", "T B R", 0, "L", 1);
        $pdf->Ln();
        $pdf->Cell(5, 10, "A", "L T B", 0, "L", 0);
        $pdf->Image('image/mp.jpg', 3.6, 3.4, 8);
        $pdf->Cell(6, 10, "", "T B", 0, "L", 0);
        $pdf->Cell(8.5, 10, "Notes", "T B R", 0, "L", 0);
        $pdf->Ln();
        $pdf->Cell(5, 7, "A", "L T B", 0, "L", 0);
        $pdf->Image('image/fc.jpg', 3.6, 13.4, 8);
        $pdf->Cell(6, 7, "", "T B", 0, "L", 0);
        $pdf->Cell(8.5, 7, "Notes", "T B R", 0, "L", 0);
        */

        $vfile = '';
        if (file_exists('upload/gambar/' . $lab_no . '.jpg')) {
            $vfile = 'upload/gambar/' . $lab_no . '.jpg';
        } else if (file_exists('upload/gambar/' . $lab_no . '.JPG')) {
            $vfile = 'upload/gambar/' . $lab_no . '.JPG';
        }
        if (file_exists($vfile)) {
            $pdf->AddPage();
//            $pdf->Image($vfile, 6.7, 5, 8);


            $text = 'Foaming Tendency pada seq 1 = ' . fSeq_I($seq_I) . ' ml';
            $pdf->SetFont('Arial', 'B', 10);
            $pdf->Ln(10);
            $pdf->Cell(19.5, 1, $text, 0, 0, 'C');
        }

    }
    $pdf->Output($lab_no . '.pdf', 'I');
    die;
}

function clearPicture($urlfile)
{
    return file_exists($urlfile) ? $urlfile : 'image/blank.jpg';
}

function find_rec($lab_no)
{
    $owner = '';

    $row = Report::find()->select(['COMPONENT', 'unit_id', 'RECV_DT1', 'name', 'lab_no'])
        ->where("lab_no='" . $lab_no . "'")->one();
    list($code1, $code2, $code3, $company) = [$row->COMPONENT, $row->unit_id, $row->RECV_DT1, $row->name];

    return [$code1, $code2, $code3, $owner, $company,];
}

// MAIN
$lab_no = $labNumber;
$vtgl1 = isset($_GET["vtgl1"]) ? $_GET["vtgl1"] : '';
$vtgl2 = isset($_GET["vtgl2"]) ? $_GET["vtgl2"] : '';
$vgroup = isset($_GET["vgroup"]) ? $_GET["vgroup"] : '';
$vcust_id = isset($_GET["vcust_id"]) ? $_GET["vcust_id"] : '';
$get_true = isset($_GET['custom']) ? $_GET['custom'] : '';
$draft = isset($_GET['draft']) ? $_GET['draft'] : '';

$vtgl_1 = '';
$vtgl_2 = '';

if (($vtgl1 != '') && ($vtgl2 != '')) {
    $vtgl_1 = substr($vtgl1, 6, 4) . "-" . substr($vtgl1, 3, 2) . "-" . substr($vtgl1, 0, 2);
    $vtgl_2 = substr($vtgl2, 6, 4) . "-" . substr($vtgl2, 3, 2) . "-" . substr($vtgl2, 0, 2);
}

show_rpt_5($lab_no, $vtgl_1, $vtgl_2, $vgroup, $vcust_id, $get_true, $draft);


