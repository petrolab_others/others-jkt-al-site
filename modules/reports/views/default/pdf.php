<?php
use yii\helpers\Url;
use app\helpers\DynamicTable;
use app\helpers\ChartDir;
$col_now=0;

$chart=new ChartDir;
$chart->deleteAllChart('wear_trending');
$chart->wear_trending(new XYChart(720, 245, 0xffffff, 0x000000, 0),$history);
$chart->deleteAllChart('tan_tbn');
$chart->tan_tbn(new XYChart(720, 245, 0xffffff, 0x000000, 0),$history);
$chart->deleteAllChart('contaminant');
$chart->contaminant(new XYChart(720, 245, 0xffffff, 0x000000, 0),$history);
$chart->deleteAllChart('visco');
$chart->visco(new XYChart(720, 245, 0xffffff, 0x000000, 0),$history);
?>
<table id="tb1">
	<tr>
		<td class="td_left">
			<img width="420" src="image/logo.png">
		</td>
		<td class="td_right">
			<div class="box-header">
				<h4 class="bold"><u>OIL ANALYSIS REPORT</u></h4>
				No. 12240/BPN/09/18
			</div>
		</td>
	</tr>
</table>
<div class="br"></div>
<div class="line_top"></div>
<div class="br"></div>
<table id="tb2">
	<tr>
		<td class="title" width="80">Company Name</td>
		<td width="4">:</td>
		<td class="value" width="180"><?=$trx->name?></td>
		<td class="title" width="100">Unit Number</td>
		<td width="4">:</td>
		<td class="value" width="180"><?= $trx->UNIT_NO ?></td>
		<td class="title" width="70">Serial Number</td>
		<td width="4">:</td>
		<td class="value">-</td>
	</tr>
	<tr>
		<td class="title">To Customer</td>
		<td>:</td>
		<td class="value">-</td>
		<td class="title">Component</td>
		<td>:</td>
		<td class="value"><?= $trx->COMPONENT ?></td>
		<td class="title">Make/Brand</td>
		<td>:</td>
		<td class="value"><?= $trx->BRAND ?></td>
	</tr>
	<tr>
		<td class="title">Location</td>
		<td>:</td>
		<td class="value"><?= $trx->address ?></td>
		<td class="title">Component Matrix</td>
		<td>:</td>
		<td class="value"><?= $trx->MATRIX ?></td>
		<td class="title">Request Order</td>
		<td>:</td>
		<td class="value">-</td>
	</tr>
	<tr>
		<td class="title">Unit Model</td>
		<td>:</td>
		<td class="value"><?= $trx->MODEL ?></td>
		<td class="title">Oil Matrix</td>
		<td>:</td>
		<td class="value"><?= $trx->OIL_MATRIX ?></td>
	</tr>
</table>
<div class="br"></div>
<div class="line_top"></div>
<div class="br"></div>
<table id="tb3">
	<tr class="tr_head">
		<th colspan="8">Test Detail</th>
		<td colspan="2" class="text-center">Overall Analysis Result:</td>
	</tr>
	<tr>
		<td colspan="3" class="td_title">Lab Number</td>
		<?php foreach($history as $trx): ?>
			<td class="value"><?= $trx->Lab_No ?></td>
		<?php endforeach; ?>
		<?=DynamicTable::get($history)?>
		<td class="text-center td_code" colspan="2" rowspan="7">
			<?=DynamicTable::evalCode($trx->EVAL_CODE)?>
		</td>
	</tr>
	<tr>
		<td colspan="3" class="td_title">Sampling Date</td>
		<?php foreach($history as $trx): ?>
			<td class="value"><?= $trx->SAMPL_DT1 ?></td>
		<?php endforeach; ?>
		<?=DynamicTable::get($history)?>
	</tr>
	<tr>
		<td colspan="3" class="td_title">Received Date</td>
		<?php foreach($history as $trx): ?>
			<td class="value"><?= $trx->RECV_DT1 ?></td>
		<?php endforeach; ?>
		<?=DynamicTable::get($history)?>
	</tr>
	<tr>
		<td colspan="3" class="td_title">Report Date</td>
		<?php foreach($history as $trx): ?>
			<td class="value"><?= $trx->RPT_DT1 ?></td>
		<?php endforeach; ?>
		<?=DynamicTable::get($history)?>
	</tr>
	<tr>
		<td colspan="3" class="td_title">Hours on Oil</td>
		<?php foreach($history as $trx): ?>
			<td class="value"><?= $trx->HRS_KM_OH ?></td>
		<?php endforeach; ?>
		<?=DynamicTable::get($history)?>
	</tr>
	<tr>
		<td colspan="3" class="td_title">Hours On Unit</td>
		<?php foreach($history as $trx): ?>
			<td class="value"><?= $trx->HRS_KM_OC ?></td>
		<?php endforeach; ?>
		<?=DynamicTable::get($history)?>
	</tr>
	<tr>
		<td colspan="3" class="td_title">Lub Oil Name</td>
		<?php foreach($history as $trx): ?>
			<td class="value"><?= $trx->OIL_TYPE ?></td>
		<?php endforeach; ?>
		<?=DynamicTable::get($history)?>
	</tr>
	<tr>
		<td colspan="3" class="td_title">Oil Change</td>
		<?php foreach($history as $trx): ?>
			<td class="value"><?= $trx->oil_change ?></td>
		<?php endforeach; ?>
		<?=DynamicTable::get($history)?>
	</tr>
	<tr class="tr_head">
		<th width="100">Physical Test</th>
		<th width="60" class="text-center">Unit</th>
		<th width="95" class="text-center">Method</th>
		<th colspan="5" class="text-center">Test Value</th>
		<th class="text-center">Attention</th>
		<th class="text-center">Urgent</th>
	</tr>
	<tr class="tr_body">
		<td class="td_title">Visc@40C (*)</td>
		<td class="text-center">cSt</td>
		<td class="text-center">ASTM D7279</td>
		<?php foreach($history as $trx): ?>
			<?=DynamicTable::tdValue($trx->visc_40_code, $trx->visc_40)?>
		<?php endforeach; ?>
		<?=DynamicTable::get($history)?>
		<td class="text-center"><?=$trx->oil_mtrx?$trx->oil_mtrx->V40maxB:''?></td>
		<td class="text-center"><?=$trx->oil_mtrx?$trx->oil_mtrx->V40maxC:''?></td>
	</tr>
	<tr class="tr_body">
		<td class="td_title">Visc@100C (*)</td>
		<td class="text-center">cSt</td>
		<td class="text-center">ASTM D7279</td>
		<?php foreach($history as $trx): ?>
			<?=DynamicTable::tdValue($trx->CST_CODE, $trx->VISC_CST)?>
		<?php endforeach; ?>
		<?=DynamicTable::get($history)?>
		<td class="text-center"><?=$trx->oil_mtrx?$trx->oil_mtrx->V100maxB:''?></td>
		<td class="text-center"><?=$trx->oil_mtrx?$trx->oil_mtrx->V100maxC:''?></td>
	</tr>
	<tr class="tr_body">
		<td class="td_title">Visc Index</td>
		<td class="text-center">-</td>
		<td class="text-center">ASTM D22070-10e1</td>
		<?php foreach($history as $trx): ?>
			<?=DynamicTable::tdValue($trx->visc_index_code, $trx->visc_index)?>
		<?php endforeach; ?>
		<?=DynamicTable::get($history)?>
		<td class="text-center"></td>
		<td class="text-center"></td>
	</tr>
	<tr class="tr_body">
		<td class="td_title">TBN</td>
		<td class="text-center">mg KOH/g</td>
		<td class="text-center">ASTM D2896-15</td>
		<?php foreach($history as $trx): ?>
			<?=DynamicTable::tdValue($trx->TBN_CODE,$trx->T_B_N)?>
		<?php endforeach; ?>
		<?=DynamicTable::get($history)?>
		<td class="text-center"><?=$trx->oil_mtrx?$trx->oil_mtrx->TBNB:''?></td>
		<td class="text-center"><?=$trx->oil_mtrx?$trx->oil_mtrx->TBNC:''?></td>
	</tr>
	<tr class="tr_body">
		<td class="td_title">TAN</td>
		<td class="text-center">mg KOH/g</td>
		<td class="text-center">ASTM D974-14e2</td>
		<?php foreach($history as $trx): ?>
			<?=DynamicTable::tdValue($trx->TAN_CODE,$trx->T_A_N)?>
		<?php endforeach; ?>
		<?=DynamicTable::get($history)?>
		<td class="text-center"><?=$trx->oil_mtrx?$trx->oil_mtrx->TANB:''?></td>
		<td class="text-center"><?=$trx->oil_mtrx?$trx->oil_mtrx->TANC:''?></td>
	</tr>
	
	<tr class="tr_head_a_u">
		<th colspan="8">Metal Additive</th>
		<td class="a_u">Warning</td>
		<td class="a_u">Limit</td>
	</tr>
	<tr class="tr_body">
		<td class="td_title">Magnesium (Mg)</td>
		<td class="text-center">ppm</td>
		<td class="text-center">ASTM D5185-13e1</td>
		<?php foreach($history as $trx): ?>
			<?=DynamicTable::tdValue($trx->MG_CODE,$trx->MAGNESIUM)?>
		<?php endforeach; ?>
		<?=DynamicTable::get($history)?>
		<td class="text-center"></td>
		<td class="text-center"></td>
	</tr>
	<tr class="tr_body">
		<td class="td_title">Calcium (Ca)</td>
		<td class="text-center">ppm</td>
		<td class="text-center">ASTM D5185-13e1</td>
		<?php foreach($history as $trx): ?>
			<?=DynamicTable::tdValue($trx->CA_CODE,$trx->CALCIUM)?>
		<?php endforeach; ?>
		<?=DynamicTable::get($history)?>
		<td class="text-center"></td>
		<td class="text-center"></td>
	</tr>
	<tr class="tr_body">
		<td class="td_title">Zinc (Zn)</td>
		<td class="text-center">ppm</td>
		<td class="text-center">ASTM D5185-13e1</td>
		<?php foreach($history as $trx): ?>
			<?=DynamicTable::tdValue($trx->ZN_CODE,$trx->ZINC)?>
		<?php endforeach; ?>
		<?=DynamicTable::get($history)?>
		<td class="text-center"></td>
		<td class="text-center"></td>
	</tr>
	<tr class="tr_body">
		<td class="td_title">Molibdenum (mo)*</td>
		<td class="text-center">ppm</td>
		<td class="text-center">ASTM D5185-13e1</td>
		<?php foreach($history as $trx): ?>
			<?=DynamicTable::tdValue($trx->Molybdenum_CODE,$trx->Molybdenum)?>
		<?php endforeach; ?>
		<?=DynamicTable::get($history)?>
		<td class="text-center"><?=$trx->oil_mtrx?$trx->wear_mtrx?$trx->wear_mtrx->MoB:'':''?></td>
		<td class="text-center"><?=$trx->oil_mtrx?$trx->wear_mtrx?$trx->wear_mtrx->MoC:'':''?></td>
	</tr>
	<tr class="tr_body">
		<td class="td_title">Boron)*</td>
		<td class="text-center">ppm</td>
		<td class="text-center">ASTM D5185-13e1</td>
		<?php foreach($history as $trx): ?>
			<?=DynamicTable::tdValue($trx->Boron_CODE,$trx->Boron)?>
		<?php endforeach; ?>
		<?=DynamicTable::get($history)?>
		<td class="text-center"></td>
		<td class="text-center"></td>
	</tr>
	<tr class="tr_body">
		<td class="td_title">Phosphor)*</td>
		<td class="text-center">ppm</td>
		<td class="text-center">ASTM D5185-13e1</td>
		<?php foreach($history as $trx): ?>
			<?=DynamicTable::tdValue($trx->phosphor_code,$trx->phosphor)?>
		<?php endforeach; ?>
		<?=DynamicTable::get($history)?>
		<td class="text-center"></td>
		<td class="text-center"></td>
	</tr>
	<tr class="tr_head_a_u">
		<th colspan="8">Contaminant</th>
		<td class="a_u"></td>
		<td class="a_u"></td>
	</tr>
	<tr class="tr_body">
		<td class="td_title">Natrium (Na)</td>
		<td class="text-center">ppm</td>
		<td class="text-center">ASTM D5185-13e1</td>
		<?php foreach($history as $trx): ?>
			<?=DynamicTable::tdValue($trx->NA_CODE, $trx->SODIUM_others)?>
		<?php endforeach; ?>
		<?=DynamicTable::get($history)?>
		<td class="text-center"><?=$trx->oil_mtrx?$trx->wear_mtrx?$trx->wear_mtrx->NaB:'':''?></td>
		<td class="text-center"><?=$trx->oil_mtrx?$trx->wear_mtrx?$trx->wear_mtrx->NaC:'':''?></td>
	</tr>
	<tr class="tr_body">
		<td class="td_title">Silicon (Si)</td>
		<td class="text-center">ppm</td>
		<td class="text-center">ASTM D5185-13e1</td>
		<?php foreach($history as $trx): ?>
			<?=DynamicTable::tdValue($trx->SI_CODE, $trx->SILICON)?>
		<?php endforeach; ?>
		<?=DynamicTable::get($history)?>
		<td class="text-center"><?=$trx->oil_mtrx?$trx->wear_mtrx?$trx->wear_mtrx->SiB:'':''?></td>
		<td class="text-center"><?=$trx->oil_mtrx?$trx->wear_mtrx?$trx->wear_mtrx->SiC:'':''?></td>
	</tr>
	<tr class="tr_body">
		<td class="td_title">Fuel Dilution *)</td>
		<td class="text-center">%v</td>
		<td class="text-center">ASTM E2412-10</td>
		<?php foreach($history as $trx): ?>
			<?=DynamicTable::tdValue($trx->DILUT_CODE, $trx->DILUTION)?>
		<?php endforeach; ?>
		<?=DynamicTable::get($history)?>
		<td class="text-center"><?=$trx->oil_mtrx?$trx->wear_mtrx?$trx->wear_mtrx->FuelB:'':''?></td>
		<td class="text-center"><?=$trx->oil_mtrx?$trx->wear_mtrx?$trx->wear_mtrx->FuelC:'':''?></td>
	</tr>
	<tr class="tr_body">
		<td class="td_title">Water Content *)</td>
		<td class="text-center">%v</td>
		<td class="text-center">ASTM E2412-10</td>
		<?php foreach($history as $trx): ?>
			<?=DynamicTable::tdValue($trx->WTR_CODE, $trx->WATER)?>
		<?php endforeach; ?>
		<?=DynamicTable::get($history)?>
		<td class="text-center"><?=$trx->oil_mtrx?$trx->wear_mtrx?$trx->wear_mtrx->WaterB:'':''?></td>
		<td class="text-center"><?=$trx->oil_mtrx?$trx->wear_mtrx?$trx->wear_mtrx->WaterC:'':''?></td>
	</tr>
	<tr class="tr_body">
		<td class="td_title">Glycol *)</td>
		<td class="text-center">%v</td>
		<td class="text-center">ASTM E2412-10</td>
		<?php foreach($history as $trx): ?>
			<?=DynamicTable::tdValue($trx->GLY_CODE, $trx->GLYCOL)?>
		<?php endforeach; ?>
		<?=DynamicTable::get($history)?>
		<td class="text-center"><?=$trx->oil_mtrx?$trx->wear_mtrx?$trx->wear_mtrx->GlycolB:'':''?></td>
		<td class="text-center"><?=$trx->oil_mtrx?$trx->wear_mtrx?$trx->wear_mtrx->GlycolC:'':''?></td>
	</tr>
	<tr class="tr_head_a_u">
		<th colspan="8">Wear Metal</th>
		<td class="a_u"></td>
		<td class="a_u"></td>
	</tr>
	<tr class="tr_body">
		<td class="td_title">Iron (Fe)</td>
		<td class="text-center">ppm</td>
		<td class="text-center">ASTM D5185-13e1</td>
		<?php foreach($history as $trx): ?>
			<?=DynamicTable::wear_metal($trx->GLY_CODE, $trx->GLYCOL)?>
		<?php endforeach; ?>
		<?=DynamicTable::get($history)?>
		<td class="text-center"><?=$trx->oil_mtrx?$trx->wear_mtrx?$trx->wear_mtrx->FeB:'':''?></td>
		<td class="text-center"><?=$trx->oil_mtrx?$trx->wear_mtrx?$trx->wear_mtrx->FeC:'':''?></td>
	</tr>
	<tr class="tr_body">
		<td class="td_title">Copper (Cu)</td>
		<td class="text-center">ppm</td>
		<td class="text-center">ASTM D5185-13e1</td>
		<?php foreach($history as $trx): ?>
			<?=DynamicTable::wear_metal($trx->CU_CODE, $trx->COPPER)?>
		<?php endforeach; ?>
		<?=DynamicTable::get($history)?>
		<td class="text-center"><?=$trx->oil_mtrx?$trx->wear_mtrx?$trx->wear_mtrx->CuB:'':''?></td>
		<td class="text-center"><?=$trx->oil_mtrx?$trx->wear_mtrx?$trx->wear_mtrx->CuC:'':''?></td>
	</tr>
	<tr class="tr_body">
		<td class="td_title">Alumunium (Al)</td>
		<td class="text-center">ppm</td>
		<td class="text-center">ASTM D5185-13e1</td>
		<?php foreach($history as $trx): ?>
			<?=DynamicTable::wear_metal($trx->AL_CODE,$trx->ALUMINIUM)?>
		<?php endforeach; ?>
		<?=DynamicTable::get($history)?>
		<td class="text-center"><?=$trx->oil_mtrx?$trx->wear_mtrx?$trx->wear_mtrx->AlB:'':''?></td>
		<td class="text-center"><?=$trx->oil_mtrx?$trx->wear_mtrx?$trx->wear_mtrx->AlC:'':''?></td>
	</tr>
	<tr class="tr_body">
		<td class="td_title">Chromium (Cr)</td>
		<td class="text-center">ppm</td>
		<td class="text-center">ASTM D5185-13e1</td>
		<?php foreach($history as $trx): ?>
			<?=DynamicTable::wear_metal($trx->CR_CODE,$trx->CHROMIUM)?>
		<?php endforeach; ?>
		<?=DynamicTable::get($history)?>
		<td class="text-center"><?=$trx->oil_mtrx?$trx->wear_mtrx?$trx->wear_mtrx->CrB:'':''?></td>
		<td class="text-center"><?=$trx->oil_mtrx?$trx->wear_mtrx?$trx->wear_mtrx->CrC:'':''?></td>
	</tr>
	<tr class="tr_body">
		<td class="td_title">Nickel (Ni)</td>
		<td class="text-center">ppm</td>
		<td class="text-center">ASTM D5185-13e1</td>
		<?php foreach($history as $trx): ?>
			<?=DynamicTable::wear_metal($trx->NI_CODE,$trx->NICKEL)?>
		<?php endforeach; ?>
		<?=DynamicTable::get($history)?>
		<td class="text-center"><?=$trx->oil_mtrx?$trx->wear_mtrx?$trx->wear_mtrx->NiB:'':''?></td>
		<td class="text-center"><?=$trx->oil_mtrx?$trx->wear_mtrx?$trx->wear_mtrx->NiC:'':''?></td>
	</tr>
	<tr class="tr_body">
		<td class="td_title">Tin (Sn)</td>
		<td class="text-center">ppm</td>
		<td class="text-center">ASTM D5185-13e1</td>
		<?php foreach($history as $trx): ?>
			<?=DynamicTable::wear_metal($trx->SN_CODE,$trx->TIN)?>
		<?php endforeach; ?>
		<?=DynamicTable::get($history)?>
		<td class="text-center"><?=$trx->oil_mtrx?$trx->wear_mtrx?$trx->wear_mtrx->SnB:'':''?></td>
		<td class="text-center"><?=$trx->oil_mtrx?$trx->wear_mtrx?$trx->wear_mtrx->SnC:'':''?></td>
	</tr>
	<tr class="tr_body">
		<td class="td_title">Lead (Pb)</td>
		<td class="text-center">ppm</td>
		<td class="text-center">ASTM D5185-13e1</td>
		<?php foreach($history as $trx): ?>
			<?=DynamicTable::wear_metal($trx->PB_CODE,$trx->LEAD)?>
		<?php endforeach; ?>
		<?=DynamicTable::get($history)?>
		<td class="text-center"><?=$trx->oil_mtrx?$trx->wear_mtrx?$trx->wear_mtrx->PbB:'':''?></td>
		<td class="text-center"><?=$trx->oil_mtrx?$trx->wear_mtrx?$trx->wear_mtrx->PbC:'':''?></td>
	</tr>
	<tr class="tr_body">
		<td class="td_title">PQ Index *)</td>
		<td class="text-center"></td>
		<td class="text-center"></td>
		<?php foreach($history as $trx): ?>
			<?=DynamicTable::wear_metal($trx->PQIndex_CODE,$trx->PQIndex)?>
		<?php endforeach; ?>
		<?=DynamicTable::get($history)?>
		<td class="text-center"><?=$trx->oil_mtrx?$trx->wear_mtrx?$trx->wear_mtrx->PQB:'':''?></td>
		<td class="text-center"><?=$trx->oil_mtrx?$trx->wear_mtrx?$trx->wear_mtrx->PQC:'':''?></td>
	</tr>
	<tr class="tr_head_a_u">
		<th colspan="8">FTIR *).</th>
		<td class="a_u">Warning</td>
		<td class="a_u">Limit</td>
	</tr>
	<tr class="tr_body">
		<td class="td_title">Soot *)</td>
		<td class="text-center">cSt</td>
		<td class="text-center">ASTM D445-12</td>
		<?php foreach($history as $trx): ?>
			<?=DynamicTable::ftir($trx->TRANS_CODE,$trx->DIR_TRANS)?>
		<?php endforeach; ?>
		<?=DynamicTable::get($history)?>
		<td class="text-center"><?=$trx->oil_mtrx?$trx->wear_mtrx?$trx->wear_mtrx->SootB:'':''?></td>
		<td class="text-center"><?=$trx->oil_mtrx?$trx->wear_mtrx?$trx->wear_mtrx->SootC:'':''?></td>
	</tr>
	<tr class="tr_body">
		<td class="td_title">Oxidation *)</td>
		<td class="text-center">cSt</td>
		<td class="text-center">ASTM D445-12</td>
		<?php foreach($history as $trx): ?>
			<?=DynamicTable::ftir($trx->OXID_CODE,$trx->OXIDATION)?>
		<?php endforeach; ?>
		<?=DynamicTable::get($history)?>
		<td class="text-center"><?=$trx->oil_mtrx?$trx->wear_mtrx?$trx->wear_mtrx->OxiB:'':''?></td>
		<td class="text-center"><?=$trx->oil_mtrx?$trx->wear_mtrx?$trx->wear_mtrx->OxiC:'':''?></td>
	</tr>
	<tr class="tr_body">
		<td class="td_title">Nitration *)</td>
		<td class="text-center">cSt</td>
		<td class="text-center">ASTM D445-12</td>
		<?php foreach($history as $trx): ?>
			<?=DynamicTable::ftir($trx->NITR_CODE,$trx->NITRATION)?>
		<?php endforeach; ?>
		<?=DynamicTable::get($history)?>
		<td class="text-center"><?=$trx->oil_mtrx?$trx->wear_mtrx?$trx->wear_mtrx->NitB:'':''?></td>
		<td class="text-center"><?=$trx->oil_mtrx?$trx->wear_mtrx?$trx->wear_mtrx->NitC:'':''?></td>
	</tr>
	<tr class="tr_body">
		<td class="td_title">Sulfation *)</td>
		<td class="text-center">cSt</td>
		<td class="text-center">ASTM D445-12</td>
		<?php foreach($history as $trx): ?>
			<?=DynamicTable::ftir($trx->SOX_CODE,$trx->SOX)?>
		<?php endforeach; ?>
		<?=DynamicTable::get($history)?>
		<td class="text-center"><?=$trx->oil_mtrx?$trx->wear_mtrx?$trx->wear_mtrx->SulB:'':''?></td>
		<td class="text-center"><?=$trx->oil_mtrx?$trx->wear_mtrx?$trx->wear_mtrx->SulC:'':''?></td>
	</tr>
	<tr class="tr_head_a_u">
		<th colspan="8">Other Analysis</th>
		<td class="a_u"></td>
		<td class="a_u"></td>
	</tr>
	<?php foreach($other_params as $other_param): ?>
		<tr class="tr_body">
			<td class="td_title"><?= $other_param->unsur ?></td>
			<td class="text-center"><?= $other_param->unit ?></td>
			<td class="text-center"><?= $other_param->methode ?></td>
			<?= $other_param->param($history,$other_param->unsur) ?>
			<td class="text-center"><?= $other_param->attention ?></td>
			<td class="text-center"><?= $other_param->urgent ?></td>
		</tr>
	<?php endforeach ?>
</table>
<div class="line_top"></div>
<div class="br"></div>
<table id="tb4">
	<tr>
		<td class="img-left">
			<img width="380" class="img-fluid" src="<?=Yii::getAlias('@chartImg')?>/wear_trending/chart.png">
		</td>
		<td class="img-right">
			<img width="380" class="img-fluid" src="<?=Yii::getAlias('@chartImg')?>/contaminant/chart.png">
		</td>
	</tr>
	<tr>
		<td class="img-left">
			<img width="380" class="img-fluid" src="<?=Yii::getAlias('@chartImg')?>/tan_tbn/chart.png">
		</td>
		<td class="img-right">
			<img width="380" class="img-fluid" src="<?=Yii::getAlias('@chartImg')?>/visco/chart.png">
		</td>
	</tr>
</table>
<div class="line_top"></div>
<section id="note">
	<b class="note-title"><u>Source of Abnormality</u></b>
	<section class="note-body">
		<p><?= $trx->RECOMM1 ?></p>	
	</section>
	<b class="note-title"><u>Action to be Taken</u></b>
	<section class="note-body">
		<p><?= $trx->RECOMM2 ?></p>		
	</section>
</section>
<br>
<table id="tb5">
	<tr>
		<td class="left">
			<p>
				(*) Berdasarkan ISO VG Grade dan SAE Viscocity Grade<br>
				Catatan : Data analisa hanya berlaku untuk sample yang diuji di laboratorium PT. Petrolab Services<br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pengaduan tidak dilayani setelah 30 hari dari tanggal report di terbitkan<br>
				*) Tidak Termasuk Ruang Lingkup Akreditasi
			</p>
		</td>
		<td class="right"><u>Manager Teknis</u></td>
	</tr>
	<tr>
		<td class="left-2"></td>
		<td class="right2"><u>Endriastuti</u></td>
	</tr>
</table>