<?php

/**
 * Created by
 * User: Wisard17
 * Date: 06/08/2018
 * Time: 05.19 PM
 */

/* @var $this \yii\web\View */
/* @var $model \app\modules\reports\models\FormFollowup */


use app\smartadmin\ActiveForm;
use yii\helpers\Html;

/* @var $form ActiveForm */
?>
<div class="add-comment-form">

    <?php $form = ActiveForm::begin([
        'id' => 'update-labNote-form',
//        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}<label class='input'>{input}</label>{error}",
//            'labelOptions' => ['class' => 'label  '],
            'inputOptions' => ['class' => 'form-control'],
        ],
        'options' => [
            'class' => 'smart-form',
        ],

    ]); ?>

    <fieldset>
        <lable>Action to be Taken</lable>
        <p><?= $model->RECOMM2?></p>
    </fieldset>

    <fieldset>
        <?= $form->field($model, 'Notes', ['template' => '{label}<label class=\'textarea\'>
        <i class="icon-append fa fa-comment"></i>
        {input}</label>{error}'])->textarea(['rows' => 4])->label('Followup for Lab No :' . $model->Lab_No) ?>
    </fieldset>


    <footer>
        <?= Html::submitButton(Yii::t('app', $model->isNewRecord ? 'Submit ' : 'Save '),
            ['class' => 'btn btn-primary', 'data-action' => 'submit_update_followup']) ?>
    </footer>

    <?php ActiveForm::end(); ?>

</div>
