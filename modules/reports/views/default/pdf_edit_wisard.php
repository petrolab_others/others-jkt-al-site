<?php

/* @var $this \yii\web\View */

/* @var $trx \app\modules\master\models\FormTransaksi|\yii\db\ActiveRecord */
/* @var $two_page int */
/* @var $bp int */

use app\helpers\DynamicTable;
use app\helpers\ChartDir;
use app\components\XYChart;

$col_now=0;

$chart=new ChartDir;
$chart->deleteAllChart('wear_trending');
$chart->wear_trending(new XYChart(720, 245, 0xffffff, 0x000000, 0),$history);
$chart->deleteAllChart('tan_tbn');
$chart->tan_tbn(new XYChart(720, 245, 0xffffff, 0x000000, 0),$history);
$chart->deleteAllChart('contaminant');
$chart->contaminant(new XYChart(720, 245, 0xffffff, 0x000000, 0),$history);
$chart->deleteAllChart('visco');
$chart->visco(new XYChart(720, 245, 0xffffff, 0x000000, 0),$history);

$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@app/smartadmin/resources/assets');
$model = $trx;
function auto($col)
{
    $out = '';
    for ($i = 0; $i < $col; $i++) {
        $out .= '<td class="value"></td>';
    }
    return $out;
}
$spaceTable = auto(5 - sizeof($model->history));
?>
<!--mpdf
<htmlpageheader name="myHTMLHeader1">
<table id="tb1">
	<tr>
		<td width="60px">
			<img width="56px" src=".<?= $directoryAsset?>/img/logo_petrolab.png">
		</td>
        <td class="" valign="middle">
            <h5 class="bold">PT PETROLAB SERVICES</h5>
            <small class="bold" style="font-size: 10px;">Independent Laboratory</small><br>
            <small style="font-size: 10px;"><?= $model->addressLogo?></small>
        </td>
		<td align="center">
			<div class="box-header">
				<h4 class="bold"><u>OIL ANALYSIS REPORT</u></h4>
				<?= $model->ADD_PHYS == '' ? '' : "No. $model->ADD_PHYS" ?>
			</div>
		</td>
	</tr>
</table>
<div class="br"></div>
<div class="line_top"></div>
<div class="br"></div>
</htmlpageheader>
mpdf-->
<!--mpdf
<sethtmlpageheader name="myHTMLHeader1" page="O" value="on" show-this-page="1" />
<sethtmlpageheader name="myHTMLHeader1" page="E" value="on" />
mpdf-->
<table id="tb2">
	<tr >
		<td class="title" width="80" valign="top">Company Name</td>
		<td width="4" valign="top">:</td>
		<td class="value" width="30%" valign="top"><?=$trx->branch2?></td>
        <td class="title" width="80" valign="top">Unit Model</td>
        <td valign="top">:</td>
        <td class="value" valign="top"><?= $trx->MODEL ?></td>
        <td class="title" width="80" valign="top">Oil Matrix</td>
        <td valign="top">:</td>
        <td class="value" valign="top"><?= $trx->OIL_MATRIX ?></td>
	</tr>
	<tr>
		<td class="title" valign="top">To Customer</td>
		<td valign="top">:</td>
		<td class="value" valign="top"><?=$trx->name?></td>
        <td class="title" valign="top">Unit Number</td>
        <td width="4" valign="top">:</td>
        <td class="value" valign="top"><?= $trx->UNIT_NO ?></td>
        <td class="title" valign="top">Serial Number</td>
        <td width="4" valign="top">:</td>
        <td class="value" valign="top"><?=$trx->unitSn?></td>
	</tr>
	<tr>
        <td class="title" valign="top">Address</td>
        <td valign="top">:</td>
        <td class="value" valign="top"><?= $trx->address ?></td>
        <td class="title" valign="top">Component</td>
        <td valign="top">:</td>
        <td class="value" valign="top"><?= $trx->COMPONENT ?></td>
        <td class="title" valign="top"><?= $bp > 0 ? 'SPB Number' : 'Make/Brand' ?></td>
        <td valign="top">:</td>
        <td class="value" valign="top"><?= $bp > 0 ? $trx->ADD_VALUE : $trx->brand2 ?></td>
	</tr>
	<tr>
        <td class="title" valign="top">Location</td>
        <td valign="top">:</td>
        <td class="value" valign="top"><?= $trx->UNIT_LOC ?></td>
        <td class="title" valign="top">Component Matrix</td>
        <td valign="top">:</td>
        <td class="value" valign="top"><?= $trx->MATRIX ?></td>
        <td class="title" valign="top">Request Order</td>
        <td valign="top">:</td>
        <td class="value" valign="top"><?=$trx->req_order?></td>
	</tr>
</table>
<div class="br"></div>
<div class="line_top"></div>
<div class="br"></div>
<table id="tb3">
	<tr class="tr_head">
		<th colspan="8">Test Detail</th>
		<td colspan="2" class="text-center">Overall Analysis Result:</td>
	</tr>
	<tr>
		<td colspan="3" class="td_title">Lab Number</td>
        <?php foreach ($model->history as $tr) { ?>
            <td class="value"><?= $tr->Lab_No ?></td>
        <?php } ?>
        <?= $spaceTable ?>
		<td class="text-center td_code" colspan="2" rowspan="7">
			<?=DynamicTable::evalCode($trx->EVAL_CODE)?>
		</td>
	</tr>
	<tr>
		<td colspan="3" class="td_title">Sampling Date</td>
        <?php foreach ($model->history as $tr) { ?>
            <td class="value"><?= $tr->SAMPL_DT1 ?></td>
        <?php } ?>
        <?= $spaceTable ?>
	</tr>
	<tr>
		<td colspan="3" class="td_title">Received Date</td>
        <?php foreach ($model->history as $tr) { ?>
            <td class="value"><?= $tr->RECV_DT1 ?></td>
        <?php } ?>
        <?= $spaceTable ?>
	</tr>
	<tr>
		<td colspan="3" class="td_title">Report Date</td>
        <?php foreach ($model->history as $tr) { ?>
            <td class="value"><?= $tr->RPT_DT1 ?></td>
        <?php } ?>
        <?= $spaceTable ?>
	</tr>
    <tr >
        <td colspan="3" class="td_title"><?= $model->getAttributeLabel('HRS_KM_OC') ?></td>
        <?php foreach ($model->history as $tr) { ?>
            <td class="value"><?= $tr->HRS_KM_OC ?></td>
        <?php } ?>
        <?= $spaceTable ?>
    </tr>
    <tr >
        <td colspan="3" class="td_title"><?= $model->getAttributeLabel('HRS_KM_TOT') ?></td>
        <?php foreach ($model->history as $tr) { ?>
            <td class="value"><?= $tr->HRS_KM_TOT ?></td>
        <?php } ?>
        <?= $spaceTable ?>
    </tr>
    <tr >
        <td colspan="3" class="td_title"><?= $model->getAttributeLabel('OIL_TYPE') ?></td>
        <?php foreach ($model->history as $tr) { ?>
            <td class="value"><?= $tr->OIL_TYPE ?></td>
        <?php } ?>
        <?= $spaceTable ?>
    </tr>
	<tr>
		<td colspan="3" class="td_title">Oil Change</td>
        <?php foreach ($model->history as $tr) { ?>
            <td class="value"><?= $tr->oil_change ?></td>
        <?php } ?>
        <?= $spaceTable ?>
        <th class='text-center'>Attention</th>
        <th class='text-center'>Urgent</th>
	</tr>

    <?php
    $check = null;
    foreach ($model->parameterByCategory() as $category => $value) {
        if ($category == 'Metal Additive' && $model->draft != 1) {
            $check = $model->checkCategoryShow($category);
            if (!is_array($check)){
                continue;
            }
        }
        if ($category == 'Physical Test') {
            echo "<tr class='tr_head'>
                <th width='100'>Physical Test</th>
                <th width='60' class='text-center'>Unit</th>
                <th width='95' class='text-center'>Method</th>
                <th colspan='5' class='text-center'>Test Value</th>
                <th class='text-center'>Min | Max</th>
                <th class='text-center'>Min | Max</th>
            </tr>";
        } elseif ($category == 'Metal Additive' || $category == 'FTIR') {
            echo "<tr class='tr_head_a_u'>
                <th colspan='8'>$category</th>
                <td class='a_u'>Warning</td>
                <td class='a_u'>Limit</td>
            </tr>";
        } else {
            echo "<tr class='tr_head_a_u'>
                <th colspan='8'>$category</th>
                <td class='a_u'></td>
                <td class='a_u'></td>
            </tr>";
        }
        foreach ($value as $parameter => $code) {
            if ($category == 'Metal Additive' && $model->draft != 1) {
                if (!in_array($parameter, $check, true)){
                    continue;
                }
            }
            if ($parameter == 'famedu' && $model->$parameter == '') {
                continue;
            }
            echo $this->render('_pdf_row_parameter', [
                'model' => $model,
                'parameter' => $parameter,
                'code' => $code,
                'spaceTable' => $spaceTable,
            ]);
        }
    }

    if (count($model->others) > 0 ) {
        echo "<tr class='tr_head_a_u'>
                <th colspan='8'>Other Analysis</th>
                <td class='a_u'></td>
                <td class='a_u'></td>
            </tr>";
        foreach ($model->others as $other) {
            echo $this->render('_pdf_others', [
                'model' => $model,
                'other' => $other,
                'spaceTable' => $spaceTable,
            ]);
        }
    }
    ?>

</table>
<div class="line_top"></div>
<div class="br"></div>
<table id="tb4">
	<tr>
		<td class="img-left">
			<img width="380" class="img-fluid" src="<?=Yii::getAlias('@chartImg')?>/wear_trending/chart.png">
		</td>
		<td class="img-right">
			<img width="380" class="img-fluid" src="<?=Yii::getAlias('@chartImg')?>/contaminant/chart.png">
		</td>
	</tr>
	<tr>
		<td class="img-left">
			<img width="380" class="img-fluid" src="<?=Yii::getAlias('@chartImg')?>/tan_tbn/chart.png">
		</td>
		<td class="img-right">
			<img width="380" class="img-fluid" src="<?=Yii::getAlias('@chartImg')?>/visco/chart.png">
		</td>
	</tr>
</table>
<div class="line_top"></div>
<?php if ($two_page == 1) {
    echo '<newpage>';
}?>
<section id="note">
	<b class="note-title"><u>Source of Abnormality</u></b>
	<section class="note-body" style="font-size: 10px; padding-left: 10px;">
		<?= $trx->RECOMM1 ?>
	</section>
	<b class="note-title"><u>Action to be Taken</u></b>
	<section class="note-body" style="font-size: 10px; padding-left: 10px;">
		<?= $trx->RECOMM2 ?>
	</section>
</section>

<div style="position: absolute;
	overflow: visible;
	left: 5mm;
	bottom: 9mm;
    font-size: 9px;
	margin: 0;">
    <p>
        (*) Berdasarkan ISO VG Grade dan SAE Viscocity Grade<br>
        *) Tidak Termasuk Ruang Lingkup Akreditasi <br>
        Catatan : Data analisa hanya berlaku untuk sample yang diuji di laboratorium PT. Petrolab Services<br>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pengaduan tidak dilayani setelah 30 hari dari tanggal report di terbitkan<br>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Report tidak boleh digandakan tanpa persetujuan tertulis dari laboratorium. <br>
        Notes : N=Normal, B=Attention, C=Urgent, D=Severe
    </p>
</div>
<table id="tb5">
	<tr >
		<td width="33%" style="height: <?= $model->draft == 1 ? 13 : 20 ?>mm;" class="right">
            <?= $model->draft == 1 ? 'Dibuat' : '' ?>
        </td>
        <td width="33%" class="right"><?= $model->draft == 1 ? 'Diperiksa' : '' ?></td>
		<td width="33%" class="right"><?= $model->draft == 1 ? 'Disetujui' : '<u>Manager Teknis</u>' ?></td>
	</tr>
	<tr>
		<td class="right"><?= $model->draft == 1 ? '<u>Pembuat Laporan</u>' : '' ?></td>
        <td class="right"><?= $model->draft == 1 ? '<u>Deputy Mgr. Teknis</u>' : '' ?></td>
		<td class="right"><?= $model->draft == 1 ? '<u>Mgr. Teknis</u>' : "<u>$model->menegerTtd</u>" ?></td>
	</tr>
</table>
