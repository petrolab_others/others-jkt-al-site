<?php

/**
 * Created by
 * User: Wisard17
 * Date: 21/11/2018
 * Time: 11.13 AM
 */

/* @var $this \yii\web\View */
/* @var $model \app\modules\master\models\FormTransaksi|\yii\db\ActiveRecord */
/* @var $parameter int|string */
/* @var $code  */
/* @var $spaceTable void */

use app\helpers\DynamicTable;
?>


<tr class="tr_body">
    <td class="td_title"><?= $model->getAttributeLabel($parameter) ?></td>
    <td class="text-center"><?= $model->getAttributeUnit($parameter) ?></td>
    <td class="text-center"><?= $model->getAttributeMethod($parameter) ?></td>
    <?php foreach ($model->history as $tr) {
        if ($tr->hasAttribute($parameter) && $tr->hasAttribute($code)) {
            echo DynamicTable::tdValue($tr->$code, $tr->$parameter);
        } elseif ($tr->hasAttribute($parameter)) {
            echo DynamicTable::tdValue('', $tr->$parameter);
        } else {
            echo DynamicTable::tdValue('', '');
        }
    } ?>
    <?= $spaceTable ?>
    <td class="text-center"><?= $model->getLimit('attention', $parameter) ?></td>
    <td class="text-center"><?= $model->getLimit('urgent', $parameter) ?></td>
</tr>
