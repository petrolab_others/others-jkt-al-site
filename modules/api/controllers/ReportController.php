<?php
/**
 * Created by
 * User     : Adam
 * Date     : 22/10/2020
 * Time     : 09.56 AM
 * File Name: ReportController.php
 **/


namespace app\modules\api\controllers;


use app\modules\api\models\UsedOilReport;
use Yii;
use yii\filters\auth\CompositeAuth;
use yii\filters\ContentNegotiator;
use yii\rest\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

/**
 * Class UserController
 * @package app\modules\api\controllers
 */
class ReportController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                // HttpBasicAuth::className(),
                // HttpBearerAuth::className(),
                // QueryParamAuth::className(),
            ],
        ];
        return $behaviors;
    }

    /**
     * @throws \yii\base\InvalidConfigException
     */

    /*=============== USED OIL start ==================*/
    public function actionUsedOil()
    {
        $requestParams = Yii::$app->getRequest()->getBodyParams();
        if (empty($requestParams)) {
            $requestParams = Yii::$app->getRequest()->getQueryParams();
        }
//        return $requestParams;
        $data = new UsedOilReport();

//        return $data->listUsedOil($requestParams);
        return $data->renderUsedOil($requestParams);
    }
    public function actionWear()
    {
        $requestParams = Yii::$app->getRequest()->getBodyParams();
        if (empty($requestParams)) {
            $requestParams = Yii::$app->getRequest()->getQueryParams();
        }
//        return $requestParams;
        $data = new UsedOilReport();

//        return $data->listUsedOil($requestParams);
        return $data->renderWear($requestParams);
    }

    public function actionDegradasi()
    {
        $requestParams = Yii::$app->getRequest()->getBodyParams();
        if (empty($requestParams)) {
            $requestParams = Yii::$app->getRequest()->getQueryParams();
        }
//        return $requestParams;
        $data = new UsedOilReport();

//        return $data->listUsedOil($requestParams);
        return $data->renderDegradasi($requestParams);
    }

    public function actionContaminant()
    {
        $requestParams = Yii::$app->getRequest()->getBodyParams();
        if (empty($requestParams)) {
            $requestParams = Yii::$app->getRequest()->getQueryParams();
        }
//        return $requestParams;
        $data = new UsedOilReport();

//        return $data->listUsedOil($requestParams);
        return $data->renderContaminant($requestParams);
    }

    public function actionView()
    {
        $requestParams = Yii::$app->getRequest()->getBodyParams();
        if (empty($requestParams)) {
            $requestParams = Yii::$app->getRequest()->getQueryParams();
        }
        
        $data = new UsedOilReport();

        return $data->detailUsedOil($requestParams);
    }
    /*=============== USED OIL end ==================*/
    
    /**
     * @param $action
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        $headers = Yii::$app->request->headers;

        return parent::beforeAction($action);
    }

    /**
     * Checks the privilege of the current user.
     *
     * This method should be overridden to check whether the current user has the privilege
     * to run the specified action against the specified data model.
     * If the user does not have access, a [[ForbiddenHttpException]] should be thrown.
     *
     * @param string $action the ID of the action to be executed
     * @param \yii\base\Model $model the model to be accessed. If `null`, it means no specific model is being accessed.
     * @param array $params additional parameters
     * @throws ForbiddenHttpException if the user does not have access
     */
    public function checkAccess($action, $model = null, $params = [])
    {
        // check if the user can access $action and $model
        // throw ForbiddenHttpException if access should be denied
        if ($action === 'update' || $action === 'delete') {
            throw new ForbiddenHttpException(sprintf('You can only %s articles that you\'ve created.', $action));
        }
    }


}