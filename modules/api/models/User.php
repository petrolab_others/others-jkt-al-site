<?php

/**
 * Created by
 * User     : Adam
 * Date     : 15/10/2020
 * Time     : 01.52 PM
 * File Name: User.php
 **/


namespace app\modules\api\models;


use app\modelsDB\Users;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\ConflictHttpException;

/**
 * Class User
 * @package app\modules\api\models
 */
class User extends Users
{

    // public $order_by;

    public $page = 1;

    public $data_per_page = 50;

    public function renderData($param)
    {
        $data = User::find()->select('*')
            ->where(['username' => $param['username']])
            ->andWhere(['password' => $param['password']])
            ->one();

        if (!empty($data)) {
            $out = [
                'status' => true,
                'user' => $data,
            ];
        }
        else{
            $out = [
                'status' => false,
                'message' => 'Incorrect Username or Password!',
            ];
        }

        return $out;
    }
}
