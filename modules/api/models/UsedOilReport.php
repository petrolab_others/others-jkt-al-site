<?php

/**
 * Created by
 * User     : Adam
 * Date     : 22/10/2020
 * Time     : 09.52 AM
 * File Name: UsedOilReport.php
 **/


namespace app\modules\api\models;


use app\components\ArrayHelper;
use app\modelsDB\TblTransaction;

/**
 * Class UsedOilReport
 * @package app\modules\api\models
 */
class UsedOilReport extends TblTransaction
{

    // public $order_by;

    public $page = 1;

    public $data_per_page = 50;

    public function listUsedOil($param)
    {
        $query = self::find()->select('Lab_No, branch, UNIT_NO, MODEL, COMPONENT, RPT_DT1, EVAL_CODE')
            ->where(['customer_id' => $param['customer_id']])
            ->andWhere(['not', ['publish' => null]])
            ->orderBy(['RECV_DT1' => SORT_DESC])
            ->all();

        // echo $data->createCommand()->getRawSql();
        // die();

        if (!empty($query)) {
            $out = [
                'status' => true,
                'data' => $query,
            ];
        }
        else{
            $out = [
                'status' => false,
                'message' => 'No Data Available',
            ];
        }

        return $out;
    }

    public function detailUsedOil($param)
    {
        $query = self::find()->select('*')
            ->innerJoinWith(['customer'])
            ->innerJoinWith(['unit'])
            ->where(['Lab_No' => $param['id']])
            ->one();

        if (!empty($query)) {
            $out = [
                'Lab_No' => $query->Lab_No, 
                'ComponentID' => $query->ComponentID, 
                'customer_id' => $query->customer_id, 
                'name' => $query->name, 
                'address' => $query->address, 
                'branch' => $query->branch, 
                'unit_id' => $query->unit_id, 
                'UNIT_NO' => $query->UNIT_NO, 
                'UNIT_LOC' => $query->UNIT_LOC, 
                'BRAND' => $query->BRAND, 
                'MODEL' => $query->MODEL, 
                'COMPONENT' => $query->COMPONENT, 
                'OIL_TYPE' => $query->OIL_TYPE, 
                'OIL_MATRIX' => $query->OIL_MATRIX, 
                'ORIG_VISC' => $query->ORIG_VISC, 
                'SAMPL_DT1' => $query->SAMPL_DT1,
                'RECV_DT1' => $query->RECV_DT1,
                'RPT_DT1' => $query->RPT_DT1,
                'HRS_KM_OH' => $query->HRS_KM_OH,
                'HRS_KM_OC' => $query->HRS_KM_OC,
                'EVAL_CODE' => $query->EVAL_CODE,
                'urgent_status' => $this->status($query->EVAL_CODE),
                'RECOMM1' => $query->RECOMM1,
                'RECOMM2' => $query->RECOMM2,
                'RECOMM3' => $query->RECOMM3,
                'RECOMM4' => $query->RECOMM4,
                'customer' => $query->customer,//customer
                'unit' => $query->unit,//unit
                'history' => $this->getHistory($query->unit_id,$query->ComponentID,$query->SAMPL_DT1)
            ];
        }
        else{
            $out = [
                'status' => false,
                'message' => 'Data Not Found',
            ];
        }

        return $out;
    }

    public function renderTransaction($data) 
    {
        $out = [];
        if (!empty($data)) {
            $out = [
                'customer_id' => $data->customer_id,
            ];
        }
        return $out;
    }

    function status($code)
    {
        $code = strtoupper($code);
        $eval = 'NORMAL';
        if ($code == 'D') {
            $eval = 'SEVERE';
        }
        if (($code == 'C') || ($code == 'U')) {
            $eval = 'URGENT';
        }
        if ($code == 'B' || $code == 'A') {
            $eval = 'ATTENTION';
        }
        if ($code == 'N') {
            $eval = 'NORMAL';
        }
        return $eval;
    }

    public $_history;

    public $limit = 5;

    public function getHistory($unit_id,$component_id,$smpl_dt1){
        if ($this->_history == null) {
            $filter = self::find()->select('Lab_No')
                ->where(['unit_id' => $unit_id])
                ->andWhere(['ComponentID' => $component_id])
                ->andWhere(['<=', 'SAMPL_DT1', $smpl_dt1])
                ->orderBy(['SAMPL_DT1' => SORT_DESC])
                ->limit($this->limit)
                ->all();
            $filter = $filter == null ? ['Lab_No' => 0] : $filter;
            $filter = ArrayHelper::getColumn($filter, 'Lab_No');
            $query = self::find();
            $query->joinWith('others');
            $query->orderBy(self::tableName() . '.SAMPL_DT1 ASC');
            $query->where(self::tableName() . ".Lab_No IN ('" . join("','", $filter) . "')");
            $this->_history = $query->all();
        }

        return $this->_history;
    }

    public function getCustomer()
    {
        return $this->hasOne(\app\modelsDB\TblCustomers::class, ['CustomerID' => 'customer_id']);
    }

    public function getUnit()
    {
        return $this->hasOne(\app\modelsDB\TblUnit::class, ['UnitID' => 'unit_id']);
    }

    public function getOthers()
    {
        return $this->hasOne(\app\modelsDB\TblOtherorder::class, ['Lab_No' => 'Lab_No']);
    }

    public function renderUsedOil($params){
        ini_set('memory_limit', '1024M');
        ini_set('max_execution_time', 300);
        $page=isset($params['page'])?$params['page']:1;
        $data_per_page=isset($params['data_per_page'])?$params['data_per_page']:50;
        $q=self::find();
        if(isset($params['customer_id'])){
            $q->where(['customer_id'=>$params['customer_id']]);
        }
        if(isset($params['lab_no'])){
            $q->andWhere(['Lab_No'=>$params['lab_no']]);
        }
        if(isset($params['unit_id'])){
            $q->andWhere(['unit_id'=>$params['unit_id']]);
        }
        $q->limit($data_per_page)->offset($data_per_page *($page-1));
        $order=isset($params['order_by'])?$params['order_by']:'RECV_DT1';
        $q->orderBy([$order=>SORT_DESC]);
        $count=$q->count();
        return [
            'data'=>$q->createCommand()->queryAll(),
            'page'=>$page,
            'data_per_page'=>$data_per_page,
            'total_page'=>floor($count/$data_per_page)+1,
            'total_data'=>$count
        ];
    }

    public function renderWear($params){
        ini_set('memory_limit', '1024M');
        ini_set('max_execution_time', 300);
        $page=isset($params['page'])?$params['page']:1;
        $data_per_page=isset($params['data_per_page'])?$params['data_per_page']:50;
        $q=self::find();
        $q->where("SI_CODE !='' OR FE_CODE !='' OR CU_CODE !=''
                                                   or CR_CODE !='' OR AL_CODE !='' OR  PB_CODE !='' OR NA_CODE !='' ");

        if(isset($params['customer_id'])){
            $q->andWhere(['customer_id'=>$params['customer_id']]);
        }
        if(isset($params['unit_no'])){
            $q->andWhere(['unit_no'=>$params['unit_no']]);
        }
        $q->limit($data_per_page)->offset($data_per_page *($page-1));
        $order=isset($params['order_by'])?$params['order_by']:'RECV_DT1';
        $q->orderBy([$order=>SORT_DESC]);
        $count=$q->count();
        $q->select(['Lab_No','customer_id','Unit_No','SAMPL_DT1','Component','FE_CODE','SI_CODE','CU_CODE','CR_CODE','AL_CODE','PB_CODE','NA_CODE','RECOMM1','RECOMM2']);
        return [
            'data'=>$q->createCommand()->queryAll(),
            'page'=>$page,
            'data_per_page'=>$data_per_page,
            'total_page'=>floor($count/$data_per_page)+1,
            'total_data'=>$count
        ];
    }
    public function renderDegradasi($params){
        ini_set('memory_limit', '1024M');
        ini_set('max_execution_time', 300);
        $page=isset($params['page'])?$params['page']:1;
        $data_per_page=isset($params['data_per_page'])?$params['data_per_page']:50;
        $q=self::find();
        $q->where("visc_40_code !='' or TBN_CODE !=''
                                                   or TAN_CODE !='' OR OXID_CODE !='' OR  CST_CODE !='' ");

        if(isset($params['customer_id'])){
            $q->andWhere(['customer_id'=>$params['customer_id']]);
        }
        if(isset($params['unit_no'])){
            $q->andWhere(['unit_no'=>$params['unit_no']]);
        }
        $q->limit($data_per_page)->offset($data_per_page *($page-1));
        $order=isset($params['order_by'])?$params['order_by']:'RECV_DT1';
        $q->orderBy([$order=>SORT_DESC]);
        $count=$q->count();
        $q->select(['Lab_No','Unit_No','customer_id','SAMPL_DT1','Component','visc_40_code','CST_CODE visc_100_code','TBN_CODE','TAN_CODE','OXID_CODE']);
        return [
            'data'=>$q->createCommand()->queryAll(),
            'page'=>$page,
            'data_per_page'=>$data_per_page,
            'total_page'=>floor($count/$data_per_page)+1,
            'total_data'=>$count
        ];
    }
    public function renderContaminant($params){
        ini_set('memory_limit', '1024M');
        ini_set('max_execution_time', 300);
        $page=isset($params['page'])?$params['page']:1;
        $data_per_page=isset($params['data_per_page'])?$params['data_per_page']:50;
        $q=self::find();
        $q->where("SI_CODE !='' or WTR_CODE !=''
                                                   or TRANS_CODE !='' OR DILUT_CODE !=''");

        if(isset($params['customer_id'])){
            $q->andWhere(['customer_id'=>$params['customer_id']]);
        }
        if(isset($params['unit_no'])){
            $q->andWhere(['unit_no'=>$params['unit_no']]);
        }
        $q->limit($data_per_page)->offset($data_per_page *($page-1));
        $order=isset($params['order_by'])?$params['order_by']:'RECV_DT1';
        $q->orderBy([$order=>SORT_DESC]);
        $count=$q->count();
        $q->select(['Lab_No','customer_id','Unit_No','SAMPL_DT1','Component','SI_CODE','WTR_CODE','TRANS_CODE SOOT_CODE','DILUT_CODE FUEL_CODE']);
        return [
            'data'=>$q->createCommand()->queryAll(),
            'page'=>$page,
            'data_per_page'=>$data_per_page,
            'total_page'=>floor($count/$data_per_page)+1,
            'total_data'=>$count
        ];
    }
}
