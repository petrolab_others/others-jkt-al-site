<?php
/**
 * Created by
 * User     : Adam
 * Date     : 15/10/2020
 * Time     : 02.36 PM
 * File Name: User.php
 **/

namespace app\modules\api;

use Yii;

/**
 * api module definition class
 */
class Module extends \yii\base\Module 
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\api\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        Yii::$app->user->enableSession = false;

    }
    
}
