<?php
/**
 * Created by
 * User: Wisard17
 * Date: 02/11/2018
 * Time: 10.57 AM
 */

namespace app\modules\master\models;


use app\modelsDB\TblUnit;
use app\modules\reports\models\Customer;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class Unit
 * @package app\modules\master\models
 *
 * @property \yii\db\ActiveQuery|\app\modules\reports\models\Customer $customer
 * @property string $customerName
 * @property \yii\db\ActiveQuery|\app\modules\master\models\Component[] $components
 * @property string $listComponent
 * @property string $componentForm
 * @property string $branchName
 * @property string $actions
 */
class Unit extends TblUnit
{
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'customerName' => 'For Customer',
            'CustomerID' => 'For Customer',
            'branchName'  => 'Customer',
        ]);
    }

    /**
     * @return string
     */
    public function getActions()
    {
        return Html::tag('div',
            Html::tag('a', 'Action <span class="caret"></span>', [
                'class' => 'btn btn-default dropdown-toggle', 'data-toggle' => 'dropdown', 'aria-expanded' => 'false'
            ]) . Html::tag('ul',
                Html::tag('li', Html::tag('a', '<i class="fa fa-caret-square-o-right"></i> view', [
                    'href' => Url::toRoute(['/master/unit/view', 'id' => $this->UnitID])])) .
                Html::tag('li', Html::tag('a', '<i class="fa fa-edit"></i> Edit', [
                    'href' => Url::toRoute(['/master/unit/edit', 'id' => $this->UnitID])])) .
                Html::tag('li', Html::tag('a', '<i class="fa fa-copy"></i> Duplicate',
                    ['href' => Url::toRoute(['/master/unit/new', 'duplicate' => $this->UnitID])])) .
                Html::tag('li', Html::tag('a', '<i class="fa fa-times"></i> Delete',
                    ['href' => Url::toRoute(['/master/unit/del', 'id' => $this->UnitID]),
                        'data-method' => 'post',
                        'data-confirm' => 'Are you absolutely sure ? You will lose all the information about this user with this action.',
                    ]))
                , ['class' => 'dropdown-menu']), ['class' => 'btn-group']);
    }

    /**
     * @return \yii\db\ActiveQuery|Customer
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['CustomerID' => 'CustomerID']);
    }

    /**
     * @return \yii\db\ActiveQuery|Component[]
     */
    public function getComponents()
    {
        return $this->hasMany(Component::className(), ['UnitID' => 'UnitID']);
    }

    public function getCustomerName()
    {
        return $this->customer == null ? '' : $this->customer->Name;
    }

    /**
     * @return string
     */
    public function getBranchName()
    {
        return $this->customer == null ? '' : ($this->customer->branch == null ? '' : $this->customer->branch->branch);
    }

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function getComponentForm()
    {
        return (new Component())->formName();
    }

    /**
     * @return string
     */
    public function getListComponent()
    {
        $out = '';
        $i = 1;
        foreach ($this->components as $component) {
            $out .= Html::tag('li', $component->ComponentID . '-' . $component->component);
            if ($i == 10) {
                $out .= Html::tag('li', Html::tag('a', 'See more...', [
                    'href' => Url::toRoute(['/master/unit/view', 'id' => $this->UnitID])]));
                break;
            }
            $i++;
        }
        return Html::tag('ul', $out);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->updatedate = date('Y-m-d H:i:s');
            return true;
        }
        return false;
    }
}