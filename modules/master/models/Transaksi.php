<?php
/**
 * Created by
 * User: Wisard17
 * Date: 07/09/2018
 * Time: 10.20 PM
 */

namespace app\modules\master\models;


use app\components\ArrayHelper;
use app\modules\customers\models\Branch;
use app\modules\reports\models\Addition;
use app\modules\reports\models\Customer;
use Yii;
use app\modules\reports\models\Report;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class Transaksi
 * @package app\modules\reports\models
 *
 * @property null|string $urlPatch
 * @property string $statusPublish
 * @property string $statusVerify
 * @property null|string $urlMpc
 */
class Transaksi extends Report
{

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'unitSn' => 'Serial Number',
            'branch2' => 'Customer Name',
            'statusPublish' => 'Published',
            'statusVerify' => 'Verified',
        ]);
    }

    /**
     * @return string
     */
    public function getActions()
    {
        return Html::tag('div',
            Html::tag('a', 'Action <span class="caret"></span>', [
                'class' => 'btn btn-default dropdown-toggle', 'data-toggle' => 'dropdown', 'aria-expanded' => 'false'
            ]) . Html::tag('ul',
                Html::tag('li', Html::tag('a', '<i class="fa fa-caret-square-o-right"></i> view', [
                    'href' => Url::toRoute(['/master/report/view', 'id' => $this->Lab_No])])) .
                Html::tag('li', Html::tag('a', '<i class="fa fa-file-pdf-o"></i> Draft', [
                    'class' => 'btn btn-flat btn-xs', 'title' => 'Download Report', 'target' => '_blank','style' => 'margin-left:10%;text-align: left;',
                    'href' => Url::to(['/reports/default/show-pdf', 'labNumber' => $this->Lab_No, 'draft' => 1])])) .
                Html::tag('li', Html::tag('a', '<i class="fa fa-file-pdf-o"></i> V.1.0 UO', [
                    'class' => 'btn btn-flat btn-xs', 'title' => 'Download Report', 'target' => '_blank','style' => 'margin-left:10%;text-align: left;',
                    'href' => Url::to(['/reports/default/show-pdf', 'labNumber' => $this->Lab_No, 'report1' => ''])])) .
                Html::tag('li', Html::tag('a', '<i class="fa fa-file-pdf-o"></i> V.2.0 ', [
                    'class' => 'btn btn-flat btn-xs', 'title' => 'Download Report', 'target' => '_blank','style' => 'margin-left:10%;text-align: left;',
                    'href' => Url::to(['/reports/default/show-pdf', 'labNumber' => $this->Lab_No, 'report2' => ''])])) .
                Html::tag('li', Html::tag('a', '<i class="fa fa-file-pdf-o"></i> V.3.0 JKT-BPN', [
                    'class' => 'btn btn-flat btn-xs', 'title' => 'Download Report', 'target' => '_blank', 'style' => 'margin-left:10%;text-align: left;',
                    'href' => Url::to(['/reports/default/show-pdf', 'labNumber' => $this->Lab_No])])) .
                Html::tag('li', Html::tag('a', '<i class="fa fa-file-pdf-o"></i> V.3.1 JKT-BPN 2 page', [
                    'class' => 'btn btn-flat btn-xs', 'title' => 'Download Report', 'target' => '_blank', 'style' => 'margin-left:10%;text-align: left;',
                    'href' => Url::to(['/reports/default/show-pdf', 'labNumber' => $this->Lab_No, 'two_page' => 1])])) .
                Html::tag('li', Html::tag('a', '<i class="fa fa-file-pdf-o"></i> V.4 BP Draft', [
                    'class' => 'btn btn-flat btn-xs', 'title' => 'Download Report', 'target' => '_blank', 'style' => 'margin-left:10%;text-align: left;',
                    'href' => Url::to(['/reports/default/show-pdf', 'labNumber' => $this->Lab_No, 'bp' => 1, 'draft' => 1])])) .
                Html::tag('li', Html::tag('a', '<i class="fa fa-file-pdf-o"></i> V.4 BP', [
                    'class' => 'btn btn-flat btn-xs', 'title' => 'Download Report', 'target' => '_blank', 'style' => 'margin-left:10%;text-align: left;',
                    'href' => Url::to(['/reports/default/show-pdf', 'labNumber' => $this->Lab_No , 'bp' => 1,])])) .
                Html::tag('li', Html::tag('a', '<i class="fa fa-edit"></i> Edit', [
                    'href' => Url::toRoute(['/master/report/edit', 'id' => $this->Lab_No])])) .
                Html::tag('li', Html::tag('a', '<i class="fa fa-refresh"></i> Update Code', [
                    'href' => Url::toRoute(['/master/report/update-eval-code', 'lab_no' => $this->Lab_No]), 'data-action' => 'update_code'])) .
                Html::tag('li', Html::tag('a', '<i class="fa fa-copy"></i> Duplicate', [
                    'href' => Url::toRoute(['/master/report/new', 'duplicate' => $this->Lab_No]), ])) .
                Html::tag('li', Html::tag('a', '<i class="fa fa-times"></i> Delete',
                    ['href' => Url::toRoute(['/master/report/del', 'id' => $this->Lab_No]),
                        'data-method' => 'post',
                        'data-confirm' => 'Are you absolutely sure ? You will lose all the information about this user with this action.',
                    ]))
                , ['class' => 'dropdown-menu']), ['class' => 'btn-group']);
    }

    /**
     * @return string
     */
    public function getStatusPublish()
    {
        $button = "Unpublished <i class='fa fa-circle-o'></i>";
        $cls = 'warning';
        if ($this->publish != null) {
            $button = "Published <i class='fa fa-check'></i>";
            $cls = 'success';
        }
        return "<a href='" . Url::toRoute(['/master/report/update-status', 'id' => $this->Lab_No]) .
            "' class='btn btn-$cls btn-xs' title='click to change status' data-action='update_status' >$button</a>";
    }


    /**
     * @return string
     */
    public function getStatusVerify()
    {
        $button = 'Not Verified ';
        $cls = 'warning';
        if ($this->verify_date != null) {
            $button = "<i class='fa fa-check'></i> Verified ";
            $cls = 'success';
        }
        return "<a href='" . Url::toRoute(['/master/report/verify-status', 'id' => $this->Lab_No]) .
            "' class='btn btn-$cls btn-xs' title='click to change status' data-action='update_status' >$button</a>";
    }

    /**
     * @return null|string
     */
    public function getUrlMpc()
    {
        $ad = Addition::findOne(['Lab_No' => $this->Lab_No, 'tbl_parameter_id' => 19]);
        return $ad == null ? null : $ad->urlFile;
    }

    /**
     * @return null|string
     */
    public function getUrlPatch()
    {
        $ad = Addition::findOne(['Lab_No' => $this->Lab_No, 'tbl_parameter_id' => 23]);
        return $ad == null ? null : $ad->urlFile;
    }

    /**
     * @param bool $runValidation
     * @param null $attributeNames
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        $result = true;

        if ((string)$this->branch === '0') {
            $branch = new Branch();
            $branch->branch = $this->branch2;
            $branch->address = $this->address;
            if (!$branch->save()) {
                $this->addError('branch', 'Customer not save with : ' . ArrayHelper::toString($branch->errors));
                $result = false;
            } else {
                $this->branch = $branch->branch;
            }
        }
        if ((string)$this->customer_id === '0') {
            $c = new Customer();
            $c->Branch = $this->branch;
            $c->Address = '-';
            $c->Name = $this->name;
            if (!$c->save()) {
                $this->addError('customer_id', 'Customer not save with : ' . ArrayHelper::toString($c->errors));
                $result = false;
            } else {
                $this->customer_id = $c->CustomerID;
            }
        }

        if ((string)$this->unit_id === '0') {
            $u = new Unit();
            $u->UnitNo = $this->UNIT_NO;
            $u->Model = $this->MODEL;
            $u->UnitLocation = $this->UNIT_LOC;
            $u->SerialNo = $this->unitSn;
            $u->CustomerID = $this->customer_id;
            if (!$u->save()) {
                $this->addError('unit_id', 'Unit not save with : ' . ArrayHelper::toString($u->errors));
                $result = false;
            } else {
                $this->unit_id = $u->UnitID;
            }
        }

        if ((string)$this->ComponentID === '0') {
            $cp = new Component();
            $cp->component = $this->component;
            $cp->ComponentCode = $this->component;
            $cp->Matrix = $this->MATRIX;
            $cp->UnitID = $this->unit_id;
            if (!$cp->save()) {
                $this->addError('ComponentID', 'Component not save with : ' . ArrayHelper::toString($cp->errors));
                $result = false;
            } else {
                $this->ComponentID = $cp->ComponentID;
            }
        }

//        if ($this->customer != null) {
//            $this->customer->branch_no = $this->customer->branch == '' ? null : $this->customer->branch->branch_no;
//            $this->customer->save();
//        }

        foreach ($this->others as $other) {
            if ($other->actionId == 'del') {
                if (!$other->isNewRecord) {
                    $other->delete();
                }
            } else if (!$other->save()) {
                $this->addError('others', "Other Parameter $other->unsur not save with : " . ArrayHelper::toString($other->errors));
                $result = false;
            }
        }
        return $result && parent::save($runValidation, $attributeNames);
    }

    public function load($data, $formName = null)
    {
        if (parent::load($data, $formName)) {
            if (isset($data['OthersOrder'])) {
                foreach ($data['OthersOrder'] as $idx => $datum) {
                    $item = OthersOrder::findOne($idx);
                    if ($item == null) {
                        $item = new OthersOrder(ArrayHelper::merge($datum, ['Lab_No' => $this->Lab_No]));
                    }
                    $item->load(['Data' => $datum], 'Data');
                    $item->actionId = isset($datum['actionId']) ? $datum['actionId'] : null;
                    $this->_others[$idx] = $item;
                }
            }
            return true;
        }
        return false;
    }
}