<?php
/**
 * Created by
 * User: Wisard17
 * Date: 29/11/2018
 * Time: 04.38 PM
 */

namespace app\modules\master\models;


use app\components\ArrayHelper;
use app\modelsDB\TblOilMatrix;
use yii\db\Query;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;

/**
 * Class OilMatrix
 * @package app\modules\master\models
 *
 * @property string $detail
 * @property string $actions
 * @property string $rowId
 */
class OilMatrix extends TblOilMatrix
{
    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            ['OIL_MATRIX', 'unique'],
        ]);
    }

    /**
     * @return string
     */
    public function getActions()
    {
        return Html::tag('div',
            Html::tag('a', '<i class="fa fa-edit"></i> Edit', [
                'href' => Url::toRoute(['/master/oil-matrix/edit', 'id' => $this->OIL_MATRIX]),
                'data-action' => 'child_edit', 'class' => 'btn btn-default btn-sm',
            ]) . Html::tag('a', '<i class="fa fa-copy"></i> Duplicate', [
                'href' => Url::toRoute(['/master/oil-matrix/new', 'duplicate' => $this->OIL_MATRIX]),
                'class' => 'btn btn-default btn-sm',
            ]) . Html::tag('a', '<i class="fa fa-times"></i> Delete', [
                'href' => Url::toRoute(['/master/oil-matrix/del', 'id' => $this->OIL_MATRIX]),
                'data-method' => 'post',
                'data-confirm' => 'Are you absolutely sure ? You will lose all the information about this user with this action.',
                'class' => 'btn btn-default btn-sm',
            ]));
    }

    public function getRowId()
    {
        return 'row_' . str_replace([' ', '-', '*', '/', '?'], '_', $this->OIL_MATRIX);
    }


    /**
     * @var array
     */
    public $allField = [
        'OIL_MATRIX' => 'OIL_MATRIX',
        'V40minB' => 'V40minB',
        'V40minC' => 'V40minC',
        'V40minD' => 'V40minD',
        'V40maxB' => 'V40maxB',
        'V40maxC' => 'V40maxC',
        'V40maxD' => 'V40maxD',
        'V100minB' => 'V100minB',
        'V100minC' => 'V100minC',
        'V100minD' => 'V100minD',
        'V100maxB' => 'V100maxB',
        'V100maxC' => 'V100maxC',
        'V100maxD' => 'V100maxD',
        'TBNB' => 'TBNB',
        'TBNC' => 'TBNC',
        'TBND' => 'TBND',
        'TANB' => 'TANB',
        'TANC' => 'TANC',
        'TAND' => 'TAND',
    ];

    public function attributeLabels()
    {
        return $this->allField;
    }

    public function getDetail()
    {
        $out = '<tr>';
        $col = 1;
        foreach ($this->attributeLabels() as $label) {
            if ($label != 'OIL_MATRIX' && $this->$label != '') {
                $out .= Html::tag('td', "$label : " . $this->$label);
                if ($col == 4) {
                    $out .= '</tr><tr>';
                    $col = 0;
                }
                $col++;
            }
        }

        return Html::tag('table', $out . '</tr>', ['class' => 'table']);
    }

    /**
     * @param $params
     * @return string
     */
    public function ordering($params)
    {
        $ncol = isset($params['order'][0]['column']) ? $params['order'][0]['column'] : 0;
        $col = (isset($params['columns'][$ncol]) && array_key_exists($params['columns'][$ncol]['data'], $this->allField)) ?
            $this->allField[$params['columns'][$ncol]['data']] : '';
        $argg = isset($params['order'][0]['dir']) ? $params['order'][0]['dir'] : 'asc';
        $table = self::tableName();

        return $col !== '' ? "$table.$col $argg " : '';
    }


    public $allData;

    public $currentData;

    /**
     * @param Query $query
     * @return int
     */
    public function calcAllData($query)
    {
        return $this->allData === null ? $query->count() : $this->allData;
    }

    /**
     * @param $params
     * @param bool $export
     * @param bool $join
     * @return $this|\yii\db\ActiveQuery|Query
     */
    public function searchData($params, $export = false, $join = true)
    {

        $odr = $this->ordering($params);

        $query = self::find();

        $query->orderBy($odr);

        $start = isset($params['start']) ? $params['start'] : 0;
        $lang = isset($params['length']) ? $params['length'] : 10;

        $this->allData = $this->calcAllData($query);

        $fltr = '';
        if (isset($params['columns'])) {
            foreach ($params['columns'] as $col) {
                if (isset($params['search']) && $params['search']['value'] !== '') {
                    $lst[] = $col['data'];
                    if (array_key_exists($col['data'], $this->allField) &&
                        !array_key_exists($col['data'], $lst) && $this->allField[$col['data']] != '') {
                        $fltr .= $fltr !== '' ? ' or ' : '';
                        $fltr .= ' `' . self::tableName() . '`.' . $this->allField[$col['data']] . " like '%" . $params['search']['value'] . "%' ";
                    }
                }
                if ($col['searchable'] === 'true' && $col['search']['value'] !== '' &&
                    array_key_exists($col['data'], $this->allField) && $this->allField[$col['data']] != '') {
                    $query->andFilterWhere(['like', '`' . self::tableName() . '`.' . $this->allField[$col['data']], $col['search']['value']]);
                }
            }
        }

        $query->andWhere($fltr);

        $this->load($params);

        $this->currentData = $query->count();

        $query->limit($lang)->offset($start);
        return $query;
    }

    /**
     * @param $params
     * @return mixed
     */
    public function searchDataTable($params)
    {
        $data = $this->searchData($params);
        return Json::encode([
            'draw' => isset ($params['draw']) ? (int)$params['draw'] : 0,
            'recordsTotal' => (int)$this->allData,
            'recordsFiltered' => (int)$this->currentData,
            'data' => $this->renderData($data, $params),
        ]);
    }

    /**
     * @param $query Query
     * @param $params
     * @return array
     */
    public function renderData($query, $params)
    {
        $out = [];

        /** @var self $model */
        foreach ($query->all() as $model) {
            $out[] = array_merge(ArrayHelper::toArray($model), [
                'actions' => $model->actions,
                'rowId' => $model->rowId,
                'detail' => $model->detail,
            ]);

        }
        return $out;
    }
}