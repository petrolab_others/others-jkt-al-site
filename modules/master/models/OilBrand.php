<?php
/**
 * Created by
 * User: Wisard17
 * Date: 05/11/2018
 * Time: 12.26 PM
 */

namespace app\modules\master\models;


use app\components\ArrayHelper;
use Yii;
use app\modelsDB\TblOilbrand;
use yii\db\Query;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;

/**
 * Class OilMatrix
 * @package app\modules\master\models
 *
 * @property string $actions
 * @property string $rowId
 */
class OilBrand extends TblOilbrand
{

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            ['OIL_TYPE', 'unique'],
        ]);
    }

    /**
     * @return string
     */
    public function getActions()
    {
        return Html::tag('div',
            Html::tag('a', 'Action <span class="caret"></span>', [
                'class' => 'btn btn-default dropdown-toggle', 'data-toggle' => 'dropdown', 'aria-expanded' => 'false'
            ]) . Html::tag('ul',
                Html::tag('li', Html::tag('a', '<i class="fa fa-caret-square-o-right"></i> view', [
                    'href' => Url::toRoute(['/master/lube-oil/view', 'id' => $this->OIL_TYPE])])) .
                Html::tag('li', Html::tag('a', '<i class="fa fa-edit"></i> Edit', [
                    'href' => Url::toRoute(['/master/lube-oil/edit', 'id' => $this->OIL_TYPE])])) .
                Html::tag('li',Html::tag('a', '<i class="fa fa-copy"></i> Duplicate', [
                    'href' => Url::toRoute(['/master/lube-oil/new', 'duplicate' => $this->OIL_TYPE]),
                ])) .
                Html::tag('li', Html::tag('a', '<i class="fa fa-times"></i> Delete',
                    ['href' => Url::toRoute(['/master/lube-oil/del', 'id' => $this->OIL_TYPE]),
                        'data-method' => 'post',
                        'data-confirm' => 'Are you absolutely sure ? You will lose all the information about this user with this action.',
                    ]))
                , ['class' => 'dropdown-menu']), ['class' => 'btn-group']);
    }

    public function getRowId()
    {
        return 'row_' . str_replace([' ', '-', '*', '/', '?'], '_', $this->OIL_TYPE);
    }


    /**
     * @var array
     */
    public $allField = [
        'OIL_TYPE' => 'OIL_TYPE',
        'ORIG_VISC' => 'ORIG_VISC',
        'SAE' => 'SAE',
        'API' => 'API',
        'OIL_MATRIX' => 'OIL_MATRIX',
        'visc_100' => 'visc_100',
        'visc_40' => 'visc_40',
        'T_A_N' => 'T_A_N',
        'T_B_N' => 'T_B_N',
    ];

    /**
     * @param $params
     * @return string
     */
    public function ordering($params)
    {
        $ncol = isset($params['order'][0]['column']) ? $params['order'][0]['column'] : 0;
        $col = (isset($params['columns'][$ncol]) && array_key_exists($params['columns'][$ncol]['data'], $this->allField)) ?
            $this->allField[$params['columns'][$ncol]['data']] : '';
        $argg = isset($params['order'][0]['dir']) ? $params['order'][0]['dir'] : 'asc';
        $table = self::tableName();

        return $col !== '' ? "$table.$col $argg " : '';
    }


    public $allData;

    public $currentData;

    /**
     * @param Query $query
     * @return int
     */
    public function calcAllData($query)
    {
        return $this->allData === null ? $query->count() : $this->allData;
    }

    /**
     * @param $params
     * @param bool $export
     * @param bool $join
     * @return $this|\yii\db\ActiveQuery|Query
     */
    public function searchData($params, $export = false, $join = true)
    {

        $odr = $this->ordering($params);

        $query = self::find();

        $query->orderBy($odr);

        $start = isset($params['start']) ? $params['start'] : 0;
        $lang = isset($params['length']) ? $params['length'] : 10;

        $this->allData = $this->calcAllData($query);

        $fltr = '';
        if (isset($params['columns'])) {
            foreach ($params['columns'] as $col) {
                if (isset($params['search']) && $params['search']['value'] !== '') {
                    $lst[] = $col['data'];
                    if (array_key_exists($col['data'], $this->allField) &&
                        !array_key_exists($col['data'], $lst) && $this->allField[$col['data']] != '') {
                        $fltr .= $fltr !== '' ? ' or ' : '';
                        $fltr .= ' `' . self::tableName() . '`.' . $this->allField[$col['data']] . " like '%" . $params['search']['value'] . "%' ";
                    }
                }
                if ($col['searchable'] === 'true' && $col['search']['value'] !== '' &&
                    array_key_exists($col['data'], $this->allField) && $this->allField[$col['data']] != '') {
                    $query->andFilterWhere(['like', '`' . self::tableName() . '`.' . $this->allField[$col['data']], $col['search']['value']]);
                }
            }
        }

        $query->andWhere($fltr);

        $this->load($params);

        $this->currentData = $query->count();

        $query->limit($lang)->offset($start);
        return $query;
    }

    /**
     * @param $params
     * @return mixed
     */
    public function searchDataTable($params)
    {
        $data = $this->searchData($params);
        return Json::encode([
            'draw' => isset ($params['draw']) ? (int)$params['draw'] : 0,
            'recordsTotal' => (int)$this->allData,
            'recordsFiltered' => (int)$this->currentData,
            'data' => $this->renderData($data, $params),
        ]);
    }

    /**
     * @param $query Query
     * @param $params
     * @return array
     */
    public function renderData($query, $params)
    {
        $out = [];

        /** @var self $model */
        foreach ($query->all() as $model) {
            $out[] = array_merge(ArrayHelper::toArray($model), [
                'actions' => $model->actions,
                'rowId' => $model->rowId,
            ]);

        }
        return $out;
    }
}