<?php
/**
 * Created by
 * User: Wisard17
 * Date: 29/11/2018
 * Time: 09.50 AM
 */

namespace app\modules\master\models\search;


use app\components\ArrayHelper;
use Yii;
use app\modules\master\models\Matrix;
use yii\db\Query;
use yii\helpers\Json;

class SearchMatrix extends Matrix
{
    /**
     * @var array
     */
    public $allField = [
        'Matrix' => 'Matrix',
        'SootB' => 'SootB',
        'SootC' => 'SootC',
        'SootD' => 'SootD',
        'OxiB' => 'OxiB',
        'OxiC' => 'OxiC',
        'OxiD' => 'OxiD',
        'NitB' => 'NitB',
        'NitC' => 'NitC',
        'NitD' => 'NitD',
        'SulB' => 'SulB',
        'SulC' => 'SulC',
        'SulD' => 'SulD',
        'FuelB' => 'FuelB',
        'FuelC' => 'FuelC',
        'FuelD' => 'FuelD',
        'GlycolB' => 'GlycolB',
        'GlycolC' => 'GlycolC',
        'GlycolD' => 'GlycolD',
        'WaterB' => 'WaterB',
        'WaterC' => 'WaterC',
        'WaterD' => 'WaterD',
        'F23' => 'F23',
        'NaB' => 'NaB',
        'NaC' => 'NaC',
        'NaD' => 'NaD',
        'SiB' => 'SiB',
        'SiC' => 'SiC',
        'SiD' => 'SiD',
        'FeB' => 'FeB',
        'FeC' => 'FeC',
        'FeD' => 'FeD',
        'CuB' => 'CuB',
        'CuC' => 'CuC',
        'CuD' => 'CuD',
        'AlB' => 'AlB',
        'AlC' => 'AlC',
        'AlD' => 'AlD',
        'CrB' => 'CrB',
        'CrC' => 'CrC',
        'CrD' => 'CrD',
        'NiB' => 'NiB',
        'NiC' => 'NiC',
        'NiD' => 'NiD',
        'SnB' => 'SnB',
        'SnC' => 'SnC',
        'SnD' => 'SnD',
        'PbB' => 'PbB',
        'PbC' => 'PbC',
        'PbD' => 'PbD',
        'MoB' => 'MoB',
        'MoC' => 'MoC',
        'MoD' => 'MoD',
        'PQB' => 'PQB',
        'PQC' => 'PQC',
        'PQD' => 'PQD',
        '4attention' => '4attention',
        '6attention' => '6attention',
        '14attention' => '14attention',
        '4urgent' => '4urgent',
        '6urgent' => '6urgent',
        '14urgent' => '14urgent',
        'SF' => 'SF',
        'iso4406B' => 'iso4406B',
        'iso4406C' => 'iso4406C',
        'iso4406D' => 'iso4406D',
    ];

    /**
     * @param $params
     * @return string
     */
    public function ordering($params)
    {
        $ncol = isset($params['order'][0]['column']) ? $params['order'][0]['column'] : 0;
        $col = (isset($params['columns'][$ncol]) && array_key_exists($params['columns'][$ncol]['data'], $this->allField)) ?
            $this->allField[$params['columns'][$ncol]['data']] : '';
        $argg = isset($params['order'][0]['dir']) ? $params['order'][0]['dir'] : 'asc';
        $table = self::tableName();

        if (isset($params['columns'][$ncol]['data']) && $params['columns'][$ncol]['data'] === 'customerName') {
            $col = 'Name';
            $table = 'tbl_customers';
        }

        return $col !== '' ? "$table.$col $argg " : '';
    }


    public $allData;

    public $currentData;

    /**
     * @param Query $query
     * @return int
     */
    public function calcAllData($query)
    {
        return $this->allData === null ? $query->count() : $this->allData;
    }

    /**
     * @param Query $query
     * @param $params
     * @return Query
     */
    public static function defaultFilterByUser($query, $params = '')
    {
        $user = Yii::$app->user->identity;

        if ($user->ruleAccess === -1) {
            return $query;
        }

        if ($user->ruleAccess === 2) {

        }

        if ($user->ruleAccess === 3) {

        }

        return $query;
    }

    /**
     * @param $params
     * @param bool $export
     * @param bool $join
     * @return $this|\yii\db\ActiveQuery|Query
     */
    public function searchData($params, $export = false, $join = true)
    {

        $odr = $this->ordering($params);

        $query = self::find();

        if (!$join) {
            $query = self::find();
        }

        $query = self::defaultFilterByUser($query, $params);

        $query->orderBy($odr);

        $start = isset($params['start']) ? $params['start'] : 0;
        $lang = isset($params['length']) ? $params['length'] : 10;

        $this->allData = $this->calcAllData($query);

        $fltr = '';
        if (isset($params['columns']))
            foreach ($params['columns'] as $col) {
                if (isset($params['search']) && $params['search']['value'] !== '') {
                    $lst[] = $col['data'];
                    if (array_key_exists($col['data'], $this->allField) &&
                        !array_key_exists($col['data'], $lst) && $this->allField[$col['data']] != '') {
                        $fltr .= $fltr !== '' ? ' or ' : '';
                        $fltr .= ' `' . self::tableName() . '`.' . $this->allField[$col['data']] . " like '%" . $params['search']['value'] . "%' ";
                    }
                }
                if ($col['searchable'] === 'true' && $col['search']['value'] !== '' &&
                    array_key_exists($col['data'], $this->allField) && $this->allField[$col['data']] != '') {
                    if ($col['data'] === 'customerName') {
                        $query->andFilterWhere(['like', '`tbl_customers`.Name', $col['search']['value']]);
                    } else {
                        $query->andFilterWhere(['like', '`' . self::tableName() . '`.' . $this->allField[$col['data']], $col['search']['value']]);
                    }
                }
            }

        $query->andWhere($fltr);

        $this->load($params);

        if ($export) {
            return $query;
        }

        $this->currentData = $query->count();

        $query->limit($lang)->offset($start);
        return $query;
    }

    /**
     * @param $params
     * @return mixed
     */
    public function searchDataTable($params)
    {
        $data = $this->searchData($params);
        return Json::encode([
            'draw' => isset ($params['draw']) ? (int)$params['draw'] : 0,
            'recordsTotal' => (int)$this->allData,
            'recordsFiltered' => (int)$this->currentData,
            'data' => $this->renderData($data, $params),
        ]);
    }

    /**
     * @param $query Query
     * @param $params
     * @return array
     */
    public function renderData($query, $params)
    {
        $out = [];

        /** @var self $model */
        foreach ($query->all() as $model) {
            $out[] = array_merge(ArrayHelper::toArray($model), [
                'actions' => $model->actions,
                'detail' => $model->detail,
            ]);

        }
        return $out;
    }
}