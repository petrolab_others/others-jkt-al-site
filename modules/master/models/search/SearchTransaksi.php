<?php
/**
 * Created by
 * User: Wisard17
 * Date: 07/09/2018
 * Time: 10.27 PM
 */

namespace app\modules\master\models\search;


use app\components\ArrayHelper;
use Yii;
use app\modules\master\models\Transaksi;
use yii\db\Query;
use yii\helpers\Json;

/**
 * Class SearchTransaksi
 * @package app\modules\reports\models\search
 */
class SearchTransaksi extends Transaksi
{
    /**
     * @var array
     */
    public $allField = [
        'grouploc' => 'grouploc',
        'Lab_No' => 'Lab_No',
        'ComponentID' => 'ComponentID',
        'customer_id' => 'customer_id',
        'name' => 'name',
        'address' => 'address',
        'branch' => 'branch',
        'unit_id' => 'unit_id',
        'UNIT_NO' => 'UNIT_NO',
        'UNIT_LOC' => 'UNIT_LOC',
        'BRAND' => 'BRAND',
        'MODEL' => 'MODEL',
        'COMPONENT' => 'COMPONENT',
        'OIL_TYPE' => 'OIL_TYPE',
        'OIL_MATRIX' => 'OIL_MATRIX',
        'ORIG_VISC' => 'ORIG_VISC',
        'SAMPL_DT1' => 'SAMPL_DT1',
        'RECV_DT1' => 'RECV_DT1',
        'RPT_DT1' => 'RPT_DT1',
        'HRS_KM_OH' => 'HRS_KM_OH',
        'HRS_KM_OC' => 'HRS_KM_OC',
        'HRS_KM_TOT' => 'HRS_KM_TOT',
        'oiladded' => 'oiladded',
        'oil_change' => 'oil_change',
        'REFF' => 'REFF',
        'ZDDP' => 'ZDDP',
        'PCCek' => 'PCCek',
        'VISC_CST' => 'VISC_CST',
        'CST_CODE' => 'CST_CODE',
        'visc_index' => 'visc_index',
        'visc_index_code' => 'visc_index_code',
        'VISC_SAE' => 'VISC_SAE',
        'SAE_CODE' => 'SAE_CODE',
        'DILUTION' => 'DILUTION',
        'DILUT_CODE' => 'DILUT_CODE',
        'DIR_TRANS' => 'DIR_TRANS',
        'TRANS_CODE' => 'TRANS_CODE',
        'OXIDATION' => 'OXIDATION',
        'OXID_CODE' => 'OXID_CODE',
        'NITRATION' => 'NITRATION',
        'NITR_CODE' => 'NITR_CODE',
        'WATER' => 'WATER',
        'WTR_CODE' => 'WTR_CODE',
        'T_A_N' => 'T_A_N',
        'TAN_CODE' => 'TAN_CODE',
        'T_B_N' => 'T_B_N',
        'TBN_CODE' => 'TBN_CODE',
        'ADD_PHYS' => 'ADD_PHYS',
        'ADD_VALUE' => 'ADD_VALUE',
        'ADD_CODE' => 'ADD_CODE',
        'NICKEL' => 'NICKEL',
        'NI_CODE' => 'NI_CODE',
        'SOX' => 'SOX',
        'SOX_CODE' => 'SOX_CODE',
        'GLYCOL' => 'GLYCOL',
        'GLY_CODE' => 'GLY_CODE',
        'SILICON' => 'SILICON',
        'SI_CODE' => 'SI_CODE',
        'IRON' => 'IRON',
        'IRON_CONV' => 'IRON_CONV',
        'FE_CODE' => 'FE_CODE',
        'COPPER' => 'COPPER',
        'COPPER_CONV' => 'COPPER_CONV',
        'CU_CODE' => 'CU_CODE',
        'ALUMINIUM' => 'ALUMINIUM',
        'ALUMUNIUM_CONV' => 'ALUMUNIUM_CONV',
        'AL_CODE' => 'AL_CODE',
        'CHROMIUM' => 'CHROMIUM',
        'CHROMIUM_CONV' => 'CHROMIUM_CONV',
        'CR_CODE' => 'CR_CODE',
        'MAGNESIUM' => 'MAGNESIUM',
        'MG_CODE' => 'MG_CODE',
        'SILVER' => 'SILVER',
        'AG_CODE' => 'AG_CODE',
        'TIN' => 'TIN',
        'SN_CODE' => 'SN_CODE',
        'LEAD' => 'LEAD',
        'LEAD_CONV' => 'LEAD_CONV',
        'PB_CODE' => 'PB_CODE',
        'SODIUM' => 'SODIUM',
        'SODIUM_CONV' => 'SODIUM_CONV',
        'NA_CODE' => 'NA_CODE',
        'CALCIUM' => 'CALCIUM',
        'CA_CODE' => 'CA_CODE',
        'ZINC' => 'ZINC',
        'ZN_CODE' => 'ZN_CODE',
        'ADD_MET' => 'ADD_MET',
        'ADD_MVAL' => 'ADD_MVAL',
        'ADD_MCODE' => 'ADD_MCODE',
        'EVAL_CODE' => 'EVAL_CODE',
        'UT_EMD' => 'UT_EMD',
        'UT_ESN' => 'UT_ESN',
        'RECOMM1' => 'RECOMM1',
        'RECOMM2' => 'RECOMM2',
        'RECOMM3' => 'RECOMM3',
        'RECOMM4' => 'RECOMM4',
        'MATRIX' => 'MATRIX',
        'Molybdenum' => 'Molybdenum',
        'Molybdenum_CODE' => 'Molybdenum_CODE',
        'Boron' => 'Boron',
        'Boron_CODE' => 'Boron_CODE',
        'Potassium' => 'Potassium',
        'Potassium_CODE' => 'Potassium_CODE',
        'Barium' => 'Barium',
        'Barium_CODE' => 'Barium_CODE',
        '4um' => '4um',
        '4um_code' => '4um_code',
        '6um' => '6um',
        '6um_code' => '6um_code',
        '15um' => '15um',
        '15um_code' => '15um_code',
        'ISO4406' => 'ISO4406',
        'ISO4406_CODE' => 'ISO4406_CODE',
        'PQIndex' => 'PQIndex',
        'PQIndex_CODE' => 'PQIndex_CODE',
        'colourcode' => 'colourcode',
        'colourcode_CODE' => 'colourcode_CODE',
        'phosphor' => 'phosphor',
        'phosphor_code' => 'phosphor_code',
        'sulphur' => 'sulphur',
        'visc_40' => 'visc_40',
        'visc_40_code' => 'visc_40_code',
        'seq_I' => 'seq_I',
        'seq_I_code' => 'seq_I_code',
        'seq_II' => 'seq_II',
        'seq_II_code' => 'seq_II_code',
        'seq_III' => 'seq_III',
        'seq_III_code' => 'seq_III_code',
        'filter_code' => 'filter_code',
        'filter_desc' => 'filter_desc',
        'magnetic_code' => 'magnetic_code',
        'magnetic_desc' => 'magnetic_desc',
        'KarlFischer' => 'KarlFischer',
        'RubbingWearSize' => 'RubbingWearSize',
        'RubbingWearConc' => 'RubbingWearConc',
        'CuttingWearSize' => 'CuttingWearSize',
        'CuttingWearConc' => 'CuttingWearConc',
        'ScuffingWearSize' => 'ScuffingWearSize',
        'ScuffingWearConc' => 'ScuffingWearConc',
        'FatigueWearSize' => 'FatigueWearSize',
        'FatigueWearConc' => 'FatigueWearConc',
        'FatigueLaminarSize' => 'FatigueLaminarSize',
        'FatigueLaminarConc' => 'FatigueLaminarConc',
        'SpheresSize' => 'SpheresSize',
        'SpheresConc' => 'SpheresConc',
        'DarkOxidesSize' => 'DarkOxidesSize',
        'DarkOxidesConc' => 'DarkOxidesConc',
        'RedOxidesSize' => 'RedOxidesSize',
        'RedOxidesConc' => 'RedOxidesConc',
        'NonFerrousSize' => 'NonFerrousSize',
        'NonFerrousConc' => 'NonFerrousConc',
        'MiscDustSize' => 'MiscDustSize',
        'MiscDustConc' => 'MiscDustConc',
        'Notes' => 'Notes',
        'VISC_CST_others' => 'VISC_CST_others',
        'T_A_N_others' => 'T_A_N_others',
        'T_B_N_others' => 'T_B_N_others',
        'NICKEL_others' => 'NICKEL_others',
        'SILICON_others' => 'SILICON_others',
        'IRON_others' => 'IRON_others',
        'COPPER_others' => 'COPPER_others',
        'ALUMINIUM_others' => 'ALUMINIUM_others',
        'CHROMIUM_others' => 'CHROMIUM_others',
        'MAGNESIUM_others' => 'MAGNESIUM_others',
        'TIN_others' => 'TIN_others',
        'LEAD_others' => 'LEAD_others',
        'SODIUM_others' => 'SODIUM_others',
        'CALCIUM_others' => 'CALCIUM_others',
        'ZINC_others' => 'ZINC_others',
        'visc_40_others' => 'visc_40_others',

        'statusPublish' => 'publish',
        'status' => 'EVAL_CODE',
        'followup' => 'Notes',
        'customerName' => 'customer_id',
        'labNumber' => 'Lab_No',
        'sampleDate' => 'SAMPL_DT1',
        'receiveDate' => 'RECV_DT1',
        'reportDate' => 'RPT_DT1',
        'unitNumber' => 'UNIT_NO',
        'component' => 'COMPONENT',
        'model' => 'MODEL',
        'serialNo' => '',
        'oilChg' => '',
    ];

    /**
     * @param $params
     * @return string
     */
    public function ordering($params)
    {
        $ncol = isset($params['order'][0]['column']) ? $params['order'][0]['column'] : 0;
        $col = (isset($params['columns'][$ncol]) && array_key_exists($params['columns'][$ncol]['data'], $this->allField)) ?
            $this->allField[$params['columns'][$ncol]['data']] : '';
        $argg = isset($params['order'][0]['dir']) ? $params['order'][0]['dir'] : 'asc';
        $table = self::tableName();

        if (isset($params['columns'][$ncol]['data']) && $params['columns'][$ncol]['data'] === 'customerName') {
            $col = 'Name';
            $table = 'tbl_customers';
        }

        return $col !== '' ? "$table.$col $argg " : '';
    }


    public $allData;

    public $currentData;

    /**
     * @param Query $query
     * @return int
     */
    public function calcAllData($query)
    {
        return $this->allData === null ? $query->count() : $this->allData;
    }

    /**
     * @param Query $query
     * @param $params
     * @return Query
     */
    public static function defaultFilterByUser($query, $params = '')
    {
        $user = Yii::$app->user->identity;

        if ($user->ruleAccess === -1) {
            return $query;
        }

        if ($user->ruleAccess === 2) {
            $allId = join(',', $user->getAllId());
            $query->andWhere('customer_id IN (' . ($allId !== '' ? $allId : '0') . ')');
            $query->andWhere('publish is not null');
        }

        if ($user->ruleAccess === 3) {
            $allId = join(',', $user->getAllId());
            $query->andWhere('customer_id IN (' . ($allId !== '' ? $allId : '0') . ')');
            $srtquery = '';
            $addRule = Json::decode($user->additionRule);
            if (isset($addRule['filter'])) {
                $filter = $addRule['filter'];
                foreach (is_array($filter) ? $filter : [] as $col => $item) {
                    if (is_array($item)) {
                        if (isset($item[1])) {
                            if ($item[1] === 'like') {
                                $srtquery = "$col like '%$item[0]%' ";
                            } else {
                                $srtquery = "$col $item[1] '$item[0]' ";
                            }
                        } else {
                            $srtquery = "$col = '$item[0]' ";
                        }
                    } else {
                        $srtquery = "$col = $item";
                    }
                }
            }
            $query->andWhere($srtquery);
            $query->andWhere('publish is not null');
        }

        return $query;
    }

    /**
     * @param $params
     * @param bool $export
     * @param bool $join
     * @return $this|\yii\db\ActiveQuery|Query
     */
    public function searchData($params, $export = false, $join = true)
    {

        $odr = $this->ordering($params);

        $query = self::find()->joinWith('customer');

        if (!$join) {
            $query = self::find();
        }

        $query = self::defaultFilterByUser($query, $params);

        $query->orderBy($odr);

        $start = isset($params['start']) ? $params['start'] : 0;
        $lang = isset($params['length']) ? $params['length'] : 10;

        $this->allData = $this->calcAllData($query);

        if (isset($params['r_start']) && $params['r_start'] != '') {
            $query->andFilterWhere(['>=', '`' . self::tableName() . '`.RECV_DT1' , date('Y-m-d H:i:s',strtotime($params['r_start']))]);
            if (isset($params['r_end']) && $params['r_end'] != '') {
                $query->andFilterWhere(['<=', '`' . self::tableName() . '`.RECV_DT1' , date('Y-m-d H:i:s',strtotime($params['r_end']))]);
            }
        }

        $fltr = '';
        if (isset($params['columns']))
            foreach ($params['columns'] as $col) {
                if (isset($params['search']) && $params['search']['value'] !== '') {
                    $lst[] = $col['data'];
                    if (array_key_exists($col['data'], $this->allField) &&
                        !array_key_exists($col['data'], $lst) && $this->allField[$col['data']] != '') {
                        $fltr .= $fltr !== '' ? ' or ' : '';
                        $fltr .= ' `' . self::tableName() . '`.' . $this->allField[$col['data']] . " like '%" . $params['search']['value'] . "%' ";
                    }
                    if ($col['data'] === 'customerName') {
                        $fltr .= $fltr !== '' ? ' or ' : '';
                        $fltr .= ' `tbl_customers`.Name ' . " like '%" . $params['search']['value'] . "%' ";
                    }
                }
                if ($col['searchable'] === 'true' && $col['search']['value'] !== '' &&
                    array_key_exists($col['data'], $this->allField) && $this->allField[$col['data']] != '') {
                    if ($col['data'] === 'customerName') {
                        $query->andFilterWhere(['like', '`tbl_customers`.Name', $col['search']['value']]);
                    } else {
                        $query->andFilterWhere(['like', '`' . self::tableName() . '`.' . $this->allField[$col['data']], $col['search']['value']]);
                    }
                }
            }

        $query->andWhere($fltr);

        $this->load($params);

        if ($export) {
            return $query;
        }

        $this->currentData = $query->count();

        $query->limit($lang)->offset($start);
        return $query;
    }

    /**
     * @param $params
     * @return mixed
     */
    public function searchDataTable($params)
    {
        $data = $this->searchData($params);
        return Json::encode([
            'draw' => isset ($params['draw']) ? intval($params['draw']) : 0,
            'recordsTotal' => intval($this->allData),
            'recordsFiltered' => intval($this->currentData),
            'data' => $this->renderData($data, $params),
        ]);
    }

    /**
     * @param $query Query
     * @param $params
     * @return array
     */
    public function renderData($query, $params)
    {
        $out = [];

        /** @var self $model */
        foreach ($query->all() as $model) {
            $out[] = array_merge(ArrayHelper::toArray($model), [
                'actions' => $model->actions,
                'status' => $model->status,
                'statusPublish' => $model->statusPublish,
                'followup' => $model->followup,
                'group' => $model->group,
                'branch' => $model->branch,
                'customerName' => $model->customerName,
                'labNumber' => $model->labNumber,
                'sampleDate' => $model->sampleDate,
                'receiveDate' => $model->receiveDate,
                'reportDate' => $model->reportDate,
                'unitNumber' => $model->unitNumber,
                'component' => $model->component,
                'model' => $model->model,
                'serialNo' => $model->serialNo,
                'oilChg' => $model->oilChg,
                'statusVerify' => $model->statusVerify,
                'rowId' => $model->rowId,
            ]);

        }
        return $out;
    }

    public function __get($name)
    {
        $param = $this->parameterByCategory();
        if (array_key_exists($name, $param['FTIR'])) {
            $value = parent::__get($name);
            return (string)$value === '0' ? '< 0.02' : $value;
        }
        if ($this->getMethod($name) == 'ASTM D5185-13e1') {
            $value = parent::__get($name);
            return (string)$value === '0' ? '< 1' : $value;
        }
        return parent::__get($name);
    }
}