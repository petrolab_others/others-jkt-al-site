<?php
/**
 * Created by
 * User: Wisard17
 * Date: 23/11/2018
 * Time: 12.15 PM
 */

namespace app\modules\master\models;


use app\components\ArrayHelper;
use app\modules\master\models\search\SearchTransaksi;
use Yii;

class UpdateEvalCode extends Transaksi
{

    public $updateEvalCode;

    /**
     * @param $params
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function updateCode($params)
    {
        $query = self::find()->joinWith('customer');

        $allField = (new SearchTransaksi())->allField;

        $count = 0;
        $fltr = '';
        if (isset($params['lab_no'])) {
            $query->andWhere(['Lab_No' => $params['lab_no']]);
            $count++;
        } elseif (isset($params['columns'])) {
            foreach ($params['columns'] as $col) {
                if (isset($params['search']) && $params['search']['value'] !== '') {
                    $lst[] = $col['data'];
                    if (array_key_exists($col['data'], $allField) &&
                        !array_key_exists($col['data'], $lst) && $allField[$col['data']] != '') {
                        $fltr .= $fltr !== '' ? ' or ' : '';
                        $fltr .= ' `' . self::tableName() . '`.' . $allField[$col['data']] . " like '%" . $params['search']['value'] . "%' ";
                        $count++;
                    }
                    if ($col['data'] === 'customerName') {
                        $fltr .= $fltr !== '' ? ' or ' : '';
                        $fltr .= ' `tbl_customers`.Name ' . " like '%" . $params['search']['value'] . "%' ";
                        $count++;
                    }
                }
                if ($col['searchable'] === 'true' && $col['search']['value'] !== '' &&
                    array_key_exists($col['data'], $allField) && $allField[$col['data']] != '') {
                    if ($col['data'] === 'customerName') {
                        $query->andFilterWhere(['like', '`tbl_customers`.Name', $col['search']['value']]);
                        $count++;
                    } else {
                        $query->andFilterWhere(['like', '`' . self::tableName() . '`.' . $allField[$col['data']], $col['search']['value']]);
                        $count++;
                    }
                }
            }
        }

        if ($count == 0) {
            $this->message = 'Filter First';
            return false;
        }

        $query->andWhere($fltr);

        $data = $query->all();
        if ($data == null) {
            $this->message = 'No Data Found';
            return false;
        }
        $statusSuccees = 0;
        $out = true;
        $msg = '';
        foreach ($data as $report) {
            if ($report->updateEvalCode()) {
                $this->data[$report->rowId] = $report->status;
                $statusSuccees++;
            } else {
                $out = false;
                $msg .=
                    "Lab No $report->Lab_No error : " . ArrayHelper::toString($report->errors) . '<br>';
            }
        }
        $this->message .= $statusSuccees > 1 ? "Success diupdate : $statusSuccees <hr>" : '';
        $this->message .= $msg;
        return $out;
    }

    /**
     * @return bool|string
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function updateEvalCode()
    {
        if ($this->publish != '') {
            $this->addError('updateEvalCode', 'Data telah dipublish');
            return false;
        }
        if ($this->oil_limit == null) {
            $this->oil_limit = $this->getWearOilMatrix()->one();
        }

        if ($this->wear_limit == null) {
            $this->wear_limit = $this->getWearMatrix()->one();
        }

        if ($this->oil_limit == null) {
            $this->addError('updateEvalCode', 'Tidak ada limit OIL MATRIX');
            $this->oil_limit = new OilMatrix();
        }

        if ($this->wear_limit == null) {
            $this->addError('updateEvalCode', 'Tidak ada limit WEAR MATRIX');
            $this->wear_limit = new Matrix();
        }

        $evalCode = '';
        foreach ($this->parameterByCategory() as $category => $items) {
            foreach ($items as $parameter => $code) {
                $c = isset($this->attributeColumnLimit()[$parameter]) ? $this->attributeColumnLimit()[$parameter] : '';
                $value = $this->$parameter;
                $value_code = '';
                if ($c != '' && $value != '') {
                    $Bstr = $c . 'B';
                    $Cstr = $c . 'C';
                    $Dstr = $c . 'D';
                    switch ($parameter) {
                        case 'visc_40' :
                        case 'VISC_CST' :
                            $minBstr = $c . 'minB';
                            $minCstr = $c . 'minC';
                            $minDstr = $c . 'minD';
                            $maxBstr = $c . 'maxB';
                            $maxCstr = $c . 'maxC';
                            $maxDstr = $c . 'maxD';
                            $minB = $this->oil_limit->hasAttribute($minBstr) ? $this->oil_limit->$minBstr : '';
                            $minC = $this->oil_limit->hasAttribute($minCstr) ? $this->oil_limit->$minCstr : '';
                            $minD = $this->oil_limit->hasAttribute($minDstr) ? $this->oil_limit->$minDstr : '';
                            $maxB = $this->oil_limit->hasAttribute($maxBstr) ? $this->oil_limit->$maxBstr : '';
                            $maxC = $this->oil_limit->hasAttribute($maxCstr) ? $this->oil_limit->$maxCstr : '';
                            $maxD = $this->oil_limit->hasAttribute($maxDstr) ? $this->oil_limit->$maxDstr : '';
                            if (($minB != '') &&
                                ($minC != '') &&
                                ($minD != '') &&
                                ($maxB != '') &&
                                ($maxC != '') &&
                                ($maxD != '')) {
                                $NilaiViscocity = '';
                                If (($maxB > $value) && ($value > $minB)) {
                                    $NilaiViscocity = '';
                                }

                                If (($minB >= $value) && ($value > $minC)) {
                                    $NilaiViscocity = 'BL';
                                    $attention = 1;
                                }
                                If (($minC >= $value) && ($value > $minD)) {
                                    $NilaiViscocity = 'CL';
                                    $urgent = 1;
                                }
                                If ($minD >= $value) {
                                    $NilaiViscocity = 'DL';
                                    $urgent = 1;
                                }


                                If (($maxB <= $value) && ($value < $maxC)) {
                                    $NilaiViscocity = 'BH';
                                    $attention = 1;
                                }
                                If (($maxC <= $value) && ($value < $maxD)) {
                                    $NilaiViscocity = 'CH';
                                    $urgent = 1;
                                }
                                If ($maxD <= $value) {
                                    $NilaiViscocity = 'DH';
                                    $urgent = 1;
                                }
                                $value_code = strlen($NilaiViscocity) > 1 ? $NilaiViscocity[0] : $NilaiViscocity;
                            }
                            break;
                        case 'T_A_N' :
                        case 'T_B_N' :
                            $B = $this->oil_limit->hasAttribute($Bstr) ? $this->oil_limit->$Bstr : '';
                            $C = $this->oil_limit->hasAttribute($Cstr) ? $this->oil_limit->$Cstr : '';
                            $D = $this->oil_limit->hasAttribute($Dstr) ? $this->oil_limit->$Dstr : '';
                            if (($B != '') && ($C != '')) {
                                if ($B > $C) {
                                    if ($value <= $B) {
                                        $value_code = 'B';
                                        $attention_TBN = 1;
                                    }
                                    if ($value <= $C) {
                                        $value_code = 'C';
                                        $attention_TBN = 1;
                                    }
                                    if ($D != '') {
                                        if ($value <= $D) {
                                            $value_code = 'D';
                                            $attention_TBN = 1;
                                        }
                                    }
                                } else {
                                    if ($value >= $B) {
                                        $value_code = 'B';
                                        $attention_TBN = 1;
                                    }
                                    if ($value >= $C) {
                                        $value_code = 'C';
                                        $urgent_TBN = 1;
                                    }
                                    if (($D != '') && $value >= $D) {
                                        $value_code = 'D';
                                        $urgent_TBN = 1;
                                    }
                                }
                            }
                            break;
                        case 'ISO4406' :
                            $um6 = substr($value, 3, 2);
                            $um14 = substr($value, 6, 2);
                            list($attention6, $attention14, $urgent6, $urgent14) = [
                                $this->wear_limit->iso6B,$this->wear_limit->iso14B,
                                $this->wear_limit->iso6C,$this->wear_limit->iso14C,
                            ];

                            $urgent = 0;
                            $attention = 0;
                            if (($attention6 == NULL || $attention14 == NULL || $urgent6 == NULL || $urgent14 == NULL)) {
                                $value_code = '';
                            } else
                                if (($urgent6 < $um6) or ($urgent14 < $um14)) {
                                    $value_code = 'C';
                                    $urgent = 1;

                                } else
                                    if (($attention6 < $um6) or ($attention14 < $um14)) {
                                        $value_code = 'B';
                                        $attention = 1;
                                    } else {
                                        $value_code = '';
                                    }
                            break;
                        default :
                            $B = $this->getLimit('attention', $parameter);
                            $C = $this->getLimit('urgent', $parameter);
                            $D = $this->getLimit('severe', $parameter);
                            if (($B != '') && ($C != '')) {
                                if ($B > $C) {
                                    if ($value <= $B) {
                                        $value_code = 'B';
                                    }
                                    if ($value <= $C) {
                                        $value_code = 'C';
                                    }
                                    if ($D != '') {
                                        if ($value <= $D) {
                                            $value_code = 'D';
                                        }
                                    }
                                } else {
                                    if ($value >= $B) {
                                        $value_code = 'B';
                                    }
                                    if ($value >= $C) {
                                        $value_code = 'C';
                                    }
                                    if (($D != '') && $value >= $D) {
                                        $value_code = 'D';
                                    }
                                }
                            }
                    }
                }
                if ($this->hasAttribute($code)) {
                    $this->$code = $value_code;
                    if (strlen($value_code) > 1) {
                        $evalCode = max($value_code[0], $evalCode);
                    } else {
                        $evalCode = max($value_code, $evalCode);
                    }
                }
            }
        }
        foreach ($this->others as $other) {
            $value_code = '';
            $B = $other->getLimit('attention');
            $C = $other->getLimit('urgent');
            $D = $other->getLimit('severe');
            $value = $other->value;
            if (($B != '') && ($C != '')) {
                if ($B > $C) {
                    if ($value <= $B) {
                        $value_code = 'B';
                    }
                    if ($value <= $C) {
                        $value_code = 'C';
                    }
                    if ($D != '') {
                        if ($value <= $D) {
                            $value_code = 'D';
                        }
                    }
                } else {
                    if ($value >= $B) {
                        $value_code = 'B';
                    }
                    if ($value >= $C) {
                        $value_code = 'C';
                    }
                    if (($D != '') && $value >= $D) {
                        $value_code = 'D';
                    }
                }
            }
            $other->value_CODE = $other->save() ? $value_code : null;
            $evalCode = $value_code > $evalCode ? $value_code : $evalCode;
        }
        $this->EVAL_CODE = $evalCode == '' ? 'N' : $evalCode;
        return $this->save();
    }

    public $message = '';

    public $status_save = '';

    public $class_message = '';

    public $data = [];

    public function save($runValidation = true, $attributeNames = null)
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            if (parent::save($runValidation, $attributeNames)) {
                $transaction->commit();
                return true;
            }
            $transaction->rollBack();
            return false;
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
}