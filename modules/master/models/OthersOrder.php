<?php
/**
 * Created by
 * User: Wisard17
 * Date: 07/09/2018
 * Time: 10.02 PM
 */

namespace app\modules\master\models;


use app\modelsDB\TblOtherorder;
use yii\helpers\Html;

/**
 * Class OthersOrder
 * @package app\modules\reports\models
 *
 * @property string $action
 * @property mixed $selectParameter
 * @property \yii\db\ActiveQuery|\app\modules\master\models\Parameters $param
 * @property string $matrix
 * @property \app\modules\master\models\Transaksi $report
 * @property \app\modules\master\models\Matrix $wearMatrix
 * @property string $idName
 */
class OthersOrder extends TblOtherorder
{

    public $_selectParameter;

    public function rules()
    {
        return array_merge(parent::rules(), [
            ['selectParameter', 'required'],
        ]);
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return Html::tag('div',
            Html::tag('a', 'Action <span class="caret"></span>', [
                'class' => "btn btn-default dropdown-toggle", 'data-toggle' => "dropdown", 'aria-expanded' => "false"
            ]) . Html::tag('ul',
//                Html::tag('li', '<a>Edit</a>') .
                Html::tag('li', '<a data-action="delate_row">Delete</a>')

                , ['class' => "dropdown-menu"]), ['class' => "btn-group"]);
    }

    public $_idName;

    /**
     * @return string
     */
    public function getIdName()
    {
        if ($this->_idName == null) {
            $this->_idName = 'new';
        }
        return $this->_idName;
    }

    /**
     * @param string $idName
     */
    public function setIdName($idName)
    {
        $this->_idName = $idName;
    }

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function formName()
    {
        return parent::formName() . "[$this->idName]";
    }

    /** @var  string|int */
    public $actionId;

    public function save($runValidation = true, $attributeNames = null)
    {
        if ($this->selectParameter == 0) {
            $parm = Parameters::findOne(['unsur' => $this->unsur, 'type' => 'other',]);
            if ($parm == null) {
                $parm = new Parameters([
                    'type' => 'other',
                    'unsur' => $this->unsur,
                ]);
            }
            $parm->load(['data' => [
                'unit' => $this->unit,
                'methode' => $this->methode,
                'attention' => $this->attention,
                'severe' => $this->severe,
                'urgent' => $this->urgent,
            ]], 'data');
            $this->tbl_parameter_id = $parm->save() ? $parm->id : null;
        }
        return parent::save($runValidation, $attributeNames);
    }

    /**
     * @return mixed
     */
    public function getSelectParameter()
    {
        if ($this->_selectParameter == null) {
            $this->_selectParameter = $this->unsur;
        }
        return $this->_selectParameter;
    }

    /**
     * @param mixed $selectParameter
     */
    public function setSelectParameter($selectParameter)
    {
        $this->_selectParameter = $selectParameter;
    }

    /** @var Parameters[] */
    public $_param = [];

    /**
     * @return Parameters|\yii\db\ActiveQuery
     */
    public function getParam()
    {
        if ($this->_param == null) {
            $this->_param = $this->hasOne(Parameters::className(), ['id' => 'tbl_parameter_id']);
        }
        return $this->_param;
    }

    public $_hubParameter;

    public function hubParameter()
    {
        if ($this->_hubParameter == null) {
            $this->_hubParameter = $this->param;
        }
        if ($this->_hubParameter == null) {
            $this->_hubParameter = Parameters::findOne(['unsur' => $this->unsur, 'type' => 'other',]);
        }
        return $this->_hubParameter;
    }

    public $_matrix;

    public function getLimit($type)
    {
        $param = $this->hubParameter();
        $wear = $this->wearMatrix;
        if ($type == 'attention') {
            if ($param != null) {
                if ($param->col_matrix != null && $wear != null) {
                    $col = $param->col_matrix . 'B';
                    if ($wear->hasAttribute($col)) {
                        return $wear->$col;
                    }
                }
                if ($this->attention == null) {
                    return $param->attention;
                }
            }
            return $this->attention;
        }
        if ($type == 'urgent') {
            if ($param != null) {
                if ($param->col_matrix != null && $wear != null) {
                    $col = $param->col_matrix . 'C';
                    if ($wear->hasAttribute($col)) {
                        return $wear->$col;
                    }
                }
                if ($this->urgent == null) {
                    return $param->urgent;
                }
            }
            return $this->urgent;
        }
        if ($type == 'severe') {
            if ($param != null) {
                if ($param->col_matrix != null && $wear != null) {
                    $col = $param->col_matrix . 'D';
                    if ($wear->hasAttribute($col)) {
                        return $wear->$col;
                    }
                }
                if ($this->severe == null) {
                    return $param->severe;
                }
            }
            return $this->severe;
        }
        return '';
    }

    /**
     * @return string
     */
    public function getMatrix()
    {
        if ($this->_matrix == null){
            $this->_matrix = $this->report == null ? '' : $this->report->MATRIX;
        }
        return $this->_matrix;
    }

    public $_wear_matrix;

    /** @var Transaksi[] */
    public $_report = [];

    /**
     * @return Transaksi|\yii\db\ActiveQuery
     */
    public function getReport()
    {
        if ($this->_report == null) {
            $this->_report = $this->hasOne(Transaksi::className(), ['Lab_No' => 'Lab_No']);
        }
        return $this->_report;
    }

    /**
     * @return Matrix
     */
    public function getWearMatrix()
    {
        if ($this->_wear_matrix == null) {
            $this->_wear_matrix = Matrix::findOne(['Matrix' => $this->matrix]);
        }
        return $this->_wear_matrix;
    }
}