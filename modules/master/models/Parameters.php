<?php
/**
 * Created by
 * User: Wisard17
 * Date: 19/11/2018
 * Time: 08.58 AM
 */

namespace app\modules\master\models;


use app\modules\reports\models\Parameter;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 *
 * @property string $actions
 */
class Parameters extends Parameter
{
    /**
     * @return string
     */
    public function getActions()
    {
        return Html::tag('div',
            Html::tag('a', 'Action <span class="caret"></span>', [
                'class' => 'btn btn-default dropdown-toggle', 'data-toggle' => 'dropdown', 'aria-expanded' => 'false'
            ]) . Html::tag('ul',
                Html::tag('li', Html::tag('a', '<i class="fa fa-caret-square-o-right"></i> view', [
                    'href' => Url::toRoute(['/master/parameter/view', 'id' => $this->id])])) .
                Html::tag('li', Html::tag('a', '<i class="fa fa-edit"></i> Edit', [
                    'href' => Url::toRoute(['/master/parameter/edit', 'id' => $this->id])])) .
                Html::tag('li', Html::tag('a', '<i class="fa fa-times"></i> Delete',
                    ['href' => Url::toRoute(['/master/parameter/del', 'id' => $this->id]),
                        'data-method' => 'post',
                        'data-confirm' => 'Are you absolutely sure ? You will lose all the information about this user with this action.',
                    ]))
                , ['class' => 'dropdown-menu']), ['class' => 'btn-group']);
    }
}