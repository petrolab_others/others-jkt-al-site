<?php
/**
 * Created by
 * User: Wisard17
 * Date: 29/11/2018
 * Time: 11.46 AM
 */

namespace app\modules\master\models;


use app\components\ArrayHelper;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/**
 * Class FormUnit
 * @package app\modules\master\models
 */
class FormUnit extends Unit
{
    /** @var Component[] */
    public $_component = [];

    /**
     * @param array $data
     * @param null $formName
     * @return bool
     */
    public function load($data, $formName = null)
    {
        if (parent::load($data, $formName)) {
            if (isset($data['Component'])) {
                foreach ($data['Component'] as $idx => $datum) {
                    $item = Component::findOne($idx);
                    if ($item == null) {
                        $item = new Component(ArrayHelper::merge($datum, ['UnitID' => $this->UnitID]));
                    }
                    $item->load(['Data' => $datum], 'Data');
                    $item->actionId = isset($datum['actionId']) ? $datum['actionId'] : null;
                    $this->_component[$idx] = $item;
                }
            }
            return true;
        }
        return false;
    }

    /**
     * @param bool $runValidation
     * @param null $attributeNames
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        if (parent::save($runValidation, $attributeNames)) {
            $result = true;
            foreach ($this->_component as $component) {
                $component->UnitID = $this->UnitID;
                if ($component->actionId == 'del') {
                    if (!$component->isNewRecord) {
                        $component->delete();
                    }
                } else if (!$component->save()) {
                    $this->addError('components', "error save component '$component->component' with (" .
                        ArrayHelper::toString($component->errors) . ')');
                    $result = false;
                }
            }
            return $result;
        }
        return false;
    }

    public function getComponents()
    {
        if ($this->_component == []) {
            $this->_component = parent::getComponents();
        }
        return $this->_component;
    }

    /**
     * @param ActiveForm $form
     * @return string
     */
    public function renderComponent($form)
    {
        $out = '<thead><tr>
                    <th class="text-center">action</th>
                    <th class="text-center" >Component</th>
                    <th class="text-center" >Component Code</th>
                    <th class="text-center">Matrix</th>
                    
                    </tr></thead><tbody>';
        foreach ($this->components as $idx => $component) {
            $component->idName = $component->ComponentID == null ? 'newrow_' . $idx : $component->ComponentID;
            $out .= Html::tag('tr', '' .
                Html::tag('td', $component->action) .
                Html::tag('td', '' .
                    $form->field($component, 'component', ['template' => '<label class="input">{input}</label>{error}'])->textInput()
                ) .
                Html::tag('td', $form->field($component, 'ComponentCode', ['template' => '<label class="input">{input}</label>{error}'])) .
                Html::tag('td', $form->field($component, 'Matrix', ['template' => '<label class="input">{input}</label>{error}']))
                , ['data-id' => $component->idName]
            );
        }

        return Html::tag('table', $out . '</tbody>', ['class' => "table table-hover"]);
    }

    public static function renderRowAjax($rowTo = 0)
    {
        $form = ActiveForm::begin([
            'fieldConfig' => [
                'template' => '<label class="input">{input}</label>{error}',
            ]
        ]);
        $component = new Component();
        $component->idName = 'newrow_' . $rowTo;

        $out = Html::tag('tr', '' .
            Html::tag('td', $component->action) .
            Html::tag('td', '' .
                $form->field($component, 'component')->textInput()
            ) .
            Html::tag('td', $form->field($component, 'ComponentCode')) .
            Html::tag('td', $form->field($component, 'Matrix'))
            , ['data-id' => $component->idName]
        );

        return [
            'data' => $out,
            'jsAdd' => ArrayHelper::toArray($form->attributes),
        ];
    }

}