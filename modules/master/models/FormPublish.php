<?php
/**
 * create Feri Gunawan
 * 2019-12-10
 */

namespace app\modules\master\models;
use app\modelsDB\TblComponent;
use app\modules\reports\models\Report;
use Yii;
use app\components\ArrayHelper;
use app\modules\master\models\search\SearchTransaksi;
use Curl\Curl;
use yii\helpers\Json;

class FormPublish extends Report
{
    public function save($runValidation = true, $attributeNames = null)
    {
        $transaction = $this::getDb()->beginTransaction();
        try {
            $this->publish = $this->publish === '' ? null : $this->publish;
            if (parent::save($runValidation = true, $attributeNames = null)) {
                $this->status_save = 'success';
                $this->message = 'Berhasil Disimpan';
                $transaction->commit();
                return true;
            }

            $this->status_save = 'error';
            $this->message = 'Data Tidak disimpan, Coba lagi.<br>' . ArrayHelper::toString($this->errors);

            $transaction->rollBack();
            return false;
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    /** @var string */
    public $message = '';

    /** @var string */
    public $status_save = '';

    /** @var array */
    public $data = [];

    /**
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function changeState()
    {
        $user = Yii::$app->user->identity;
        if (in_array($user->ruleAccess, [2, 3], true)) {
            $this->addError('publish', 'this user does not have permission ');
            return false;
        }
         $this->publish = date('Y-m-d H:i:s');

        return $this->save();
    }

    /** @var string  */
//    public $url_api = 'http://localhost/report/web/api/';
    public $url_api = 'https://report.petrolab.co.id/api/';



    /** @var array  */
    public $dataApi = [];

    /**
     * @param \app\modules\master\models\FormDataUpdateStatus[] $data
     * @return bool
     * @throws \ErrorException
     */
    public function saveApi($data)
    {

//        $this->url_api = Yii::$app->params['api_erp'];
        $api = [];
        $tgl_max=max(ArrayHelper::getColumn($data,'RECV_DT1'));
        $tgl_min=min(ArrayHelper::getColumn($data,'RECV_DT1'));
        $comp=TblComponent::find()->where("updatedate>='$tgl_min' and updatedate<='$tgl_max'")->orderBy('updatedate ASC')->all();
        foreach ($comp as $updateStatus) {
            $api['component'][] = $updateStatus->toArray();
        }
        foreach ($data as $updateStatus) {
                $api['data'][] = $updateStatus->toArray();
        }
        if ($api == []) {
            return true;
        }
        $c = new Curl();
        $c->setTimeout(10000);
        $c->setOpt(CURLOPT_SSL_VERIFYPEER, false);
        $c->post($this->url_api . 'data-upload', ['all' =>Json::encode($api)]);
        if ($c->error) {
            return false;
        }
        if (isset($c->response->sv)) {
            $this->dataApi = [
                'save' => ArrayHelper::toArray($c->response->sv),
            ];
        }
        return true;
    }

    /**
     * @param $params
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function updateStatus($params)
    {

        $model = new SearchTransaksi();
        $filter = $model->searchData($params, true);

        $query = self::find()->joinWith('customer');
        $query->where = $filter->where;
//        $query->joinWith = $filter->joinWith;

        if ($this->Lab_No != '') {
            $query->andWhere(['Lab_No' => $this->Lab_No]);
        }

        if ($query->where == [] || $query->where == '') {
            $this->message = 'Filter First';
            return false;
        }

        $data = $query->noCache()->all();

        if ($data == null) {
            $this->message = 'No Data Found';
            return false;
        }

        if (!$this->saveApi($data)) {
            $this->message = 'Not Connect to http://report.petrolab.co.id';
            return false;
        }

        $statusSuccees = 0;
        $out = true;
        $msg = '';
        foreach ($data as $report) {
            if (isset($this->dataApi['save'][$report->Lab_No])) {
//                if ($report->changeState()) {
//                    $this->data[$report->rowId] = $report->statusPublish;
                    $statusSuccees++;

//                } else {
//                    $out = false;
//                    $msg .=
//                        "Lab No $report->Lab_No error : " . ArrayHelper::toString($report->errors) . '<br>';
//                }
            } else {
                if (isset($this->dataApi['der'][$report->Lab_No])) {
                    $msg .=
                        "Lab No $report->Lab_No error disimpan di http://report.petrolab.co.id " .
                        $this->dataApi['der'][$report->Lab_No] . '<br>';
                } else {
                    $report->changeState();
//                    $this->data[$report->rowId] = $report->statusPublish;
                }
            }
        }
        $this->message .= $statusSuccees > 1 ? "Success diupload : $statusSuccees <hr>" : '';
        $this->message .= $msg;
        return $out;
    }
}