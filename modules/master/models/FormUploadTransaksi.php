<?php /** @noinspection ALL */
/** @noinspection ALL */

/**
 * Created by
 * User: Wisard17
 * Date: 21/11/2018
 * Time: 12.39 PM
 */

namespace app\modules\master\models;


use app\components\ArrayHelper;
use app\components\Excel;
use yii\web\UploadedFile;

/**
 * Class FormUploadTransaksi
 * @package app\modules\master\models
 */
class FormUploadTransaksi extends Transaksi
{
    public $uploadFile;

    public function rules()
    {
        return array_merge(parent::rules(), [
            [['Lab_No'], 'required'],
            [['ComponentID', 'customer_id', 'unit_id', 'visc_index'], 'safe'],
            [['mpc'], 'string'],
            [['uploadFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'xlxs, xls, csv', 'maxSize' => 10],
        ]);
    }

    /**
     * @param UploadedFile|null $image
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function upload(UploadedFile $image = null)
    {
        $transaction = $this::getDb()->beginTransaction();
        try {
            $excel = new Excel();
            if ($image != null) {
                $excel = $excel::renderData($image->tempName);
                $arrExcel = $excel->getActiveSheet()->toArray();
                $countInput = 0;
                $countError = 0;
                $header = [];
                $idxLabNo = '';
                $l = ['ADD_PHYS' => 'ReportNumber', 'req_order' => 'ReqOrder', 'RECOMM1' => 'RECOMM1',
                    'RECOMM2' => 'RECOMM2', 'RECOMM3' => 'RECOMM3', 'Lab_No' => 'Lab_No'];
                foreach ($arrExcel as $i => $item) {
                    if ($i == 0) {
                        foreach ($item as $key => $col) {
                            if ($this->hasAttribute($col)) {
                                if ($col == 'Lab_No') {
                                    $idxLabNo = $key;
                                } else {
                                    $header[$key] = $col;
                                }
                            } else {
                                $c2 = $this->indexOf($l, str_replace(' ', '', $col));
                                if ($c2) {
                                    if ($col == 'Lab_No') {
                                        $idxLabNo = $key;
                                    } else {
                                        $header[$key] = $c2;
                                    }
                                } else {
                                    $this->dataMessage .= "Kolom $col tidak terdaftar dalam table DataBase <br>";
                                }
                            }
                        }
                        if ($idxLabNo === '') {
                            $this->dataMessage .= "File Excel tidak memiliki Kolom <strong>Lab_No</strong> sebagai identifikasi data <br>";
                            return false;
                        }
                    } else {
                        $labNo = isset($item[$idxLabNo]) ? $item[$idxLabNo] : '';
                        $report = self::findOne(['Lab_No' => $labNo]);
                        if ($report == null) {
                            $report = new self(['Lab_No' => $labNo]);
                        }
                        foreach ($header as $k => $h) {
                            if ($report->hasAttribute($h)) {
                                if (in_array($h, ['SAMPL_DT1', 'RECV_DT1', 'RPT_DT1', 'publish'], true)){
                                    if (is_numeric($item[$k])) {
                                        /** @noinspection SummerTimeUnsafeTimeManipulationInspection */
                                        $time = ($item[$k] - 25569) * 86400;
                                        $report->$h = date('Y-m-d',  $time);
                                    } else {
                                        $report->$h = date('Y-m-d', strtotime($item[$k]));
                                    }
                                } else {
                                    $report->$h = (string)$item[$k];
                                }
                            }
                        }
                        if ($report->save()) {
                            $countInput++;
                        } else {
                            $countError++;
                            $this->dataMessage .= "Lab No : $item[$idxLabNo] error dengan " . ArrayHelper::toString($report->errors) . ' <br>';
                        }
                    }
                }

                $this->status_save = $countInput > 0 ? 'success' : 'error';
                $this->message = "Upload berhasil dengan <br><br> Jumlah Success : $countInput <br>" .
                    ($countError > 0 ? "Jumlah Error : $countError <br>" : '') . '<br><hr>';
                $transaction->commit();
                return true;
            }
            $this->status_save = 'error';
            $this->message = 'Data Tidak disimpan, Coba lagi.<br>' . ArrayHelper::toString($this->errors);

            $transaction->rollBack();
            return false;
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    /**
     * @param array $arr
     * @param $keyword
     * @return bool|int|string
     */
    protected function indexOf(array $arr, $keyword)
    {
        foreach ($arr as $idx => $item) {
            if (stripos($item, $keyword) !== false) {
                return $idx;
                break;
            }
        }
        return false;
    }

    public $message = '';

    public $status_save = '';

    public $class_message = '';

    public $dataMessage = '';
}