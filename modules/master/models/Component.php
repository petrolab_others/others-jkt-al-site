<?php
/**
 * Created by
 * User: Wisard17
 * Date: 08/09/2018
 * Time: 09.43 AM
 */

namespace app\modules\master\models;


use app\modelsDB\TblComponent;
use yii\helpers\Html;

/**
 * Class Component
 * @package app\modules\master\models
 *
 * @property string $idName
 * @property string $action
 */
class Component extends TblComponent
{

    /**
     * @return string
     */
    public function getAction()
    {
        return Html::tag('div',
            Html::tag('a', 'Action <span class="caret"></span>', [
                'class' => "btn btn-default dropdown-toggle", 'data-toggle' => "dropdown", 'aria-expanded' => "false"
            ]) . Html::tag('ul',
//                Html::tag('li', '<a>Edit</a>') .
                Html::tag('li', '<a data-action="delate_row">Delete</a>')

                , ['class' => "dropdown-menu"]), ['class' => "btn-group"]);
    }

    public $_idName;

    /**
     * @return string
     */
    public function getIdName()
    {
        if ($this->_idName == null) {
            $this->_idName = 'new';
        }
        return $this->_idName;
    }

    /**
     * @param string $idName
     */
    public function setIdName($idName)
    {
        $this->_idName = $idName;
    }

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function formName()
    {
        return parent::formName() . "[$this->idName]";
    }

    /** @var  string|int */
    public $actionId;

}