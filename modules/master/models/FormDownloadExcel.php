<?php
/**
 * Created by
 * User: Wisard17
 * Date: 28/11/2018
 * Time: 09.37 AM
 */

namespace app\modules\master\models;


use app\components\Excel;
use app\components\ArrayHelper;
use app\modules\master\models\search\SearchTransaksi;
use PHPExcel_Style_Border;

/**
 * Class FormDownloadExcel
 * @package app\modules\master\models
 *
 * @property array $propertySelect
 * @property array $parameterSelect
 */
class FormDownloadExcel extends SearchTransaksi
{

    public $code = false;

    public $csv = false;

    public $type = 'excel';

    public $property = [
        'Lab_No',
        'EVAL_CODE',
        'SAMPL_DT1',
        'RECV_DT1',
        'RPT_DT1',
        'MODEL',
        'UNIT_NO',
        'UNIT_LOC',
        'unitSn',
        'RECOMM1',
        'RECOMM2',
        'Notes',
    ];

    public $parameter = [];

    public $temp_parameter = [
        'visc_40' => 'visc_40_code',
        'VISC_CST' => 'CST_CODE',
        'visc_index' => 'visc_index_code',
        'T_B_N' => 'TBN_CODE',
        'T_A_N' => 'TAN_CODE',
        'MAGNESIUM' => 'MG_CODE',
        'CALCIUM' => 'CA_CODE',
        'ZINC' => 'ZN_CODE',
        'Molybdenum' => 'Molybdenum_CODE',
        'Boron' => 'Boron_CODE',
        'phosphor' => 'phosphor_code',
        'SODIUM_others' => 'NA_CODE',
        'SILICON_others' => 'SI_CODE',
        'DILUTION' => 'DILUT_CODE',
        'WATER' => 'WTR_CODE',
        'GLYCOL' => 'GLY_CODE',
        'IRON_others' => 'FE_CODE',
        'COPPER_others' => 'CU_CODE',
        'ALUMINIUM_others' => 'AL_CODE',
        'CHROMIUM_others' => 'CR_CODE',
        'NICKEL_others' => 'NI_CODE',
        'TIN_others' => 'SN_CODE',
        'LEAD_others' => 'PB_CODE',
        'PQIndex' => 'PQIndex_CODE',
        'SILVER' => 'AG_CODE',
        'titanium' => 'titanium_code',
        'DIR_TRANS' => 'TRANS_CODE',
        'OXIDATION' => 'OXID_CODE',
        'NITRATION' => 'NITR_CODE',
        'SOX' => 'SOX_CODE',
        'particle4' => 'particle4_code',
        'particle6' => 'particle6_code',
        'particle14' => 'particle14_code',
        'iso4406' => 'iso4406_code',
        'particle5' => 'particle5_code',
        'particle15' => 'particle15_code',
        'particle25' => 'particle25_code',
        'particle50' => 'particle50_code',
        'particle100' => 'particle100_code',
        'nasclass1638' => 'nasclass1638_code',
        'mpc' => 'mpc_code',
        'uploadMpc' => 'file',
        'patch_test' => 'patch_test_code',
        'v_sample' => 'v_sample_code',
        'magnification' => 'magnification_code',
        'uploadPatch' => 'file',
        'Others'=> 'others',
    ];

    public $temp_property = [
        'Lab_No',
        'EVAL_CODE',
        'SAMPL_DT1',
        'RECV_DT1',
        'RPT_DT1',
        'branch',
        'name',
        'address',
        'brand2',
        'MODEL',
        'UNIT_NO',
        'UNIT_LOC',
        'unitSn',
        'COMPONENT',
        'MATRIX',
        'OIL_TYPE',
        'OIL_MATRIX',
        'ORIG_VISC',
        'HRS_KM_OC',
        'HRS_KM_TOT',
        'OIL_TYPE',
        'oil_change',
        'oil_change',
        'ADD_PHYS',
        'req_order',
        'grouploc',
        'ADD_VALUE',
        'RECOMM1',
        'RECOMM2',
        'RECOMM3',
        'RECOMM4',
        'Notes',
    ];

    public function getParameterSelect()
    {
        $out = [];
        foreach ($this->temp_parameter as $param => $item) {
            $out[$param] = $this->getAttributeLabel($param);
        }
        return $out;
    }

    public function getPropertySelect()
    {
        $out = [];
        foreach ($this->temp_property as $param) {
            if ($param == 'Lab_No') {
                $out[$param] = $param;
            } else {
                $out[$param] = $this->getAttributeLabel($param);
            }
        }
        return $out;
    }

    public function rules()
    {
        return array_merge(parent::rules(), [
            [['parameter', 'code', 'property', 'csv'], 'safe']
        ]);
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'code' => 'Download with parameter code',
            'csv' => 'Change to CSV format',
        ]);
    }

    /**
     * Initializes the view.
     */
    public function init()
    {
        parent::init();

        if ($this->property == null) {
            $this->property = $this->temp_property;
        }

    }

    protected $_tu;

    public function getUnit()
    {
        if ($this->_tu == null) {
            $this->_tu = parent::getUnit();
        }
        return $this->_tu;
    }

    /**
     * @return mixed|string
     */
    public function getSerialNo()
    {
        return $this->unit == null ? '' : $this->unit->SerialNo;
    }

    public function getUnitLocation()
    {
        return $this->unit == null ? '' : $this->unit->SerialNo;
    }

    public function hasAttribute($name)
    {
        if (in_array($name, ['serialNo', 'unitLocation'], true)) {
            return true;
        }

        return parent::hasAttribute($name);
    }

    public $for_customer = 1;

    public $txt = [
        'labno' => 'Lab_No',
        'unitno' => 'UNIT_NO',
        'serialno' => 'serialNo', //di tbl Unit
        'description' => '',
        'custid' => 'customer_id',
        'cust_name' => 'name',
        'modelid' => 'MODEL',
        'makeid' => 'BRAND',
        'compartid' => 'COMPONENT',
        'compart' => 'COMPONENT',
        'compartsn' => '',
        'oiltypeid' => 'OIL_TYPE',
        'oilgradeid' => 'ORIG_VISC',
        'coolantid' => '',
        'notesos' => 'RECOMM1',
        'secondary_notesos' => 'RECOMM2',
        'label' => '',
        'last_interpret_date' => '',
        'sampledate' => 'SAMPL_DT1',
        'processdate' => 'RECV_DT1',
        'oiladded' => 'oiladded',
        'oilhours' => 'HRS_KM_OC',
        'actual_fluid_hours' => '',
        'meterread' => 'HRS_KM_TOT',
        'oilchanged' => 'oil_change',
        'evalcode' => 'EVAL_CODE',
        'jobsite' => 'unitLocation',  //tbl Unit
        'sampledsite' => '',
        'dateReported' => 'RPT_DT1',
        'Confirmed' => '',
        'problemsolved' => '',
        'actiontaken' => '',
        'jobno' => '',
        'branchreceiveddate' => '',
        'dayscustomerkept' => '',
        'senttolabdate' => '',
        'forwardingtime' => '',
        'received' => 'RECV_DT1',
        'hoursdiff' => '',
        'registered' => '',
        'hoursdiff1' => '',
        'processing' => '',
        'testcomplete' => '',
        'hoursdiff2' => '',
        'interpreted' => '',
        'hoursdiff3' => '',
        'reported' => '',
        'hoursdiff4' => '',
        'interpreter' => '',
        'odor' => '',
        'ph' => '',
        'ec' => '',
        'nitrite' => '',
        'coolantcolor' => '',
        'coolantcontaminants' => '',
        'precipitateamount' => '',
        'magnetism' => '',
        'coolantappearance' => '',
        'glycol' => 'GLYCOL',
        'si' => 'SILICON',
        'al' => 'ALUMINIUM',
        'cr' => 'CHROMIUM',
        'fe' => 'IRON',
        'pb' => 'LEAD',
        'cu' => 'COPPER',
        'sn' => 'TIN',
        'ni' => 'NICKEL',
        'na' => 'SODIUM',
        'k' => '',
        'mo' => 'Molybdenum',
        'zn' => '',
        'mg' => 'MAGNESIUM',
        'ca' => 'CALCIUM',
        'p' => '',
        'b' => '',
        'st' => '',
        'oxi' => 'OXIDATION',
        'nit' => 'NITRATION',
        'sul' => 'sulphur',
        'v100' => 'visc_index',
        'tbn' => 'T_B_N',
        'tan' => 'T_A_N',
        'W' => '',
        'fp' => '',
        'gf' => '',
        'pq' => '',
        'wpct' => '',
        'pc06_2' => 'PCCek',
        'pc14_2' => 'PCCek',
        'iso6' => 'ISO4406',
        'iso.14' => 'ISO4406',
        'visual' => '',
        'remarks' => '',
        'username' => '',
        'loaddate' => '',
        'pc04' => 'PCCek',
        'pc06' => 'PCCek',
        'pc14' => 'PCCek',
        'fuel cleanliness' => 'ISO4406',
        'water and sediment' => '',
        'watercontent' => 'WATER',
        'sulphurcontent' => 'sulphur',
        'ashcontent' => '', // Ash Content di tbl parameter
        'carbonresidue' => '',
        'flashpointpmcc' => '',   //Flash Point PMCC  tbl other parameter
        'dfapigravity' => '',
        'precipitatecolor' => '',
        'precipitateappearance' => '',
        'customergroups' => '',
        'marketstatus' => '',
        'reportlogo' => '',
        'region' => '',
        'cmu' => '',
    ];

    /**
     * @param null $params
     * @return string
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     * @throws \PHPExcel_Writer_Exception
     */
    public function generateExcel($params = null)
    {
        $cointFilter = 0;
        if (isset($params['columns']))
            foreach ($params['columns'] as $col) {
                if (isset($params['search']) && $params['search']['value'] !== '') {
                    $cointFilter++;
                }
                if ($col['searchable'] === 'true' && $col['search']['value'] !== '') {
                    $cointFilter++;
                }
            }


        $query = $this->searchData($params, true);

        $data = $query->all();

        $modelOthers = OthersOrder::find()->select(['unsur'])->andWhere("Lab_No IN ('".join("','", ArrayHelper::getColumn($query->select('Lab_No')->all(), 'Lab_No'))."')")->groupBy('unsur')->orderBy('unsur')->asArray()->all();
        
        if ($this->parameter == null) {
            $this->parameter = array_keys($this->temp_parameter);
        }

        $e = new Excel();
        $p = $e->newExcel();
        $row = 2;
        $col = 'A';
        $col_oth = false;
        $c = [];

        if ($this->type == 'txt') {
            foreach ($data as $record) {
                foreach ($this->txt as $ix => $value) {
                    if (!isset($c[$ix])) {
                        $c[$ix] = $col;
                        $p->getActiveSheet()->setCellValue($c[$ix] . 1, $ix);
                        $col++;
                    }
                    $p->getActiveSheet()->setCellValue($c[$ix] . $row,
                        $record->hasAttribute($value) ? $record->$value : '');
                }
                $row++;
            }

            $fileName = 'Export_file_' . date('d-m-Y_H-i-s') . '.txt';
            $e->downloadCustom($p, $fileName, '|');

            return $fileName;
        }

        $labelProperty = $this->propertySelect;
        $labelParameter = $this->parameterSelect;
        foreach ($data as $record) {
            foreach ($this->property as $value) {
                if (!isset($c[$value])) {
                    $c[$value] = $col;
                    $p->getActiveSheet()->setCellValue($c[$value] . 1,
                        isset($labelProperty[$value]) ? $labelProperty[$value] : $value);
                    $col++;
                }
                $p->getActiveSheet()->setCellValue($c[$value] . $row,
                    $record->hasAttribute($value) ? $record->$value : '');
            }

            foreach ($this->parameter as $value) {
                $code = isset($this->temp_parameter[$value]) ? $this->temp_parameter[$value] : '';
                if ($code == 'file') {
                    continue;
                }
                if (!isset($c[$value]) && $code <> 'others') {
                    $c[$value] = $col;
                    $p->getActiveSheet()->setCellValue($c[$value] . 1,
                        $this->for_customer == 1 && isset($labelParameter[$value]) ? $labelParameter[$value] : $value);
                    $col++;
                    if ($this->code && $code != '') {
                        $c[$code] = $col;
                        $p->getActiveSheet()->setCellValue($c[$code] . 1, $code);
                        $col++;
                    }
                }
                if($code <> 'others'){
                    $p->getActiveSheet()->setCellValue($c[$value] . $row,
                        $record->hasAttribute($value) ? $record->$value : '');
                    if ($this->code && $code != '') {
                        $p->getActiveSheet()->setCellValue($c[$code] . $row,
                            $record->hasAttribute($code) ? $record->$code : '');
                    }
                }
                
                if($value == 'Others'){
                    if($col_oth == false){
                        if(isset($modelOthers)){
                            foreach ($modelOthers as $val) {
                                $key1 = $val['unsur'];
                                $c[$key1] = $col;
                                $p->getActiveSheet()->setCellValue($c[$key1] . 1,$key1);
                                $col++;
                                if ($this->code && $code != '') {
                                    $cd = $key1.'_code';
                                    $c[$cd] = $col;
                                    $p->getActiveSheet()->setCellValue($c[$cd] . 1, $cd);
                                    $col++;
                                }   
                            }
                        }
                        $col_oth = true;
                    }

                    $others = $record->otherHis;
                    if(isset($others)){
                        foreach ($others as $key => $val) {                            
                            $p->getActiveSheet()->setCellValue($c[$key] . $row,$val['value']);
                            if ($this->code && $code != '') {
                                $p->getActiveSheet()->setCellValue($c[$key.'_code'] . $row,$val['code']);
                            }
                        }
                    }
                }
            }
            $row++;
        }
        $fileName = 'Export_excel_' . date('d-m-Y_H-i-s');
        if ($this->csv) {
            $e->downloadCsv($p, $fileName);
            return '';
        }
        $e->download($p, $fileName);
        return '';
    }


}