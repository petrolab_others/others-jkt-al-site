<?php
/**
 * Created by
 * User: Wisard17
 * Date: 05/11/2018
 * Time: 11.49 AM
 */

namespace app\modules\master\models;


use app\modelsDB\TblWearFtirMatrix;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class Matrix
 * @package app\modules\master\models
 *
 * @property string $detail
 * @property mixed $iso14C
 * @property mixed $iso6C
 * @property mixed $iso14B
 * @property mixed $iso4B
 * @property mixed $iso6B
 * @property mixed $iso4C
 * @property string $actions
 */
class Matrix extends TblWearFtirMatrix
{
    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            ['Matrix', 'unique'],
        ]);
    }

    public function attributeLabels()
    {
        return array_merge([], [
            'Matrix' => 'Matrix',
            'SootB' => 'SootB',
            'SootC' => 'SootC',
            'SootD' => 'SootD',
            'OxiB' => 'OxiB',
            'OxiC' => 'OxiC',
            'OxiD' => 'OxiD',
            'NitB' => 'NitB',
            'NitC' => 'NitC',
            'NitD' => 'NitD',
            'SulB' => 'SulB',
            'SulC' => 'SulC',
            'SulD' => 'SulD',
            'FuelB' => 'FuelB',
            'FuelC' => 'FuelC',
            'FuelD' => 'FuelD',
            'GlycolB' => 'GlycolB',
            'GlycolC' => 'GlycolC',
            'GlycolD' => 'GlycolD',
            'WaterB' => 'WaterB',
            'WaterC' => 'WaterC',
            'WaterD' => 'WaterD',
            'NaB' => 'NaB',
            'NaC' => 'NaC',
            'NaD' => 'NaD',
            'SiB' => 'SiB',
            'SiC' => 'SiC',
            'SiD' => 'SiD',
            'FeB' => 'FeB',
            'FeC' => 'FeC',
            'FeD' => 'FeD',
            'CuB' => 'CuB',
            'CuC' => 'CuC',
            'CuD' => 'CuD',
            'AlB' => 'AlB',
            'AlC' => 'AlC',
            'AlD' => 'AlD',
            'CrB' => 'CrB',
            'CrC' => 'CrC',
            'CrD' => 'CrD',
            'NiB' => 'NiB',
            'NiC' => 'NiC',
            'NiD' => 'NiD',
            'SnB' => 'SnB',
            'SnC' => 'SnC',
            'SnD' => 'SnD',
            'PbB' => 'PbB',
            'PbC' => 'PbC',
            'PbD' => 'PbD',
            'MoB' => 'MoB',
            'MoC' => 'MoC',
            'MoD' => 'MoD',
            'PQB' => 'PQB',
            'PQC' => 'PQC',
            'PQD' => 'PQD',
            'iso4406B' => 'iso4406B',
            'iso4406C' => 'iso4406C',
            'iso4406D' => 'iso4406D',
            '4attention' => '4attention',
            '6attention' => '6attention',
            '14attention' => '14attention',
            '4urgent' => '4urgent',
            '6urgent' => '6urgent',
            '14urgent' => '14urgent',
            'F23' => 'F23',
            'SF' => 'SF',
        ]);
    }

    /**
     * @return mixed
     */
    public function getIso4B()
    {
        $p = '4attention';
        return $this->$p;
    }

    /**
     * @return mixed
     */
    public function getIso4C()
    {
        $p = '4urgent';
        return $this->$p;
    }

    /**
     * @return mixed
     */
    public function getIso6B()
    {
        $p = '6attention';
        return $this->$p;
    }

    /**
     * @return mixed
     */
    public function getIso6C()
    {
        $p = '6urgent';
        return $this->$p;
    }

    /**
     * @return mixed
     */
    public function getIso14B()
    {
        $p = '14attention';
        return $this->$p;
    }

    /**
     * @return mixed
     */
    public function getIso14C()
    {
        $p = '14urgent';
        return $this->$p;
    }

    /**
     * @return string
     */
    public function getActions()
    {
        return Html::tag('div',
            Html::tag('a', '<i class="fa fa-edit"></i> Edit', [
                'href' => Url::toRoute(['/master/matrix/edit', 'id' => $this->Matrix]),
                'data-action' => 'child_edit', 'class' => 'btn btn-default btn-sm',
            ]) . Html::tag('a', '<i class="fa fa-copy"></i> Duplicate', [
                'href' => Url::toRoute(['/master/matrix/new', 'duplicate' => $this->Matrix]),
                'class' => 'btn btn-default btn-sm',
            ]) . Html::tag('a', '<i class="fa fa-times"></i> Delete', [
                'href' => Url::toRoute(['/master/matrix/del', 'id' => $this->Matrix]),
                'data-method' => 'post',
                'data-confirm' => 'Are you absolutely sure ? You will lose all the information about this user with this action.',
                'class' => 'btn btn-default btn-sm',
            ]));
    }

    public function getDetail()
    {
        $out = '<tr>';
        $col = 1;
        foreach ($this->attributeLabels() as $label) {
            if ($label != 'Matrix' && $this->$label != '') {
                $out .= Html::tag('td', "$label : " . $this->$label);
                if ($col == 4) {
                    $out .= '</tr><tr>';
                    $col = 0;
                }
                $col++;
            }
        }

        return Html::tag('table', $out . '</tr>', ['class' => 'table']);
    }
}