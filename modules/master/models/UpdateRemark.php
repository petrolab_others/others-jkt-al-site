<?php
/**
 * Created by
 * User: Wisard17
 * Date: 03/12/2018
 * Time: 03.33 PM
 */

namespace app\modules\master\models;


use app\modelsDB\TblDegradasi;
use app\modelsDB\TblKeausan;
use app\modelsDB\TblKontaminan;
use app\modules\master\models\search\SearchTransaksi;

/**
 * Class UpdateRemark
 * @package app\modules\master\models
 */
class UpdateRemark extends Transaksi
{
    public $updateRemark;

    /**
     * @param $params
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function updateCode($params)
    {
        $query = self::find()->joinWith('customer');

        $allField = (new SearchTransaksi())->allField;

        $count = 0;
        $fltr = '';
        if (isset($params['columns'])) {
            foreach ($params['columns'] as $col) {
                if (isset($params['search']) && $params['search']['value'] !== '') {
                    $lst[] = $col['data'];
                    if (array_key_exists($col['data'], $allField) &&
                        !array_key_exists($col['data'], $lst) && $allField[$col['data']] != '') {
                        $fltr .= $fltr !== '' ? ' or ' : '';
                        $fltr .= ' `' . self::tableName() . '`.' . $allField[$col['data']] . " like '%" . $params['search']['value'] . "%' ";
                        $count++;
                    }
                    if ($col['data'] === 'customerName') {
                        $fltr .= $fltr !== '' ? ' or ' : '';
                        $fltr .= ' `tbl_customers`.Name ' . " like '%" . $params['search']['value'] . "%' ";
                        $count++;
                    }
                }
                if ($col['searchable'] === 'true' && $col['search']['value'] !== '' &&
                    array_key_exists($col['data'], $allField) && $allField[$col['data']] != '') {
                    if ($col['data'] === 'customerName') {
                        $query->andFilterWhere(['like', '`tbl_customers`.Name', $col['search']['value']]);
                        $count++;
                    } else {
                        $query->andFilterWhere(['like', '`' . self::tableName() . '`.' . $allField[$col['data']], $col['search']['value']]);
                        $count++;
                    }
                }
            }
        }

        if (isset($params['lab_no'])) {
            $query->andWhere(['Lab_No' => $params['lab_no']]);
            $count++;
        }

        if ($count == 0) {
            $this->message = 'Filter First';
            return false;
        }

        $query->andWhere($fltr);

        $data = $query->all();
        if ($data == null) {
            $this->message = 'No Data Found';
            return false;
        }
        $statusSuccees = 0;
        $out = true;
        $msg = '';
        foreach ($data as $report) {
            if ($report->updateRemark1()) {
                $this->data[$report->rowId] = $report->status;
                $statusSuccees++;
            } else {
                $out = false;
                $msg .=
                    "Lab No $report->Lab_No error : (" . ArrayHelper::toString($report->errors) . ')<br>';
            }
        }
        $this->message .= $statusSuccees > 1 ? "Success diupdate : $statusSuccees <hr>" : '';
        $this->message .= $msg;
        return $out;
    }

    /**
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function updateRemark1()
    {
        if ($this->publish != '') {
            $this->addError('updateEvalCode', 'Data telah dipublish');
            return false;
        }

        $ada_wear = 0;
        $Fe = 0;
        $Cu = 0;
        $Al = 0;
        $Cr = 0;
        $Pb = 0;
        $Si = 0;
        $Kombinasimetal = '';
        $status = '';
        if (($this->FE_CODE == 'B') || ($this->CU_CODE == 'B') || ($this->AL_CODE == 'B') || ($this->CR_CODE == 'B') || ($this->PB_CODE == 'B')) {
            $status = 'Caution';
        }
        if (($this->FE_CODE == 'C') || ($this->CU_CODE == 'C') || ($this->AL_CODE == 'C') || ($this->CR_CODE == 'C') || ($this->PB_CODE == 'C')) {
            $status = 'Citical';
        }


        if (($this->FE_CODE == 'B') || ($this->FE_CODE == 'C')) {
            $Fe = 1;

            $Kombinasimetal .= $this->add_data($Kombinasimetal, 'Fe');
            $ada_wear = 1;
        }

        if (($this->CU_CODE == 'B') || ($this->CU_CODE == 'C')) {
            $Cu = 1;
            $Kombinasimetal .= $this->add_data($Kombinasimetal, 'Cu');
            $ada_wear = 1;
        }

        if (($this->AL_CODE == 'B') || ($this->AL_CODE == 'C')) {
            $Al = 1;
            $Kombinasimetal .= $this->add_data($Kombinasimetal, 'Al');
            $ada_wear = 1;
        }

        if (($this->CR_CODE == 'B') || ($this->CR_CODE == 'C') || ($this->CR_CODE == 'N')) {
            $Cr = 1;
            //$Kombinasimetal=$Kombinasimetal.", Cr";
            $Kombinasimetal .= $this->add_data($Kombinasimetal, 'Cr');
            $ada_wear = 1;
        }

        if (($this->PB_CODE == 'B') || ($this->PB_CODE == 'C')) {
            $Pb = 1;
            // $Kombinasimetal=$Kombinasimetal.", Pb";
            $Kombinasimetal .= $this->add_data($Kombinasimetal, 'Pb');
            $ada_wear = 1;
        }

        if ($this->SILICON > 20) {
            $Si = 1;
        }
        list($lokasi_keausan, $penyebab_keausan, $Metoda_Inspeksi, $nemu) = $this->cariKeausan($Fe, $Cu, $Al, $Cr, $Pb, $Si);
        if ($lokasi_keausan == '') {
            $lokasi_keausan = ' (kombinasi WEAR tidak memenuhi panduan KOWA, Cek kembali hasil analisa WEAR ) ';
        }

        $ada_konta = 0;
        $Kontaminasi = '';
        $Air = 0;
        $Fuel = 0;
        $Debu = 0;
        if ($this->WATER > 0) {
            //$Kontaminasi='Air ';
            $Air = 1;
            $Kontaminasi = $this->add_data($Kontaminasi, 'Air');
            $ada_konta = 1;
        }
        if ($this->DILUTION > 0) {
            //$Kontaminasi=$Kontaminasi.',Fuel ';
            $Fuel = 1;
            $Kontaminasi = $this->add_data($Kontaminasi, 'Fuel');
            $ada_konta = 1;
        }
        if ($this->SILICON > 20) {
            //$Kontaminasi=$Kontaminasi.',Debu ';
            $Debu = 1;
            $Kontaminasi = $this->add_data($Kontaminasi, 'Debu');
            $ada_konta = 1;
        }

        if ($this->EVAL_CODE == 'N') {
            $status = 'Normal';
        }
        if ($this->EVAL_CODE == 'B') {
            $status = 'CAUTION';
        }
        if ($this->EVAL_CODE == 'C') {
            $status = 'CRITICAL';
        }

        $kon = TblKontaminan::findOne([
            'component' => $this->component,
            'water' => $Air,
            'dilution' => $Fuel,
            'silicon' => $Debu,
        ]);

        $Inspeksi_kontaminasi = $kon == null ? '' : $kon->Inspeksi;
        $Penyebab_kontaminan = $kon == null ? '' : $kon->Penyebab_kontaminan;

        $Inspeksi_kontaminasi = ' KONTAMINASI: ' . $Inspeksi_kontaminasi;

        $ada_degradasi = 0;
        $PenyebabD = '';
        $Degradasi = '';
        $Metoda_InspeksiD = '';


        $unsur_Visc = 'Visc';
        $unsur_code = 'CST_CODE';
        $d1 = $this->degradasi($unsur_Visc, $unsur_code);

        $PenyebabD = $this->add_data($PenyebabD, $d1['Penyebab']);
        $Degradasi = $this->add_data($Degradasi, $d1['Gejala']);
        $Metoda_InspeksiD = $this->add_data($Metoda_InspeksiD, $d1['Metoda_Inspeksi']);

        $unsur_Visc40 = 'Visc40';
        $unsur_code = 'visc_40_code';
        $d2 = $this->degradasi($unsur_Visc40, $unsur_code);

        $PenyebabD = $this->add_data($PenyebabD, $d2['Penyebab']);
        $Degradasi = $this->add_data($Degradasi, $d2['Gejala']);
        $Metoda_InspeksiD = $this->add_data($Metoda_InspeksiD, $d2['Metoda_Inspeksi']);

        $unsur_TAN = 'TAN';
        $unsur_code = 'TAN_CODE';
        $d3 = $this->degradasi($unsur_TAN, $unsur_code);

        $PenyebabD = $this->add_data($PenyebabD, $d3['Penyebab']);
        $Degradasi = $this->add_data($Degradasi, $d3['Gejala']);
        $Metoda_InspeksiD = $this->add_data($Metoda_InspeksiD, $d3['Metoda_Inspeksi']);

        $unsur_TBN = 'TBN';
        $unsur_code = 'TBN_CODE';
        $d3 = $this->degradasi($unsur_TBN, $unsur_code);

        $PenyebabD = $this->add_data($PenyebabD, $d3['Penyebab']);
        $Degradasi = $this->add_data($Degradasi, $d3['Gejala']);
        $Metoda_InspeksiD = $this->add_data($Metoda_InspeksiD, $d3['Metoda_Inspeksi']);

        $unsur_OXI = 'OXI';
        $unsur_code = 'OXID_CODE';
        $d3 = $this->degradasi($unsur_OXI, $unsur_code);

        $PenyebabD = $this->add_data($PenyebabD, $d3['Penyebab']);
        $Degradasi = $this->add_data($Degradasi, $d3['Gejala']);
        $Metoda_InspeksiD = $this->add_data($Metoda_InspeksiD, $d3['Metoda_Inspeksi']);

        if ($Metoda_InspeksiD != "") {
            $Metoda_InspeksiD = "DEGRADASI : " . $Metoda_InspeksiD;
            $ada_degradasi = 1;
        }

//        if (($this->TRANS_CODE == 'B') || ($this->TRANS_CODE == 'C'))
//            lain_lain('Soot', $Lain_Penyebab, $Lain_Metoda);

        $Lain_Penyebab1 = '';
        $Lain_Metoda1 = '';

        if (($ada_wear == 0) and ($ada_degradasi == 0) and ($ada_konta == 0)) {
            $TextRecomm1 = "ANALISA NORMAL";
        }
        $KI = $Inspeksi_kontaminasi;
        $WM = $Metoda_Inspeksi;
        $DI = $Metoda_InspeksiD;

        $this->remark($ada_wear, $ada_konta, $ada_degradasi, $status, $Kombinasimetal, $Kontaminasi, $Penyebab_kontaminan, $Degradasi, $lokasi_keausan, $penyebab_keausan, $PenyebabD, $WM, $KI, $DI, $Lain_Penyebab1, $Lain_Metoda1);

        return $this->save();
    }

    protected function add_data($base, $add)
    {
        return $base == '' && $add == '' ? $add : ', ' . $add;
    }

    protected function cariKeausan($Fe, $Cu, $Al, $Cr, $Pb, $Si)
    {
        $keausan = TblKeausan::findOne([
            'component' => $this->component,
            'Fe' => $Fe,
            'Cu' => $Cu,
            'Al' => $Al,
            'Cr' => $Cr,
            'Pb' => $Pb,
            'Si' => $Si
        ]);
        if ($keausan != null) {
            return [
                'lokasi_keausan' => $keausan->Lokasi,
                'penyebab_keausan' => $keausan->Penyebab,
                'Metoda_Inspeksi' => $keausan->Metoda_Inspeksi,
                'nemu' => 'NEMU YA'
            ];
        }
        return [
            'lokasi_keausan' => '',
            'penyebab_keausan' => '',
            'Metoda_Inspeksi' => '',
            'nemu' => 'NEMU TIDAK'
        ];
    }

    protected function degradasi($unsur, $unsur_code)
    {
        if ($this->hasAttribute($unsur_code) && $this->$unsur_code != '') {
            $deg = TblDegradasi::findOne([$unsur => $this->$unsur_code]);
            if ($deg !== null) {
                return [
                    'Penyebab' => $deg->Penyebab, 'Metoda_Inspeksi' => $deg->Metoda_Inspeksi, 'Gejala' => $deg->Gejala
                ];
            }
        }
        return [
            'Penyebab' => '', 'Metoda_Inspeksi' => '', 'Gejala' => ''
        ];
    }

    protected function remark($ada_wear, $ada_kontaminasi, $ada_degradasi, $ec, $wear, $kontaminasi, $Penyebab_kontaminan, $degradasi, $WL, $WP, $DP, $WM, $KI, $DI, $Lain_Penyebab1, $Lain_Metoda1)
    {

        $remark1 = "HASIL ANALISA NORMAL";
        $remark2 = '';
        if ($no == 1) {
            $remark1 = $ph1 . $ec . $ph2 . $wear . $ph3 . $kontaminasi . $ph4 . $degradasi . $ph5 . $WL . $ph6 . $WP . " " . $DP;
            $remark2 = $WM . " " . $KI . " " . $DI;
        }

        if ($no == 2) {
            $remark1 = $ph1 . $ec . $ph2 . $wear . $ph5 . $WL . $ph6 . $WP;
            $remark2 = $WM;
        }

        if ($no == 3) {
            $remark1 = $ph1 . $ec . $ph2 . $wear . $ph3 . $kontaminasi . $ph5 . $WL . $ph6 . $WP . " " . $DP . " " . $Penyebab_kontaminan;
            $remark2 = $WM . " " . $KI;
        }

        if ($no == 4) {
            $remark1 = $ph1 . $ec . $ph2 . $wear . $ph4 . $degradasi . $ph5 . $WL . $ph6 . $WP . " " . $DP;
            $remark2 = $WM . " " . $DI;
        }

        if ($no == 5) {
            $remark1 = $ph1 . $ec . $ph2 . $kontaminasi . $ph4 . $degradasi . $ph6 . $WP . ", " . $Penyebab_kontaminan . ", " . $DP;
            $remark2 = $KI . " " . $DI;
        }


        if ($no == 6) {
            $remark1 = $ph1 . $ec . $ph2 . $kontaminasi;
            $remark2 = $KI;
        }
        if ($no == 7) {
            $remark1 = $ph1 . $ec . $ph2 . $degradasi . $ph6 . $DP;
            $remark2 = $DI;
        }

//        $remark1 = $remark1 . ' ' . $Lain_Penyebab1;
//        $remark2 = $remark2 . ' ' . $Lain_Metoda1;
        return ['remark1' => $remark1, 'remark2' => $remark2];
    }
}