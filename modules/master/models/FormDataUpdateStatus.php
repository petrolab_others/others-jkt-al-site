<?php
/**
 * Created by
 * User: Wisard17
 * Date: 09/10/2018
 * Time: 01.20 PM
 */

namespace app\modules\master\models;


use app\components\ArrayHelper;

/**
 * Class FormDataUpdateStatus
 * @package app\modules\master\models
 */
class FormDataUpdateStatus extends Transaksi
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['publish'], 'string'],
        ];
    }

    /**
     * @param bool $runValidation
     * @param null $attributeNames
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        $transaction = $this::getDb()->beginTransaction();
        try {
            $this->publish = $this->publish === '' ? null : $this->publish;
            $err = $this->errors;
            if (parent::save($runValidation, $attributeNames) && count($err) < 1) {

                $this->status_save = 'success';
                $this->message = 'Berhasil Disimpan';
                $transaction->commit();
                return true;
            }

            $this->status_save = 'error';
            $this->message = 'Data Tidak disimpan, Coba lagi.<br>' . ArrayHelper::toString($err);

            $transaction->rollBack();
            return false;
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch(\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    public $message = '';

    public $status_save = '';

    /**
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function changeState()
    {
        $user = \Yii::$app->user->identity;
        if (in_array($user->ruleAccess, [2, 3], true)) {
            $this->addError('publish', 'this user does not have permission ');
            $this->message = 'Data Tidak disimpan, Coba lagi.<br>' . ArrayHelper::toString($this->errors);
            return false;
        }
        if ($this->verify_date == '' && $this->publish == '') {
            $this->addError('publish', 'data belum diverifikasi');
            $this->message = 'Data Tidak disimpan, Coba lagi.<br>' . ArrayHelper::toString($this->errors);
            return false;
        }
        $this->publish = $this->publish == '' ? date('Y-m-d H:i:s') : null;
        return $this->save();
    }
}