<?php
/**
 * Created by
 * User: Wisard17
 * Date: 07/09/2018
 * Time: 10.40 PM
 */

namespace app\modules\master\models;


use app\components\ArrayHelper;
use app\modules\reports\models\Customer;
use Yii;
use app\modules\reports\models\Addition;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\UploadedFile;

/**
 * Class FormTransaksi
 * @package app\modules\reports\models
 *
 * @property string $branch2
 */
class FormTransaksi extends Transaksi
{

    /** @var UploadedFile */
    public $uploadMpc;
    /** @var UploadedFile */
    public $uploadPatch;

    public function rules()
    {
        return array_merge(parent::rules(), [
            [['uploadMpc', 'uploadPatch'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
            [['branch2', 'unitSn', 'unitHourL', 'oilHourL'], 'string'],
            [['unitHour', 'oilHour'], 'safe'],
            [['OIL_MATRIX', 'branch', 'customer_id', 'unit_id', 'ComponentID'], 'required'],
        ]);
    }

    public function load($data, $formName = null)
    {
        if (parent::load($data, $formName)) {
            $this->uploadMpc = UploadedFile::getInstance($this, 'uploadMpc');
            $this->uploadPatch = UploadedFile::getInstance($this, 'uploadPatch');
            $unitL = $this->unitHourL == '01' ? '' : $this->unitHourL;
            $oilL = $this->oilHourL == '01' ? '' : $this->oilHourL;
            $this->HRS_KM_TOT = $this->unitHour !== '' ? $this->unitHour . ' ' . $unitL : null;
            $this->HRS_KM_OC = $this->oilHour !== '' ? $this->oilHour . ' ' . $oilL :
                ($oilL == 'NEW OIL' ? $oilL : null);
            $this->ISO4406 = $this->iso4406;
            return true;
        }
        return false;
    }

    /**
     * @return Unit|\yii\db\ActiveQuery
     */
    public function getUnit()
    {
        $u = parent::getUnit();
        if ($u->one() == null) {
            return new Unit();
        }
        return $u;
    }

    /**
     * @return Customer|\yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        $c = parent::getCustomer();
        if ($c->one() == null) {
            return new Customer();
        }
        return $c;
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->verify_date != '') {
                $this->addError('statusVerify', 'Data telah terkunci silahkan hubungi manager');
                return false;
            }
            if ($this->publish != '') {
                $this->addError('statusPublish', 'Data telah dipublish, silahkan unpublished terlebih dahulu');
                return false;
            }
            return true;
        }
        return false;
    }

    /**
     * @param bool $runValidation
     * @param null $attributeNames
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            if (parent::save($runValidation, $attributeNames)) {
                if ($this->uploadMpc != null) {
                    $p = ['Lab_No' => $this->Lab_No, 'tbl_parameter_id' => 19];
                    $ad = Addition::findOne($p);
                    if ($ad == null) {
                        $ad = new Addition($p);
                    }
                    $filename = 'mpc_' . str_replace(['/', '\\', ':', '|'], '_', $this->Lab_No) . '_' . time();
                    if ($this->uploadMpc->saveAs($ad->dirBerkas . "/$filename." . $this->uploadMpc->extension)) {
                        $ad->value = "$filename." . $this->uploadMpc->extension;
                        $ad->save();
                    }
                }
                if ($this->uploadPatch != null) {
                    $p = ['Lab_No' => $this->Lab_No, 'tbl_parameter_id' => 23];
                    $ad = Addition::findOne($p);
                    if ($ad == null) {
                        $ad = new Addition($p);
                    }
                    $filename = 'patch_' . str_replace(['/', '\\', ':', '|'], '_', $this->Lab_No) . '_' . time();
                    if ($this->uploadPatch->saveAs($ad->dirBerkas . "/$filename." . $this->uploadPatch->extension)) {
                        $ad->value = "$filename." . $this->uploadPatch->extension;
                        $ad->save();
                    }
                }
                $transaction->commit();
                return true;
            }
            $transaction->rollBack();
            return false;
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    /**
     * @param ActiveForm $form
     * @return string
     */
    public function renderOther($form)
    {
        $out = '<thead><tr>
                    <th class="text-center">action</th>
                    <th class="text-center" >Parameter</th>
                    <th class="text-center" >Unit</th>
                    <th class="text-center">Method</th>
                    <th class="text-center" >Value</th>
                    
                    </tr></thead><tbody>';
        foreach ($this->others as $idx => $other) {
            $other->idName = $other->OtherOrderId == null ? 'newrow_' . $idx : $other->OtherOrderId;
            $out .= Html::tag('tr', '' .
                Html::tag('td', $other->action) .
                Html::tag('td', ''.
                    $form->field($other, 'selectParameter', ['template' => '<label class="input">{input}</label>{error}'])
                        ->dropDownList($other->unsur == '' ? [] : [$other->unsur => $other->unsur], ['class' => 'form-control selectother',
                            'data-url' => Url::toRoute(['/master/parameter/list-other_parameters']),
                        ]) .
                    $form->field($other, 'tbl_parameter_id', ['template' => '{input}'])->hiddenInput() .
                    Html::tag('div', $form->field($other, 'unsur') .
//                        $form->field($other, 'value_CODE')->dropDownList(['' => '', 'B' => 'B', 'C' => 'C', 'D' => 'D']) .
                        $form->field($other, 'attention') .
                        $form->field($other, 'urgent') .
                        $form->field($other, 'severe')
                        , ['class' => 'otherparam_ hide_parameter_'])
                ) .
                Html::tag('td', $form->field($other, 'unit', ['template' => '<label class="input">{input}</label>{error}'])) .
                Html::tag('td', $form->field($other, 'methode', ['template' => '<label class="input">{input}</label>{error}'])) .
                Html::tag('td', $form->field($other, 'value', ['template' => '{label}
                        <div class="col-lg-8"><div class=" input-group">{input} <div class="input-group-btn" >' .
//    Html::tag('span', '<i class="fa fa-gear"></i>', [
//        'class' => 'btn btn-default option_parameter',
//        'tabindex'=> '0', 'data-html' => 'true',
//    ]) .
                        Html::activeDropDownList($other, 'value_CODE', ['' => 'Code', 'B' => 'B', 'C' => 'C', 'D' => 'D'],
                            ['class' => 'btn btn-default']) . ' </div></div>{error}{hint}
                        </div>',
                        'options' => ['class' => 'form-group has-feedback']
                    ]
                )->label(false))
                , ['data-id' => $other->idName]
            );
        }

        return Html::tag('table', $out . '</tbody>', ['class' => "table table-hover"]);
    }

    public static function renderRowAjax($rowTo = 0)
    {
        $form = ActiveForm::begin();
        $other = new OthersOrder();
        $other->idName = 'newrow_' . $rowTo;

        $out = Html::tag('tr', '' .
            Html::tag('td', $other->action) .
            Html::tag('td', ''.
                $form->field($other, 'selectParameter', ['template' => '<label class="input">{input}</label>{error}'])
                    ->dropDownList([], ['class' => 'form-control selectother',
                        'data-url' => Url::toRoute(['/master/parameter/list-other_parameters']),
                    ]) .
                Html::tag('div', $form->field($other, 'unsur') .
//                    $form->field($other, 'value_CODE')->dropDownList(['' => 'Code', 'B' => 'B', 'C' => 'C', 'D' => 'D']) .
                    $form->field($other, 'attention') .
                    $form->field($other, 'urgent') .
                    $form->field($other, 'severe')
                , ['class' => 'otherparam_ hide_parameter_'])
            ) .
            Html::tag('td', $form->field($other, 'unit', ['template' => '<label class="input">{input}</label>{error}'])) .
            Html::tag('td', $form->field($other, 'methode', ['template' => '<label class="input">{input}</label>{error}'])) .

            Html::tag('td', $form->field($other, 'value', ['template' => '{label}
                        <div class="col-lg-8"><div class=" input-group">{input} <div class="input-group-btn" >' .
//    Html::tag('span', '<i class="fa fa-gear"></i>', [
//        'class' => 'btn btn-default option_parameter',
//        'tabindex'=> '0', 'data-html' => 'true',
//    ]) .
                Html::activeDropDownList($other, 'value_CODE', ['' => 'Code', 'B' => 'B', 'C' => 'C', 'D' => 'D'],
                    ['class' => 'btn btn-default']) . ' </div></div>{error}{hint}
                        </div>',
                'options' => ['class' => 'form-group has-feedback']
            ])->label(false))
            , ['data-id' => $other->idName]
        );

        return [
            'data' => $out,
            'jsAdd' => ArrayHelper::toArray($form->attributes),
        ];
    }


}