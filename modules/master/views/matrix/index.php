<?php

/**
 * Created by
 * User: Wisard17
 * Date: 29/11/2018
 * Time: 09.58 AM
 */

/* @var $this \yii\web\View */

use app\smartadmin\Alert;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Matrix');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Master'), 'url' => Url::toRoute(['/master'])];
$this->params['breadcrumbs'][] = $this->title;

?>


<div class="row">

    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
        <h1 class="page-title txt-color-blueDark"><i class="fa-fw fa fa-shopping-cart "></i>
            <?= $this->title ?>
        </h1>
    </div>

    <!-- right side of the page with the sparkline graphs -->
    <!-- col -->
    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
        <div class="pull-right" style="margin-right: 10%">
            <?= Html::a('<i class="fa fa-plus-circle"></i>
                    Add', Url::toRoute(['/master/matrix/new']), ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
    <!-- end col -->

</div>
<!-- end row -->

<!--
	The ID "widget-grid" will start to initialize all widgets below
	You do not need to use widgets if you dont want to. Simply remove
	the <section></section> and you can use wells or panels instead
	-->

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <?= Alert::widget() ?>

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget well" id="wid-id-0">
                <!-- widget options:
                    usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                    data-widget-colorbutton="false"
                    data-widget-editbutton="false"
                    data-widget-togglebutton="false"
                    data-widget-deletebutton="false"
                    data-widget-fullscreenbutton="false"
                    data-widget-custombutton="false"
                    data-widget-collapsed="true"
                    data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-comments"></i> </span>
                    <h2>Widget Title </h2>

                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->
                        <input class="form-control" type="text">
                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body no-padding">
                        <div class="ibox-content" id="uploadspase">

                            <?= \app\modules\master\models\DataTableView::widget([
                                'columns' => [
                                    'actions',
                                    'Matrix',
                                    'detail',
                                ],
                                'options' => ['order' => "[1, 'asc']"],
                                'model' => new \app\modules\master\models\Matrix(),
                            ]) ?>
                        </div>
                        <!-- end widget content -->

                    </div>
                    <!-- end widget div -->

                </div>
                <!-- end widget -->

        </article>
        <!-- WIDGET END -->

    </div>

    <!-- end row -->

    <!-- row -->

    <div class="row">

        <!-- a blank row to get started -->
        <div class="col-sm-12">
            <!-- your contents here -->
        </div>

    </div>

    <!-- end row -->

</section>



