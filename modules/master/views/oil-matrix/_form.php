<?php

/**
 * Created by
 * User: Wisard17
 * Date: 30/11/2018
 * Time: 12.17 PM
 */

/* @var $this \yii\web\View */
/* @var $model \app\modules\master\models\FormTransaksi|\app\modules\master\models\OilMatrix|\yii\db\ActiveRecord */


/* @var $form ActiveForm */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$classAlertError = sizeof($model->errors) > 0 ? 'alert alert-warning' : '';
?>
<div class="matrix_form">

    <?php $form = ActiveForm::begin([
        'id' => 'form-matrix',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-8\">{input}\n{error}</div>",
            'labelOptions' => ['class' => 'col-lg-4 control-label'],
        ],
    ]); ?>

    <div class="<?= $classAlertError ?>"><?= $form->errorSummary($model); ?></div>

    <div class="row">
        <div class="col-lg-4">
            <?php
            $col = 1;
            foreach ($model->attributeLabels() as $label) {
                echo $form->field($model, $label)->textInput();
                if ($label == 'OIL_MATRIX') {
                    echo '</div></div><div class="row"><div class="col-lg-4">';
                    $col = 0;
                }
                if ($col == 3) {
                    echo '</div><div class="col-lg-4">';
                    $col = 0;
                }
                $col++;
            }
            ?>
        </div>
    </div>
    <hr>

    <fieldset>
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success', 'data-action' => '']) ?>
    </fieldset>

    <?php ActiveForm::end(); ?>

</div>