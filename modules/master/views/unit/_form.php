<?php

/**
 * Created by
 * User: Wisard17
 * Date: 01/12/2018
 * Time: 01.11 PM
 */

/* @var $this \yii\web\View */
/* @var $model \app\modules\master\models\FormUnit */


/* @var $form ActiveForm */

use conquer\select2\Select2Asset;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;

$classAlertError = sizeof($model->errors) > 0 ? 'alert alert-warning' : '';
?>
    <div class="matrix_form">

        <?php $form = ActiveForm::begin([
            'id' => 'form-unit',
            'layout' => 'horizontal',
            'fieldConfig' => [
                'template' => "{label}\n<div class=\"col-lg-8\">{input}\n{error}</div>",
                'labelOptions' => ['class' => 'col-lg-4 control-label'],
            ],
        ]); ?>

        <div class="<?= $classAlertError ?>"><?= $form->errorSummary($model); ?></div>

        <div class="row">
            <h4 class="col-lg-6">Customer : <span id="name_branch"><?= $model->branchName ?></span></h4>
        </div>
        <hr>
        <div class="row">
            <div class="col-lg-6">
                <?php
                $col = 1;
                foreach ($model->attributeLabels() as $label => $value) {
                    if (in_array($label, ['UnitID', 'updatedate'], true)) {
                        continue;
                    }
                    if ($label == 'CustomerID') {
                        echo $form->field($model, $label)->dropDownList($model->CustomerID == '' ? [] : [
                            $model->CustomerID => $model->customer->Name,
                        ], [
                            'class' => 'form-control select2ajax',
                            'data-url' => Url::toRoute(['/customers/default/list-customer']),
                        ]);
                    } else {
                        echo $form->field($model, $label)->textInput();
                    }
                    if ($col == 4) {
                        echo '</div><div class="col-lg-6">';
                        $col = 0;
                    }
                    $col++;
                }
                ?>
            </div>
        </div>
        <fieldset>
            <div class="col-lg-12">
                <?= $model->renderComponent($form) ?>
                <a data-action="add_row_table" class="btn btn-primary btn-xs btn-block">
                    <i class="fa fa-plus-circle"></i> Add Component
                </a>
            </div>
        </fieldset>
        <hr>

        <fieldset>
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success', 'data-action' => '']) ?>
        </fieldset>

        <?php ActiveForm::end(); ?>

    </div>
<?php
Select2Asset::register($this);
$cls = Json::encode($model::className());
$jsCode = <<< JS
$(function () {
    let form = $('#form-unit');

    let par = function () {
        return {
            ajax: {
                url: function (a) {
                    return this.attr('data-url');
                },
                data: function (params) {
                    let el = this;
                    let query = {
                        q: params.term,
                        param: {
                            col: el.attr('data-depend'),
                            fromcol: el.attr('data-depend-from'),
                            filter: form.find('#' + el.attr('data-depend-id')).val(),
                            model: $cls,
                        }
                    };
                    return query;
                },
                processResults: function (data) {
                    return {
                        results: data.results
                    };
                }
            }
        }
    };
    form.find('.select2ajax').select2(par());

    form.find('#formunit-customerid').on('select2:select', function (e) {
        let data = e.params.data;
        form.find('#name_branch').html(data.Branch);
    });
    
    form.delegate('[data-action=add_row_table]', 'click', function () {
        var elm = $(this);
        var tbl = elm.closest('fieldset').find('table');
        var form = elm.closest('form');
        $.ajax({
            method: 'post',
            data: {'reg_new_row': (tbl.find('tr').length - 1)},
            error: function (response) {
                alert_notif({status: "error", message: 'tidak bisa save'});
            },
            success: function (response) {
                var t = $(response.data);

                console.log(t);
                genselectother(t.attr('data-id'));
                tbl.find('tbody').append(t);

                $.each(response.jsAdd, function (i, v) {
                    var vali = eval("var f = function(){ return " + v.validate.expression + ";}; f() ;");
                    v.validate = vali;
                    $(form).yiiActiveForm('add', v);
                });
            }
        });
    });

    form.delegate('[data-action=delate_row]', 'click', function () {
        var elm = $(this);
        var row = elm.closest('tr');
        p = '$model->componentForm';
        p = p.replace('[new]', '[' + row.attr('data-id') + ']');

        row.html('<input type="hidden" name="' + p + '[actionId]" value="del" >');

    });

    function genselectother(id_var) {
        form.delegate('#othersorder-' + id_var + '-selectparameter', 'select2:select', function (e) {
            let td = $(this).closest('td');
            console.log(td);
            let data = e.params.data;
            if (data.id != 0) {
                form.find('#othersorder-' + id_var + '-unsur').val(data.unsur).blur();
                form.find('#othersorder-' + id_var + '-unit').val(data.unit).blur();
                form.find('#othersorder-' + id_var + '-methode').val(data.methode).blur();
                form.find('#othersorder-' + id_var + '-attention').val(data.attention).blur();
                form.find('#othersorder-' + id_var + '-urgent').val(data.urgent).blur();
                form.find('#othersorder-' + id_var + '-severe').val(data.severe).blur();
                form.find('#othersorder-' + id_var + '-tbl_parameter_id').val(data.tbl_parameter_id).blur();
                td.find('.otherparam').addClass('hide_parameter');
            } else {
                form.find('#othersorder-' + id_var + '-unsur').val(null).blur().removeAttr('readonly');
                form.find('#othersorder-' + id_var + '-unit').val(null).blur().removeAttr('readonly');
                form.find('#othersorder-' + id_var + '-methode').val(null).blur().removeAttr('readonly');
                form.find('#othersorder-' + id_var + '-attention').val(null).blur().removeAttr('readonly');
                form.find('#othersorder-' + id_var + '-urgent').val(null).blur().removeAttr('readonly');
                form.find('#othersorder-' + id_var + '-severe').val(null).blur().removeAttr('readonly');
                form.find('#othersorder-' + id_var + '-tbl_parameter_id').val(null).blur().removeAttr('readonly');
                td.find('.otherparam').removeClass('hide_parameter');
            }
        });
    }

    function alert_notif(res) {
        $.sound_on = false;
        var color = res.status === 'success' ? '#659265' : '#C46A69';
        var icon = res.status === 'success' ? 'fa-check' : 'fa-times';
        $.smallBox({
            title: res.status,
            content: res.message,
            color: color,
            iconSmall: "fa " + icon + " fa-2x fadeInRight animated",
            timeout: 4000
        });
    }
});
JS;

$this->registerJs($jsCode, 4, 'unitk');