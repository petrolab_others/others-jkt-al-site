<?php

/**
 * Created by
 * User: Wisard17
 * Date: 19/11/2018
 * Time: 11.28 AM
 */

/* @var $this \yii\web\View */
/* @var $model \app\modules\master\models\FormParameter|\yii\db\ActiveRecord */


/* @var $form ActiveForm */


use conquer\select2\Select2Asset;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\bootstrap\Tabs;

$readOnly = $model->isNewRecord ? '' : 'readonly';
$classAlertError = sizeof($model->errors) > 0 ? 'alert alert-warning' : '';
?>
<div class="transaksi_form">

    <?php $form = ActiveForm::begin([
        'id' => 'form-transaksi',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-8\">{input}\n{error}</div>",
            'labelOptions' => ['class' => 'col-lg-4 control-label'],
        ],
    ]); ?>

    <div class="<?= $classAlertError ?>"><?= $form->errorSummary($model); ?></div>

    <fieldset>
        <div class="row">
            <div class="col-lg-6">
                <?= $form->field($model, 'unsur')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'unit')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'methode')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-lg-6">
                <?= $form->field($model, 'attention')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'urgent')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'severe')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
    </fieldset>

    <fieldset>
        <div class="row">
            <div class="col-lg-6">
                <?= $form->field($model, 'col_name')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'col_matrix')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'tbl_category_parameter_id')->dropDownList([]) ?>
            </div>
        </div>
    </fieldset>

    <fieldset>
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </fieldset>

    <?php ActiveForm::end(); ?>

</div>