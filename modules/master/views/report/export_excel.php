<?php

/**
 * Created by
 * User: Wisard17
 * Date: 28/11/2018
 * Time: 09.38 AM
 */

/* @var $this \yii\web\View */

/* @var $model \app\modules\master\models\FormDownloadExcel */


use yii\helpers\Url;
use app\smartadmin\ActiveForm;
use yii\helpers\Html;

$this->title = Yii::t('app', 'Download Excel');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Reports'), 'url' => Url::toRoute(['/reports'])];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Reports Admin'), 'url' => Url::toRoute(['/master/report'])];
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .select2-container--default .select2-selection--multiple .select2-selection__rendered li {
        color: black;
    }
    .select2-container--default .select2-selection--multiple .select2-selection__choice__remove {
        color: #99999963;
        cursor: pointer;
        display: initial;
        font-weight: bold;
        margin-right: -7px;
    }
</style>
<div class="widget-body ">
    <?php $form = ActiveForm::begin([
        'id' => 'form-pilih-excel',
        'fieldConfig' => [
            'template' => "{label}<label class='input'>{input}</label>{error}{hint}",
            'inputOptions' => ['class' => 'form-control'],
        ],
        'method' => 'post',
        'options' => [
            'class' => 'smart-form',

        ],
    ]); ?>

    <fieldset>
        <div class="row">
            <section class="col col-6">
                <?= $form->field($model, 'property')->dropDownList($model->propertySelect,
                    ['multiple' => 'multiple', 'class' => 'form-control select2param select2']) ?>
                <p style="font-size: 11px;"> <a style="cursor: pointer;" data-action="default">Default</a>
                    <a style="cursor: pointer;"data-action="lab_no">Only Lab_No</a>
                    <a style="cursor: pointer;" data-action="req">Report Number</a>
                </p>

            </section>
            <section class="col col-6">
                <?= $form->field($model, 'parameter')->dropDownList($model->parameterSelect,
                    ['multiple' => 'multiple', 'class' => 'form-control select2param'])
                    ->hint('if there is no choice, it will export all parameter') ?>
                <?= $form->field($model, 'code')->checkbox() ?>
            </section>
        </div>
    </fieldset>
    <fieldset>
        <div class="row">
            <section class="col col-6">
                <?= $form->field($model, 'csv')->checkbox() ?>
            </section>
        </div>
    </fieldset>
    <footer>
        <?= Html::submitButton('Download',
            ['class' => 'btn btn-primary', 'data-action' => 'export_excel']) ?>
    </footer>

    <?php ActiveForm::end(); ?>
</div>

<?php
$idProprty = Html::getInputId($model, 'property');
$jsCode = <<< JS
$(function () {
    let form = $('#form-pilih-excel');

    function property(select) {
        form.find('#' + '$idProprty').val(select).trigger('change');
    }

    form.find('[data-action=lab_no]').on('click', function () {
        property(['Lab_No']);
    });
    form.find('[data-action=req]').on('click', function () {
        property(['Lab_No', 'ADD_PHYS', 'req_order',]);
    });
    form.find('[data-action=default]').on('click', function () {
        property(['Lab_No', 'EVAL_CODE', 'SAMPL_DT1', 'RECV_DT1', 'RPT_DT1', 'MODEL', 'UNIT_NO', 'UNIT_LOC',
            'unitSn', 'RECOMM1', 'RECOMM2', 'Notes',])();
    });
});
JS;


$this->registerJs($jsCode, 4, 'excel');