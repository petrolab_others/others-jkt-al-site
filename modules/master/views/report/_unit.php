<?php

/**
 * Created by
 * User: Wisard17
 * Date: 07/11/2018
 * Time: 09.33 AM
 */

/* @var $this \yii\web\View */
/* @var $model \app\modules\master\models\FormTransaksi|\yii\db\ActiveRecord */
/* @var $form \yii\bootstrap\ActiveForm */
use yii\helpers\Url;

?>
<fieldset>
    <h3>Unit Property</h3>
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'grouploc')->textInput(['maxlength' => true,]) ?>
            <?= $form->field($model, 'branch2')->textInput(['maxlength' => true, $readOnly => $readOnly]) ?>
            <?= $form->field($model, 'name')->textInput(['maxlength' => true, $readOnly => $readOnly]) ?>
            <?= $form->field($model, 'address')->textarea(['rows' => 3, $readOnly => $readOnly]) ?>
            <?= $form->field($model, 'brand2')->textInput(['maxlength' => true, $readOnly => $readOnly]) ?>
            <?= $form->field($model, 'MODEL')->textInput(['maxlength' => true, $readOnly => $readOnly]) ?>
            <?= $form->field($model, 'UNIT_NO')->textInput(['maxlength' => true, $readOnly => $readOnly]) ?>
            <?= $form->field($model, 'UNIT_LOC')->textInput(['maxlength' => true, $readOnly => $readOnly]) ?>
            <?= $form->field($model, 'unitSn')->textInput(['maxlength' => true, $readOnly => $readOnly]) ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'COMPONENT')->textInput(['maxlength' => true, $readOnly => $readOnly]) ?>
            <?= $form->field($model, 'MATRIX')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'OIL_TYPE')->dropDownList($model->OIL_TYPE == '' ? [] :
                [$model->OIL_TYPE => $model->OIL_TYPE], ['class' => 'form-control select2ajax',
                'data-url' => Url::toRoute(['/master/matrix/list-oil-brand']),
            ]) ?>
            <?= $form->field($model, 'OIL_MATRIX')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'ORIG_VISC')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'oil_change')->dropDownList(['Yes' => 'Yes', 'No' => 'No'], ['prompt' => '--- Select ---']) ?>
            <?= $form->field($model, 'ADD_PHYS')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'req_order')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'ADD_VALUE')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
</fieldset>
