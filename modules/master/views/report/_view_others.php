<?php

/**
 * Created by
 * User: Wisard17
 * Date: 19/11/2018
 * Time: 03.47 PM
 */

/* @var $this \yii\web\View */
/* @var $model \app\modules\master\models\FormTransaksi|\yii\db\ActiveRecord */
/* @var $other \app\modules\master\models\OthersOrder */
/* @var $spaceTable string */

?>

<tr>
    <td class="b-r"><?= $other->unsur ?></td>
    <td class="b-r c"><?= $other->unit ?></td>
    <td class="b-r m"><?= $other->methode ?></td>
    <?php foreach ($model->history as $tr) {
        $ot = isset($tr->otherHis[$other->unsur]) ? $tr->otherHis[$other->unsur] : ['value' => '', 'code' => ''];
        echo tdValue($ot['value'], $ot['code']);
    } ?>
    <?= $spaceTable ?>
    <td class="b-r c"><?= $other->getLimit('attention') ?></td>
    <td class="b-r c"><?= $other->getLimit('urgent') ?></td>
</tr>
