<?php

/**
 * Created by
 * User: Wisard17
 * Date: 08/09/2018
 * Time: 12.16 AM
 */

/* @var $this \yii\web\View */
/* @var $model \app\modules\master\models\FormTransaksi */
/* @var $breadcrumbs array */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Report Lab Number : ') . $model->Lab_No;
foreach ($breadcrumbs as $breadcrumb) {
    $this->params['breadcrumbs'][] = $breadcrumb;
}
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">

    <div class="col-lg-9">
        <h1 class="page-title txt-color-blueDark"><i class="fa-fw fa fa-book"></i>
            <?= $this->title ?>
        </h1>
    </div>

    <!-- right side of the page with the sparkline graphs -->
    <!-- col -->
    <div class=" col-lg-3">
        <div class="pull-right" style="margin-right: 10%">
            <?= Html::a('<i class="fa fa-plus-circle"></i>
                    Add', Url::toRoute(['/master/report/new']), ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
    <!-- end col -->

</div>
<!-- widget edit box -->
<div>
    <section id="widget-grid">
        <!-- row -->
        <div class="row">
            <!-- SINGLE GRID -->
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <div class="jarviswidget jarviswidget-color-darken" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false">
                    <header>
                        <h2><strong>View Reports Data</strong></h2>
                    </header>

                    <!-- widget div-->
                    <div>

                        <!-- widget content -->
                        <div class="widget-body" >
                            <?=$this->render('_view', ['model' => $model])?>
                        </div>
                        <!-- end widget content -->

                    </div>
                    <!-- end widget div -->

                </div>
                <!-- end widget -->

            </article><!-- GRID END -->
        </div><!-- end row -->
    </section>

</div>

