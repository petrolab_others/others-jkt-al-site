<?php

/**
 * Created by
 * User: Wisard17
 * Date: 16/11/2018
 * Time: 01.43 PM
 */

/* @var $this \yii\web\View */
/* @var $model \app\modules\master\models\FormTransaksi|\yii\db\ActiveRecord */
/* @var $parameter int|string */
/* @var $code string*/

?>

<tr>
    <td class="b-r"><?= $model->getAttributeLabel($parameter) ?></td>
    <td class="b-r c"><?= $model->getAttributeUnit($parameter) ?></td>
    <td class="b-r m"><?= $model->getAttributeMethod($parameter) ?></td>
    <?php foreach ($model->history as $tr) {
        if ($tr->hasAttribute($parameter) && $tr->hasAttribute($code)) {
            echo tdValue($tr->$parameter, $tr->$code);
        } elseif ($tr->hasAttribute($parameter)) {
            echo tdValue($tr->$parameter, '');
        } else {
            echo tdValue('', '');
        }
    } ?>
    <?= $spaceTable ?>
    <td class="b-r c"><?= $model->getLimit('attention', $parameter) ?></td>
    <td class="b-r c"><?= $model->getLimit('urgent', $parameter) ?></td>
</tr>