<?php

/**
 * Created by
 * User: Wisard17
 * Date: 16/11/2018
 * Time: 09.10 AM
 */

/* @var $this \yii\web\View */
/* @var $model \app\modules\master\models\FormTransaksi|\yii\db\ActiveRecord */

use yii\helpers\Url;
?>

<div class="row">
    <div class="col-md-12 c">
        <ul class="pagination">
            <li class="<?= $model->page == 1 ? 'disabled' : '' ?>">
                <a href="<?= $model->page == 1 ? '#' :
                    Url::to(['', 'id' => $model->Lab_No, 'page' => $model->page - 1]) ?>"><</a></li>
            <?php for ($i = 1; $i <= $model->maxPage; $i++) { ?>
                <li class="<?= $model->page == $i ? 'active' : '' ?>">
                    <a href="<?= Url::to(['', 'id' => $model->Lab_No, 'page' => $i]) ?>"
                       class="<?= $i == $model->page ? 'active_page' : '' ?>"> <?= $i ?></a>
                </li>
            <?php } ?>
            <li class="<?= $model->page == $model->maxPage ? 'disabled' : '' ?>">
                <a href="<?= $model->page == $model->maxPage ? '#' :
                    Url::to(['', 'id' => $model->Lab_No, 'page' => $model->page + 1]) ?>">></a></li>
        </ul>
    </div>
</div>
