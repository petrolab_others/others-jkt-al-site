<?php

/**
 * Created by
 * User: Wisard17
 * Date: 07/09/2018
 * Time: 10.25 PM
 */

/* @var $this \yii\web\View */

use app\smartadmin\Alert;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Reports Admin');
$this->params['breadcrumbs'][] = $this->title;

$pr = [
    'actions',
    'status',
    'statusVerify',
    'statusPublish',
    'branch',
    'customerName',
    'labNumber',
    'sampleDate',
    'receiveDate',
    'reportDate',
    'OIL_TYPE',
    'unitNumber',
    'component',
    'model',
    'serialNo',
    'req_order',
    'oilChg',
];
$o = '8';
$user = Yii::$app->user->identity;
if (!$user->validateRules('manager')) {
    unset($pr[2]);
    $o = '7';
}
?>


<div class="row">

    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
        <h1 class="page-title txt-color-blueDark"><i class="fa-fw fa fa-shopping-cart "></i>
            <?= $this->title ?>
        </h1>
    </div>

    <!-- right side of the page with the sparkline graphs -->
    <!-- col -->
    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
        <div class="pull-right" style="margin-right: 10%">
            <?= Html::tag('div',
                Html::tag('a', 'Actions <span class="caret"></span>', [
                    'class' => 'btn btn-default dropdown-toggle', 'data-toggle' => 'dropdown', 'aria-expanded' => 'false'
                ]) . Html::tag('ul',
//                Html::tag('li', Html::tag('a', '<i class="fa fa-caret-square-o-right"></i> view', [
//                    'href' => '#'])) .
//                Html::tag('li', Html::tag('a', '<i class="fa fa-file-pdf-o"></i> PDF Ver. 1.0', [
//                    'class' => 'btn btn-flat btn-xs', 'title' => 'Download Report', 'target' => '_blank',
//                    'href' => '#'])) .
//                Html::tag('li', Html::tag('a', '<i class="fa fa-file-pdf-o"></i> PDF Ver. 2.0', [
//                    'class' => 'btn btn-flat btn-xs', 'title' => 'Download Report', 'target' => '_blank',
//                    'href' => '#'])) .
                    Html::tag('li', Html::tag('a', '<i class="fa fa-file-excel-o"></i> Export Excel', [
                        'title' => 'Download Excel', 'data-action' => 'admin_excel',
                        'href' => Url::toRoute(['/master/report/download-excel']),])) .
                    Html::tag('li', Html::tag('a', '<i class="fa fa-file-text-o"></i> Export TXT ALP', [
                        'title' => 'Download txt', 'data-action' => 'txt_file',
                        'href' => Url::toRoute(['/master/report/download-custom']),])) .
                    Html::tag('li', Html::tag('a', '<i class="fa fa-refresh"></i> Update Code', [
                        'href' => Url::toRoute(['/master/report/update-eval-code']), 'data-action' => 'update_code'])).
                    Html::tag('li', Html::tag('a', '<i class="fa fa-gear"></i> Send Data', [
                        'href' => Url::toRoute(['/master/report/publish']), 'data-action' => 'publish']))
                    , ['class' => 'dropdown-menu']), ['class' => 'btn-group', 'role' => 'group']) ?>
            <?= Html::a('<i class="fa fa-plus-circle"></i>
                    Add', Url::toRoute(['/master/report/new']), ['class' => 'btn btn-primary']) ?>

        </div>
    </div>
    <!-- end col -->

</div>
<!-- end row -->

<!--
	The ID "widget-grid" will start to initialize all widgets below
	You do not need to use widgets if you dont want to. Simply remove
	the <section></section> and you can use wells or panels instead
	-->

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <?= Alert::widget() ?>

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget well" id="wid-id-0">
                <!-- widget options:
                    usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                    data-widget-colorbutton="false"
                    data-widget-editbutton="false"
                    data-widget-togglebutton="false"
                    data-widget-deletebutton="false"
                    data-widget-fullscreenbutton="false"
                    data-widget-custombutton="false"
                    data-widget-collapsed="true"
                    data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-comments"></i> </span>
                    <h2>Widget Title </h2>

                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->
                        <input class="form-control" type="text">
                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body no-padding">
                        <div class="ibox-content" id="uploadspase">
                            <div style="margin: 10px">
                                <form id="fltr-data" class="form-inline" method="GET">
                                    <small>Receive Date</small>
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <div class="form-group field-filter-r_start">
                                                <label class="sr-only" for="filter-r_start">Start</label>
                                                <div class="input-group">
                                                    <input type="text" id="filter-r_start" class="form-control datejuifilter "
                                                           name="filter[r_start]" placeholder="Start From">
                                                    <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                                </div>
                                                <p class="help-block help-block-error"></p>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-group field-filter-r_end">
                                                <label class="sr-only" for="filter-r_end">End</label>
                                                <div class="input-group">
                                                    <input type="text" id="filter-r_end" class="form-control datejuifilter "
                                                           name="filter[r_end]" placeholder="End">
                                                    <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                                </div>
                                                <p class="help-block help-block-error"></p>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <a id="generate" class="btn btn-default">Generate</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <?= \app\modules\master\models\DataTableView::widget([
                                'columns' => $pr,
                                'options' => ['order' => "[$o, 'desc']"],
                                'model' => new \app\modules\master\models\Transaksi(),
                            ]) ?>
                        </div>
                        <!-- end widget content -->

                    </div>
                    <!-- end widget div -->

                </div>
                <!-- end widget -->

        </article>
        <!-- WIDGET END -->

    </div>

    <!-- end row -->

    <!-- row -->

    <div class="row">

        <!-- a blank row to get started -->
        <div class="col-sm-12">
            <!-- your contents here -->
        </div>

    </div>

    <!-- end row -->

</section>

<?php

$js = <<< JS

$('#fltr-data').find('.datejuifilter').datepicker({
                    showButtonPanel: true,
                    altField: "yyyy-mm-dd",
                    dateFormat: "yy-mm-dd",
                    beforeShow: function (input) {
                        setTimeout(function () {
                            var buttonPane = $(input)
                                .datepicker("widget")
                                .find(".ui-datepicker-buttonpane");
                            $("<button>", {
                                text: "Reset",
                                click: function () {
                                    $.datepicker._clearDate(input);
                                }
                            }).appendTo(buttonPane).addClass("ui-datepicker-clear ui-state-default ui-priority-primary ui-corner-all");
                        }, 1);
                    },
                });

JS;


$this->registerJs($js, 4, 'fltr');
