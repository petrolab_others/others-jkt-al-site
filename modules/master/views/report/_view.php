<?php

/**
 * Created by
 * User: Wisard17
 * Date: 13/11/2018
 * Time: 10.42 AM
 */

/* @var $this \yii\web\View */

/* @var $model \app\modules\master\models\FormTransaksi|\yii\db\ActiveRecord */

use yii\helpers\Url;

function auto($col)
{
    $out = '';
    for ($i = 0; $i < $col; $i++) {
        $out .= '<td class="b-r c">-</td>';
    }
    return $out;
}

$spaceTable = auto(5 - sizeof($model->history));

function color($code)
{
    $cd = evalCode($code);
    if ($cd == 'N') {
        return '';
    } elseif ($cd == 'B') {
        return '#FF6F00';
    } elseif ($cd == 'C') {
        return '#D50000';
    } elseif ($cd == 'D') {
        return '#D50000';
    } elseif ($cd == 'E') {
        return '#D50000';
    } else {
        return '';
    }

}

function logo($code)
{
    $cd = evalCode($code);
    if ($cd == 'N') {
        return '/image/biru.jpg';
    } elseif ($cd == 'B') {
        return '/image/kuning.jpg';
    } elseif ($cd == 'C') {
        return '/image/merah.jpg';
    } elseif ($cd == 'D') {
        return '/image/merah.jpg';
    } elseif ($cd == 'E') {
        return '/image/merah.jpg';
    } else {
        return '/image/biru.jpg';
    }

}

function status($code)
{
    $code = strtoupper($code);
    $eval = 'NORMAL';
    if ($code == 'D') {
        $eval = 'SEVERE';
    }
    if (($code == 'C') || ($code == 'U')) {
        $eval = 'URGENT';
    }
    if ($code == 'B' || $code == 'A') {
        $eval = 'ATTENTION';
    }
    if ($code == 'N') {
        $eval = 'NORMAL';
    }
    return $eval;
}

function evalCode($code)
{
    $code = strtoupper($code);
    if ($code == 'S') {
        $code = 'D';
    }
    if ($code == 'U') {
        $code = 'C';
    }
    if ($code == 'A') {
        $code = 'B';
    }
    return $code;
}

function tdValue($value, $code = 'N')
{
    $n = $code;
    $code = strlen($code) > 1 ? $code[0] : $code;
    $code = evalCode($code);
    $c = status($code) == 'NORMAL' ? '' : "/$n";
    return '<td style="color:' . color($code) . ';" class="b-r c">' . $value . $c . '</td>';
}

?>

<div class="row">
    <div class="col-md-12">
        <div class="tb">
            <table class="auto" style="font-size: 12px">
                <tr>
                    <td class="title" style="width: 115px"><?= $model->getAttributeLabel('branch') ?></td>
                    <td width="1">:</td>
                    <td style="width: 22%"> <?= $model->branch ?></td>
                    <td class="title" style="width: 122px"><?= $model->getAttributeLabel('MODEL') ?></td>
                    <td>:</td>
                    <td style="width: 20%"> <?= $model->MODEL ?></td>
                    <td class="title"><?= $model->getAttributeLabel('OIL_MATRIX') ?></td>
                    <td>:</td>
                    <td> <?= $model->OIL_MATRIX ?></td>

                </tr>
                <tr>
                    <td class="title"><?= $model->getAttributeLabel('name') ?></td>
                    <td>:</td>
                    <td> <?= $model->name ?></td>
                    <td class="title" ><?= $model->getAttributeLabel('UNIT_NO') ?></td>
                    <td width="1">:</td>
                    <td> <?= $model->UNIT_NO ?></td>
                    <td class="title" ><?= $model->getAttributeLabel('unitSn') ?></td>
                    <td width="1">:</td>
                    <td> <?= $model->unitSn ?></td>
                </tr>
                <tr>
                    <td class="title"><?= $model->getAttributeLabel('address') ?></td>
                    <td>:</td>
                    <td> <?= $model->address ?></td>
                    <td class="title"><?= $model->getAttributeLabel('COMPONENT') ?></td>
                    <td>:</td>
                    <td> <?= $model->COMPONENT ?></td>
                    <td class="title"><?= $model->getAttributeLabel('brand2') ?></td>
                    <td>:</td>
                    <td> <?= $model->brand2 ?></td>
                </tr>
                <tr>
                    <td class="title"><?= $model->getAttributeLabel('UNIT_LOC') ?></td>
                    <td>:</td>
                    <td> <?= $model->UNIT_LOC ?></td>
                    <td class="title"><?= $model->getAttributeLabel('MATRIX') ?></td>
                    <td>:</td>
                    <td> <?= $model->MATRIX ?></td>
                    <td class="title"><?= $model->getAttributeLabel('req_order') ?></td>
                    <td>:</td>
                    <td> <?= $model->req_order ?></td>
                </tr>
            </table>
            <br>
        </div>
    </div>
</div>


<?php if ($model->numHistory > 5) {
    echo $this->render('_view_pageing', ['model' => $model]);
} ?>

<div class="row">
    <div class="col-md-12">
        <div class="tb">
            <table class="auto">
                <tbody>
                <tr class="title-header">
                    <th colspan="3">Test Detail</th>
                    <th class="td-value c"></th>
                    <th class="td-value c"></th>
                    <th class="td-value c"></th>
                    <th class="td-value c"></th>
                    <th class="td-value c"></th>
                    <th colspan="2" class="td-value c">Overall Aanalysis Result</th>
                </tr>
                <tr id="lab-number">
                    <td colspan="3" class="b-r"><?= $model->getAttributeLabel('Lab_No') ?></td>
                    <?php foreach ($model->history as $tr) { ?>
                        <td class="b-r c"><?= $tr->Lab_No ?></td>
                    <?php } ?>
                    <?= $spaceTable ?>
                    <td align="center" rowspan="7" colspan="2">
                        <img width="47" src="<?= Url::to(logo($model->EVAL_CODE)) ?>">
                        <p><b><?= status($model->EVAL_CODE) ?></b></p>
                    </td>
                </tr>
                <tr id="sampling-date">
                    <td colspan="3" class="b-r"><?= $model->getAttributeLabel('SAMPL_DT1') ?></td>
                    <?php foreach ($model->history as $tr) { ?>
                        <td class="b-r c"><?= $tr->SAMPL_DT1 ?></td>
                    <?php } ?>
                    <?= $spaceTable ?>
                </tr>
                <tr id="receive-date">
                    <td colspan="3" class="b-r"><?= $model->getAttributeLabel('RECV_DT1') ?></td>
                    <?php foreach ($model->history as $tr) { ?>
                        <td class="b-r c"><?= $tr->RECV_DT1 ?></td>
                    <?php } ?>
                    <?= $spaceTable ?>
                </tr>
                <tr id="report-date">
                    <td colspan="3" class="b-r"><?= $model->getAttributeLabel('RPT_DT1') ?></td>
                    <?php foreach ($model->history as $tr) { ?>
                        <td class="b-r c"><?= $tr->RPT_DT1 ?></td>
                    <?php } ?>
                    <?= $spaceTable ?>
                </tr>
                <tr id="hours-on-oil">
                    <td colspan="3" class="b-r"><?= $model->getAttributeLabel('HRS_KM_OC') ?></td>
                    <?php foreach ($model->history as $tr) { ?>
                        <td class="b-r c"><?= $tr->HRS_KM_OC ?></td>
                    <?php } ?>
                    <?= $spaceTable ?>
                </tr>
                <tr id="hours-on-unit">
                    <td colspan="3" class="b-r"><?= $model->getAttributeLabel('HRS_KM_TOT') ?></td>
                    <?php foreach ($model->history as $tr) { ?>
                        <td class="b-r c"><?= $tr->HRS_KM_TOT ?></td>
                    <?php } ?>
                    <?= $spaceTable ?>
                </tr>
                <tr id="lube-oil-name">
                    <td colspan="3" class="b-r"><?= $model->getAttributeLabel('OIL_TYPE') ?></td>
                    <?php foreach ($model->history as $tr) { ?>
                        <td class="b-r c"><?= $tr->OIL_TYPE ?></td>
                    <?php } ?>
                    <?= $spaceTable ?>
                </tr>
                <tr id="oil-change">
                    <td colspan="3" class="b-r"><?= $model->getAttributeLabel('oil_change') ?></td>
                    <?php foreach ($model->history as $tr) { ?>
                        <td class="b-r c"><?= $tr->oil_change ?></td>
                    <?php } ?>
                    <?= $spaceTable ?>
                    <td></td>
                    <td></td>
                </tr>

                <?php
                foreach ($model->parameterByCategory() as $category => $value) {
                    if ($category == 'Physical Test') {
                        echo "<tr class='title-header'>
                            <th>$category</th>
                            <th>Unit</th>
                            <th colspan='6'>Method</th>
                            <th class='title-r c'>Attention</th>
                            <th class='title-r c'>Urgent</th>
                        </tr>";
                    } elseif ($category == 'Metal Additive' || $category == 'FTIR') {
                        echo "<tr class='title-header'>
                            <th colspan='8'>$category</th>
                            <th class='title-r c'>Warning</th>
                            <th class='title-r c'>Limit</th>
                        </tr>";
                    } else {
                        echo "<tr class='title-header'>
                            <th colspan='8'>$category</th>
                            <td class='b-r c none'></td>
                            <td class='b-r c none'></td>
                        </tr>";
                    }
                    foreach ($value as $parameter => $code) {
                        echo $this->render('_view_row_parameter', [
                            'model' => $model,
                            'parameter' => $parameter,
                            'code' => $code,
                            'spaceTable' => $spaceTable,
                        ]);
                    }
                }

                if (count($model->others) > 0 ) {
                    echo "<tr class='title-header'>
                            <th colspan='8'>Other Analysis</th>
                            <td class='title-r c'></td>
                            <td class='title-r c'></td>
                        </tr>";
                    foreach ($model->others as $other) {
                        echo $this->render('_view_others', [
                            'model' => $model,
                            'other' => $other,
                            'spaceTable' => $spaceTable,
                        ]);
                    }
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<hr>
<?= $this->render('_view_chart', ['model' => $model]) ?>

<style>
    .title-header {
        background-color: #BDBDBD;
    }

    .title-header th {
        padding-top: 5px;
        padding-bottom: 5px;
    }

    .title-header .title-r {
        background-color: #EEEEEE !important;
        color: #757575 !important;
    }

    .title-header .none {
        background-color: white !important;
    }

    td {
        padding-right: 5px;
    }

    .td-value {
        width: 100px;
    }

    .input-td {
        width: 100px !important;
    }

    .b-r {
        border-right: 1.5px solid #BDBDBD;
        border-left: 1.5px solid #BDBDBD;
    }

    .c {
        text-align: center;
    }

    .m {
        font-size: 10px;
    }

    .tb {
        border: 1.5px solid #BDBDBD;
    }

    .auto {
        width: 100%;
        font-size: 11px;
    }

    .auto td {
        padding-left: 5px;
        padding-right: 5px;
    }
</style>