<?php

/**
 * Created by
 * User: Wisard17
 * Date: 07/11/2018
 * Time: 09.23 AM
 */

/* @var $this \yii\web\View */
/* @var $model \app\modules\master\models\FormTransaksi|\yii\db\ActiveRecord */

/* @var $form \yii\bootstrap\ActiveForm */

use yii\jui\DatePicker;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<fieldset>
    <h3>Test Details</h3>
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'Lab_No', ['template' => '{label}
                        <div class="col-lg-8"><div class=" input-group">{input} <div class="input-group-btn">
                        <div class="btn btn-default" type="button" data-action="gen_lan_no" data-url="' .
                Url::toRoute(['/master/report/lab-no', 'type' => isset(Yii::$app->params['typeLabNo']) ? Yii::$app->params['typeLabNo'] : 'J']) . '">
                        <i class="fa fa-history"></i></div>
                        </div></div>{error}
                        </div>',
                'options' => ['class' => 'form-group has-feedback']
            ])->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'unitHour', ['template' => '{label}
                        <div class="col-lg-8"><div class=" input-group">{input} <div class="input-group-btn" >' .
                Html::activeDropDownList($model, 'unitHourL',
                    ['Hour(s)' => 'Hour(s)', 'Day(s)' => 'Day(s)', 'Month(s)' => 'Month(s)', 'Year(s)' => 'Year(s)', 'Km' => 'Km', '01' => 'Not Set'],
                    ['class' => 'btn btn-default']) . ' </div></div>{error}
                        </div>',
                'options' => ['class' => 'form-group has-feedback']
            ])->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'oilHour', ['template' => '{label}
                        <div class="col-lg-8"><div class=" input-group">{input} <div class="input-group-btn" >' .
                Html::activeDropDownList($model, 'oilHourL',
                    ['Hour(s)' => 'Hour(s)', 'Day(s)' => 'Day(s)', 'Month(s)' => 'Month(s)', 'Year(s)' => 'Year(s)', 'Km' => 'Km', 'NEW OIL' => 'NEW OIL', '01' => 'Not Set'],
                    ['class' => 'btn btn-default']) . ' </div></div>{error}
                        </div>',
                'options' => ['class' => 'form-group has-feedback']
            ])->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'SAMPL_DT1', ['template' => '{label}
                        <div class="col-lg-8">{input}<i class="fa fa-calendar form-control-feedback" style="top: 11px;"></i>{error}</div>',
                'options' => ['class' => 'form-group has-feedback']
            ])
                ->widget(DatePicker::className(), [
                    'clientOptions' => ['altField' => 'yyyy-mm-dd'],
                    'options' => ['class' => 'form-control'],
                ]) ?>
            <?= $form->field($model, 'RECV_DT1', ['template' => '{label}
                        <div class="col-lg-8">{input}<i class="fa fa-calendar form-control-feedback" style="top: 11px;"></i>{error}</div>',
                'options' => ['class' => 'form-group has-feedback']
            ])
                ->widget(DatePicker::className(), [
                    'clientOptions' => ['altField' => 'yyyy-mm-dd'],
                    'options' => ['class' => 'form-control'],
                ]) ?>
            <?= $form->field($model, 'RPT_DT1', ['template' => '{label}
                        <div class="col-lg-8">{input}<i class="fa fa-calendar form-control-feedback" style="top: 11px;"></i>{error}</div>',
                'options' => ['class' => 'form-group has-feedback']
            ])
                ->widget(DatePicker::className(), [
                    'clientOptions' => ['altField' => 'yyyy-mm-dd'],
                    'options' => ['class' => 'form-control'],
                ]) ?>
        </div>
    </div>
</fieldset>
