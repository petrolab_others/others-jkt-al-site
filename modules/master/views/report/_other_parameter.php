<?php

/**
 * Created by
 * User: Wisard17
 * Date: 07/11/2018
 * Time: 09.21 AM
 */

/* @var $this \yii\web\View */
/* @var $model \app\modules\master\models\FormTransaksi|\yii\db\ActiveRecord */
/* @var $form \yii\bootstrap\ActiveForm|static */

?>

<fieldset>
    <h3>OTHERS</h3>
    <div class="row" id="other_parameter">
        <div class="col-lg-12">
            <?= $model->renderOther($form)?>
            <a data-action="add_row_table" class="btn btn-primary btn-xs btn-block">
                <i class="fa fa-plus-circle"></i> Add Parameter
            </a>
        </div>
    </div>
</fieldset>
