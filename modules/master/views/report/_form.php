<?php

/**
 * Created by
 * User: Wisard17
 * Date: 08/09/2018
 * Time: 12.17 AM
 */

/* @var $this \yii\web\View */
/* @var $model \app\modules\master\models\FormTransaksi|\yii\db\ActiveRecord */

/* @var $form ActiveForm */


use app\smartadmin\Alert;
use conquer\select2\Select2Asset;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\bootstrap\Tabs;

$readOnly = $model->isNewRecord ? '' : 'readonly';
$classAlertError = sizeof($model->errors) > 0 ? 'alert alert-warning' : '';
?>
    <div class="transaksi_form">

        <?php $form = ActiveForm::begin([
            'id' => 'form-transaksi',
            'layout' => 'horizontal',
            'fieldConfig' => [
                'template' => "{label}\n<div class=\"col-lg-8\">{input}\n{error}</div>",
                'labelOptions' => ['class' => 'col-lg-4 control-label'],
            ],
        ]); ?>

        <div class="<?= $classAlertError ?>"><?= $form->errorSummary($model); ?><?= Alert::widget() ?></div>

        <div class="row">
            <div class="col-lg-6">
                <?= $form->field($model, 'branch')->dropDownList($model->branch == '' ? [] :
                    [$model->branch => $model->branch], ['class' => 'form-control select2ajax',
                    'data-url' => Url::toRoute(['/customers/default/list-branch'])]) ?>
                <?= $form->field($model, 'customer_id')->dropDownList($model->customer_id == '' ? [] :
                    [$model->customer_id => $model->customer->Name], ['class' => 'form-control select2ajax',
                    'data-url' => Url::toRoute(['/customers/default/list-customer']), 'data-depend-from' => 'branch',
                    'data-depend' => 'branch', 'data-depend-id' => 'formtransaksi-branch',
                ]) ?>
            </div>
            <div class="col-lg-6">
                <?= $form->field($model, 'unit_id')->dropDownList($model->unit_id == '' ? [] :
                    [$model->unit_id => $model->unit->UnitNo], ['class' => 'form-control select2ajax',
                    'data-url' => Url::toRoute(['/master/unit/list-unit']), 'data-depend-from' => 'customer_id',
                    'data-depend' => 'CustomerID', 'data-depend-id' => 'formtransaksi-customer_id',
                ]) ?>
                <?= $form->field($model, 'ComponentID')->dropDownList($model->ComponentID == '' ? [] :
                    [$model->ComponentID => $model->component], ['class' => 'form-control select2ajax',
                    'data-url' => Url::toRoute(['/master/unit/list-component']), 'data-depend-from' => 'unit_id',
                    'data-depend' => 'UnitID', 'data-depend-id' => 'formtransaksi-unit_id',
                ]) ?>
            </div>
        </div>
        <hr>

        <?php
        $itab = [];
        $itab[] = [
            'label' => 'Unit Property',
            'content' => $this->render('_unit', ['model' => $model, 'form' => $form, 'readOnly' => $readOnly]) .
                $this->render('_test_detail', ['model' => $model, 'form' => $form]),
            'active' => $itab == [],
        ];
//        $itab[] = [
//            'label' => 'Test Detail',
//            'content' => $this->render('_test_detail', ['model' => $model, 'form' => $form]),
//            'active' => $itab == [],
//        ];
        $itab[] = [
            'label' => 'Parameter',
            'content' => $this->render('_parameter', ['model' => $model, 'form' => $form]),
            'active' => $itab == [],
        ];
        $itab[] = [
            'label' => 'Other Parameter',
            'content' => $this->render('_other_parameter', ['model' => $model, 'form' => $form]),
            'active' => $itab == [],
        ];
        $itab[] = [
            'label' => 'Eval Code & Recommendations',
            'content' => $this->render('_eval_code', ['model' => $model, 'form' => $form]),
            'active' => $itab == [],
        ];
        echo Tabs::widget(['items' => $itab]);

        ?>

        <fieldset>
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        </fieldset>

        <?php ActiveForm::end(); ?>

    </div>

    <style>
        .hide_parameter {
            display: none;
        }
    </style>
<?php
Select2Asset::register($this);
$cls = Json::encode($model::className());
$loadingHtml = '<div align="center"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-spin margin-bottom"></i></div>';
$jsCode = <<< JS
$(function () {
    let form = $('#form-transaksi');
    form.find('#formtransaksi-branch').on('select2:select', function (e) {
        form.find('#formtransaksi-customer_id').val(null).trigger('change');
        form.find('#formtransaksi-unit_id').val(null).trigger('change');
        form.find('#formtransaksi-componentid').val(null).trigger('change');
        form.find('#formtransaksi-name').val(null).blur().removeAttr('readonly');
        form.find('#formtransaksi-model').val(null).blur().removeAttr('readonly');
        form.find('#formtransaksi-unit_no').val(null).blur().removeAttr('readonly');
        form.find('#formtransaksi-unit_loc').val(null).blur().removeAttr('readonly');
        form.find('#formtransaksi-unitsn').val(null).blur().removeAttr('readonly');
        form.find('#formtransaksi-brand2').val(null).blur().removeAttr('readonly');
        form.find('#formtransaksi-component').val(null).blur().removeAttr('readonly');
        let data = e.params.data;
        if (data.id != 0) {
            form.find('#formtransaksi-branch2').val(data.text).blur().attr('readonly', 'readonly');
            form.find('#formtransaksi-address').val(data.address).blur().attr('readonly', 'readonly');
        } else {
            form.find('#formtransaksi-branch2').val(null).blur().removeAttr('readonly');
            form.find('#formtransaksi-address').val(null).blur().removeAttr('readonly');
        }

    });
    form.find('#formtransaksi-customer_id').on('select2:select', function (e) {
        form.find('#formtransaksi-unit_id').val(null).trigger('change');
        form.find('#formtransaksi-componentid').val(null).trigger('change');
        form.find('#formtransaksi-model').val(null).blur().removeAttr('readonly');
        form.find('#formtransaksi-unit_no').val(null).blur().removeAttr('readonly');
        form.find('#formtransaksi-unit_loc').val(null).blur().removeAttr('readonly');
        form.find('#formtransaksi-unitsn').val(null).blur().removeAttr('readonly');
        form.find('#formtransaksi-brand2').val(null).blur().removeAttr('readonly');
        form.find('#formtransaksi-component').val(null).blur().removeAttr('readonly');
        let data = e.params.data;
        if (data.id != 0) {
            form.find('#formtransaksi-name').val(data.text).blur().attr('readonly', 'readonly');
        } else {
            form.find('#formtransaksi-name').val(null).blur().removeAttr('readonly');
        }
    });
    form.find('#formtransaksi-unit_id').on('select2:select', function (e) {
        form.find('#formtransaksi-componentid').val(null).trigger('change');
        form.find('#formtransaksi-component').val(null).blur().removeAttr('readonly');
        let data = e.params.data;
        if (data.id != 0) {
            form.find('#formtransaksi-model').val(data.Model).blur().attr('readonly', 'readonly');
            form.find('#formtransaksi-unit_no').val(data.UnitNo).blur().attr('readonly', 'readonly');
            form.find('#formtransaksi-unit_loc').val(data.UnitLocation).blur().attr('readonly', 'readonly');
            form.find('#formtransaksi-unitsn').val(data.SerialNo).blur().attr('readonly', 'readonly');
            form.find('#formtransaksi-brand2').val(data.Brand).blur().attr('readonly', 'readonly');
        } else {
            form.find('#formtransaksi-model').val(null).blur().removeAttr('readonly');
            form.find('#formtransaksi-unit_no').val(null).blur().removeAttr('readonly');
            form.find('#formtransaksi-unit_loc').val(null).blur().removeAttr('readonly');
            form.find('#formtransaksi-unitsn').val(null).blur().removeAttr('readonly');
            form.find('#formtransaksi-brand2').val(null).blur().removeAttr('readonly');
        }
    });
    form.find('#formtransaksi-componentid').on('select2:select', function (e) {
        let data = e.params.data;
        if (data.id != 0) {
            form.find('#formtransaksi-component').val(data.text).blur().attr('readonly', 'readonly');
            form.find('#formtransaksi-matrix').val(data.MATRIX).blur();
        } else {
            form.find('#formtransaksi-component').val(null).blur().removeAttr('readonly');
            form.find('#formtransaksi-matrix').val(null).blur().removeAttr('readonly');
        }
    });
    form.find('#formtransaksi-oil_type').on('select2:select', function (e) {
        let data = e.params.data;
        if (data.id != 0) {
            form.find('#formtransaksi-oil_matrix').val(data.OIL_MATRIX).blur();
            form.find('#formtransaksi-orig_visc').val(data.ORIG_VISC).blur();
        } else {
            form.find('#formtransaksi-oil_matrix').val(null).blur().removeAttr('readonly');
            form.find('#formtransaksi-orig_visc').val(null).blur().removeAttr('readonly');
        }
    });
    let par = function () {
        return {
            ajax: {
                url: function (a) {
                    return this.attr('data-url');
                },
                data: function (params) {
                    let el = this;
                    let query = {
                        q: params.term,
                        param: {
                            col: el.attr('data-depend'),
                            fromcol: el.attr('data-depend-from'),
                            filter: form.find('#' + el.attr('data-depend-id')).val(),
                            model: $cls,
                        }
                    };
                    return query;
                },
                processResults: function (data) {
                    return {
                        results: data.results
                    };
                }
            }
        }
    };
    form.find('.select2ajax').select2(par());

    form.delegate('[data-action=gen_lan_no]', 'click', function () {
        var elm = $(this);
        var label = elm.closest('.form-group');
        var input = label.find('input');
        $.ajax({
            url: elm.attr('data-url'),
            method: 'get',
            beforeSubmit: function (d) {
                elm.find('i').addClass('fa-spin');
            },
            error: function (response) {
                elm.find('i').removeClass('fa-spin');
            },
            success: function (response) {
                elm.find('i').removeClass('fa-spin');
                input.val(response.lab_no).blur();
            },

        });
    });

    form.delegate('[data-action=delate_row]', 'click', function () {
        var elm = $(this);
        var row = elm.closest('tr');
        p = '$model->otherForm';
        p = p.replace('[new]', '[' + row.attr('data-id') + ']');

        row.html('<input type="hidden" name="' + p + '[actionId]" value="del" >');

    });

    function select_detail(t) {
        t.find('.selectother').select2({
            placeholder: "Select a Parameter",
            width: "200px",
            ajax: {
                url: function (a) {
                    return this.attr('data-url');
                }
            }
        });
    }

    let cho = 0;
    form.find('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        if ($(e.target).attr('href') == '#w0-tab2' && cho === 0) {
            var p = form.find($(e.target).attr('href'));
            select_detail(p);
            $.each(p.find('tr'), function (i, v) {
                let elm = $(v);
                if (elm.attr('data-id') !== undefined) {
                    genselectother(elm.attr('data-id'));
                }
            });
            cho++;
        }
    })

    form.delegate('[data-action=add_row_table]', 'click', function () {
        var elm = $(this);
        var tbl = elm.closest('fieldset').find('table');
        var form = elm.closest('form');
        $.ajax({
            method: 'post',
            data: {'reg_new_row': (tbl.find('tr').length - 1)},
            error: function (response) {
                alert_notif({status: "error", message: 'tidak bisa save'});
            },
            success: function (response) {
                var t = $(response.data);
                if ($.fn.select2 !== undefined)
                    select_detail(t);
                console.log(t);
                genselectother(t.attr('data-id'));
                tbl.find('tbody').append(t);
                // $(form).yiiActiveForm(response.jsAdd, []);
                $.each(response.jsAdd, function (i, v) {
                    var vali = eval("var f = function(){ return " + v.validate.expression + ";}; f() ;");
                    v.validate = vali;
                    $(form).yiiActiveForm('add', v);
                });
            }
        });
    });

    function genselectother(id_var) {
        form.delegate('#othersorder-' + id_var + '-selectparameter', 'select2:select', function (e) {
            let td = $(this).closest('td');
            console.log(td);
            let data = e.params.data;
            if (data.id != 0) {
                form.find('#othersorder-' + id_var + '-unsur').val(data.unsur).blur();
                form.find('#othersorder-' + id_var + '-unit').val(data.unit).blur();
                form.find('#othersorder-' + id_var + '-methode').val(data.methode).blur();
                form.find('#othersorder-' + id_var + '-attention').val(data.attention).blur();
                form.find('#othersorder-' + id_var + '-urgent').val(data.urgent).blur();
                form.find('#othersorder-' + id_var + '-severe').val(data.severe).blur();
                form.find('#othersorder-' + id_var + '-tbl_parameter_id').val(data.tbl_parameter_id).blur();
                td.find('.otherparam').addClass('hide_parameter');
            } else {
                form.find('#othersorder-' + id_var + '-unsur').val(null).blur().removeAttr('readonly');
                form.find('#othersorder-' + id_var + '-unit').val(null).blur().removeAttr('readonly');
                form.find('#othersorder-' + id_var + '-methode').val(null).blur().removeAttr('readonly');
                form.find('#othersorder-' + id_var + '-attention').val(null).blur().removeAttr('readonly');
                form.find('#othersorder-' + id_var + '-urgent').val(null).blur().removeAttr('readonly');
                form.find('#othersorder-' + id_var + '-severe').val(null).blur().removeAttr('readonly');
                form.find('#othersorder-' + id_var + '-tbl_parameter_id').val(null).blur().removeAttr('readonly');
                td.find('.otherparam').removeClass('hide_parameter');
            }
        });
    }
    
    form.find('.option_parameter').popover({
        content: function() {
            let el = $(this);
            let idDiv = el.attr('id-div');
            return '<div id="' + idDiv + '">$loadingHtml</div>';
        }
    });

    function alert_notif(res) {
        $.sound_on = false;
        var color = res.status === 'success' ? '#659265' : '#C46A69';
        var icon = res.status === 'success' ? 'fa-check' : 'fa-times';
        $.smallBox({
            title: res.status,
            content: res.message,
            color: color,
            iconSmall: "fa " + icon + " fa-2x fadeInRight animated",
            timeout: 4000
        });
    }
});
JS;

$this->registerJs($jsCode, 4, 'asdafasf');
