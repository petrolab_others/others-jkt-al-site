<?php

/**
 * Created by
 * User: Wisard17
 * Date: 16/11/2018
 * Time: 09.07 AM
 */

/* @var $this \yii\web\View */
/* @var $model \app\modules\master\models\FormTransaksi|\yii\db\ActiveRecord */

?>
<!--chart-->
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <div class="row">
                <div class="jarviswidget" id="wid-id-1" data-widget-colorbutton="false"
                     data-widget-fullscreenbutton="false" data-widget-editbutton="false" data-widget-sortable="false">
                    <header>
                        <h2>Wear Trending </h2>
                    </header>
                    <!-- widget div-->
                    <div>
                        <!-- widget edit box -->
                        <div class="jarviswidget-editbox">
                            <!-- This area used as dropdown edit box -->
                            <input class="form-control" type="text">

                        </div>
                        <!-- end widget edit box -->
                        <!-- widget content -->
                        <div class="widget-body">
                            <!-- this is what the user will see -->
                            <canvas id="chart1" height="120"></canvas>
                        </div>
                        <!-- end widget content -->

                    </div>
                    <!-- end widget div -->
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="row">
                <div class="jarviswidget" id="wid-id-2" data-widget-colorbutton="false"
                     data-widget-fullscreenbutton="false" data-widget-editbutton="false" data-widget-sortable="false">
                    <header>
                        <h2>Contaminant - FTIR Analysis </h2>
                    </header>
                    <!-- widget div-->
                    <div>
                        <!-- widget edit box -->
                        <div class="jarviswidget-editbox">
                            <!-- This area used as dropdown edit box -->
                            <input class="form-control" type="text">

                        </div>
                        <!-- end widget edit box -->
                        <!-- widget content -->
                        <div class="widget-body">
                            <!-- this is what the user will see -->
                            <canvas id="chart2" height="120"></canvas>
                        </div>
                        <!-- end widget content -->

                    </div>
                    <!-- end widget div -->
                </div>
            </div>
        </div>

    </div>
    <div class="row">

        <div class="col-md-6">
            <div class="row">
                <div class="jarviswidget" id="wid-id-3" data-widget-colorbutton="false"
                     data-widget-fullscreenbutton="false" data-widget-editbutton="false" data-widget-sortable="false">
                    <header>
                        <h2>TAN - TBN </h2>
                    </header>
                    <!-- widget div-->
                    <div>
                        <!-- widget edit box -->
                        <div class="jarviswidget-editbox">
                            <!-- This area used as dropdown edit box -->
                            <input class="form-control" type="text">

                        </div>
                        <!-- end widget edit box -->
                        <!-- widget content -->
                        <div class="widget-body">
                            <!-- this is what the user will see -->
                            <canvas id="chart3" height="120"></canvas>
                        </div>
                        <!-- end widget content -->

                    </div>
                    <!-- end widget div -->
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="row">
                <div class="jarviswidget" id="wid-id-4" data-widget-colorbutton="false"
                     data-widget-fullscreenbutton="false" data-widget-editbutton="false" data-widget-sortable="false">
                    <header>
                        <h2>Viscosity </h2>
                    </header>
                    <!-- widget div-->
                    <div>
                        <!-- widget edit box -->
                        <div class="jarviswidget-editbox">
                            <!-- This area used as dropdown edit box -->
                            <input class="form-control" type="text">

                        </div>
                        <!-- end widget edit box -->
                        <!-- widget content -->
                        <div class="widget-body">
                            <!-- this is what the user will see -->
                            <canvas id="chart4" height="120"></canvas>
                        </div>
                        <!-- end widget content -->

                    </div>
                    <!-- end widget div -->
                </div>
            </div>
        </div>

    </div>
</div>
