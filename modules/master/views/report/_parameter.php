<?php

/**
 * Created by
 * User: Wisard17
 * Date: 07/11/2018
 * Time: 09.16 AM
 */

/* @var $this \yii\web\View */
/* @var $model \app\modules\master\models\FormTransaksi|\yii\db\ActiveRecord */
/* @var $form \yii\bootstrap\ActiveForm */

use yii\helpers\Html;

$m = $model->getUrlMpc();
$p = $model->getUrlPatch();

$item1 = [''=> 'Code', 'B' => 'B', 'C' => 'C', 'D' => 'D'];
$item2 = [''=> 'Code', 'BL' => 'BL', 'BH' => 'BH', 'CL' => 'CL', 'CH' => 'CH', 'DL' => 'DL', 'DH' => 'DH'];

foreach ($model->parameterByCategory2() as $category => $value) {
    if ($category == 'MPC') {
        break;
    }
    echo "<fieldset><h3>$category</h3><div class='row'>";
    foreach ($value as $parameter => $code) {
        if ($code == '') {
            echo $form->field($model, $parameter)->textInput(['maxlength' => true]);
            continue;
        }
        echo '<div class="col-lg-6">';
        echo $this->render('_parameter_code', ['form' => $form, 'model' => $model,
            'parameter' => $parameter, 'code' => $code, 'items' =>
                in_array($parameter, ['VISC_CST_sp', 'visc_40_sp']) ? $item2 : $item1]);
        echo '</div>';
    }
    echo '</div></fieldset>';
}
?>

<fieldset>
    <h3>MPC</h3>
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'mpc')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'uploadMpc')->fileInput() ?>
        </div>
        <div class="col-lg-6">
            <?php if ($m != null) {
                echo Html::img($m, ['alt' => 'mpc-img', 'width' => '100%']);
            } ?>
        </div>
    </div>
</fieldset>
<fieldset>
    <h3>Patch Test</h3>
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'patch_test')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'v_sample')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'magnification')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'uploadPatch')->fileInput() ?>
            <?php if ($p != null) {
                echo Html::img($p, ['alt' => 'patch-img', 'width' => '100%']);
            } ?>
        </div>
    </div>
</fieldset>
