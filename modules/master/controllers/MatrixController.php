<?php
/**
 * Created by
 * User: Wisard17
 * Date: 05/11/2018
 * Time: 11.47 AM
 */

namespace app\modules\master\controllers;


use app\components\ArrayHelper;
use app\modules\master\models\OilBrand;
use app\modules\master\models\search\SearchMatrix;
use Yii;
use app\models\AccessControl;
use conquer\select2\Select2Action;
use yii\base\Model;
use yii\db\ActiveQuery;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class MatrixController
 * @package app\modules\master\controllers
 */
class MatrixController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['detail', 'index', 'add', 'edit'],
                'rules' => [
                    [
                        'actions' => ['detail', 'index', 'add', 'edit'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'del' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'list-oil-brand' => [
                'class' => Select2Action::className(),
                'dataCallback' => [$this, 'dataCallback'],
            ],
        ];
    }

    /**
     * @param $q
     * @return array
     */
    public function dataCallback($q)
    {
        $query = new ActiveQuery(OilBrand::className());
        $get = Yii::$app->request->get('param');
        $add = '';
        $out = [];
        if (isset($get['col'])) {
            $add = "$get[col] = '$get[filter]'";
            if ($get['filter'] == '') {
                /** @var Model $model */
                $model = new $get['model']();
                $n = isset($model->attributeLabels()[$get['fromcol']]) ? $model->attributeLabels()[$get['fromcol']] : $get['fromcol'];
                $out[] = [
                    'id' => '',
                    'text' => $n . ' select first',
                ];
            }
        }
        $out = array_merge($out, $query->select([
            'OIL_TYPE as id',
            'OIL_TYPE as text',
            'OIL_TYPE',
            'ORIG_VISC',
            'OIL_MATRIX',
        ])->filterWhere(['like', 'OIL_TYPE', $q])->andWhere($add)
            ->asArray()
            ->limit(20)
            ->all());
        return [
            'results' => $out,
        ];
    }

    /**
     * @return mixed|string
     */
    public function actionIndex()
    {
        $searchModel = new SearchMatrix();
        $request = Yii::$app->request->queryParams;

        if (Yii::$app->request->isAjax) {
            return $searchModel->searchDataTable($request);
        }

        return $this->render('index');
    }

    /**
     * @return string|\yii\web\Response
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function actionNew()
    {
        $model = new SearchMatrix();

        $param = Yii::$app->request->queryParams;
        if (!Yii::$app->request->isPost && isset($param['duplicate'])) {
            $duplicate = SearchMatrix::findOne($param['duplicate']);
            if ($duplicate != null) {
                $model->load(['Data' => ArrayHelper::toArray($duplicate)], 'Data');
            }
        }

        $post = Yii::$app->request->post();

        if ($model->load($post) && $model->save()) {
            return $this->redirect(Url::toRoute(['/master/matrix/index', 'id' => $model->Matrix]));
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return string|Response
     * @throws NotFoundHttpException
     */
    public function actionEdit($id)
    {
        $model = $this->findModel($id);

        $post = Yii::$app->request->post();

        if ($model->load($post) && $model->save()) {
            return $this->redirect(Url::toRoute(['/master/matrix/index', 'id' => $model->Matrix]));
        }

        return $this->render('edit', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDel($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Orders model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SearchMatrix the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SearchMatrix::find()->where("Matrix = '$id'")->one()) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}