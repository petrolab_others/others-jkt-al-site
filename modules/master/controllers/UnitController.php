<?php
/**
 * Created by
 * User: Wisard17
 * Date: 03/11/2018
 * Time: 12.52 PM
 */

namespace app\modules\master\controllers;


use app\components\ArrayHelper;
use app\modules\master\models\Component;
use app\modules\master\models\FormUnit;
use Yii;
use app\models\AccessControl;
use app\modules\master\models\search\SearchUnit;
use conquer\select2\Select2Action;
use yii\base\Model;
use yii\db\ActiveQuery;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class UnitController
 * @package app\modules\master\controllers
 */
class UnitController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['detail', 'index', 'add', 'edit'],
                'rules' => [
                    [
                        'actions' => ['detail', 'index', 'add', 'edit'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'del' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'list-unit' => [
                'class' => Select2Action::className(),
                'dataCallback' => [$this, 'dataCallback'],
            ],
            'list-component' => [
                'class' => Select2Action::className(),
                'dataCallback' => [$this, 'dataComponent'],
            ],
        ];
    }

    /**
     * @param $q
     * @return array
     */
    public function dataCallback($q)
    {
        $query = new ActiveQuery(SearchUnit::className());
        $get = Yii::$app->request->get('param');
        $add = '';
        $out = [];
        if (isset($get['col'])) {
            $add = "$get[col] = '$get[filter]'";
            if ($get['filter'] == '') {
                /** @var Model $model */
                $model = new $get['model']();
                $n = isset($model->attributeLabels()[$get['fromcol']]) ? $model->attributeLabels()[$get['fromcol']] : $get['fromcol'];
                $out[] = [
                    'id' => '',
                    'text' => $n . ' select first',
                ];
            }
        }
        $out = array_merge($out, $query->select([
            'UnitID as id',
            'UnitNo as text',
            'UnitNo',
            'Model',
            'SerialNo',
            'Brand',
            'UnitLocation',
        ])->filterWhere(['like', 'UnitNo', $q])->andWhere($add)
            ->asArray()
            ->limit(20)
            ->all());
        if ($out == []) {
            $out[] = [
                'id' => 0,
                'text' => 'New Unit',
                'UnitNo' => '',
                'Model' => '',
                'SerialNo' => '',
                'Brand' => '',
                'UnitLocation' => '',
            ];
        }
        return [
            'results' => $out,
        ];
    }

    /**
     * @param $q
     * @return array
     */
    public function dataComponent($q)
    {
        $query = new ActiveQuery(Component::className());
        $get = Yii::$app->request->get('param');
        $add = '';
        $out = [];
        if (isset($get['col'])) {
            $add = "$get[col] = '$get[filter]'";
            if ($get['filter'] == '') {
                /** @var Model $model */
                $model = new $get['model']();
                $n = isset($model->attributeLabels()[$get['fromcol']]) ? $model->attributeLabels()[$get['fromcol']] : $get['fromcol'];
                $out[] = [
                    'id' => '',
                    'text' => $n . ' select first',
                ];
            }
        }

        $query->orFilterWhere(['like', 'component', $q])
            ->orFilterWhere(['like', 'ComponentCode', $q])
            ->andWhere($add)
            ->limit(30);
        /** @var Component $record */
        foreach ($query->all() as $record) {
            $out[] = [
                'id' => $record->ComponentID,
                'text' => $record->component == '' ? $record->ComponentCode : $record->component,
                'MATRIX' => $record->Matrix,
            ];
        }

        if ($out == []) {
            $out[] = [
                'id' => 0,
                'text' => 'New Component',
            ];
        }
        return [
            'results' => $out,
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new SearchUnit();
        $request = Yii::$app->request->queryParams;

        if (Yii::$app->request->isAjax) {
            return $searchModel->searchDataTable($request);
        }

        return $this->render('index');
    }

    /**
     * @return array|string|Response
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionNew()
    {
        $model = new FormUnit();

        $param = Yii::$app->request->queryParams;
        if (!Yii::$app->request->isPost && isset($param['duplicate'])) {
            $duplicate = FormUnit::findOne($param['duplicate']);
            if ($duplicate != null) {
                $arr = [];
                foreach ($duplicate->components as $i => $item) {
                    $arr['newrow_' . $i] = ArrayHelper::toArray($item);
                    unset($arr['newrow_' . $i]['ComponentID'], $arr['newrow_' . $i]['UnitID']);
                }
                $model->load(['Data' => ArrayHelper::toArray($duplicate), 'Component' => $arr], 'Data');
            }
        }

        $post = Yii::$app->request->post();

        if (Yii::$app->request->isAjax && isset($post['reg_new_row'])) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $model::renderRowAjax($post['reg_new_row']);
        }

        if ($model->load($post) && $model->save()) {
            return $this->redirect(Url::toRoute(['/master/unit/view', 'id' => $model->UnitID]));
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return array|string|Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionEdit($id)
    {
        $model = $this->findModel($id);

        $post = Yii::$app->request->post();

        if (Yii::$app->request->isAjax && isset($post['reg_new_row'])) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $model::renderRowAjax($post['reg_new_row']);
        }

        if ($model->load($post) && $model->save()) {
            return $this->redirect(Url::toRoute(['/master/unit/view', 'id' => $model->UnitID]));
        }

        return $this->render('edit', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDel($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Orders model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FormUnit the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FormUnit::find()->where("UnitID = '$id'")->one()) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}