<?php
/**
 * Created by
 * User: Wisard17
 * Date: 29/11/2018
 * Time: 04.35 PM
 */

namespace app\modules\master\controllers;


use app\components\ArrayHelper;
use app\modules\master\models\OilMatrix;
use Yii;
use app\models\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Class OilMatrixController
 * @package app\modules\master\controllers
 */
class OilMatrixController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['detail', 'index', 'add', 'edit'],
                'rules' => [
                    [
                        'actions' => ['detail', 'index', 'add', 'edit'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'del' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @return mixed|string
     */
    public function actionIndex()
    {
        $searchModel = new OilMatrix();
        $request = Yii::$app->request->queryParams;

        if (Yii::$app->request->isAjax) {
            return $searchModel->searchDataTable($request);
        }

        return $this->render('index');
    }

    /**
     * @return string|\yii\web\Response
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function actionNew()
    {
        $model = new OilMatrix();

        $param = Yii::$app->request->queryParams;
        if (!Yii::$app->request->isPost && isset($param['duplicate'])) {
            $duplicate = OilMatrix::findOne($param['duplicate']);
            if ($duplicate != null) {
                $model->load(['Data' => ArrayHelper::toArray($duplicate)], 'Data');
            }
        }


        $post = Yii::$app->request->post();

        if ($model->load($post) && $model->save()) {
            return $this->redirect(Url::toRoute(['/master/oil-matrix/index', 'id' => $model->OIL_MATRIX]));
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionEdit($id)
    {
        $model = $this->findModel($id);

        $post = Yii::$app->request->post();

        if ($model->load($post) && $model->save()) {
            return $this->redirect(Url::toRoute(['/master/oil-matrix/index', 'id' => $model->OIL_MATRIX]));
        }

        return $this->render('edit', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDel($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Orders model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OilMatrix the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = OilMatrix::find()->where("OIL_MATRIX = '$id'")->one()) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}