<?php

namespace app\modules\master\controllers;

use app\modules\reports\models\Customer;
use yii\web\Controller;

/**
 * Default controller for the `master` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionPutBranceNo()
    {
        ini_set('memory_limit', '2048M');
        ini_set('max_execution_time', 3000);
        $c = Customer::find()->all();
        foreach ($c as $record) {
            $record->branch_no = $record->branch == '' ? null : $record->branch->branch_no;
            $record->save();
        }
    }
}
