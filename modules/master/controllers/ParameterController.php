<?php
/**
 * Created by
 * User: Wisard17
 * Date: 16/11/2018
 * Time: 03.42 PM
 */

namespace app\modules\master\controllers;


use app\models\AccessControl;
use app\modules\master\models\FormParameter;
use app\modules\master\models\search\SearchOtherParameter;
use app\modules\master\models\search\SearchParameter;
use Yii;
use conquer\select2\Select2Action;
use yii\db\ActiveQuery;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Class ParameterController
 * @package app\modules\master\controllers
 */
class ParameterController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['detail', 'index', 'add', 'edit'],
                'rules' => [
                    [
                        'actions' => ['detail', 'index', 'add', 'edit'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'del' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'list-other_parameters' => [
                'class' => Select2Action::className(),
                'dataCallback' => [$this, 'dataCallback'],
            ],
        ];
    }

    /**
     * @param $q
     * @return array
     */
    public function dataCallback($q)
    {
        $query = new ActiveQuery(SearchOtherParameter::className());
        $out = [];
        $out = array_merge($out, $query->select([
            'id as tbl_parameter_id',
            'unsur as id',
            'unsur as text',
            'unsur',
            'unit',
            'methode',
            'attention',
            'urgent',
            'severe',
        ])->filterWhere(['like', 'unsur', $q])->andWhere(['type' => 'other'])->distinct()
            ->asArray()
            ->limit(20)
            ->all());
        if ($out == []) {
            $out[] = [
                'id' => 0,
                'text' => 'New Parameter',
                'unsur' => '',
                'unit' => '',
                'methode' => '',
                'attention' => '',
                'urgent' => '',
                'severe' => '',
                'tbl_parameter_id' => '',
            ];
        }
        return [
            'results' => $out,
        ];
    }

    /**
     * @return mixed|string
     */
    public function actionIndex()
    {
        $searchModel = new SearchParameter();
        $request = Yii::$app->request->queryParams;

        if (Yii::$app->request->isAjax) {
            return $searchModel->searchDataTable($request);
        }

        return $this->render('index');
    }

    /**
     * @return mixed|string
     */
    public function actionOthers()
    {
        $searchModel = new SearchOtherParameter();
        $request = Yii::$app->request->queryParams;

        if (Yii::$app->request->isAjax) {
            return $searchModel->searchDataTable($request);
        }

        return $this->render('index_others');
    }

    /**
     * @param string $type
     * @return string|\yii\web\Response
     */
    public function actionNew($type = 'param')
    {
        $model = new FormParameter();

        $post = Yii::$app->request->post();
        $model->type = $type;
        if ($model->load($post) && $model->save()) {
            return $this->redirect(Url::toRoute(['/master/parameter/index', 'id' => $model->id]));
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function actionEdit($id)
    {
        $model = $this->findModel($id);

        $post = Yii::$app->request->post();

        if ($model->load($post) && $model->save()) {
            return $this->redirect(Url::toRoute(['/master/parameter/index', 'id' => $model->id]));
        }

        return $this->render('edit', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDel($id)
    {
        $model = $this->findModel($id);
        $param = $model->type == 'other' ? 'others' : 'index';
        $model->delete();

        return $this->redirect([$param]);
    }

    /**
     * Finds the Orders model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FormParameter|array|null|\yii\db\ActiveRecord the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FormParameter::find()->where("id = $id")->one()) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}