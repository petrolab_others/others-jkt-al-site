<?php
/**
 * Created by
 * User: Wisard17
 * Date: 07/09/2018
 * Time: 10.22 PM
 */

namespace app\modules\master\controllers;


use app\assets\UploadBlueimpAsset;
use app\components\ArrayHelper;
use app\models\AccessControl;
use app\modelsDB\OptionsParam;
use app\modules\master\models\FormDataUpdateStatus;
use app\modules\master\models\FormDownloadExcel;
use app\modules\master\models\FormPublish;
use app\modules\master\models\FormTransaksi;
use app\modules\master\models\FormUploadTransaksi;
use app\modules\master\models\search\SearchTransaksi;
use app\modules\master\models\UpdateEvalCode;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\widgets\ActiveForm;
use Curl\Curl;

/**
 * Class ReportController
 * @package app\modules\master\controllers
 */
class ReportController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['detail', 'index', 'new', 'edit'],
                'rules' => [
                    [
                        'actions' => ['detail', 'index', 'new', 'edit'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return in_array(Yii::$app->user->identity->ruleAccess, [-1, 4, 5, 6], true);
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'del' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @return mixed|string
     */
    public function actionIndex()
    {
        $searchModel = new SearchTransaksi();
        $request = Yii::$app->request->queryParams;

        if (Yii::$app->request->isAjax) {
            return $searchModel->searchDataTable($request);
        }

        return $this->render('index');
    }

    /**
     * @return string|\yii\web\Response
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function actionNew()
    {
        $model = new FormTransaksi();

        $param = Yii::$app->request->queryParams;
        if (!Yii::$app->request->isPost && isset($param['duplicate'])) {
            $duplicate = FormTransaksi::findOne($param['duplicate']);
            if ($duplicate != null) {
                $model->load(['Data' => ArrayHelper::toArray($duplicate)], 'Data');
            }
        }

        $post = Yii::$app->request->post();

        if (Yii::$app->request->isAjax && isset($post['reg_new_row'])) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $model::renderRowAjax($post['reg_new_row']);
        }

        if ($model->load($post) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Data saved, Lab Number = ' . $model->Lab_No);
            if (isset($param['input'])) {
                return $this->redirect(Url::toRoute(['/master/report/new', 'duplicate' => $model->Lab_No, 'input' => 1]));
            }
            return $this->redirect(Url::toRoute(['/master/report/view', 'id' => $model->Lab_No]));
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function actionEdit($id)
    {
        $model = $this->findModel($id);

        $post = Yii::$app->request->post();

        if (Yii::$app->request->isAjax && isset($post['reg_new_row'])) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $model::renderRowAjax($post['reg_new_row']);
        }

        if ($model->load($post) && $model->save()) {
            return $this->redirect(Url::toRoute(['/master/report/view', 'id' => $model->Lab_No]));
        }

        return $this->render('edit', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id, 2);
        $get = Yii::$app->request->get();
        if (isset($get['page'])) {
            $model->page = $get['page'];
        }

        return $this->render('view', [
            'model' => $model,
            'breadcrumbs' => [
                ['label' => 'Master', 'url' => Url::toRoute(['/master/default/index'])],
                ['label' => 'Reports Admin', 'url' => Url::toRoute(['/master/report/index'])],
            ],
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDel($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Orders model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @param null $change
     * @return FormTransaksi|SearchTransaksi the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $change = null)
    {
        if ($change == 2 && ($model = SearchTransaksi::find()->where("Lab_No = '$id'")->one()) !== null) {
            return $model;
        }
        if (($model = FormTransaksi::find()->where("Lab_No = '$id'")->one()) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * @param $id
     * @return array|string
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function actionUpdateStatus($id)
    {
        $st = FormDataUpdateStatus::findOne(['Lab_No' => $id]);

        if (!empty($st) && Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $valid = ActiveForm::validate($st);
            if (sizeof($valid) > 0) {
                return array_merge($valid, [
                    'status' => $st->save() ? 'success' : 'error',
                    'message' => $st->message,
                    'button' => $st->statusPublish,
                    'modelClose' => 'no',
                ]);
            }
            return [
                'status' => $st->changeState() ? 'success' : 'error',
                'message' => $st->message,
                'button' => $st->statusPublish,
            ];
        }

        return '';
    }

    /**
     * @param $id
     * @return array|string
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function actionPublish()
    {
        $st = new FormPublish();
        $request = Yii::$app->request->queryParams;
        $request = array_merge($request, Yii::$app->request->post());
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->isAjax) {
            ini_set('max_execution_time', -1);
            ini_set('memory_limit', -1);
            return [
                'status' => $st->updateStatus($request) ? 'success' : 'error',
                'message' => $st->message,
                'data' => $st->data,
                'modelClose' => 'no',
            ];
        }
        return '';
    }

    /**
     * @param $id
     * @return array|string
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function actionVerifyStatus($id)
    {
        $st = FormDataUpdateStatus::findOne(['Lab_No' => $id]);

        if (!empty($st) && Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $valid = \yii\bootstrap\ActiveForm::validate($st);
            if (sizeof($valid) > 0) {
                return array_merge($valid, [
                    'status' => $st->save() ? 'success' : 'error',
                    'message' => $st->message,
                    'button' => $st->statusVerify,
                    'modelClose' => 'no',
                ]);
            }
            $user = Yii::$app->user->identity;
            if (!$user->validateRules('manager')) {
                $st->addError('verify_date', 'this user does not have permission ');
            }
            if ($st->verify_date == '') {
                $st->verify_date = date('Y-m-d H:i:s');
                $st->verify_by = $user->username;
            } else {
                $st->verify_date = null;
                $st->verify_by = null;
            }
            return [
                'status' => $st->save() ? 'success' : 'error',
                'message' => $st->message,
                'button' => $st->statusVerify,
            ];

        }

        return '';
    }

    public function actionLabNo($type)
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'status' => 'success',
                'message' => $type,
                'lab_no' => FormTransaksi::generateLabNumber($type),
            ];
        }
        return '';
    }

    /**
     * @return array|string
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function actionUpload()
    {
        $form = new FormUploadTransaksi();
        if (Yii::$app->request->isAjax) {
            $fileUpload = UploadedFile::getInstance($form, 'uploadFile');
            Yii::$app->response->format = Response::FORMAT_JSON;
            ini_set('memory_limit', '2048M');
            ini_set('max_execution_time', 3000);
            return [
                'response_type' => $form->upload($fileUpload) ? 'success' : 'error',
                'status_save' => $form->status_save,
                'message' => $form->message,
                'class_message' => $form->class_message,
                'data' => $form->dataMessage,
            ];
        }
        UploadBlueimpAsset::register($this->view);
        return $this->render('upload', ['model' => $form]);
    }

    /**
     * @return array|string
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function actionUpdateEvalCode()
    {
        $model = new UpdateEvalCode();
        $request = Yii::$app->request->queryParams;
        $request = array_merge($request, Yii::$app->request->post());
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->isAjax) {
            ini_set('memory_limit', '2048M');
            ini_set('max_execution_time', 1000);
            return [
                'status' => $model->updateCode($request) ? 'success' : 'warning',
                'message' => $model->message,
                'class_message' => $model->class_message,
                'data' => $model->data,
            ];
        }
        return '';
    }

    /**
     * @return string
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     * @throws \PHPExcel_Writer_Exception
     */
    public function actionDownloadExcel()
    {
        $request = Yii::$app->request->queryParams;
        $model = new FormDownloadExcel();
        if ($model->load(Yii::$app->request->post())) {
            $model->for_customer = 0;
            header('Set-Cookie: fileDownload=true; path=/');
            header('Cache-Control: max-age=60, must-revalidate');
            return $model->generateExcel($request);
        }
        return $this->renderAjax('export_excel', [
            'model' => $model,
        ]);
    }

    public function actionDownloadCustom()
    {
        $request = Yii::$app->request->queryParams;
        $model = new FormDownloadExcel();
        $model->type = 'txt';
        $file = 'assets/' . $model->generateExcel($request);
        return Yii::$app->response->sendFile($file);
    }

    public function actionOptionParam($lab_no, $parameter)
    {
        if (Yii::$app->request->isAjax) {
            $model = OptionsParam::findOne(['Lab_No' => $lab_no, 'col_name' => $parameter]);
            return $this->renderAjax('_option_param', ['model' => $model]);
        }
        return '';
    }
}