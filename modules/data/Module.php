<?php

namespace app\modules\data;

use Yii;
use app\models\AccessControl;
use yii\base\ActionEvent;
use yii\base\Application;
use yii\base\BootstrapInterface;
use yii\base\Event;
use yii\web\Controller;

/**
 * data module definition class
 */
class Module extends \yii\base\Module implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\data\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Bootstrap method to be called during application bootstrap stage.
     * @param \yii\base\Application $app the application currently running
     */
    public function bootstrap($app)
    {
        if ($app instanceof Application) {
            $app->getUrlManager()->addRules([
                ['class' => 'yii\web\UrlRule', 'pattern' => $this->id, 'route' => $this->id . '/default/index'],
                ['class' => 'yii\web\UrlRule', 'pattern' => $this->id . '/<action:(new|upload)>', 'route' => $this->id . '/default/<action>'],
                ['class' => 'yii\web\UrlRule', 'pattern' => $this->id . '/<id:\d+>/<action:(edit|del|print)>', 'route' => $this->id . '/default/<action>'],
                ['class' => 'yii\web\UrlRule', 'pattern' => $this->id . '/<id:\d+>', 'route' => $this->id . '/default/view'],
                ['class' => 'yii\web\UrlRule', 'pattern' => $this->id . '/<controller:[\w\-]+>/<action:[\w\-]+>', 'route' => $this->id . '/<controller>/<action>'],
            ], false);
        }

        Event::on(Controller::className(), Controller::EVENT_AFTER_ACTION, function (ActionEvent $event) {
            if (!Yii::$app->user->isGuest) {
                $user = Yii::$app->user->identity;

            }
        });

    }
}
