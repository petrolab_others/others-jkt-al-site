<?php
/**
 * Created by
 * User: Wisard17
 * Date: 06/04/2019
 * Time: 01.36 PM
 */

namespace app\modules\data\controllers;


use app\components\ArrayHelper;
use app\modules\data\models\FormOilMatrix;
use Yii;
use app\models\AccessControl;
use app\modules\data\models\SearchOilMatrix;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class OilMatrixController
 * @package app\modules\data\controllers
 */
class OilMatrixController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['detail', 'index', 'new', 'edit', 'view', 'del'],
                'rules' => [
                    [
                        'actions' => ['detail', 'index', 'new', 'edit', 'view', 'del'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'del' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new SearchOilMatrix();
        $request = Yii::$app->request->queryParams;

        if (Yii::$app->request->isAjax) {
            return $searchModel->searchDataTable($request);
        }

        return $this->render('index');
    }

    /**
     * @return array|string|Response
     * @throws \Exception
     */
    public function actionNew()
    {
        $model = new FormOilMatrix();

        $post = Yii::$app->request->post();

        if (Yii::$app->request->isAjax && isset($post['reg_new_row'])) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $model::renderRowAjax($post['reg_new_row']);
        }

        if ($model->load($post) && $model->save()) {
            return $this->redirect(Url::toRoute(['/data/reference/index', 'id' => $model->id]));
        }

        $param = Yii::$app->request->queryParams;
        if (!Yii::$app->request->isPost && isset($param['duplicate'])) {
            $duplicate = FormOilMatrix::findOne($param['duplicate']);
            if ($duplicate != null) {
                $arr = [];
                foreach ($duplicate->parameterHasOilMatrix as $i => $item) {
                    $arr['newrow_' . $i] = ArrayHelper::toArray($item);
                    unset($arr['newrow_' . $i]['OIL_MATRIX']);
                }
                $model->load(['Data' => ArrayHelper::toArray($duplicate), 'ParameterHasOilMatrix' => $arr], 'Data');
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return array|string|Response
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public function actionEdit($id)
    {
        $model = $this->findModel($id);

        $post = Yii::$app->request->post();
        if (Yii::$app->request->isAjax && isset($post['reg_new_row'])) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $model::renderRowAjax($post['reg_new_row']);
        }
        if ($model->load($post) && $model->save()) {
            return $this->redirect(Url::toRoute(['/data/oil-matrix/index', 'id' => $model->id]));
        }

        return $this->render('edit', [
            'model' => $model,
        ]);
    }


    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     */
    public function actionDel($id)
    {
        $model = $this->findModel($id);
        $model->delete();
//        $model->active = 0;
//        $model->save();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Orders model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FormOilMatrix the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FormOilMatrix::find()->where("OIL_MATRIX = '$id'")->one()) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}