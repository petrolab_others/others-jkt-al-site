<?php
/**
 * Created by
 * User: Wisard17
 * Date: 25/02/2019
 * Time: 10.20 AM
 */

namespace app\modules\data\controllers;


use app\components\ArrayHelper;
use app\modules\data\models\FormParameter;
use app\modules\data\models\SearchParameter;
use conquer\select2\Select2Action;
use Yii;
use app\models\AccessControl;
use yii\db\ActiveQuery;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class ParameterController
 * @package app\modules\data\controllers
 */
class ParameterController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['detail', 'index', 'new', 'edit', 'view', 'del'],
                'rules' => [
                    [
                        'actions' => ['detail', 'index', 'new', 'edit', 'view', 'del'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'del' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'list-parameters' => [
                'class' => Select2Action::className(),
                'dataCallback' => [$this, 'dataCallback'],
            ],
        ];
    }

    /**
     * @param $q
     * @return array
     */
    public function dataCallback($q)
    {
        $query = new ActiveQuery(SearchParameter::className());
        $get = Yii::$app->request->get('param');
        $out = [];
        $add = '';
        if (isset($get['parent_only']) && $get['parent_only'] == 1) {
            $add .= ' and parameter.parent < 1 or parameter.parent is null';
        }
        if (isset($get['data'])) {
            parse_str($get['data'], $d);
            if (isset($d['list_parameter'])) {
                $query->andWhere(['not in', 'id', $d['list_parameter']]);
            }
        }
        if (isset($get['matrix_type'])) {
            $query->andWhere(['matrix_type' => $get['matrix_type']]);
        }
        if ($q != '') {
            $query->andWhere(['like', 'name', $q]);
        }

        $query->andWhere('parameter.active = 1' . $add)->limit(20)->distinct();

        foreach ($query->all() as $parameter) {
            /** @var SearchParameter $parameter */
            $out[] = [
                'id' => $parameter->id,
                'text' => $parameter->parentModel == null ? $parameter->name :
                    $parameter->parentModel->name . '-' . $parameter->name ,
            ];
        }

        return [
            'results' => $out,
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new SearchParameter();
        $request = Yii::$app->request->queryParams;

        if (Yii::$app->request->isAjax) {
            return $searchModel->searchDataTable($request);
        }

        return $this->render('index');
    }

    /**
     * @return array|string|Response
     * @throws \yii\db\Exception
     */
    public function actionNew()
    {
        $model = new FormParameter();

        $post = Yii::$app->request->post();

        if (Yii::$app->request->isAjax && isset($post['reg_new_row'])) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $model::renderRowAjax($post['reg_new_row']);
        }

        if ($model->load($post) && $model->save()) {
            return $this->redirect(Url::toRoute(['/data/parameter/index', 'id' => $model->id]));
        }
        $param = Yii::$app->request->queryParams;
        if (!Yii::$app->request->isPost && isset($param['duplicate'])) {
            $duplicate = FormParameter::findOne($param['duplicate']);
            if ($duplicate != null) {
                $model->load(['Data' => ArrayHelper::toArray($duplicate)], 'Data');
                $model->_childList = $duplicate->childList;
            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return array|string|Response
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     */
    public function actionEdit($id)
    {
        $model = $this->findModel($id);

        $post = Yii::$app->request->post();
        if (Yii::$app->request->isAjax && isset($post['reg_new_row'])) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $model::renderRowAjax($post['reg_new_row']);
        }
        if ($model->load($post) && $model->validate()) {
            $model->save(false);
            return $this->redirect(Url::toRoute(['/data/parameter/index', 'id' => $model->id]));
        }

        return $this->render('edit', [
            'model' => $model,
        ]);
    }


    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     */
    public function actionDel($id)
    {
        $model = $this->findModel($id);
        $model->active = 0;
        $model->save();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Orders model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FormParameter the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FormParameter::find()->where("id = '$id'")->one()) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}