<?php
/**
 * Created by
 * User     : Wisard17
 * Date     : 20/06/2019
 * Time     : 04.53 PM
 * File Name: CategoryTypeController.php
 **/


namespace app\modules\data\controllers;


use app\modules\data\models\TypeView;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class CategoryTypeController
 * @package app\modules\data\controllers
 */
class CategoryTypeController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new TypeView();
        $request = Yii::$app->request->queryParams;

        if (Yii::$app->request->isAjax) {
            return $searchModel->searchDataTable($request);
        }

        return $this->render('index');
    }

    /**
     * @return array|string|Response
     * @throws \Exception
     */
    public function actionNew()
    {
        $model = new TypeView();

        $post = Yii::$app->request->post();

        if ($model->load($post) && $model->save()) {
            return $this->redirect(Url::toRoute(['/data/category-type/index', 'id' => $model->id]));
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return array|string|Response
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public function actionEdit($id)
    {
        $model = $this->findModel($id);

        $post = Yii::$app->request->post();

        if ($model->load($post) && $model->save()) {
            return $this->redirect(Url::toRoute(['/data/category-type/index', 'id' => $model->id]));
        }

        return $this->render('edit', [
            'model' => $model,
        ]);
    }


    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionDel($id)
    {
        $model = $this->findModel($id);
//        $model->delete();
//        $model->active = 0;
//        $model->save();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Orders model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TypeView the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TypeView::find()->where("id = '$id'")->one()) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}