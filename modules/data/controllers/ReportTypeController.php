<?php
/**
 * Created by
 * User: Wisard17
 * Date: 01/04/2019
 * Time: 10.06 AM
 */

namespace app\modules\data\controllers;


use app\components\ArrayHelper;
use app\modules\data\models\FormTypeReport;
use app\modules\data\models\SearchTypeReport;
use Yii;
use app\models\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class ReportTypeController
 * @package app\modules\data\controllers
 */
class ReportTypeController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['detail', 'index', 'new', 'edit', 'view', 'del'],
                'rules' => [
                    [
                        'actions' => ['detail', 'index', 'new', 'edit', 'view', 'del'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'del' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new SearchTypeReport();
        $request = Yii::$app->request->queryParams;

        if (Yii::$app->request->isAjax) {
            return $searchModel->searchDataTable($request);
        }

        return $this->render('index');
    }

    /**
     * @return string|Response
     * @throws \Throwable
     */
    public function actionNew()
    {
        $model = new FormTypeReport();

        $post = Yii::$app->request->post();

        if (Yii::$app->request->isAjax && isset($post['new_param'])) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return $model::renderRowAjax($post['new_param']);
        }

        if ($model->load($post) && $model->save()) {
            return $this->redirect(Url::toRoute(['/data/report-type/index', 'id' => $model->id]));
        }
        $param = Yii::$app->request->queryParams;
        if (!Yii::$app->request->isPost && isset($param['duplicate'])) {
            $duplicate = FormTypeReport::findOne($param['duplicate']);
            if ($duplicate != null) {
                $model->load(['Data' => ArrayHelper::toArray($duplicate)], 'Data');
                $model->_parameter_list = $duplicate->parametersList;
            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return string|Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     */
    public function actionEdit($id)
    {
        $model = $this->findModel($id);

        $post = Yii::$app->request->post();
        if (Yii::$app->request->isAjax && isset($post['new_param'])) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $model::renderRowAjax($post['new_param']);
        }
        if ($model->load($post) && $model->save()) {
            return $this->redirect(Url::toRoute(['/data/report-type/index', 'id' => $model->id]));
        }

        return $this->render('edit', [
            'model' => $model,
        ]);
    }


    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     */
    public function actionDel($id)
    {
        $model = $this->findModel($id);
        $model->name .= ' del -' . date('Y-m-d H:i:s');
        $model->active = 0;
        $model->save();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Orders model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FormTypeReport the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FormTypeReport::find()->where("id = '$id'")->one()) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}