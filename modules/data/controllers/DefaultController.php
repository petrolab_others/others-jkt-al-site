<?php

namespace app\modules\data\controllers;


use app\assets\UploadBlueimpAsset;
use app\components\ArrayHelper;
use app\modules\data\models\FormDownloadExcel;
use app\modules\data\models\FormUpdatePublish;
use app\modules\data\models\FormUploadData;
use app\modules\data\models\RenderPDF;
use app\modules\data\models\SearchData;
use app\modules\data\models\UpdateEvalCode;
use Yii;
use app\models\AccessControl;
use app\modules\data\models\FormData;
use yii\bootstrap\ActiveForm;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * Default controller for the `data` module
 */
class DefaultController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['detail', 'index', 'new', 'edit', 'view', 'del'],
                'rules' => [
                    [
                        'actions' => ['detail', 'index', 'new', 'edit', 'view', 'del'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'del' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new SearchData();
        $request = Yii::$app->request->queryParams;

        if (Yii::$app->request->isAjax) {
            return $searchModel->searchDataTable($request);
        }

        return $this->render('index');

    }

    /**
     * @return string|Response
     * @throws \Exception
     */
    public function actionNew()
    {
        $model = new FormData();

        $post = Yii::$app->request->post();

        if ($model->load($post) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Data saved, Lab Number = ' . $model->lab_number);
            if (isset($param['input'])) {
                return $this->redirect(Url::toRoute(['/data/default/new', 'duplicate' => $model->id, 'input' => 1]));
            }
            return $this->redirect(Url::toRoute(['/data/default/view', 'id' => $model->id]));
        }

        $param = Yii::$app->request->queryParams;
        if (!Yii::$app->request->isPost && isset($param['duplicate'])) {
            $duplicate = FormData::findOne($param['duplicate']);
            if ($duplicate != null) {
                $model->load(['Data' => ArrayHelper::toArray($duplicate)], 'Data');
            }
        }

//        $model->_wiz = true;
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return array|string
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function actionUpdateStatus($id)
    {
        $st = FormUpdatePublish::findOne(['lab_number' => $id]);

        if (!empty($st) && Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $valid = ActiveForm::validate($st);
            if (sizeof($valid) > 0) {
                return array_merge($valid, [
                    'status' => $st->save() ? 'success' : 'error',
                    'message' => $st->message,
                    'button' => $st->statusPublish,
                    'modelClose' => 'no',
                ]);
            }
            return [
                'status' => $st->changeState() ? 'success' : 'error',
                'message' => $st->message,
                'button' => $st->statusPublish,
            ];
        }

        return '';
    }

    /**
     * @param $id
     * @return array|string
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function actionVerifyStatus($id)
    {
        $st = FormUpdatePublish::findOne(['lab_number' => $id]);

        if (!empty($st) && Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $valid = ActiveForm::validate($st);
            if (sizeof($valid) > 0) {
                return array_merge($valid, [
                    'status' => $st->save() ? 'success' : 'error',
                    'message' => $st->message,
                    'button' => $st->statusVerify,
                    'modelClose' => 'no',
                ]);
            }
            $user = Yii::$app->user->identity;
            if (!$user->validateRules('manager')) {
                $st->addError('verify_date', 'this user does not have permission ');
            }
            if ($st->verify_date == '') {
                $st->verify_date = date('Y-m-d H:i:s');
                $st->verify_by = $user->username;
            } else {
                $st->verify_date = null;
                $st->verify_by = null;
            }
            return [
                'status' => $st->save() ? 'success' : 'error',
                'message' => $st->message,
                'button' => $st->statusVerify,
            ];

        }

        return '';
    }

    public function actionUpdateEvalCode()
    {
        $model = new UpdateEvalCode();
        $request = Yii::$app->request->queryParams;
        $request = array_merge($request, Yii::$app->request->post());
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->isAjax) {
            ini_set('memory_limit', '2048M');
            ini_set('max_execution_time', 1000);
            return [
                'status' => $model->updateCode($request) ? 'success' : 'warning',
                'message' => $model->message,
                'class_message' => $model->class_message,
                'data' => $model->data,
            ];
        }
        return '';
    }

    /**
     * @param $type
     * @param int $id
     * @return array|string
     */
    public function actionRenderParameter($type, $id = 0)
    {
        $model = FormData::findOne($id);
        if ($model == null) {
            $model = new FormData();
        }
        $model->type_report_id = $type;

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $form = ActiveForm::begin([
                'layout' => 'horizontal',
                'fieldConfig' => [
                    'template' => "{label}\n<div class=\"col-lg-8\">{input}\n{error}</div>",
                    'labelOptions' => ['class' => 'col-lg-4 control-label'],
                ]]);
            return [
                'page' => $this->renderAjax('_parameter', [
                    'model' => $model,
                    'form' => $form,
                ]),
                'mpc' => $model->mpcShow ? [
                    'header' => '<li><a id="mpc_h" href="#mpc_p" data-toggle="tab">MPC</a></li>',
                    'body' =>  '<div id="mpc_p" class="tab-pane">' . $this->renderAjax('_mpc', [
                        'model' => $model,
                        'form' => $form,
                    ]) . '</div>',
                ] : '',
                'bst' => $model->bstShow ? [
                    'header' => '<li><a id="bst_h" href="#bst_p" data-toggle="tab">Blotter Spot Test</a></li>',
                    'body' =>  '<div id="bst_p" class="tab-pane">' . $this->renderAjax('_bst', [
                            'model' => $model,
                            'form' => $form,
                        ]) . '</div>',
                ] : '',
                'ffp' => $model->ffpShow ? [
                    'header' => '<li><a id="ffp_h" href="#ffp_p" data-toggle="tab">Ftir Finger Print</a></li>',
                    'body' =>  '<div id="ffp_p" class="tab-pane">' . $this->renderAjax('_ffp', [
                            'model' => $model,
                            'form' => $form,
                        ]) . '</div>',
                ] : '',
                'fcs' => $model->fcsShow ? [
                    'header' => '<li><a id="fcs_h" href="#fcs_p" data-toggle="tab">Foaming Characteristic</a></li>',
                    'body' =>  '<div id="fcs_p" class="tab-pane">' . $this->renderAjax('_fcs', [
                            'model' => $model,
                            'form' => $form,
                        ]) . '</div>',
                ] : '',
                'ptest' => $model->ptestShow ? [
                    'header' => '<li><a id="ptest_h" href="#ptest_p" data-toggle="tab">Patch Show</a></li>',
                    'body' =>  '<div id="ptest_p" class="tab-pane">' . $this->renderAjax('_ptest', [
                            'model' => $model,
                            'form' => $form,
                        ]) . '</div>',
                ] : '',
            ];
        }
        return '';
    }

    /**
     * @param $id
     * @return array|string|Response
     * @throws NotFoundHttpException
     */
    public function actionEdit($id)
    {
        $model = $this->findModel($id);

        $post = Yii::$app->request->post();

        if ($model->load($post) && $model->save()) {
            return $this->redirect(Url::toRoute(['/data/default/view', 'id' => $model->id]));
        }

        return $this->render('edit', [
            'model' => $model,
        ]);
    }


    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $report = RenderPDF::findOne(['id' => $id]);
        if ($report == null) {
            throw new NotFoundHttpException();
        }

        $report->draft = isset($get['draft']) ? true : false;
        $report->mpc_only = isset($get['mpc_only']) ? true : false;
        $report->chartShow = false;
        $report->mpcShow = false;
        $report->viewMode = 'html';

        return $this->render('view', [
            'model' => $report,
        ]);
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDel($id)
    {
        $model = $this->findModel($id);
//        $model->active = 0;
        $model->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Orders model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FormData the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FormData::find()->where("id = '$id'")->one()) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * @param $type
     * @return array|string
     * @throws \yii\db\Exception
     */
    public function actionLabNo($type)
    {
        $post = Yii::$app->request->post();
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'status' => 'success',
                'message' => $type,
                'lab_no' => FormData::generateLabNumber($post['FormData']['type_report_id']),
            ];
        }
        return '';
    }

    /**
     * @return array|string
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function actionUpload()
    {
        $form = new FormUploadData();
        if (Yii::$app->request->isAjax) {
            $fileUpload = UploadedFile::getInstance($form, 'uploadFile');
            Yii::$app->response->format = Response::FORMAT_JSON;
            ini_set('memory_limit', '2048M');
            ini_set('max_execution_time', 3000);
            return [
                'response_type' => $form->upload($fileUpload) ? 'success' : 'error',
                'status_save' => $form->status_save,
                'message' => $form->message,
                'class_message' => $form->class_message,
                'data' => $form->dataMessage,
            ];
        }
        UploadBlueimpAsset::register($this->view);
        return $this->render('upload', ['model' => $form]);
    }

    /**
     * @param int $header_only
     * @return string
     * @throws \PHPExcel_Exception
     */
    public function actionDownloadExcel($header_only = 0)
    {
        $request = Yii::$app->request->queryParams;
        $model = new FormDownloadExcel();
        if ($header_only == 1) {
            header('Set-Cookie: fileDownload=true; path=/');
            header('Cache-Control: max-age=60, must-revalidate');
            return $model->generateExcel($request, 'template', $header_only == 1);
        }
        if ($model->load(Yii::$app->request->post())) {
            $model->for_customer = 0;
            header('Set-Cookie: fileDownload=true; path=/');
            header('Cache-Control: max-age=60, must-revalidate');
            return $model->generateExcel($request);
        }
        if (isset($request['data-export']) && $request['data-export'] == 1) {
            $model->code = true;
            $model->for_customer = 0;
            $model->property = ['lab_number',];
            header('Set-Cookie: fileDownload=true; path=/');
            header('Cache-Control: max-age=60, must-revalidate');
            return $model->generateExcel($request, 'upload_data_analisa_' . date('Ymd'));
        }
        return $this->renderAjax('export_excel', [
            'model' => $model,
        ]);
    }

    /**
     * @return array
     */
    public function actionConversionDataToRowData()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        ini_set('memory_limit', -1);
        ini_set('max_execution_time', -1);
        $request = Yii::$app->request->queryParams;
        $start = isset($request['start']) ? $request['start'] : null;
        $end = isset($request['end']) ? $request['end'] : null;
//        $p = new FormData();
//        print_r($p->attributes());die;
        return SearchData::conversionData($start, $end);
    }

    public function actionUpdateDataPama()
    {
        $model = new FormUpdatePublish();
        $request = Yii::$app->request->queryParams;
        $request = array_merge($request, Yii::$app->request->post());
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->isAjax) {
            ini_set('memory_limit', '2048M');
            ini_set('max_execution_time', 1000);
            return [
                'status' => $model->updateStatus($request) ? 'success' : 'warning',
                'message' => $model->message,
//                'class_message' => $model->class_message,
//                'data' => $model->data,
            ];
        }
        return '';
    }
}
