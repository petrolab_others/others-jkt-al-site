<?php
/**
 * Created by
 * User: Wisard17
 * Date: 21/03/2019
 * Time: 10.15 AM
 */

namespace app\modules\data\controllers;


use app\modules\data\models\RenderExcel;
use app\modules\data\models\RenderPDF;
use kartik\mpdf\Pdf;
use Yii;
use app\models\AccessControl;
use app\modules\data\models\SearchData;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class ReportController
 * @package app\modules\data\controllers
 */
class ReportController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['detail', 'index', 'new', 'edit', 'view', 'del'],
                'rules' => [
                    [
                        'actions' => ['detail', 'index', 'new', 'edit', 'view', 'del'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'del' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new SearchData();
        $request = Yii::$app->request->queryParams;

        if (Yii::$app->request->isAjax) {
            return $searchModel->searchDataTable($request);
        }

        return $this->render('index');
    }

    /**
     * @param $lab_number
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionPdf($lab_number)
    {
        Yii::$app->response->format = Response::FORMAT_RAW;

        $get = Yii::$app->request->get();

        $report = RenderPDF::findOne(['lab_number' => $lab_number]);
        if ($report == null) {
            throw new NotFoundHttpException();
        }

        $report->draft = isset($get['draft']) ? true : false;
        $report->mpc_only = isset($get['mpc_only']) ? true : false;

        if (isset($get['his_count'])) {
            $report->limit = (int)$get['his_count'];
        }

        $report->show_all_param = true;//isset($get['show_all_param']) ? true : false;
        $content = '';

        $content .= $this->renderPartial('render_pdf', [
            'model' => $report,
        ]);

        $filename = $report->customerName . $lab_number;
        $filename = str_replace(array('/', '\\', ':', '?'), '_', $filename);

        $pdf = new Pdf([
            'filename' => "$filename.pdf",
            // set to use core fonts only
            'mode' => Pdf::MODE_CORE,
            // A4 paper format
            'format' => 'A4',
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            'marginBottom' => 10,
            // your html content input
            'content' => $content == '' ? 'No Page' : $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => [
                '@vendor/bower-asset/bootstrap/dist/css/bootstrap.min.css',
                '@app/web/css/pdf.css',
                '@app/assets/css/pdf.css',
            ],
            // any css to be embedded if required
            'cssInline' => '.kv-heading-1{font-size:18px;}.logo1{position:fixed}',
            // set mPDF properties on the fly
            'options' => [
//                'title' => 'Krajee Report Title',
                'watermarkAngle' => 45,
                'showWatermarkText' => true,
            ],
            // call mPDF methods on the fly
            'methods' => [
                'SetTitle' => 'Report PDF',
                'SetAuthor' => 'PT. Petrolab Services',
                'SetWatermarkText' => $report->draft ? 'DRAFT' : '',
                //'SetHeader'=>['Krajee Report Header'],
                //'SetFooter'=>['{PAGENO}'],
                'SetHTMLFooter' => [$this->renderPartial('footer', ['model' => $report])],
            ]
        ]);
        $pdf->marginTop = 22;
        $pdf->marginRight = 10;
        $pdf->marginLeft = 10;
        $pdf->marginBottom = 17;
        $pdf->marginHeader = 5;
        $pdf->marginFooter = 5;

        // return the pdf output as per the destination setting
        return $pdf->render();
    }

    /**
     * @param $lab_number
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionMultiPagePdf($lab_number)
    {
        Yii::$app->response->format = Response::FORMAT_RAW;

        $get = Yii::$app->request->get();

        $report = RenderPDF::findOne(['lab_number' => $lab_number]);
        if ($report == null) {
            throw new NotFoundHttpException();
        }

        $report->draft = isset($get['draft']) ? true : false;
        $report->mpc_only = isset($get['mpc_only']) ? true : false;

        if (isset($get['his_count'])) {
            $report->limit = (int)$get['his_count'];
        }

        $report->show_all_param = true;//isset($get['show_all_param']) ? true : false;
        $content = '';

        $content .= $this->renderPartial('render_pdf_multi_page', [
            'model' => $report,
        ]);

        $filename = $report->customerName . $lab_number;
        $filename = str_replace(array('/', '\\', ':', '?'), '_', $filename);

        $pdf = new Pdf([
            'filename' => "$filename.pdf",
            // set to use core fonts only
            'mode' => Pdf::MODE_CORE,
            // A4 paper format
            'format' => 'A4',
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            'marginBottom' => 10,
            // your html content input
            'content' => $content == '' ? 'No Page' : $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => [
                '@vendor/bower-asset/bootstrap/dist/css/bootstrap.min.css',
                '@app/web/css/pdf.css',
                '@app/assets/css/pdf.css',
            ],
            // any css to be embedded if required
            'cssInline' => '.kv-heading-1{font-size:18px;}.logo1{position:fixed}',
            // set mPDF properties on the fly
            'options' => [
//                'title' => 'Krajee Report Title',
                'watermarkAngle' => 45,
                'showWatermarkText' => true,
            ],
            // call mPDF methods on the fly
            'methods' => [
                'SetTitle' => 'Report PDF',
                'SetAuthor' => 'PT. Petrolab Services',
                'SetWatermarkText' => $report->draft ? 'DRAFT' : '',
                //'SetHeader'=>['Krajee Report Header'],
                //'SetFooter'=>['{PAGENO}'],
                'SetHTMLFooter' => [$this->renderPartial('footer', ['model' => $report])],
            ]
        ]);
        $pdf->marginTop = 22;
        $pdf->marginRight = 10;
        $pdf->marginLeft = 10;
        $pdf->marginBottom = 17;
        $pdf->marginHeader = 5;
        $pdf->marginFooter = 5;

        // return the pdf output as per the destination setting
        return $pdf->render();
    }

    /**
     * @param $lab_number
     * @return string
     * @throws NotFoundHttpException
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     * @throws \PHPExcel_Writer_Exception
     */
    public function actionExcel($lab_number)
    {
        $model = RenderExcel::findOne(['lab_number' => $lab_number]);
        if ($model == null) {
            throw new NotFoundHttpException();
        }
        header('Set-Cookie: fileDownload=true; path=/');
        header('Cache-Control: max-age=60, must-revalidate');
        return $model->generateExcel();
    }
}