<?php

/**
 * Created by
 * User: Wisard17
 * Date: 01/04/2019
 * Time: 10.35 AM
 */

/* @var $this \yii\web\View */

/* @var $model \app\modules\data\models\FormTypeReport */


use conquer\select2\Select2Asset;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$readOnly = $model->isNewRecord ? '' : 'readonly';
$classAlertError = sizeof($model->errors) > 0 ? 'alert alert-warning' : '';
?>
    <div class="transaksi_form">

        <?php $form = ActiveForm::begin([
            'id' => 'form-report-type',
            'layout' => 'horizontal',
            'fieldConfig' => [
                'template' => "{label}\n<div class=\"col-lg-8\">{input}\n{error}</div>",
                'labelOptions' => ['class' => 'col-lg-4 control-label'],
            ],
        ]); ?>

        <div class="<?= $classAlertError ?>"><?= $form->errorSummary($model); ?></div>

        <div class="row">
            <div class="col-lg-6">
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'header_name')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'lab_no_temp')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'rk_no')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'draft_rk_no')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-lg-6">
                <?= $form->field($model, 'history')->checkbox() ?>
                <?= $form->field($model, 'reference')->checkbox() ?>
                <?= $form->field($model, 'chart')->checkbox() ?>
                <?= $form->field($model, 'mpc')->checkbox() ?>
                <?= $form->field($model, 'blotter_spot_test')->checkbox() ?>
                <?= $form->field($model, 'ffp')->checkbox() ?>
                <?= $form->field($model, 'ptest')->checkbox() ?>
                <?= $form->field($model, 'limit_type')->dropDownList([1 => 'Matrix', 2 => 'Typical']) ?>
                <?= $form->field($model, 'type_view_id')->dropDownList($model->listView(), ['prompt' => 'Select']) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <?= $form->field($model, 'deskription', [
                    'template' => "{label}\n<div class=\"col-lg-10\">{input}\n{error}</div>",
                    'labelOptions' => ['class' => 'col-lg-2 control-label'],
                ])->textarea(['rows' => 4]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    <label for="parameter" class="col-lg-2 control-label">Parameters</label>
                    <div class="col-lg-10">
                        <ul class="list-parameter" id="list-parameter">
                            <?= $model->renderDetail($form) ?>
                        </ul>
                        <div>
                            <?= $form->field($model, 'list_parameter', ['template' => '
                                        <div class="col-lg-8"><div class=" input-group">{input} <div class="input-group-btn">
                                        <div class="btn btn-primary btn-block" type="button" data-action="add_param" >
                                        <i class="fa fa-plus-circle"></i></div>
                                        </div></div>{error}
                                        </div>',
                                'options' => ['class' => 'form-group has-feedback']
                            ])->dropDownList([], ['class' => 'form-control select2ajax', 'style' => '100%',
                                'prompt' => '- Select Parameter -', 'data-url' => Url::toRoute(['/data/parameter/list-parameters']),
                            ]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="footer">
            <div class="col-lg-2"></div>
            <div class="">
                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
    <style>
        .list-parameter li {
            margin: 2px;
            width: 150px;
        }

        .list-parameter li p {
            white-space: normal;
        }
    </style>

<?php
Select2Asset::register($this);
$payerHtmlId = '';//Html::getInputId($model, 'payer_id');
$modelForm = $model->formName();
$jsCode = <<< JS
$(function () {
    var form = $('#form-report-type');
    function alert_notif(res) {
        $.sound_on = false;
        var color = res.status === 'success' ? '#659265' : '#C46A69';
        var icon = res.status === 'success' ? 'fa-check' : 'fa-times';
        $.smallBox({
            title: res.status,
            content: res.message,
            color: color,
            iconSmall: "fa " + icon + " fa-2x fadeInRight animated",
            timeout: 4000
        });
    }
    let par = function () {
        return {
            ajax: {
                url: function (a) {
                    return this.attr('data-url');
                },
                data: function (params) {
                    let el = this;
                    return {
                        q: params.term,
                        param: {
                            parent_only: 1,
                            data: JSON.stringify(form.serialize()),
                        }
                    };
                },
                processResults: function (data) {
                    return {
                        results: data.results
                    };
                }
            }
        }
    };
    
    form.find('.list-parameter').sortable();
    form.find('.list-parameter').disableSelection();

    form.find('.select2ajax').select2(par());
    
    form.delegate('[data-action=del_param]', 'click', function () {
        let elm = $(this);
        let li = elm.closest('li');
        let ul = elm.closest('ul');
        let p = li.attr('data-id');
        ul.after('<input type="hidden" name="$modelForm\[parameter_del_list][]" value="' + p + '" >');
        li.remove();
    });

    form.delegate('[data-action=add_param]', 'click', function () {
        let elm = $(this);
        let form2 = elm.closest('form');
        $.ajax({
            method: 'post',
            data: {new_param: form2.find('#formtypereport-list_parameter').val()},
            success: function(r) {
                form2.find('ul').append(r);
                form2.find('#formtypereport-list_parameter').val(null).trigger('change');
            }
        });
    });
});
JS;

$this->registerJs($jsCode, 4, 'form-report-type');
