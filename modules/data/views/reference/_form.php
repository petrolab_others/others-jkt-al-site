<?php

/**
 * Created by
 * User: Wisard17
 * Date: 01/04/2019
 * Time: 10.35 AM
 */

/* @var $this \yii\web\View */
/* @var $model \app\modules\data\models\FormTypeReport */


use app\modules\data\models\ParameterHasReference;
use conquer\select2\Select2Asset;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Json;

$readOnly = $model->isNewRecord ? '' : 'readonly';
$classAlertError = sizeof($model->errors) > 0 ? 'alert alert-warning' : '';
?>
    <div class="transaksi_form">

        <?php $form = ActiveForm::begin([
            'id' => 'form-reference',
            'layout' => 'horizontal',
            'fieldConfig' => [
                'template' => "{label}\n<div class=\"col-lg-8\">{input}\n{error}</div>",
                'labelOptions' => ['class' => 'col-lg-4 control-label'],
            ],
        ]); ?>

        <div class="<?= $classAlertError ?>"><?= $form->errorSummary($model); ?></div>

        <div class="row">
            <div class="col-lg-6">
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            </div>
            <div class="col-lg-6">
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                <label for="parameter" class="col-lg-2 control-label">Parameters</label>
                    <div class="col-lg-10">
                        <?= $model->renderDetail($form) ?>
                        <a data-action="add_row_table" class="btn btn-primary btn-xs btn-block">
                            <i class="fa fa-plus-circle"></i> Add Item
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="footer">
            <div class="col-lg-2"></div>
            <div class="">
                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
    <style>
        .list-parameter li {
            margin: 2px;
            width: 150px;
        }
        .list-parameter li p {
            white-space: normal;
        }
    </style>

<?php
$payerHtmlId = '';//Html::getInputId($model, 'payer_id');
$modelForm = (new ParameterHasReference())->formName();
Select2Asset::register($this);
$cls = Json::encode($model::className());
$jsCode = <<< JS
$(function () {
    var form = $('#form-reference');
    let par = function () {
        return {
            ajax: {
                url: function (a) {
                    return this.attr('data-url');
                },
                data: function (params) {
                    let el = this;
                    return {
                        q: params.term,
                        param: {
                            col: el.attr('data-depend'),
                            fromcol: el.attr('data-depend-from'),
                            fromId: el.attr('data-base-id'),
                            filter: form.find('#' + el.attr('data-depend-id')).val(),
                            model: $cls,
                        }
                    };
                },
                processResults: function (data) {
                    return {
                        results: data.results
                    };
                }
            }
        }
    };
    function alert_notif(res) {
        $.sound_on = false;
        var color = res.status === 'success' ? '#659265' : '#C46A69';
        var icon = res.status === 'success' ? 'fa-check' : 'fa-times';
        $.smallBox({
            title: res.status,
            content: res.message,
            color: color,
            iconSmall: "fa " + icon + " fa-2x fadeInRight animated",
            timeout: 4000
        });
    }
    
    form.delegate('[data-action=delate_row]', 'click', function () {
        var elm = $(this);
        var row = elm.closest('tr');
        p = '$modelForm';
        p = p.replace('[new]', '[' + row.attr('data-id') + ']');

        row.html('<input type="hidden" name="' + p + '[actionId]" value="del" >');

    });
    

    form.delegate('[data-action=add_row_table]', 'click', function () {
        var elm = $(this);
        var tbl = elm.closest('.row').find('table');
        var form = elm.closest('form');
        $.ajax({
            method: 'post',
            data: {'reg_new_row': (tbl.find('tr').length - 1)},
            error: function (response) {
                alert_notif({status: "error", message: 'tidak bisa di tampilkan'});
            },
            success: function (response) {
                var t = $(response.data);
                t.find('.select2ajax').select2(par());
                console.log(t);
                tbl.find('tbody').append(t);
                // $(form).yiiActiveForm(response.jsAdd, []);
                $.each(response.jsAdd, function (i, v) {
                    var vali = eval("var f = function(){ return " + v.validate.expression + ";}; f() ;");
                    v.validate = vali;
                    $(form).yiiActiveForm('add', v);
                });
            }
        });

    });
    
    form.find('.select2ajax').select2(par());
});
JS;

$this->registerJs($jsCode, 4, 'form-report-type');
