<?php

/**
 * Created by
 * User: Wisard17
 * Date: 28/02/2019
 * Time: 09.11 AM
 */

/* @var $this \yii\web\View */

/* @var $model \app\modules\data\models\FormParameter */


use app\modules\data\models\ChildParameter;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$readOnly = $model->isNewRecord ? '' : 'readonly';
$classAlertError = sizeof($model->errors) > 0 ? 'alert alert-warning' : '';
?>
<div class="transaksi_form">

    <?php $form = ActiveForm::begin([
        'id' => 'form-parameter',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-8\">{input}\n{error}</div>",
            'labelOptions' => ['class' => 'col-lg-4 control-label'],
        ],
    ]); ?>

    <div class="<?= $classAlertError ?>"><?= $form->errorSummary($model); ?></div>

    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'unit')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'method')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'col_name')->textInput(['maxlength' => true, 'readonly' => 'readonly']) ?>
            <?= $form->field($model, 'matrix_type')->dropDownList([1 => 'Matrix', 2 => 'Oil Matrix']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <?= $form->field($model, 'notes', [
                'template' => "{label}\n<div class=\"col-lg-10\">{input}\n{error}</div>",
                'labelOptions' => ['class' => 'col-lg-2 control-label'],
            ])->textarea(['rows' => 4]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <?= $model->renderDetail($form) ?>
            <a data-action="add_row_table" class="btn btn-primary btn-xs btn-block">
                <i class="fa fa-plus-circle"></i> Add Item
            </a>
        </div>
    </div>

    <div class="footer">
        <div class="col-lg-2"></div>
        <div class="">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
    <style>
        #form-parameter > div:nth-child(5) > div > table > tbody > tr:nth-child(1) > td:nth-child(2) > div {
            margin-bottom: 0px;
        }
    </style>

<?php
$payerHtmlId = Html::getInputId($model, 'payer_id');
$modelForm = (new ChildParameter())->formName();
$jsCode = <<< JS
$(function () {
    var form = $('#form-parameter');
    function alert_notif(res) {
        $.sound_on = false;
        var color = res.status === 'success' ? '#659265' : '#C46A69';
        var icon = res.status === 'success' ? 'fa-check' : 'fa-times';
        $.smallBox({
            title: res.status,
            content: res.message,
            color: color,
            iconSmall: "fa " + icon + " fa-2x fadeInRight animated",
            timeout: 4000
        });
    }

    form.delegate('[data-action=delate_row]', 'click', function () {
        var elm = $(this);
        var row = elm.closest('tr');
        p = '$modelForm';
        p = p.replace('[new]', '[' + row.attr('data-id') + ']');

        row.html('<input type="hidden" name="' + p + '[actionId]" value="del" >');

    });
    

    form.delegate('[data-action=add_row_table]', 'click', function () {
        var elm = $(this);
        var tbl = elm.closest('.row').find('table');
        var form = elm.closest('form');
        $.ajax({
            method: 'post',
            data: {'reg_new_row': (tbl.find('tr').length - 1)},
            error: function (response) {
                alert_notif({status: "error", message: 'tidak bisa save'});
            },
            success: function (response) {
                var t = $(response.data);
                
                tbl.find('tbody').append(t);
                // $(form).yiiActiveForm(response.jsAdd, []);
                $.each(response.jsAdd, function (i, v) {
                    var vali = eval("var f = function(){ return " + v.validate.expression + ";}; f() ;");
                    v.validate = vali;
                    $(form).yiiActiveForm('add', v);
                });
            }
        });

    });
});
JS;

$this->registerJs($jsCode, 4, 'form-parm');