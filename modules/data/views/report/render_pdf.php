<?php


/**
 * Created by
 * User: Wisard17
 * Date: 21/03/2019
 * Time: 10.27 AM
 */

/* @var $this \yii\web\View */
/* @var $model \app\modules\data\models\RenderPDF */

$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@app/smartadmin/resources/assets');



use app\helpers\DynamicTable;

function auto($col)
{
    $out = '';
    for ($i = 0; $i < $col; $i++) {
        $out .= '<td class="value"></td>';
    }
    return $out;
}

$spaceTable = auto($model->limit - sizeof($model->history));
?>
<!--mpdf
<htmlpageheader name="myHTMLHeader1_<?= $model->type ?>">
    <div style="position: absolute;left: 10mm;top: 7mm;">
        <img width="56px" src=".<?= $directoryAsset ?>/img/logo_petrolab.png">
    </div>
    <div style="position: absolute;left: 26mm;top: 8mm;">
        <h5 class="bold" style="font-size: 18px;">PT PETROLAB SERVICES</h5>
        <h6 style="font-size: 15px;">Independent Laboratory</h6>
        <small style="font-size: 10px;"></small>
    </div>
    <div style="position: absolute;right: 15mm;top: 8mm;width: 77mm;text-align: center;font-size: 10px;">
        <div style="border:0.1mm solid #000000;
	background-color: #f0f2ff;
	background-gradient: linear #5f60f3 #f0f2ff 0 2 0 0.5;
	border-radius: 2mm; padding: 5px;
	background-clip: border-box; text-align: center; vertical-align: center;">
            <h5 class="bold" style="color: #000000; margin: auto;font-size: 20px;"><?= $model->nameType() ?></h5>
        </div>
        <?= $model->report_number == '' ? '' : "No. $model->report_number" ?>
    </div>
</htmlpageheader>
mpdf-->
<!--mpdf
<sethtmlpageheader name="myHTMLHeader1_<?= $model->type ?>" page="O" value="on" show-this-page="1" />
<sethtmlpageheader name="myHTMLHeader1_<?= $model->type ?>" page="E" value="on" />
mpdf-->
<?= $this->render('_h', ['model' => $model])?>
<table width="100%" id="tb3" style="font-size: 12px; page-break-inside: avoid;">
    <?php
    echo $this->render('_h_table', ['model' => $model, 'spaceTable' => $spaceTable, 'eval_code' => $model->eval_code]);


    $check = null;
    $i = 1;
    foreach ($model->renderParameter() as $category => $value) {
        if ((string)$value->parameter->parent == '0') {
            if (!$value->parameters->checkValue($model) & !$model->show_all_param) {
                continue;
            }
            echo $this->render('_pdf_row_parent', [
                'model' => $model,
                'parameter' => $value->parameters,
                'no' => $i,
                'spaceTable' => $spaceTable,
            ]);
        } else {
            $c = $value->parameter->col_name;
            if (!$model->hasAttribute($c)) {
                continue;
            }
            if ($model->$c == '' & !$model->show_all_param) {
                continue;
            }
            echo $this->render('_pdf_row_parameter', [
                'model' => $model,
                'parameter_id' => $value->parameter->id,
                'tab' => '',
                'parameter' => $c,
                'code' => $c . '_code',
                'no' => $i,
                'spaceTable' => $spaceTable,
            ]);
        }
        $i++;
    }
    ?>
</table>
<div class="line_top"></div>
<?php
if ((string)$model->typeReport->limit_type == '2') {
    $sp = $model->spesifikasi == null ? '' : $model->spesifikasi->note;
    echo '<div class="br"></div>
<div style="width: 100%; font-size: 10px;">
        ' . $sp . '
</div>
';
}
?>

<?php
echo $model->chartShow ? $this->render('_grafik', ['model' => $model]) : '';
echo in_array($model->CustomerID, ['28807']) && $model->type_report_id === 860 ? $this->render('_grafik_edit_2', ['model' => $model]) : '';
echo $model->bstShow ? $this->render('_bst', ['model' => $model]) : '';
echo $model->ffpShow ? $this->render('_ffp', ['model' => $model]) : '';
echo $model->fcsShow ? $this->render('_fcs', ['model' => $model]) : '';
echo $model->ptestShow ? $this->render('_ptest', ['model' => $model]) : '';
echo $model->filtratShow ? $this->render('_filtrat', ['model' => $model]) : '';
if ($model->mpc_only) {
    echo $this->render('_add_mpc', ['model' => $model, 'spaceTable' => $spaceTable,]);

} else {
    echo $model->mpcShow ? $this->render('_mpc', ['model' => $model]) : '';
    echo $this->render('_remark', ['model' => $model]);
}


echo $model->viewMode == 'pdf' ? $this->render('_down_page', ['model' => $model]) : '';
?>
