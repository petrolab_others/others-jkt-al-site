<?php



/* @var $this \yii\web\View */
/* @var $model \app\modules\data\models\RenderPDF|\app\modules\master\models\FormTransaksi|\yii\db\ActiveRecord */
/* @var $eval_code string */

if ($model->isHistory) {
    echo $this->render('_property_h', ['model' => $model, 'spaceTable' => $spaceTable, 'eval_code' => $eval_code]);
} else {
    echo $this->render('_property', ['model' => $model, 'eval_code' => $eval_code]);
}

$add_col = '';
$rowSpan = '';
$ref_col = '';
$secondRow = '';
$cspn = '';
if ($model->isHistory) {
    $cspn = "colspan='$model->limit'";
}
if ((string)$model->typeReport->limit_type == '1') {
    $add_col .= "<th width='50' class='text-center' colspan='2'>Limit </th>";
    $rowSpan = "rowspan='2'";
    $secondRow = "<tr class='tr_head'>
            <th width='50' class='text-center' > Min</th>
                <th width='50' class='text-center' > Max</th>
        </tr>";
}
if ((string)$model->typeReport->limit_type == '2') {
    $add_col .= "<th width='100' class='text-center'>Typical</th>";
}

if ($model->typeReport->reference == 1) {
    $ref_col .= "<th width='100' class='text-center' $rowSpan>Reference</th>";
}
echo "<tr class='tr_head'>
                <th width='20' $rowSpan>No</th>
                <th width='170' $rowSpan>Parameter</th>
                <th width='70' class='text-center' $rowSpan>Unit</th>
                <th width='120' class='text-center' $rowSpan>Method</th>
                <th  class='text-center' $cspn $rowSpan>Result</th>
                $ref_col
                $add_col
            </tr>$secondRow";