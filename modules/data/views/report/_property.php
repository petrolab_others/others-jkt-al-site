<?php

/**
 * Created by
 * User: Wisard17
 * Date: 02/04/2019
 * Time: 02.14 PM
 */

/* @var $this \yii\web\View */

/* @var $model \app\modules\data\models\RenderPDF */

use app\helpers\DynamicTable;

if (!isset($eval_code)) {
    $eval_code = $model->eval_code;
}

$mx_col = $model->maxCol;
$limit_place = 1;
$km = 4;
if ((string)$model->typeReport->limit_type == '1') {
    $limit_place = 2;
}
if ($model->typeReport->reference == 1) {
    $limit_place++;
}
if ($limit_place == 1) {
    $km = 3;
    $limit_place = 2;
}
$right = $limit_place;

$left = $mx_col - $right;

$col_header = ['sampleDate', 'receiveDate', 'analysisDate', 'reportDate', 'hm_oil', 'hm_unit', 'sample_name'];
?>
    <tr class="tr_head">
        <th colspan="<?= $left?>">Test Detail</th>
        <td colspan="<?= $right?>" class="text-center">Overall Analysis Result</td>
    </tr>
    <tr>
        <td colspan="<?= $km?>" class="td_title" style="font-size: 11px"><?= $model->getAttributeLabel('lab_number') ?></td>
        <td class="value" style="font-size: 11px"><?= $model->lab_number ?></td>
        <td class="text-center " colspan="<?= $limit_place?>" rowspan="<?= count($col_header) + 1?>">
            <?= DynamicTable::evalCode($eval_code) ?>
        </td>
    </tr>
<?php
foreach ($col_header as $item) {
    ?>
    <tr>
        <td colspan="<?= $km?>" class="td_title" style="font-size: 11px"><?= $model->getAttributeLabel($item) ?></td>
        <td class="value" style="font-size: 11px"><?= $model->$item ?></td>
    </tr>
    <?php
}
