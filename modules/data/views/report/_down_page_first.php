<?php



/* @var $this \yii\web\View */
/* @var $model \app\modules\data\models\RenderPDF|\app\modules\master\models\FormTransaksi|\yii\db\ActiveRecord */

?>
<div style="position: absolute;
    /*top: 200px;*/
	overflow: visible;
	bottom: 15mm;
    font-size: 9px;
	margin: 0;">
    <p>
        Catatan : Data analisa hanya berlaku untuk sample yang diuji di laboratorium PT. Petrolab Services<br>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pengaduan tidak dilayani setelah 30 hari dari tanggal report di terbitkan<br>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Report tidak boleh digandakan tanpa persetujuan tertulis dari laboratorium. <br>
        Notes : N=Normal, B=Attention, C=Urgent, D=Severe <br>
        <sup>*)</sup> Di luar ruang lingkup Akreditasi<br>
        <sup>++)</sup> Parameter Subcontract <br>
    </p>
</div>
