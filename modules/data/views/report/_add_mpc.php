<?php



/* @var $this \yii\web\View */
/* @var $model \app\modules\data\models\RenderPDF|\app\modules\master\models\FormTransaksi|\yii\db\ActiveRecord */

use app\helpers\DynamicTable;
use app\modules\data\models\Parameters;

echo $this->render('_remark', ['model' => $model]);
echo $this->render('_down_page', ['model' => $model]);
echo '<pagebreak />';
echo $this->render('_h', ['model' => $model]);

echo '<table width="100%" id="tb3" style="font-size: 12px;">';
echo $this->render('_h_table', ['model' => $model, 'spaceTable' => $spaceTable, 'eval_code' => $model->mpc_code]);

$p = Parameters::findOne(33);
$parameter = $p->col_name;
$code = $p->col_name . '_code';
$parameter_id = 33;
?>
    <tr class="tr_body">
        <td class="td_title" style="font-size: 10px">1</td>
        <td class="td_title" style="font-size: 10px"><?= $p->name ?></td>
        <td class="text-center" style="font-size: 10px"><?= $p->unit ?></td>
        <td class="text-center" style="font-size: 10px"><?= $p->method ?></td>
        <?php
        if ($model->isHistory) {
            foreach ($model->history as $tr) {
                $p = $tr->hasAttribute($parameter) ? $tr->$parameter : '';
                $c = $tr->hasAttribute($parameter) ? $tr->$code : '';
                echo DynamicTable::tdValue($c, $p);
            }
            echo $spaceTable;
        } else {
            $p = $model->hasAttribute($parameter) ? $model->$parameter : '';
            $c = $model->hasAttribute($parameter) ? $model->$code : '';
            echo DynamicTable::tdValue($c, $p);
        }
        ?>
        <?= $model->renderLastCol($parameter_id) ?>

    </tr>
<?php

echo '</table>';


echo $model->mpcShow ? $this->render('_mpc', ['model' => $model]) : '';

?>
<section id="note">
    <b class="note-title"><u>Remark</u></b>
    <section class="note-body" style="font-size: 10px; padding-left: 10px;">
        <?= $model->recom2 ?>
    </section>
</section>
