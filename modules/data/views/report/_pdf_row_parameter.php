<?php

/**
 * Created by
 * User: Wisard17
 * Date: 21/03/2019
 * Time: 04.43 PM
 */

/* @var $this \yii\web\View */
/* @var $model \app\modules\data\models\RenderPDF */
/* @var $parameter string */
/* @var $code string */
/* @var $tab string */
/* @var $no string */
/* @var $parameter_id int */

use app\helpers\DynamicTable;
?>

<tr class="tr_body">
    <td class="td_title" style="font-size: 10px"><?= $no?></td>
    <td class="td_title" style="font-size: 10px"><?= $tab . $model->getAttributeLabel($parameter) ?></td>
    <td class="text-center" style="font-size: 10px"><?= $model->getAttributeUnit($parameter) ?></td>
    <td class="text-center" style="font-size: 10px"><?= $model->getAttributeMethod($parameter) ?></td>
    <?php
    if ($model->isHistory) {
        foreach ($model->history as $tr) {
            $p = $tr->hasAttribute($parameter) ? $tr->$parameter : '';
            $c = $tr->hasAttribute($parameter) ? $tr->$code : '';
            echo DynamicTable::tdValue($c, $p);
        }
        echo $spaceTable;
    } else {
        $p = $model->hasAttribute($parameter) ? $model->$parameter : '';
        $c = $model->hasAttribute($parameter) ? $model->$code : '';
        echo DynamicTable::tdValue($c, $p);
    }
     ?>
    <?= $model->renderLastCol($parameter_id) ?>

</tr>
