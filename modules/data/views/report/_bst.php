<?php

/**
 * Created by
 * User     : Wisard17
 * Date     : 30/10/2019
 * Time     : 10.55 AM
 * File Name: ${FILE_NAME}
**/

/* @var $this \yii\web\View */
/* @var $model \app\modules\data\models\RenderPDF|\app\modules\master\models\FormTransaksi|\yii\db\ActiveRecord */


use yii\helpers\Html;

?>
<div id='chart'>
    <table style="width: 100%">
        <tr>
            <td style="border: 1px solid black;margin: 2px;padding: 2px;" align="center">
                BLOTTER SPOT TEST ANALYSIS
            </td>
        </tr>
        <tr>
            <td style="border: 1px solid black;margin: 2px;padding: 2px;" align="center">
                <?php
                if ($model->pathFileBst != '') {
                    echo Html::img(".$model->urlFileBst", ['height' => '110px']);
                }
                ?>
            </td>
        </tr>
    </table>
</div>