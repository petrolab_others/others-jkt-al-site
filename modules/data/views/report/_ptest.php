<?php

/**
 * Created by
 * User     : Adam
 * Date     : 09/04/2020
 * Time     : 04.32 PM
 * File Name: ${FILE_NAME}
 **/

/* @var $this \yii\web\View */
/* @var $model \app\modules\data\models\RenderPDF|\app\modules\master\models\FormTransaksi|\yii\db\ActiveRecord */


use yii\helpers\Html;

?>
<div id='chart'>

    <table style="width: 100%">
        <tr>
            <td style="border: 1px solid black;margin: 2px;padding: 2px;" colspan="2" align="center">
                PATCH TEST
            </td>
        </tr>
        <tr>
            <?php
                if ($model->pathFilePtest1 != '') {
                    $width1 = !empty($model->pathFilePtest2) ? '50%' : '100%';
                    echo "<td style='border: 1px solid black;margin: 2px;padding: 2px;' width='{$width1}' align='center'>";
                    echo Html::img(".$model->urlFilePtest1", ['height' => '175px', 'width' => '300px']);
                    echo "</td>";
                }
                ?>
            <?php
                if ($model->pathFilePtest2 != '') {
                    $width2 = !empty($model->pathFilePtest1) ? '50%' : '100%';
                    echo "<td style='border: 1px solid black;margin: 2px;padding: 2px;' width='{$width2}' align='center'>";
                    echo Html::img(".$model->urlFilePtest2", ['height' => '175px', 'width' => '300px']);
                    echo "</td>";
                }
            ?>
        </tr>
    </table>
</div>