<!-- header customer name -->
<?php echo $this->render('_h', ['model' => $model])?>

<!-- start table -->
<table width="100%" id="tb3" style="font-size: 12px; page-break-inside: avoid;">
    <?php
    echo $this->render('_h_table', ['model' => $model, 'spaceTable' => $spaceTable, 'eval_code' => $model->eval_code]);

    foreach ($allParameter as $eachParameter) {
        echo $eachParameter;
    }
    
    ?>
</table>