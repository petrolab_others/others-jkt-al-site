<?php


/**
 * Created by
 * User: Wisard17
 * Date: 21/03/2019
 * Time: 10.27 AM
 */

/* @var $this \yii\web\View */
/* @var $model \app\modules\data\models\RenderPDF */

$listHeader = [
    [['attr' => 'customerName', 'width' => '90'], ['attr' => 'unitNo', 'width' => '90']],
    ['address', 'componentName'],
    ['forCustomer', 'OIL_MATRIX'],
    ['unitLocation', 'matrix'],
];

if ((string)$model->typeReport->limit_type == '2') {
    $listHeader[2][1] = 'sample_name';
    $listHeader[3][1] = 'typicalType';
}

if ($model->req_order != '') {
    $listHeader[4][0] = 'blank';
    $listHeader[4][1] = 'req_order';
}

$left = '<table>';
$right = '<table>';

foreach ($listHeader as $row) {
    foreach ($row as $key => $item) {
        if ($item == 'blank') {
            if ($key == 0) {
                $left .= '<tr><td></td><td></td><td></td><td></td></tr>';
            } else {
                $right .= '<tr><td></td><td></td><td></td><td></td></tr>';
            }
            continue;
        }
        $attr = '';
        $width = '';
        if (is_array($item)) {
            $attr = isset($item['attr']) ? $item['attr'] : '';
            $w = isset($item['width']) ? $item['width'] : '';
            $width = "width='$w'";
        } elseif (is_string($item)) {
            $attr = $item;
            $width = '';
        }
        $ps = '<tr><td align="left" valign="top"
            style="font-size: 11px;" ' . $width . '>' . $model->getAttributeLabel($attr) . '</td>
        <td width="10" valign="top" style="font-size: 11px;">:</td>
        <td align="left" width="30%" valign="top" style="font-size: 11px;">' . $model->$attr . '</td>
        <td width="10"></td></tr>';
        if ($key == 0) {
            $left .= $ps;
        } else {
            $right .= $ps;
        }
    }
}
$left .= '</table>';
$right .= '</table>';

?>

<div class="br"></div>
<div class="line_top"></div>
<table width="100%" style="font-size: 12px;">
    <tr><td width="50%" valign="top"><?= $left?></td><td valign="top"><?= $right?></td></tr>
</table>
<div class="line_top"></div>
<div class="br"></div>
