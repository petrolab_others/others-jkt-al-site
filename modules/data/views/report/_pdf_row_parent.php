<?php

/**
 * Created by
 * User: Wisard17
 * Date: 22/03/2019
 * Time: 10.35 AM
 */

/* @var $this \yii\web\View */
/* @var $model \app\modules\data\models\RenderPDF|\app\modules\master\models\FormTransaksi|\yii\db\ActiveRecord */
/* @var $parameter \app\modules\data\models\Parameters */
/* @var $no int */


?>
    <tr class="tr_body">
        <td class="td_title" style="font-size: 10px"><?= $no ?></td>
        <td class="td_title" style="font-size: 10px"><b ><?= $parameter->name ?></b></td>
        <td class="text-center" style="font-size: 10px"><?= $parameter->unit ?></td>
        <td class="text-center" style="font-size: 10px"><?= $parameter->method ?></td>
        <?php
        if ($model->isHistory) {
            echo '<td class="text-center"></td><td class="text-center"></td><td class="text-center"></td>
                <td class="text-center"></td><td class="text-center"></td>';
        } else {
            echo '<td class="text-center"></td>';
        }
        ?>
        <?= $model->renderLastCol($parameter->id) ?>
    </tr>
<?php
foreach ($parameter->child as $category => $value) {
    $c = $value->col_name;
    if (!$model->hasAttribute($c)) {
        continue;
    }
    if ($model->$c == '' ) {
        continue;
    }
    echo $this->render('_pdf_row_parameter', [
        'model' => $model,
        'parameter_id' => $value->id,
        'tab' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'parameter' => $c,
        'code' => $c . '_code',
        'no' => '',
        'spaceTable' => $spaceTable,
    ]);

}