<?php

/**
 * Created by
 * User     : Adam
 * Date     : 07/01/2020
 * Time     : 04.32 PM
 * File Name: ${FILE_NAME}
 **/

/* @var $this \yii\web\View */
/* @var $model \app\modules\data\models\RenderPDF|\app\modules\master\models\FormTransaksi|\yii\db\ActiveRecord */


use yii\helpers\Html;

?>
<div id='chart'>
    <table style="width: 100%">
        <tr>
            <td style="border: 1px solid black;margin: 2px;padding: 2px;" align="center">
                FTIR FINGER PRINT ANALYSIS
            </td>
        </tr>
        <tr>
            <td style="border: 1px solid black;margin: 2px;padding: 2px;" align="center">
                <?php
                if ($model->pathFileFfp != '') {
                    echo Html::img(".$model->urlFileFfp", ['height' => '305px', 'width' => '100%', "style" => "display:inline-block"]);
                }
                if ($model->pathFileFfp2 != '') {
                    echo "</br>";
                    echo Html::img(".$model->urlFileFfp2", ['height' => '305px', 'width' => '100%', "style" => "display:inline-block"]);
                }
                ?>
            </td>
        </tr>
    </table>
</div>