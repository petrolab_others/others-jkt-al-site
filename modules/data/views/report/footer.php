<?php

/**
 * Created by
 * User: Wisard17
 * Date: 21/03/2019
 * Time: 10.23 AM
 */

/* @var $this \yii\web\View */
/* @var $model \app\modules\data\models\RenderPDF */
$rk = $model->rkNo == '' ? 'RK/5.10/01/01/05' : $model->rkNo;
if ($model->draft) {
    $rk = $model->rkNoDraft == '' ? 'RK/5.10/01/01/05' : $model->rkNoDraft;
}
?>
<div style="width: 100%; font-size: 9px;" align="right"><?= $rk?></div>
<footer style="width: 100%; background: #d6d6c2;font-size:9px;">
    <p align="center" ><?= "PT Petrolab Services"."<br>"."Jl Pisangan Lama III No 28 - Jakarta Timur 13230; Telp: +62 21 2968 8694; Fax: +62 21 2968 8693"."<br>"."customerservice@petrolab.co.id;  http://www.petrolab.co.id" ?></p>
</footer>
<div style="font-size:9px;margin-top: -15px; margin-right: 10px;" align="right">Page {PAGENO} of {nb}</div>
