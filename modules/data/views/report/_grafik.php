<?php

/**
 * Created by
 * User: Wisard17
 * Date: 08/04/2019
 * Time: 04.28 PM
 */

/* @var $this \yii\web\View */
/* @var $model \app\modules\data\models\RenderPDF */

?>
<div id='chart'>
    <table style="width: 100%">
        <tr>
            <td style="border: 1px solid black;margin: 2px;padding: 2px;">
                <img style="width: 100%" src="<?= $model->chart1() ?>">
            </td>
            <td style="border: 1px solid black;margin: 2px;padding: 2px;">
                <img style="width: 100%" src="<?= $model->chart2() ?>">
            </td>
        </tr>
        <tr>
            <td style="border: 1px solid black;margin: 2px;padding: 2px;">
                <img style="width: 100%" src="<?= $model->chart3() ?>">
            </td>
            <td style="border: 1px solid black;margin: 2px;padding: 2px;">
                <img style="width: 100%" src="<?= $model->chart4() ?>">
            </td>
        </tr>
    </table>
</div>