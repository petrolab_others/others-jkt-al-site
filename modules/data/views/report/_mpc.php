<?php

/**
 * Created by
 * User: Wisard17
 * Date: 09/04/2019
 * Time: 10.42 AM
 */

/* @var $this \yii\web\View */
/* @var $model \app\modules\data\models\RenderPDF */

use yii\helpers\Html;

?>
<div id='chart'>
    <table style="width: 100%">
        <tr>
            <td style="border: 1px solid black;margin: 2px;padding: 2px;" colspan="2" align="center">
                MPC (MEMBRANE PATCH COLORIMETRY) ANALYSIS
            </td>
        </tr>
        <tr>
            <td style="border: 1px solid black;margin: 2px;padding: 2px;" width="50%" align="center">
                <?php
                if ($model->mpc_pic != '') {
                    echo Html::img(".$model->urlFileMpc", ['height' => '110px']);
                }
                ?>
            </td>
            <td style="border: 1px solid black;margin: 2px;padding: 2px;" width="50%">
                <img style="width: 50%" src="<?= $model->chartMpc() ?>">
            </td>
        </tr>
    </table>
</div>