<?php



/* @var $this \yii\web\View */
/* @var $model \app\modules\data\models\RenderPDF|\app\modules\master\models\FormTransaksi|\yii\db\ActiveRecord */

?>
<div style="position: absolute;
    /*top: 200px;*/
	overflow: visible;
	bottom: 15mm;
    font-size: 9px;
	margin: 0;">
    <p>
        Catatan : Data analisa hanya berlaku untuk sample yang diuji di laboratorium PT. Petrolab Services<br>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pengaduan tidak dilayani setelah 30 hari dari tanggal report di terbitkan<br>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Report tidak boleh digandakan tanpa persetujuan tertulis dari laboratorium. <br>
        Notes : N=Normal, B=Attention, C=Urgent, D=Severe <br>
        <sup>*)</sup> Di luar ruang lingkup Akreditasi<br>
        <sup>++)</sup> Parameter Subcontract <br>
    </p>
</div>
<div style="position: absolute;
    /*top: 120px;*/
    overflow: visible;
    right: 5mm;
    bottom: <?= $model->draft ? 35 : 20 ?>mm;
    margin: 0;">
    <table id="tb5">
        <tr>
            <td width="33%" style="height: <?= $model->draft ? 13 : 20 ?>mm;" class="right">
                <?= $model->draft ? 'Dibuat' : '' ?>
            </td>
            <td width="33%" class="right"><?= $model->draft ? 'Diperiksa' : '' ?></td>
            <td width="33%" class="right"><?= $model->draft ? 'Disetujui' : '<u>Manager Teknis</u>' ?></td>
        </tr>
        <tr>
            <td class="right"><?= $model->draft ? '<u>Pembuat Laporan</u>' : '' ?></td>
            <td class="right"><?= $model->draft ? '<u>Deputy Mgr. Teknis</u>' : '' ?></td>
            <td class="right"><?= $model->draft ? '<u>Mgr. Teknis</u>' : '<u>' . $model->ttdMeneger('J') . '</u>' ?></td>
        </tr>
    </table>
</div>
