<?php

/**
 * Created by
 * User: Wisard17
 * Date: 02/04/2019
 * Time: 02.15 PM
 */

/* @var $this \yii\web\View */

/* @var $model \app\modules\data\models\RenderPDF */

use app\helpers\DynamicTable;


if (!isset($eval_code)) {
    $eval_code = $model->eval_code;
}

$km = 4;
$limit_place = 1;
if ((string)$model->typeReport->limit_type == '1') {
    $limit_place = 2;
}
if ($model->typeReport->reference == 1) {
    $limit_place++;
}

$col_header = ['sampleDate', 'receiveDate', 'reportDate', 'hm_unit', 'hm_oil', 'sample_name', 'oil_change'];
?>
    <tr class="tr_head">
        <th colspan="<?= $model->limit + 4?>">Test Detail</th>
        <td colspan="<?= $limit_place?>" class="text-center">Overall Analysis Result</td>
    </tr>
    <tr>
        <td colspan="<?= $km?>" class="td_title" style="font-size: 11px"><?= $model->getAttributeLabel('lab_number') ?></td>
        <?php foreach ($model->history as $tr) { ?>
            <td class="value" style="font-size: 11px"><?= $tr->lab_number ?></td>
        <?php } ?>
        <?= $spaceTable ?>
        <td class="text-center " colspan="<?= $limit_place?>" rowspan="<?= count($col_header) + 1?>">
            <?= DynamicTable::evalCode($eval_code) ?>
        </td>
    </tr>
<?php
foreach ($col_header as $item) {
    ?>
    <tr>
        <td colspan="<?= $km?>" class="td_title" style="font-size: 10px"><?= $model->getAttributeLabel($item) ?></td>
        <?php foreach ($model->history as $tr) { ?>
            <td class="value" style="font-size: 10px"><?= $tr->$item ?></td>
        <?php } ?>
        <?= $spaceTable ?>
    </tr>
    <?php
}

