<?php

/**
 * Created by
 * User     : Adam
 * Date     : 07/01/2020
 * Time     : 04.32 PM
 * File Name: ${FILE_NAME}
 **/

/* @var $this \yii\web\View */
/* @var $model \app\modules\data\models\RenderPDF|\app\modules\master\models\FormTransaksi|\yii\db\ActiveRecord */


use yii\helpers\Html;

?>
<div id='chart'>
    <table style="width: 100%;border:1px solid black">
        <tr>
            <td style="border: 1px solid black;margin: 2px;padding: 2px;" align="center" colspan=3>
                Foaming Characteristic
            </td>
        </tr>
        <tr>
            <td style="margin: 2px;padding: 2px;" align="center">
                Sequence I
            </td>
            <td style="margin: 2px;padding: 2px;" align="center">
                Sequence II
            </td>
            <td style="margin: 2px;padding: 2px;" align="center">
                Sequence III
            </td>
        </tr>
        <tr>
            <td style="margin: 2px;padding: 2px;" align="center">
                <?php
                if ($model->pathFileFcs1 != '') {
                    echo Html::img(".$model->urlFileFcs1", ['height' => '305px', 'width' => '30%', "style" => "display:inline-block"]);
                }
                ?>
            </td>
            <td style="margin: 2px;padding: 2px;" align="center">
                <?php
                    if ($model->pathFileFcs2 != '') {
                        echo "</br>";
                        echo Html::img(".$model->urlFileFcs2", ['height' => '305px', 'width' => '30%', "style" => "display:inline-block"]);
                    }
                 ?>
            </td>
            <td style="margin: 2px;padding: 2px;" align="center">
                <?php
                    if ($model->pathFileFcs3 != '') {
                        echo "</br>";
                        echo Html::img(".$model->urlFileFcs3", ['height' => '305px', 'width' => '30%', "style" => "display:inline-block"]);
                    }
                 ?>
            </td>
        </tr>
    </table>
</div>
