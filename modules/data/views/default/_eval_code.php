<?php

/**
 * Created by
 * User: Wisard17
 * Date: 24/11/2018
 * Time: 12.46 PM
 */

/* @var $this \yii\web\View */
/* @var $model \app\modules\master\models\FormTransaksi|\yii\db\ActiveRecord */
/* @var $form \yii\bootstrap\ActiveForm */

use yii\helpers\Url;

?>
<fieldset>
    <h3>Eval Code</h3>
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'eval_code')->dropDownList([
                '' => 'Not Selected',
                'N' => 'Normal',
                'B' => 'Attention',
                'C' => 'Urgent',
                'D' => 'Severe',
            ]) ?>
        </div>
        <div class="col-lg-6">

        </div>
    </div>

    <h3>Recommendations</h3>
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'recom1')->textarea(['rows' => 3]) ?>
            <?= $form->field($model, 'recom2')->textarea(['rows' => 3]) ?>
        </div>

    </div>
</fieldset>
