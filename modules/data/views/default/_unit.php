<?php

/**
 * Created by
 * User: Wisard17
 * Date: 07/11/2018
 * Time: 09.33 AM
 */

/* @var $this \yii\web\View */
/* @var $model \app\modules\data\models\FormData */
/* @var $form \yii\bootstrap\ActiveForm */

use yii\helpers\Url;

?>
<fieldset>
    <h3> Property</h3>
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'branch_no')->dropDownList($model->branch_no == '' ? [] :
                [$model->branch_no => $model->customer->branch], ['class' => 'form-control select2ajax',
                'data-url' => Url::toRoute(['/customers/default/list-branch']), 'data-base-id' => 'branch_no',]) ?>
            <?= $form->field($model, 'CustomerID')->dropDownList($model->CustomerID == '' ? [] :
                [$model->CustomerID => $model->pelanggan->Name], ['class' => 'form-control select2ajax',
                'data-url' => Url::toRoute(['/customers/default/list-customer']), 'data-depend-from' => 'branch',
                'data-depend' => 'branch_no', 'data-depend-id' => 'formdata-branch_no',
            ]) ?>
            <?= $form->field($model, 'UnitID')->dropDownList($model->UnitID == '' ? [] :
                [$model->UnitID => $model->unit == null ? 'unit Delete' : $model->unit->UnitNo], ['class' => 'form-control select2ajax',
                'data-url' => Url::toRoute(['/master/unit/list-unit']), 'data-depend-from' => 'CustomerID',
                'data-depend' => 'CustomerID', 'data-depend-id' => 'formdata-customerid',
            ]) ?>
            <?= $form->field($model, 'ComponentID')->dropDownList($model->ComponentID == '' ? [] :
                [$model->ComponentID => $model->component == null ? 'Componen delete' : $model->component->component], ['class' => 'form-control select2ajax',
                'data-url' => Url::toRoute(['/master/unit/list-component']), 'data-depend-from' => 'UnitID',
                'data-depend' => 'UnitID', 'data-depend-id' => 'formdata-unitid',
            ]) ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'report_number')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'req_order')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
</fieldset>
