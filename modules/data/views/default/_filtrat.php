<?php

/**
 * Created by
 * User     : Adam
 * Date     : 11/11/2021
 * Time     : 11.18 AM
 * File Name: ${FILE_NAME}
 **/

/* @var $this \yii\web\View */
/* @var $model \app\modules\data\models\FormData|\app\modules\master\models\FormTransaksi|\yii\db\ActiveRecord */
/* @var $form \yii\bootstrap\ActiveForm */

use yii\helpers\Html; ?>

<fieldset>
    <h3>FILTRAT ANALYSIS</h3>
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'filtrat')->fileInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-6">
            <?php
            if ($model->pathFileFiltrat != '') {
                echo Html::img($model->urlFileFiltrat, ['width' => '100%']);
            } ?>
        </div>
    </div>
</fieldset>
