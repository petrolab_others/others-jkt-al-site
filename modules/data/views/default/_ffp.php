<?php

/**
 * Created by
 * User     : Adam
 * Date     : 07/01/2020
 * Time     : 11.18 AM
 * File Name: ${FILE_NAME}
 **/

/* @var $this \yii\web\View */
/* @var $model \app\modules\data\models\FormData|\app\modules\master\models\FormTransaksi|\yii\db\ActiveRecord */
/* @var $form \yii\bootstrap\ActiveForm */

use yii\helpers\Html; ?>

<fieldset>
    <h3>FTIR FINGER PRINT ANALYSIS</h3>
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'ftir')->fileInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'ftir2')->fileInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-6">
            <?php
            if ($model->pathFileFfp != '') {
                echo Html::img($model->urlFileFfp, ['width' => '100%']);
            } ?>
            <?php
            if ($model->pathFileFfp2 != '') {
                echo "</br>";
                echo Html::img($model->urlFileFfp2, ['width' => '100%']);
            } ?>
        </div>
    </div>
</fieldset>
