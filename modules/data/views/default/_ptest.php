<?php

/**
 * Created by
 * User     : Adam
 * Date     : 09/04/2020
 * Time     : 11.18 AM
 * File Name: ${FILE_NAME}
 **/

/* @var $this \yii\web\View */
/* @var $model \app\modules\data\models\FormData|\app\modules\master\models\FormTransaksi|\yii\db\ActiveRecord */
/* @var $form \yii\bootstrap\ActiveForm */

use yii\helpers\Html; ?>

<fieldset>
    <h3>PATCH TEST</h3>
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'ptest1')->fileInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-6">
            <?php
            if ($model->pathFilePtest1 != '') {
                echo Html::img($model->urlFilePtest1, ['width' => '100%']);
            } ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'ptest2')->fileInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-6">
            <?php
            if ($model->pathFilePtest2 != '') {
                echo Html::img($model->urlFilePtest2, ['width' => '100%']);
            } ?>
        </div>
    </div>
</fieldset>
