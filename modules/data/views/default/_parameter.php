<?php

/**
 * Created by
 * User: Wisard17
 * Date: 25/03/2019
 * Time: 10.07 AM
 */

/* @var $this \yii\web\View */
/* @var $model \app\modules\data\models\FormData */
/* @var $form \yii\bootstrap\ActiveForm */


$item = ['' => 'Code', 'B' => 'B', 'C' => 'C', 'D' => 'D'];

echo "<div id='data-param'><fieldset><h3></h3><div class='row'>";
if ($model->isHistory) {
    echo '<div class="col-lg-6">';
    echo $form->field($model, 'oil_change')
        ->dropDownList(['Yes' => 'Yes', 'No' => 'No'], ['prompt' => 'Select change', 'class' => 'form-control select2default']);

    echo '</div>';
}
foreach ($model->renderParameter() as $parameter) {
    echo '<div class="col-lg-6">';
    if ((string)$parameter->parameters->parent == '0') {
        echo '<h4 style="text-align: center;">'.$parameter->parameters->name.'</h4>';
        foreach ($parameter->parameters->child as $childParameter) {
            echo $this->render('_parameter_code', ['form' => $form, 'model' => $model,
                'parameter' => $childParameter->col_name, 'code' => $childParameter->col_name . '_code',
                'items' => $item]);
        }
        echo '</div>';
        continue;
    }
    echo $this->render('_parameter_code', ['form' => $form, 'model' => $model,
        'parameter' => $parameter->parameters->col_name, 'code' => $parameter->parameters->col_name . '_code',
        'items' => $item]);
    echo '</div>';

}

echo '</div></fieldset></div>';
