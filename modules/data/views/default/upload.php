<?php

/**
 * Created by
 * User: Wisard17
 * Date: 23/03/2019
 * Time: 01.10 PM
 */

/* @var $this \yii\web\View */
/* @var $model \app\modules\data\models\FormUploadData */


use app\smartadmin\assets\plugins\FileDownloadAssets;
use yii\helpers\Url;
use app\smartadmin\Alert;
use app\smartadmin\ActiveForm;
use yii\helpers\Html;


$this->title = Yii::t('app', 'Upload Analysis Data');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Reports'), 'url' => Url::toRoute(['/reports'])];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Reports Admin'), 'url' => Url::toRoute(['/master/report'])];
$this->params['breadcrumbs'][] = $this->title;
$classAlerError = sizeof($model->errors) > 0 ? 'alert alert-warning' : '';
?>
    <div class="row">

        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-6">
            <h1 class="page-title txt-color-blueDark"><i class="fa-fw fa fa-industry"></i>
                <?= $this->title ?>
            </h1>
        </div>


        <!-- right side of the page with the sparkline graphs -->
        <!-- col -->
        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-6">
            <!-- sparks -->
            <ul id="sparks">
                <?= Html::a('<i class="fa fa-file-archive-o"></i>
                    Template', Url::toRoute(['/data/default/download-excel', 'header_only' => 1]),
                    ['class' => 'btn btn-info', 'data-action' => 'excel-template']) ?>

            </ul>
            <!-- end sparks -->
        </div>
        <!-- end col -->

    </div>

    <section id="widget-grid" class="">

        <!-- START ROW -->
        <div class="row">

            <!-- NEW COL START -->
            <article class="col-sm-12 col-md-12 col-lg-12">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false"
                     data-widget-custombutton="false">
                    <!-- widget options:
                    usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                    data-widget-colorbutton="false"
                    data-widget-editbutton="false"
                    data-widget-togglebutton="false"
                    data-widget-deletebutton="false"
                    data-widget-fullscreenbutton="false"
                    data-widget-custombutton="false"
                    data-widget-collapsed="true"
                    data-widget-sortable="false"

                    -->
                    <header>
                        <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                        <h2>Form </h2>

                    </header>

                    <!-- widget div-->
                    <div>

                        <!-- widget edit box -->
                        <div class="jarviswidget-editbox">
                            <!-- This area used as dropdown edit box -->
                            <?= Alert::widget() ?>
                        </div>
                        <!-- end widget edit box -->

                        <!-- widget content -->
                        <div class="widget-body ">
                            <div class="add-comment-form">

                                <?php $form = ActiveForm::begin([
                                    'id' => 'upload-form',
                                    'fieldConfig' => [
                                        'template' => "{label}<label class='input'>{input}</label>{error}",
//                                        'labelOptions' => ['class' => 'label  '],
                                        'inputOptions' => ['class' => 'form-control'],
                                    ],
                                    'options' => [
                                        'class' => 'smart-form',
                                    ],

                                ]); ?>

                                <fieldset>
                                    <div class="<?= $classAlerError ?>"><?= $form->errorSummary($model); ?></div>
                                </fieldset>
                                <fieldset>
                                    <?= $form->field($model, 'uploadFile', ['template' => '{label}<label class=\'input\'>
                                        <i class="icon-append fa fa-calculator"></i>
                                        {input}</label>{error}'])->fileInput(['class' => ''])->label('Upload Result') ?>
                                    <div class="progress progress-bar-default">
                                        <div id="pst" style="width: 0%" aria-valuemax="100" aria-valuemin="0"
                                             aria-valuenow="43"
                                             role="progressbar" class="progress-bar">
                                            <span class="sr-only">43% Complete (success)</span>
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset>
                                    <div id="message-data" class="alert">

                                    </div>
                                </fieldset>


                                <footer>
                                    <?= ''//Html::submitButton(Yii::t('app', $model->isNewRecord ? 'Submit ' : 'Save '),
                                    //['class' => 'btn btn-primary', 'data-action' => 'submit_update_followup'])   ?>
                                </footer>

                                <?php ActiveForm::end(); ?>

                            </div>
                        </div>
                        <!-- end widget content -->

                    </div>
                    <!-- end widget div -->

                </div>
                <!-- end widget -->

            </article>
            <!-- END COL -->

        </div>

        <!-- END ROW-->

    </section>
    <!-- end widget grid -->

<?php
$url = Url::to();
$idUpload = Html::getInputId($model, 'uploadFile');
FileDownloadAssets::register($this);
$csrf = Yii::$app->request->getCsrfToken();
$this->registerJs(/** @lang JavaScript */
    "$(function () {
    'use strict';
    // Change this to the location of your server-side upload handler:
    var form = $('#upload-form');
    var url = '$url';
    var msg = form.find('#message-data');
    form.find('#$idUpload').fileupload({
        url: url,
        dataType: 'json',
        done: function (e, data) {
            console.log(data);
            if (data.result.status_save === 'error') {
                form.find('#pst').css('width', 0 + '%');
                msg.attr('class', 'alert alert-warning');
            }
            if (data.result.status_save === 'success') {
                msg.attr('class', 'alert alert-info');
                form.find('#pst').css('width', '100%');
            }
            msg.html(data.result.message + data.result.data);
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 90, 10);
            form.find('#pst').css('width', progress + '%');
        },
        beforeSend: function (XMLHttpRequest) {
            console.log(XMLHttpRequest);
            form.find('#pst').css('width', '0%');
            msg.html('');
        },
        error: function(e) {
            form.find('#pst').css('width', '0%');
            msg.attr('class', 'alert alert-danger');
            msg.html('Try Again... <br><hr>');
        }
    });
    
    $('[data-action=excel-template]').click( function (a) {
        a.preventDefault();
        a.stopPropagation();
        var elm = $(this);
        
        $.fileDownload(elm.attr('href'), {
            preparingMessageHtml: \"We are preparing your report, please wait...\",
            failMessageHtml: \"There was a problem generating your report, the report is to big, filter first and please try again.\",
            httpMethod: 'get',
            data: {}
        });
        return false;
    });
});
", 4, 'render');
