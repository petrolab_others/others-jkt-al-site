<?php

/**
 * Created by
 * User     : Wisard17
 * Date     : 30/10/2019
 * Time     : 11.02 AM
 * File Name: ${FILE_NAME}
**/

/* @var $this \yii\web\View */
/* @var $model \app\modules\data\models\FormData|\app\modules\master\models\FormTransaksi|\yii\db\ActiveRecord */
/* @var $form \yii\bootstrap\ActiveForm */

use yii\helpers\Html; ?>

<fieldset>
    <h3>BLOTTER SPOT TEST ANALYSIS</h3>
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'blotter')->fileInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-6">
            <?php
            if ($model->pathFileBst != '') {
                echo Html::img($model->urlFileBst, ['width' => '100%']);
            } ?>
        </div>
    </div>
</fieldset>
