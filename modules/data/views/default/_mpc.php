<?php

/**
 * Created by
 * User: Wisard17
 * Date: 04/03/2019
 * Time: 09.19 AM
 */

/* @var $this \yii\web\View */
/* @var $model \app\modules\data\models\FormData */
/* @var $form \yii\bootstrap\ActiveForm */


use yii\helpers\Html; ?>

<fieldset>
    <h3>MPC (MEMBRANE PATCH COLORIMETRY) ANALYSIS</h3>
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'mpc')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'mpc_code')->dropDownList([
                '' => 'Not Selected',
                'N' => 'Normal',
                'B' => 'Attention',
                'C' => 'Urgent',
                'D' => 'Severe',
            ]) ?>
            <?= $form->field($model, 'pic')->fileInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-6">
            <?php
            if ($model->mpc_pic != '') {
                echo Html::img($model->urlFileMpc, ['width' => '100%']);
            }
            ?>
        </div>
    </div>
</fieldset>
