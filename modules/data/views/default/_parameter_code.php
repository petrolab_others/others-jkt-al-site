<?php

/**
 * Created by
 * User: Wisard17
 * Date: 02/12/2018
 * Time: 02.22 PM
 */

/* @var $this \yii\web\View */
/* @var $form \app\smartadmin\ActiveForm|\yii\bootstrap\ActiveForm|\yii\widgets\ActiveForm */
/* @var $model \app\modules\master\models\FormTransaksi|\yii\db\ActiveRecord */
/* @var $parameter string */
/* @var $code string */

/* @var $items array */

use yii\helpers\Html;

?>

<?= $form->field($model, $parameter, ['template' => '{label}
                        <div class="col-lg-8"><div class=" input-group">{input} <div class="input-group-btn" >' .
//    Html::tag('span', '<i class="fa fa-gear"></i>', [
//        'class' => 'btn btn-default option_parameter',
//        'tabindex'=> '0', 'data-html' => 'true',
//    ]) .
    Html::activeDropDownList($model, $code, $items,
        ['class' => 'btn btn-default']) . ' </div></div>{error}{hint}
                        </div>',
    'options' => ['class' => 'form-group has-feedback']
])->textInput(['maxlength' => true]) ?>