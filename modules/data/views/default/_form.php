<?php

/**
 * Created by
 * User: Wisard17
 * Date: 04/03/2019
 * Time: 09.19 AM
 */

/* @var $this \yii\web\View */
/* @var $model \app\modules\data\models\FormData */


/* @var $form ActiveForm */


use app\smartadmin\Alert;
use conquer\select2\Select2Asset;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\bootstrap\Tabs;

$readOnly = $model->isNewRecord ? '' : 'readonly';
$classAlertError = sizeof($model->errors) > 0 ? 'alert alert-warning' : '';
?>
    <div class="transaksi_form">

        <?php $form = ActiveForm::begin([
            'id' => 'form-data',
            'layout' => 'horizontal',
            'fieldConfig' => [
                'template' => "{label}\n<div class=\"col-lg-8\">{input}\n{error}</div>",
                'labelOptions' => ['class' => 'col-lg-4 control-label'],
            ],
        ]); ?>

        <div class="<?= $classAlertError ?>"><?= $form->errorSummary($model); ?><?= Alert::widget() ?></div>

        <div class="row">
            <div class="col-lg-6">
                <?= $form->field($model, 'type_report_id')
                    ->dropDownList($model->listReport(), ['prompt' => 'Select Type', 'class' => 'form-control select2default']) ?>
            </div>
        </div>
        <hr>
        <?php
        $itab = [];
        $itab[] = [
            'label' => 'Property',
            'content' => $this->render('_unit', ['model' => $model, 'form' => $form, 'readOnly' => $readOnly])
                . $this->render('_test_detail', ['model' => $model, 'form' => $form]),
            'active' => $itab == [],
        ];
        $itab[] = [
            'label' => 'Parameter',
            'content' => $this->render('_parameter', ['model' => $model, 'form' => $form]),
            'active' => $itab == [],
        ];
        $itab[] = [
            'label' => 'Eval Code & Recommendations',
            'content' => $this->render('_eval_code', ['model' => $model, 'form' => $form]),
            'active' => $itab == [],
        ];
        if ($model->mpcShow) {
            $itab[] = [
                'label' => 'MPC',
                'content' => $this->render('_mpc', ['model' => $model, 'form' => $form]),
                'active' => $itab == [],
                'options' => ['id' => 'mpc_p'],
                'linkOptions' => ['id' => 'mpc_h'],
            ];
        }
        if ($model->bstShow) {
            $itab[] = [
                'label' => 'Blotter Spot Test',
                'content' => $this->render('_bst', ['model' => $model, 'form' => $form]),
                'active' => $itab == [],
                'options' => ['id' => 'bst_p'],
                'linkOptions' => ['id' => 'bst_h'],
            ];
        }
        if ($model->ffpShow) {
            $itab[] = [
                'label' => 'Ftir Finger Print',
                'content' => $this->render('_ffp', ['model' => $model, 'form' => $form]),
                'active' => $itab == [],
                'options' => ['id' => 'ffp_p'],
                'linkOptions' => ['id' => 'ffp_h'],
            ];
        }
        if ($model->fcsShow) {
            $itab[] = [
                'label' => 'Foaming Characteristic',
                'content' => $this->render('_fcs', ['model' => $model, 'form' => $form]),
                'active' => $itab == [],
                'options' => ['id' => 'fcs_p'],
                'linkOptions' => ['id' => 'fcs_h'],
            ];
        }
        if ($model->ptestShow) {
            $itab[] = [
                'label' => 'Patch Test',
                'content' => $this->render('_ptest', ['model' => $model, 'form' => $form]),
                'active' => $itab == [],
                'options' => ['id' => 'ptest_p'],
                'linkOptions' => ['id' => 'ptest_h'],
            ];
        }
        if ($model->filtratShow) {
            $itab[] = [
                'label' => 'Filtrat',
                'content' => $this->render('_filtrat', ['model' => $model, 'form' => $form]),
                'active' => $itab == [],
                'options' => ['id' => 'filtrat_p'],
                'linkOptions' => ['id' => 'filtrat_h'],
            ];
        }
        echo Tabs::widget(['items' => $itab, 'options' => ['id' => 'tabs']]);
        ?>

        <fieldset>
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        </fieldset>

        <?php ActiveForm::end(); ?>

    </div>

    <style>
        .hide_parameter {
            display: none;
        }
    </style>
<?php
Select2Asset::register($this);
$cls = Json::encode($model::className());
$loadingHtml = '<div align="center"><i class="fa fa-spinner fa-pulse fa-2x fa-fw fa-spin margin-bottom"></i></div>';
$typeId = Html::getInputId($model, 'type_report_id');
$urlParameter = Url::to(['render-parameter']);
$idModel = $model->id;
$jsCode = <<< JS
$(function () {
    let form = $('#form-data');

    form.find('#$typeId').on('change', function(e) {
        let elm = $(e.target);
        removeTab();
        $.ajax({
            url: '$urlParameter?type=' + elm.val() + '&id=' + '$idModel',
            success: function(response) {
                form.find('#data-param').parent().html(response.page);
                renderTo(response);
            }
        });
    });

    function renderTo(r) {
        let tabs = form.find('#tabs');
        let tabsBody = form.find('.tab-content');
        if (r.mpc !== '' && r.mpc.header !== undefined) {
            tabs.append(r.mpc.header);
            tabsBody.append(r.mpc.body);
        }
        if (r.bst !== '' && r.bst.header !== undefined) {
            tabs.append(r.bst.header);
            tabsBody.append(r.bst.body);
        }
        if (r.ffp !== '' && r.ffp.header !== undefined) {
            tabs.append(r.ffp.header);
            tabsBody.append(r.ffp.body);
        }
        if (r.ptest !== '' && r.ptest.header !== undefined) {
            tabs.append(r.ptest.header);
            tabsBody.append(r.ptest.body);
        }
        if (r.filtrat !== '' && r.filtrat.header !== undefined) {
            tabs.append(r.filtrat.header);
            tabsBody.append(r.filtrat.body);
        }
    }

    function removeTab() {
        let c_mpc = form.find('#mpc_p');
        if (c_mpc.length > 0) {
            c_mpc.remove();
            form.find('#mpc_h').closest('li').remove();
        }

        let c_bst = form.find('#bst_p');
        if (c_bst.length > 0) {
            c_bst.remove();
            form.find('#bst_h').closest('li').remove();
        }

        let c_ffp = form.find('#ffp_p');
        if (c_ffp.length > 0) {
            c_ffp.remove();
            form.find('#ffp_h').closest('li').remove();
        }

        let c_ptest = form.find('#ptest_p');
        if (c_ptest.length > 0) {
            c_ptest.remove();
            form.find('#ptest_h').closest('li').remove();
        }

        let c_filtrat = form.find('#filtrat_p');
        if (c_filtrat.length > 0) {
            c_filtart.remove();
            form.find('#filtrat_h').closest('li').remove();
        }
    }

    form.find('#formdata-branch_no').on('select2:select', function (e) {
        form.find('#formdata-customerid').val(null).trigger('change');
        form.find('#formdata-unitid').val(null).trigger('change');
        form.find('#formdata-componentid').val(null).trigger('change');
        let data = e.params.data;
        if (data.id != 0) {
        } else {
        }
    });
    form.find('#formdata-customerid').on('select2:select', function (e) {
        form.find('#formdata-unitid').val(null).trigger('change');
        form.find('#formdata-componentid').val(null).trigger('change');
    });
    form.find('#formdata-unitid').on('select2:select', function (e) {
        form.find('#formdata-componentid').val(null).trigger('change');
    });
    form.find('#formdata-componentid').on('select2:select', function (e) {
        let data = e.params.data;
        if (data.id != 0) {
            form.find('#formdata-matrix')//.append('<option value="'+ data.MATRIX+'">' +data.MATRIX+ '</option>')
            .val(data.MATRIX).trigger('change');
        } else {
            form.find('#formdata-matrix').val(null).trigger('change');
        }
    });
    let par = function () {
        return {
            ajax: {
                url: function (a) {
                    return this.attr('data-url');
                },
                data: function (params) {
                    let el = this;
                    return {
                        q: params.term,
                        param: {
                            col: el.attr('data-depend'),
                            fromcol: el.attr('data-depend-from'),
                            fromId: el.attr('data-base-id'),
                            filter: form.find('#' + el.attr('data-depend-id')).val(),
                            model: $cls,
                        }
                    };
                },
                processResults: function (data) {
                    return {
                        results: data.results
                    };
                }
            }
        }
    };

    form.find('.select2ajax').select2(par());
    form.find('.select2default').select2();

    form.delegate('[data-action=gen_lan_no]', 'click', function () {
        var elm = $(this);
        var label = elm.closest('.form-group');
        var input = label.find('input');
        $.ajax({
            url: elm.attr('data-url'),
            method: 'post',
            data: form.serialize(),
            beforeSubmit: function (d) {
                elm.find('i').addClass('fa-spin');
            },
            error: function (response) {
                elm.find('i').removeClass('fa-spin');
            },
            success: function (response) {
                elm.find('i').removeClass('fa-spin');
                input.val(response.lab_no).blur();
            },

        });
    });

    function alert_notif(res) {
        $.sound_on = false;
        var color = res.status === 'success' ? '#659265' : '#C46A69';
        var icon = res.status === 'success' ? 'fa-check' : 'fa-times';
        $.smallBox({
            title: res.status,
            content: res.message,
            color: color,
            iconSmall: "fa " + icon + " fa-2x fadeInRight animated",
            timeout: 4000
        });
    }
});
JS;

$this->registerJs($jsCode, 4, 'data-analisa');
