<?php
/**
 * Created by
 * User: Wisard17
 * Date: 25/02/2019
 * Time: 10.13 AM
 */

namespace app\modules\data\models;


use app\components\Migration;
use RuntimeException;
use Yii;
use app\components\ArrayHelper;
use app\components\Formatter;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class DataAnalisa
 * @package app\modules\data\models
 *
 * @property \app\modules\data\models\Customers|\yii\db\ActiveQuery $customer
 * @property \yii\db\ActiveQuery|\app\modules\data\models\Pelanggan $pelanggan
 * @property \yii\db\ActiveQuery|\app\modules\data\models\Unit $unit
 * @property string $customerName
 * @property string $forCustomer
 * @property string $address
 * @property string $brand
 * @property string $unitLocation
 * @property string $unitNo
 * @property string $serialNumber
 * @property string $model
 * @property string $sampleDate
 * @property string $reportDate
 * @property string $analysisDate
 * @property string $receiveDate
 * @property \app\modules\data\models\Parameters[] $parameters
 * @property Parameters[]|array $paramByCol
 * @property string $reportType
 * @property string $status
 * @property string $statusPublish
 * @property string $rkNoDraft
 * @property string $rkNo
 * @property \app\modules\data\models\DataAnalisa[] $history
 * @property float|int $maxPage
 * @property int $numHistory
 * @property bool $isHistory
 * @property \app\modules\data\models\ParameterHasReference[] $allReference
 * @property \app\modules\data\models\Typical[] $allTypical
 * @property \app\modules\data\models\ParameterHasMatrix[] $allMatrix
 * @property \app\modules\data\models\ParameterHasOilMatrix[] $allOilMatrix
 * @property \app\modules\data\models\Component|\yii\db\ActiveQuery $component
 * @property string $componentName
 * @property bool $mpcShow
 * @property bool $chartShow
 * @property string $pathFileMpc
 * @property null|string $viewFileMpc
 * @property string $urlFileMpc
 * @property string $dirBerkas
 * @property string $typicalType
 * @property string $oilHourL
 * @property string $unitHourL
 * @property string $oilHour
 * @property string $unitHour
 * @property string $rowId
 * @property string $statusVerify
 * @property string $urlFileBst
 * @property string $urlFileFfp
 * @property string $urlFileFiltrat
 * @property string $pathFileBst
 * @property string $pathFileFfp
 * @property string $pathFileFfp2
 * @property string $pathFileFcs1
 * @property string $pathFileFcs2
 * @property string $pathFileFcs3
 * @property string $pathFileFiltrat
 * @property null|string $viewFileBst
 * @property bool $bstShow
 * @property bool $ffpShow
  * @property bool $fcsShow
 * @property bool $filtratShow
 * @property string $action
 */
class DataAnalisa extends \app\modelsDB\DataAnalisa
{

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'branch_no' => 'Customer Name',
            'CustomerID' => 'For Customer',
            'type_report_id' => 'Report Type',
            'model' => 'Eng. Type/Model',
            'componentName' => 'Eng. Type/Model',
            'brand' => 'Eng. Builder',
            'unitNo' => 'Unit No/SN',
            'unitLocation' => 'Eng Location',
            'hm_unit' => 'Hours on Unit',
            'hm_oil' => 'Hours on Oil',
            'mpc' => 'MPC',
            'spesifikasi_id' => 'Spesifikasi',
            'typicalType' => 'Typical',
            'statusPublish' => 'Published',
            'statusVerify' => 'Verified',
        ]);
    }

    public function rules()
    {
        $add = [];
        foreach ($this->parameters as $parameter) {
            if ($parameter->col_name != '') {
                $add[] = $parameter->col_name;
                $add[] = $parameter->col_name . '_code';
            }
        }
        return array_merge(parent::rules(), [
            [$add, 'safe'],
            ['lab_number', 'unique'],
        ]);
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return Html::tag('div',
            Html::tag('a', 'Action <span class="caret"></span>', [
                'class' => 'btn btn-default dropdown-toggle', 'data-toggle' => 'dropdown', 'aria-expanded' => 'false'
            ]) . Html::tag('ul',
                Html::tag('li', Html::tag('a', '<i class="fa fa-caret-square-o-right"></i> view',
                    ['href' => Url::toRoute(['/data/default/view', 'id' => $this->id])])) .
                Html::tag('li', Html::tag('a', '<i class="fa fa-file-pdf-o"></i> Draft', [
                    'class' => 'btn btn-flat btn-xs', 'title' => 'Download Report', 'target' => '_blank', 'style' => 'margin-left:10%;text-align: left;',
                    'href' => Url::to(['/data/report/pdf', 'lab_number' => $this->lab_number, 'draft' => 1])])) .
                Html::tag('li', Html::tag('a', '<i class="fa fa-file-pdf-o"></i> V.1.0', [
                    'class' => 'btn btn-flat btn-xs', 'title' => 'Download Report', 'target' => '_blank', 'style' => 'margin-left:10%;text-align: left;',
                    'href' => Url::to(['/data/report/pdf', 'lab_number' => $this->lab_number])])) .
                Html::tag('li', Html::tag('a', '<i class="fa fa-file-pdf-o"></i> V.1.0 Multi-Page (Draft)', [
                    'class' => 'btn btn-flat btn-xs', 'title' => 'Download Report', 'target' => '_blank', 'style' => 'margin-left:10%;text-align: left;',
                    'href' => Url::to(['/data/report/multi-page-pdf', 'lab_number' => $this->lab_number, 'draft' => 1])])) .
                Html::tag('li', Html::tag('a', '<i class="fa fa-file-pdf-o"></i> V.1.0 Multi-Page', [
                    'class' => 'btn btn-flat btn-xs', 'title' => 'Download Report', 'target' => '_blank', 'style' => 'margin-left:10%;text-align: left;',
                    'href' => Url::to(['/data/report/multi-page-pdf', 'lab_number' => $this->lab_number])])) .
                Html::tag('li', Html::tag('a', '<i class="fa fa-file-pdf-o"></i> V.1.1-mpc (draft)', [
                    'class' => 'btn btn-flat btn-xs', 'title' => 'Download Report', 'target' => '_blank', 'style' => 'margin-left:10%;text-align: left;',
                    'href' => Url::to(['/data/report/pdf', 'lab_number' => $this->lab_number, 'mpc_only' => 1, 'draft' => 1])])) .
                Html::tag('li', Html::tag('a', '<i class="fa fa-file-pdf-o"></i> V.1.1-mpc', [
                    'class' => 'btn btn-flat btn-xs', 'title' => 'Download Report', 'target' => '_blank', 'style' => 'margin-left:10%;text-align: left;',
                    'href' => Url::to(['/data/report/pdf', 'lab_number' => $this->lab_number, 'mpc_only' => 1])])) .
                Html::tag('li', Html::tag('a', '<i class="fa fa-file-excel-o"></i> Excel ', [
                    'class' => 'btn btn-flat btn-xs', 'title' => 'Download Excel', 'target' => '_blank', 'style' => 'margin-left:10%;text-align: left;',
                    'href' => Url::to(['/data/report/excel', 'lab_number' => $this->lab_number])])) .
                Html::tag('li', Html::tag('a', '<i class="fa fa-edit"></i> Edit',
                    ['href' => Url::toRoute(['/data/default/edit', 'id' => $this->id])])) .
                Html::tag('li', Html::tag('a', '<i class="fa fa-refresh"></i> Update Code', [
                    'href' => Url::toRoute(['/data/default/update-eval-code', 'lab_no' => $this->lab_number]), 'data-action' => 'update_code'])) .
                Html::tag('li', Html::tag('a', '<i class="fa fa-copy"></i> Duplicate',
                    ['href' => Url::toRoute(['/data/default/new', 'duplicate' => $this->id])])) .
                Html::tag('li', Html::tag('a', '<i class="fa fa-times"></i> Delete',
                    ['href' => Url::toRoute(['/data/default/del', 'id' => $this->id]),
                        'data-method' => 'post',
                        'data-confirm' => 'Are you absolutely sure ? You will lose all the information about this user with this action.',
                    ]))
                , ['class' => 'dropdown-menu']), ['class' => 'btn-group']);
    }

    /**
     * @return \yii\db\ActiveQuery|Customers
     */
    public function getCustomer()
    {
        return $this->hasOne(Customers::className(), ['branch_no' => 'branch_no'])->indexBy('branch_no');
    }

    /**
     * @return \yii\db\ActiveQuery|Pelanggan
     */
    public function getPelanggan()
    {
        return $this->hasOne(Pelanggan::className(), ['CustomerID' => 'CustomerID'])->indexBy('CustomerID');
    }

    /**
     * @return \yii\db\ActiveQuery|Unit
     */
    public function getUnit()
    {
        return $this->hasOne(Unit::className(), ['UnitID' => 'UnitID'])->indexBy('UnitID');
    }

    /**
     * @return \yii\db\ActiveQuery|Component
     */
    public function getComponent()
    {
        return $this->hasOne(Component::className(), ['ComponentID' => 'ComponentID'])->indexBy('ComponentID');
    }

    public function listReport()
    {
        return ArrayHelper::map(TypeReport::find()->where(['active' => 1])->all(), 'id', 'name');
    }

    public $no_lab_temp = '{count}/O/L/{y}';

    /**
     * @param $type
     * @return string
     * @throws \yii\db\Exception
     */
    public static function generateLabNumber($type)
    {
        $r_type = TypeReport::findOne(['id' => $type]);
        if (empty($r_type)) {
            return '';
        }
        $temp = $r_type->tempLabNo;
        $temp = str_replace(array('{y}', '{m}'), array(substr(date('Y'), 2, 2),
            Formatter::numberToRomanRepresentation(date('m'))), $temp);
        $coPos = strpos($temp, '{c');
        $clPos = strpos($temp, 't}') + 2;
        $first = $coPos > 0 ? substr($temp, 0, $coPos) : '';
        $last = substr($temp, $clPos);

        $p = self::find()->select(['MID(lab_number,' . ($coPos + 1) . ',5) as urut', 'lab_number'])
            ->where(" lab_number like '$first" . "_____$last'")
            ->orderBy('urut DESC')->createCommand()->queryOne();
        if (empty($p)) {
            return $first . "00001$last";
        }
        $int = (int)$p['urut'] + 1;
        return $first . str_pad($int, 5, '0', STR_PAD_LEFT) . $last;
    }


    public function listSpesifikasi()
    {
        return ArrayHelper::map(Spesifikasi::find()->all(), 'id', 'name');
    }

    public function listMatrix()
    {
        return ArrayHelper::map(Matrix::find()->all(), 'Matrix', 'Matrix');
    }

    public function listOilMatrix()
    {
        return ArrayHelper::map(OilMatrix::find()->all(), 'OIL_MATRIX', 'OIL_MATRIX');
    }

    public function listReference()
    {
        return ArrayHelper::map(Reference::find()->all(), 'id', 'name');
    }

    /** @var Parameters[] */
    public $_parameters;

    /**
     * @return Parameters[]
     */
    public function getParameters()
    {
        if ($this->_parameters == null) {
            $arrId = [];
            foreach ($this->renderParameter() as $item) {
                $arrId[] = $item->parameter_id;
                foreach ($item->parameters->child as $childParameter) {
                    $arrId[] = $childParameter->id;
                }
            }
            $this->_parameters = Parameters::findAll($arrId);
        }
        return $this->_parameters;
    }

    public $_rpt_param;

    /**
     * @return ParameterHasTypeReport[]
     */
    public function renderParameter()
    {
        if ($this->_rpt_param == null) {
            $query = ParameterHasTypeReport::find();
            $query->joinWith('parameters');
            $query->where(['type_report_id' => $this->type_report_id])
                ->andWhere('parameter.parent < 1 or parameter.parent is null')
                ->andWhere('parameter.active = 1');
            $this->_rpt_param = $query->indexBy('parameter_id')->orderBy('order')->all();
        }
        return $this->_rpt_param;
    }

    /**
     * @return string
     */
    public function getCustomerName()
    {
        return $this->customer == null ? '' : $this->customer->branch;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->customer == null ? '' : $this->customer->address;
    }

    /**
     * @return string
     */
    public function getForCustomer()
    {
        return $this->pelanggan == null ? '' : $this->pelanggan->Name;
    }

    /**
     * @return string
     */
    public function getUnitNo()
    {
        return $this->unit == null ? '' : $this->unit->UnitNo;
    }

    /**
     * @return string
     */
    public function getSerialNumber()
    {
        return $this->unit == null ? '' : $this->unit->SerialNo;
    }

    /**
     * @return string
     */
    public function getBrand()
    {
        return $this->unit == null ? '' : $this->unit->Brand;
    }

    /**
     * @return string
     */
    public function getModel()
    {
        return $this->unit == null ? '' : $this->unit->Model;
    }

    /**
     * @return string
     */
    public function getUnitLocation()
    {
        return $this->unit == null ? '' : $this->unit->UnitLocation;
    }

    /**
     * @return string
     */
    public function getComponentName()
    {
        return $this->component == null ? '' : $this->component->component;
    }

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function getSampleDate()
    {
        return Yii::$app->formatter->asDate($this->sampling_date);
    }

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function getReceiveDate()
    {
        return Yii::$app->formatter->asDate($this->received_date);
    }

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function getReportDate()
    {
        return Yii::$app->formatter->asDate($this->report_date);
    }

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function getAnalysisDate()
    {
        if ($this->start_analisa == '') {
            return '';
        }
        $en = '';
        if ($this->end_analisa != '') {
            $en = ' > ' . date('Y-m-d', strtotime($this->start_analisa));
        }
        return date('Y-m-d', strtotime($this->start_analisa)) . $en;
//        $formater = Yii::$app->formatter;
//        return $formater->asDate($this->start_analisa, 'd M') . '-'
//            . $formater->asDate($this->end_analisa, 'd M') . ' ' . $formater->asDate($this->start_analisa, 'Y');
    }

    /** @var Parameters */
    public $_param_by_col;

    /**
     * @return Parameters[]|array
     */
    public function getParamByCol()
    {
        if ($this->_param_by_col == null) {
            $this->_param_by_col = ArrayHelper::index($this->parameters, 'col_name');
        }
        return $this->_param_by_col;
    }

    /**
     * @param $attribute
     * @return string
     */
    public function getAttributeUnit($attribute)
    {
        if (isset($this->paramByCol[$attribute])) {
            return $this->paramByCol[$attribute]->unit;
        }
        return $this->generateAttributeLabel($attribute . '_unit');
    }

    /**
     * @param $attribute
     * @return string
     */
    public function getAttributeMethod($attribute)
    {
        if (isset($this->paramByCol[$attribute])) {
            return $this->paramByCol[$attribute]->method;
        }
        return $this->generateAttributeLabel($attribute . '_method');
    }

    /**
     * @param string $attribute
     * @return string
     */
    public function getAttributeLabel($attribute)
    {
        $footNote = '';
//        foreach ($this->getFootNote() as $key => $items) {
//            if (in_array($attribute, $items, true)) {
//                $footNote = $key;
//            }
//        }
        if (isset($this->paramByCol[$attribute])) {
            return $this->paramByCol[$attribute]->name . " $footNote";
        }
        return parent::getAttributeLabel($attribute) . " $footNote";
    }

    /** @var Typical[] */
    public $_typical_all;


    /**
     * @return Typical[]
     */
    public function getAllTypical()
    {
        if ($this->_typical_all == null) {
            $this->_typical_all = Typical::find()->where(['spesifikasi_id' => $this->spesifikasi_id])
                ->indexBy('new_oil_parameter_id')->all();
        }
        return $this->_typical_all;
    }

    /**
     * @param $parameter_id
     * @param string $temp
     * @return string
     */
    public function typical($parameter_id, $temp = '')
    {
        $n = isset($this->allTypical[$parameter_id]) ? $this->allTypical[$parameter_id]->value : '';
        return str_replace('{value}', $n, $temp);
    }


    /** @var ParameterHasMatrix[] */
    public $_matrix_all;


    /**
     * @return ParameterHasMatrix[]
     */
    public function getAllMatrix()
    {
        if ($this->_matrix_all == null) {
            $this->_matrix_all = ParameterHasMatrix::find()->where(['matrix' => $this->matrix])
                ->indexBy('parameter_id')->all();
        }
        return $this->_matrix_all;
    }

    /** @var ParameterHasOilMatrix[] */
    public $_oil_matrix_all;


    /**
     * @return ParameterHasOilMatrix[]
     */
    public function getAllOilMatrix()
    {
        if ($this->_oil_matrix_all == null) {
            $this->_oil_matrix_all = ParameterHasOilMatrix::find()->where(['OIL_MATRIX' => $this->OIL_MATRIX])
                ->indexBy('parameter_id')->all();
        }
        return $this->_oil_matrix_all;
    }

    /**
     * @param $parameter_id
     * @param string $temp
     * @return string
     */
    public function matrix($parameter_id, $temp = '')
    {
        $max = isset($this->allOilMatrix[$parameter_id]) ? $this->allOilMatrix[$parameter_id]->max : '';
        $min = isset($this->allOilMatrix[$parameter_id]) ? $this->allOilMatrix[$parameter_id]->min : '';
        $max = isset($this->allMatrix[$parameter_id]) ? $this->allMatrix[$parameter_id]->max : $max;
        $min = isset($this->allMatrix[$parameter_id]) ? $this->allMatrix[$parameter_id]->min : $min;
        return str_replace('{value}', $min, $temp) . str_replace('{value}', $max, $temp);
    }

    /** @var ParameterHasReference[] */
    public $_reference_all;

    /**
     * @return ParameterHasReference[]
     */
    public function getAllReference()
    {
        if ($this->_reference_all == null) {
            $this->_reference_all = ParameterHasReference::find()->where(['reference_id' => $this->reference_id])
                ->indexBy('parameter_id')->all();
        }
        return $this->_reference_all;
    }

    /**
     * @param $parameter_id
     * @param string $temp
     * @return string
     */
    public function reference($parameter_id, $temp = '')
    {
        $n = isset($this->allReference[$parameter_id]) ? $this->allReference[$parameter_id]->value : '';
        return str_replace('{value}', $n, $temp);
    }

    /**
     * @param $parameter_id
     * @param string $temp
     * @return string
     */
    public function renderLastCol($parameter_id, $temp = '<td class="text-center" style="font-size: 10px">{value}</td>')
    {
        $out = '';
        if ($this->typeReport->reference == 1) {
            $out .= $this->reference($parameter_id, $temp);
        }
        if ((string)$this->typeReport->limit_type == '1') {
            $out .= $this->matrix($parameter_id, $temp);
        }
        if ((string)$this->typeReport->limit_type == '2') {
            $out .= $this->typical($parameter_id, $temp);
        }
        return $out;
    }

    /**
     * @param Parameters $parameter
     * @return array|null
     */
    public function renderLimit($parameter)
    {
        if ((string)$this->typeReport->limit_type == '1') {
            return $this->renderMatrix($parameter);
        }
        if ((string)$this->typeReport->limit_type == '2') {
            return $this->renderTypical($parameter);
        }
        return null;
    }

    /**
     * @param Parameters $parameter
     * @return array
     */
    public function renderMatrix($parameter)
    {
        if ($parameter->matrix_type == 2) {
            $l = isset($this->allOilMatrix[$parameter->id]) ? $this->allOilMatrix[$parameter->id] : null;
            return [
                'Bmin' => $l == null ? '' : $l->Bmin,
                'Cmin' => $l == null ? '' : $l->Cmin,
                'Dmin' => $l == null ? '' : $l->Dmin,
                'Bmax' => $l == null ? '' : $l->Bmax,
                'Cmax' => $l == null ? '' : $l->Cmax,
                'Dmax' => $l == null ? '' : $l->Dmax,
            ];
        }
        if ($parameter->matrix_type == 1) {
            $l = isset($this->allMatrix[$parameter->id]) ? $this->allMatrix[$parameter->id] : null;
            return [
                'B' => $l == null ? '' : $l->B,
                'C' => $l == null ? '' : $l->C,
                'D' => $l == null ? '' : $l->D,
            ];
        }
        return [];
    }

    /**
     * @param Parameters $parameter
     * @return array
     */
    public function renderTypical($parameter)
    {
        $l = isset($this->allTypical[$parameter->id]) ? $this->allTypical[$parameter->id] : null;
        if ($l != null) {
            return [
                'B' => $l->condition2,
                'C' => $l->condition3,
                'D' => $l->condition4,
            ];
        }
        return [];
    }

    public $_tp;

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTypeReport()
    {
        if ($this->_tp == null) {
            $this->_tp = parent::getTypeReport();
        }
        return $this->_tp;
    }

    /**
     * @return string
     */
    public function getReportType()
    {
        return $this->typeReport == null ? '' : $this->typeReport->name;
    }

    /**
     * @return string
     */
    public function getRkNo()
    {
        return $this->typeReport == null ? '' : $this->typeReport->rk_no;
    }

    /**
     * @return string
     */
    public function getRkNoDraft()
    {
        return $this->typeReport == null ? '' : $this->typeReport->draft_rk_no;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        $code = $this->eval_code;
        $status = $this->getTextEvalCode($code);
        $label = '';
        if (($code == 'C') || ($code == 'U') || $code == 'D') {
            $label = 'label-danger';
        }
        if ($code == 'B' || $code == 'A') {
            $label = 'label-warning';
        }
        if ($code == 'N') {
            $label = 'label-success';
        }
        return $this->eval_code == '' ? '' : Html::tag('span', $status, ['class' => 'label ' . $label]);
    }

    /**
     * @param $code
     * @return string
     */
    public function getTextEvalCode($code)
    {
        $code = strtoupper($code);
        $eval = 'NORMAL';
        if ($code == 'D') {
            $eval = 'SEVERE';
        }
        if (($code == 'C') || ($code == 'U')) {
            $eval = 'URGENT';
        }
        if ($code == 'B' || $code == 'A') {
            $eval = 'ATTENTION';
        }
        if ($code == 'N') {
            $eval = 'NORMAL';
        }
        return $eval;
    }

    /**
     * @return string
     */
    public function getStatusPublish()
    {
        $button = "Unpublished <i class='fa fa-circle-o'></i>";
        $cls = 'warning';
        if ($this->publish != null) {
            $button = "Published <i class='fa fa-check'></i>";
            $cls = 'success';
        }
        return "<a href='" . Url::toRoute(['/data/default/update-status', 'id' => $this->lab_number]) .
            "' class='btn btn-$cls btn-xs' title='click to change status' data-action='update_status' >$button</a>";
    }

    /**
     * @return string
     */
    public function getStatusVerify()
    {
        $button = "Not Verified ";
        $cls = 'warning';
        if ($this->verify_date != null) {
            $button = "<i class='fa fa-check'></i> Verified ";
            $cls = 'success';
        }
        return "<a href='" . Url::toRoute(['/data/default/verify-status', 'id' => $this->lab_number]) .
            "' class='btn btn-$cls btn-xs' title='click to change status' data-action='update_status' >$button</a>";
    }

    public $_history;

    public $offset = 0;
    public $limit = 5;

    public $page = 1;
    public $_numHistory;

    /**
     * @return self[]
     */
    public function getHistory()
    {
        if ($this->_history == null) {
            if ($this->page > 1) {
                $this->offset = $this->limit * ($this->page - 1);
            }
            $history = ['CustomerID' => $this->CustomerID];
            if ($this->UnitID != '') {
                $history = ['UnitID' => $this->UnitID,];
            }
            if ($this->ComponentID != '') {
                $history = ['ComponentID' => $this->ComponentID, 'UnitID' => $this->UnitID,];
            }
            $tgl = 'report_date';
            if ($this->received_date != '') {
                $tgl = 'received_date';
            }
            if ($this->sampling_date != '') {
                $tgl = 'sampling_date';
            }

            $filter = self::find()->orderBy("$tgl DESC")->select(['lab_number'])
                ->where($history)
                ->andWhere("$tgl <= '" . $this->$tgl . "'")
                ->limit($this->limit)->offset($this->offset)->all();
            $filter = $filter == null ? ['lab_number' => 0] : $filter;
            $filter = ArrayHelper::getColumn($filter, 'lab_number');
            $query = self::find();
            $query->orderBy(self::tableName() . ".$tgl ASC");
            $query->where(['IN', self::tableName() . '.lab_number', $filter]);
            $this->_history = $query->all();
        }
        return $this->_history;
    }

    /**
     * @return int
     */
    public function getNumHistory()
    {
        if ($this->_numHistory == null) {
            $history = ['CustomerID' => $this->CustomerID];
            if ($this->UnitID != '') {
                $history = ['UnitID' => $this->UnitID,];
            }
            if ($this->ComponentID != '') {
                $history = ['ComponentID' => $this->ComponentID, 'UnitID' => $this->UnitID,];
            }
            $tgl = 'report_date';
            if ($this->received_date != '') {
                $tgl = 'received_date';
            }
            if ($this->sampling_date != '') {
                $tgl = 'sampling_date';
            }

            $this->_numHistory = self::find()->orderBy("$tgl DESC")->select(['lab_number'])
                ->where($history)
                ->andWhere("$tgl <= '$this->$tgl'")->count();
        }
        return $this->_numHistory;
    }

    /**
     * @return float|int
     */
    public function getMaxPage()
    {
        return floor($this->numHistory / $this->limit) + 1;
    }

    /**
     * @return bool
     */
    public function getIsHistory()
    {
        return $this->typeReport == null ? false : ($this->typeReport->history == 1);
    }

    private $_chS;
    private $_mpS;

    /**
     * @return bool
     */
    public function getChartShow()
    {
        if ($this->_chS === null) {
            $this->_chS = $this->typeReport == null ? false : ($this->typeReport->chart == 1);
        }
        return $this->_chS;
    }

    /**
     * @return bool
     */
    public function getMpcShow()
    {
        if ($this->_mpS === null) {
            $this->_mpS = $this->typeReport == null ? false : ($this->typeReport->mpc == 1);
        }
        return $this->_mpS;
    }

    /**
     * @param $value
     */
    public function setChartShow($value)
    {
        $this->_chS = $value;
    }

    /**
     * @param $value
     */
    public function setMpcShow($value)
    {
        $this->_mpS = $value;
    }

    public function getDirBerkas()
    {
        $dir = Yii::getAlias('@uploaddir');
        if (!file_exists($dir . '/report') && !mkdir($concurrentDirectory = $dir . '/report', 0777, true) && !is_dir($concurrentDirectory)) {
            throw new RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
        }
        $dirFile = $dir . '/report/' . 'others';
        if (!file_exists($dirFile) && !mkdir($dirFile, 0777, true) && !is_dir($dirFile)) {
            throw new RuntimeException(sprintf('Directory "%s" was not created', $dirFile));
        }

        return $dirFile;
    }

    /**
     * @return string
     */
    public function getPathFileMpc()
    {
        return $this->dirBerkas . '/' . $this->mpc_pic;
    }

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function getUrlFileMpc()
    {
        Yii::$app->assetManager->publish($this->dirBerkas);
        return Yii::$app->assetManager->getPublishedUrl($this->dirBerkas) . '/' . $this->mpc_pic;
    }

    /**
     * @return null|string
     */
    public function getViewFileMpc()
    {
        if ($this->urlFileMpc == null) {
            return null;
        }

        return Html::a('Lihat File', $this->urlFileMpc, ['class' => 'btn btn-default ', 'target' => '_blank']);
    }

    public function getTypicalType()
    {
        return $this->spesifikasi == null ? '' : $this->spesifikasi->name;
    }

    public $_unit_hour;
    public $_unit_hour_l;
    public $_oil_hour;
    public $_oil_hour_l;

    /**
     * @return string
     */
    public function getUnitHour()
    {
        if ($this->_unit_hour === null) {
            $t = explode(' ', $this->hm_unit);
            if (is_array($t)) {
                $this->_unit_hour = isset($t[0]) ? $t[0] : '';
            }
        }
        return $this->_unit_hour;
    }

    /**
     * @param string $unit_hour
     */
    public function setUnitHour($unit_hour)
    {
        $this->_unit_hour = str_replace(' ', '', $unit_hour);
    }

    /**
     * @return string
     */
    public function getUnitHourL()
    {
        if ($this->_unit_hour_l === null) {
            $t = explode(' ', $this->hm_unit);
            if (is_array($t)) {
                $this->_unit_hour_l = isset($t[1]) ? $t[1] : '';
            }
        }
        $this->_unit_hour_l = $this->_unit_hour_l == 'Jam' ? 'Hour' : $this->_unit_hour_l;
        $this->_unit_hour_l = $this->_unit_hour_l == '' ? '01' : $this->_unit_hour_l;
        return $this->_unit_hour_l;
    }

    /**
     * @param string $unit_hour_l
     */
    public function setUnitHourL($unit_hour_l)
    {
        if ($unit_hour_l === '01') {
            $unit_hour_l = '';
        }
        $this->_unit_hour_l = $unit_hour_l;
    }

    /**
     * @return string
     */
    public function getOilHour()
    {
        if ($this->_oil_hour === null) {
            $t = explode(' ', $this->hm_oil);
            if (is_array($t)) {
                $this->_oil_hour = isset($t[0]) ? $t[0] : '';
            }
            if (str_replace(' ', '', $this->hm_oil) === 'NEWOIL') {
                $this->_oil_hour = '';
            }
        }
        return $this->_oil_hour;
    }

    /**
     * @param string $oil_hour
     */
    public function setOilHour($oil_hour)
    {
        $this->_oil_hour = str_replace(' ', '', $oil_hour);
    }

    /**
     * @return string
     */
    public function getOilHourL()
    {
        if ($this->_oil_hour_l === null) {
            $t = explode(' ', $this->hm_oil);
            if (is_array($t)) {
                $t2 = isset($t[2]) ? " $t[2]" : '';
                $this->_oil_hour_l = isset($t[1]) ? $t[1] . $t2 : '';
            }
            if (str_replace(' ', '', $this->hm_oil) === 'NEWOIL') {
                $this->_oil_hour_l = 'NEW OIL';
            }
            $this->_oil_hour_l = $this->_oil_hour_l == 'Jam' ? 'Hour' : $this->_oil_hour_l;
            $this->_oil_hour_l = $this->_oil_hour_l == '' ? '01' : $this->_oil_hour_l;
        }
        return $this->_oil_hour_l;
    }

    /**
     * @param string $oil_hour_l
     */
    public function setOilHourL($oil_hour_l)
    {
        if ($oil_hour_l === '01') {
            $oil_hour_l = '';
        }
        $this->_oil_hour_l = $oil_hour_l;
    }

    public function getRowId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getPathFileBst()
    {
        $file = $this->dirBerkas . '/bst_' . $this->rowId . '.jpg';
        if (file_exists($file)) {
            return $file;
        }
        return '';
    }

    /**
     * @return string|null
     * @throws \yii\base\InvalidConfigException
     */
    public function getUrlFileBst()
    {
        if ($this->pathFileBst == '') {
            return null;
        }
        Yii::$app->assetManager->publish($this->dirBerkas);
        return Yii::$app->assetManager->getPublishedUrl($this->dirBerkas) . '/bst_' . $this->rowId . '.jpg';
    }

    /**
     * @return null|string
     */
    public function getViewFileBst()
    {
        if ($this->urlFileBst == null) {
            return null;
        }

        return Html::a('Lihat File', $this->urlFileBst, ['class' => 'btn btn-default ', 'target' => '_blank']);
    }

    protected $_bst;

    /**
     * @return bool
     */
    public function getBstShow()
    {
        if ($this->_bst === null) {
            $this->_bst = $this->typeReport == null ? false : ($this->typeReport->blotter_spot_test == 1);
        }
        return $this->_bst;
    }

    /**
     * @return string
     */
    public function getPathFileFfp()
    {
        $file = $this->dirBerkas . '/ffp_' . $this->rowId . '.jpg';
        if (file_exists($file)) {
            return $file;
        }
        return '';
    }

    /**
     * @return string
     */
    public function getPathFileFfp2()
    {
        $file = $this->dirBerkas . '/ffp2_' . $this->rowId . '.jpg';
        if (file_exists($file)) {
            return $file;
        }
        return '';
    }

    /**
     * @return string
     */
    public function getPathFileFiltrat()
    {
        $file = $this->dirBerkas . '/filtrat_' . $this->rowId . '.jpg';
        if (file_exists($file)) {
            return $file;
        }
        return '';
    }


    /**
     * @return string|null
     * @throws \yii\base\InvalidConfigException
     */
    public function getUrlFileFfp()
    {
        if ($this->pathFileFfp == '') {
            return null;
        }
        Yii::$app->assetManager->publish($this->dirBerkas);
        return Yii::$app->assetManager->getPublishedUrl($this->dirBerkas) . '/ffp_' . $this->rowId . '.jpg';
    }

    /**
     * @return string|null
     * @throws \yii\base\InvalidConfigException
     */
    public function getUrlFileFfp2()
    {
        if ($this->pathFileFfp == '') {
            return null;
        }
        Yii::$app->assetManager->publish($this->dirBerkas);
        return Yii::$app->assetManager->getPublishedUrl($this->dirBerkas) . '/ffp2_' . $this->rowId . '.jpg';
    }

    /**
     * @return string|null
     * @throws \yii\base\InvalidConfigException
     */
    public function getUrlFileFiltrat()
    {
        if ($this->pathFileFiltrat == '') {
            return null;
        }
        Yii::$app->assetManager->publish($this->dirBerkas);
        return Yii::$app->assetManager->getPublishedUrl($this->dirBerkas) . '/filtrat_' . $this->rowId . '.jpg';
    }


    /**
     * @return null|string
     */
    public function getViewFileFfp()
    {
        if ($this->urlFileFfp == null) {
            return null;
        }

        return Html::a('Lihat File', $this->urlFileFfp, ['class' => 'btn btn-default ', 'target' => '_blank']);
    }

    /**
     * @return null|string
     */
    public function getViewFileFiltrat()
    {
        if ($this->urlFileFiltrat == null) {
            return null;
        }

        return Html::a('Lihat File', $this->urlFileFiltrat, ['class' => 'btn btn-default ', 'target' => '_blank']);
    }


    protected $_ffp;

    /**
     * @return bool
     */
    public function getFfpShow()
    {
        if ($this->_ffp === null) {
            $this->_ffp = $this->typeReport == null ? false : ($this->typeReport->ffp == 1);
        }
        return $this->_ffp;
    }

    protected $_fcs;
    /**
     * @return bool
     */
    public function getFcsShow()
    {
        $this->_fcs = $this->typeReport->id == 121 ? true : false;
        return $this->_fcs;
    }

    /**
     * @return string
     */
    public function getPathFileFcs1()
    {
        $file = $this->dirBerkas . '/fcs1_' . $this->rowId . '.jpg';
        if (file_exists($file)) {
            return $file;
        }
        return '';
    }

    /**
     * @return string
     */
    public function getPathFileFcs2()
    {
        $file = $this->dirBerkas . '/fcs2_' . $this->rowId . '.jpg';
        if (file_exists($file)) {
            return $file;
        }
        return '';
    }

    /**
     * @return string
     */
    public function getPathFileFcs3()
    {
        $file = $this->dirBerkas . '/fcs3_' . $this->rowId . '.jpg';
        if (file_exists($file)) {
            return $file;
        }
        return '';
    }

    /**
     * @return string|null
     * @throws \yii\base\InvalidConfigException
     */
    public function getUrlFileFcs1()
    {
        if ($this->pathFileFcs1 == '') {
            return null;
        }
        Yii::$app->assetManager->publish($this->dirBerkas);
        return Yii::$app->assetManager->getPublishedUrl($this->dirBerkas) . '/fcs1_' . $this->rowId . '.jpg';
    }

    /**
     * @return string|null
     * @throws \yii\base\InvalidConfigException
     */
    public function getUrlFileFcs2()
    {
        if ($this->pathFileFcs2 == '') {
            return null;
        }
        Yii::$app->assetManager->publish($this->dirBerkas);
        return Yii::$app->assetManager->getPublishedUrl($this->dirBerkas) . '/Fcs2_' . $this->rowId . '.jpg';
    }

    /**
     * @return string|null
     * @throws \yii\base\InvalidConfigException
     */
    public function getUrlFileFcs3()
    {
        if ($this->pathFileFcs3 == '') {
            return null;
        }
        Yii::$app->assetManager->publish($this->dirBerkas);
        return Yii::$app->assetManager->getPublishedUrl($this->dirBerkas) . '/Fcs3_' . $this->rowId . '.jpg';
    }

    //url12

    /**
     * @return null|string
     */
    public function getViewFileFcs1()
    {
        if ($this->urlFileFcs1 == null) {
            return null;
        }

        return Html::a('Lihat File', $this->urlFileFcs1, ['class' => 'btn btn-default ', 'target' => '_blank']);
    }

    /**
     * @return null|string
     */
    public function getViewFileFcs2()
    {
        if ($this->urlFileFcs2 == null) {
            return null;
        }

        return Html::a('Lihat File', $this->urlFileFcs2, ['class' => 'btn btn-default ', 'target' => '_blank']);
    }

    /**
     * @return null|string
     */
    public function getViewFileFcs3()
    {
        if ($this->urlFileFcs3 == null) {
            return null;
        }

        return Html::a('Lihat File', $this->urlFileFcs3, ['class' => 'btn btn-default ', 'target' => '_blank']);
    }


    /**
     * @return string
     */
    public function getPathFilePtest1()
    {
        $file = $this->dirBerkas . '/ptest1_' . $this->rowId . '.jpg';
        if (file_exists($file)) {
            return $file;
        }
        return '';
    }

    /**
     * @return string
     */
    public function getPathFilePtest2()
    {
        $file = $this->dirBerkas . '/ptest2_' . $this->rowId . '.jpg';
        if (file_exists($file)) {
            return $file;
        }
        return '';
    }
    //fpatch1
    //fpatch2
    protected $_filtrat;

    /**
     * @return bool
     */
    public function getFiltratShow()
    {
        if ($this->_filtrat === null) {
            $this->_filtrat = $this->typeReport == null ? false : ($this->typeReport->filtrat == 1);
        }
        return $this->_filtrat;
    }

    /**
     * @return string|null
     * @throws \yii\base\InvalidConfigException
     */
    public function getUrlFilePtest1()
    {
        if ($this->pathFilePtest1 == '') {
            return null;
        }
        Yii::$app->assetManager->publish($this->dirBerkas);
        return Yii::$app->assetManager->getPublishedUrl($this->dirBerkas) . '/ptest1_' . $this->rowId . '.jpg';
    }

    /**
     * @return string|null
     * @throws \yii\base\InvalidConfigException
     */
    public function getUrlFilePtest2()
    {
        if ($this->pathFilePtest2 == '') {
            return null;
        }
        Yii::$app->assetManager->publish($this->dirBerkas);
        return Yii::$app->assetManager->getPublishedUrl($this->dirBerkas) . '/ptest2_' . $this->rowId . '.jpg';
    }

    //url12

    /**
     * @return null|string
     */
    public function getViewFilePtest1()
    {
        if ($this->urlFilePtest1 == null) {
            return null;
        }

        return Html::a('Lihat File', $this->urlFilePtest1, ['class' => 'btn btn-default ', 'target' => '_blank']);
    }

    /**
     * @return null|string
     */
    public function getViewFilePtest2()
    {
        if ($this->urlFilePtest2 == null) {
            return null;
        }

        return Html::a('Lihat File', $this->urlFilePtest2, ['class' => 'btn btn-default ', 'target' => '_blank']);
    }

    //show12

    protected $_ptest;

    /**
     * @return bool
     */
    public function getPtestShow()
    {
        if ($this->_ptest === null) {
            $this->_ptest = $this->typeReport == null ? false : ($this->typeReport->ptest == 1);
        }
        return $this->_ptest;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDataAnalisaHasParameters()
    {
        return $this->hasMany(DataAnalisaHasParameter::className(), ['data_analisa_id' => 'id']);
    }

    /** @var [] */
    public $_parms;

    /**
     * @param string $name
     * @return bool
     */
    public function hasAttribute($name)
    {
        if ($this->_parms == null) {
            $this->_parms = Parameters::find()->select(['id', 'col_name'])
                ->where("col_name <> '' or col_name is not null")->noCache()
                ->distinct()->indexBy('col_name')->all();
        }
        if (isset($this->_parms[$name])) {
            return true;
        }
        return parent::hasAttribute($name);
    }

    /**
     * add attribute from parameter
     *
     * @return array
     */
    public function attributes()
    {
        if ($this->_parms == null) {
            $this->_parms = Parameters::find()->select(['id', 'col_name'])
                ->where("col_name <> '' or col_name is not null")->noCache()
                ->distinct()->indexBy('col_name')->all();
        }
        return array_merge(parent::attributes(), array_keys($this->_parms));
    }

    /** @var array|DataAnalisaHasParameter[] */
    public $_temp_value = [];

    /**
     * @return DataAnalisaHasParameter[]|array|\yii\db\ActiveRecord[]
     */
    public function temp_value()
    {
        if ($this->_temp_value == []) {
            $this->_temp_value = DataAnalisaHasParameter::find()->where(['data_analisa_id' => $this->id])->indexBy('col_name')->all();
        }
        return $this->_temp_value;
    }

    /**
     * init
     */
    public function init()
    {
        parent::init();
        $this->attributes();
    }


    /**
     * @param array $data
     * @param null $formName
     * @return bool
     */
    public function load($data, $formName = null)
    {
        return parent::load($data, $formName);
    }

    /**
     * @param bool $runValidation
     * @param null $attributeNames
     * @return bool
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        if (parent::save($runValidation, $attributeNames)) {
            foreach ($this->temp_value() as $i => $item) {
                $item->data_analisa_id = $this->id;
                if ($item->isNewRecord && $item->value == '') {
                    continue;
                }
                if (!$item->save()) {
                    $this->addError($i, ArrayHelper::toString($item->errors));
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    /**
     * @param string $name
     * @return mixed|null
     */
    public function __get($name)
    {
        $code = false;
        $col_name = $name;
        if ((strpos($name, '_code') !== false) && !in_array($name, ['iso_code__4406', 'iso_code_4406', 'eval_code'], true)) {
            if ($name == 'iso_code__4406_code') {
                $col_name = 'iso_code__4406';
            } elseif ($name == 'iso_code_4406_code') {
                $col_name = 'iso_code_4406';
            } else {
                $col_name = str_replace('_code', '', $name);
            }
            $code = true;
        }

        if (isset($this->_parms[$col_name]) && $this->_parms[$col_name] != '') {
            $tp = $this->temp_value();
            if (isset($tp[$col_name])) {
                if ($code) {
                    return $tp[$col_name]->code;
                }
                return $tp[$col_name]->value;
            }

            if ($this->_parms[$col_name]->id > 0) {
                return '';
            }

            if (in_array($col_name, ['anti_rust_as_nitrite_in_house_methode', 'nitrite', 'sulfite', 'free_fatty_acid', 'four_ball_epastm_d_2596', 'toluene_soluble_astm_d89312', 'test', 'testtttt', 'total_ruller', 'sulphur_content_02', 'parafin00', 'napthane00', 'aromatic02', 'unknown00', 'olefin00'], true)) {
                return '';
            }
            if ($code) {
                return '';
            }


//            $migration = new Migration();
//
//            if ($migration->columnExists(self::tableName(), $name)) {
////            $a = self::find()->where(['like', 'col_name', $name])->count();
////            $a /= 2;
////            $num = str_pad((int)$a, 2, '0', STR_PAD_LEFT);
//
//                return '';
//            }
//
////            if ($this->_list_col == null) {
////                $this->_list_col = Yii::$app->db->createCommand("select COLUMN_NAME
////              from information_schema.columns
////             where table_schema = 'others_npmp12'
////               and table_name = 'data_analisa'")
////                    ->queryAll();
////
//////            print_r();die;
////            }
////            if (!in_array($name, ArrayHelper::getColumn($this->_list_col, 'COLUMN_NAME'), true)) {
////                return '';
////            }
        }
        return parent::__get($name);
    }

    public $_list_col;

    public function __set($name, $value)
    {
        $code = false;
        $col_name = $name;
        if ((strpos($name, '_code') !== false) && !in_array($name, ['iso_code__4406', 'iso_code_4406', 'eval_code'], true)) {
            if ($name == 'iso_code__4406_code') {
                $col_name = 'iso_code__4406';
            } elseif ($name == 'iso_code_4406_code') {
                $col_name = 'iso_code_4406';
            } else {
                $col_name = str_replace('_code', '', $name);
            }
            $code = true;
        }

        if ($col_name != '' && isset($this->_parms[$col_name])) {
            $tp = $this->temp_value();

            //$pt = DataAnalisaHasParameter::find()->where(['data_analisa_id' => $this->id, 'col_name' => $col_name])->one();
            if ((string)$value === '' && !isset($tp[$col_name])) {
                $value = null;
            }
            $pt = isset($tp[$col_name]) ? $tp[$col_name] : null;
            if ($value !== null || $pt != null) {
                if (!isset($this->_temp_value[$col_name])) {
                    if ($pt == null) {
                        $pt = new DataAnalisaHasParameter(['data_analisa_id' => $this->id, 'col_name' => $col_name]);
                    }
                    $this->_temp_value[$col_name] = $pt;
                }

                if ($code) {
                    $this->_temp_value[$col_name]->code = $value;
                } else {
                    $this->_temp_value[$col_name]->value = $value;
                }
            }
        } else {
            parent::__set($name, $value);
        }

    }


    /**
     * @param int $start
     * @param int $end
     * @return array
     */
    public static function conversionData($start, $end)
    {
        if ($start == null) {
            $start = 1;
        }
        if ($end == null) {
            $end = 100;
        }
        $all = self::find()->andWhere("id < $end and id >= $start")->noCache()->all();
        $out = [];
        $param = Parameters::find()->select(['col_name'])
            ->where("col_name <> '' or col_name is not null")
            ->noCache()
            ->distinct()->indexBy('col_name')->all();
        foreach ($all as $ip) {
            $item = $ip->toArray();
            foreach ($param as $p => $p_object) {
                if ($p != '') {
                    $c = $p . '_code';
                    $ip->$p = isset($item[$p]) && $item[$p] != '' ? $item[$p] : null;
                    $ip->$c = isset($item[$c]) && $item[$c] != '' ? $item[$c] : null;
                }
            }
            $out[$ip->id] = $ip->save() ? 'success' : ['error' => $ip->errors];
        }
        return $out;
    }


}
