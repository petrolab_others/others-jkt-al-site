<?php
/**
 * Created by
 * User: Wisard17
 * Date: 25/02/2019
 * Time: 10.10 AM
 */

namespace app\modules\data\models;


use app\modelsDB\Parameter;
use yii\base\Exception;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class Parameters
 * @package app\modules\data\models
 *
 * @property string $action
 * @property \app\modules\data\models\ChildParameter $parentModel
 * @property string $namePParent
 * @property \app\modules\data\models\ChildParameter[] $child
 */
class Parameters extends Parameter
{

    /**
     * @return string
     */
    public function getAction()
    {
        return Html::tag('div',
            Html::tag('a', 'Action <span class="caret"></span>', [
                'class' => 'btn btn-default dropdown-toggle', 'data-toggle' => 'dropdown', 'aria-expanded' => 'false'
            ]) . Html::tag('ul',
//                Html::tag('li', Html::tag('a', '<i class="fa fa-caret-square-o-right"></i> view',
//                    ['href' => Url::toRoute(['/data/parameter/view', 'id' => $this->id])])) .
                Html::tag('li', Html::tag('a', '<i class="fa fa-edit"></i> Edit',
                    ['href' => Url::toRoute(['/data/parameter/edit', 'id' => $this->id])])) .
                Html::tag('li', Html::tag('a', '<i class="fa fa-copy"></i> Duplicate',
                    ['href' => Url::toRoute(['/data/parameter/new', 'duplicate' => $this->id])])) .
                Html::tag('li', Html::tag('a', '<i class="fa fa-times"></i> Delete',
                    ['href' => Url::toRoute(['/data/parameter/del', 'id' => $this->id]),
                        'data-method' => 'post',
                        'data-confirm' => 'Are you absolutely sure ? You will lose all the information about this user with this action.',
                    ]))
                , ['class' => 'dropdown-menu']), ['class' => 'btn-group']);
    }

    /**
     * @return \yii\db\ActiveQuery|ChildParameter[]
     * @throws \yii\base\InvalidConfigException
     */
    public function getChild()
    {
        return $this->hasMany(ChildParameter::className(), ['id' => 'child'])->andWhere(['active' => 1])
            ->viaTable('parameter_child', ['parent' => 'id']);
//        return $this->hasMany(ChildParameter::className(), ['parent' => 'id'])->indexBy('id')->andWhere(['active' => 1]);
    }

    /**
     * @return \yii\db\ActiveQuery|ChildParameter
     * @throws \yii\base\InvalidConfigException
     */
    public function getParentModel()
    {
        return $this->hasOne(ChildParameter::className(), ['id' => 'parent'])->andWhere(['active' => 1])
            ->viaTable('parameter_child', ['child' => 'id']);
        //return $this->hasOne(ChildParameter::className(), ['id' => 'parent'])->indexBy('id')->andWhere(['active' => 1]);
    }

    /**
     * @param DataAnalisa $model
     * @return bool
     */
    public function checkValue($model)
    {
        foreach ($this->child as $item) {
            $col = $item->col_name;
            try {
                if ($model->$col != '') {
                    return true;
                }
            } catch (Exception $e) {

            }
        }
        return false;
    }

    /**
     * @return string
     */
    public function getNamePParent()
    {
        return $this->parentModel == null ? $this->name : $this->parentModel->name . '-' . $this->name;
    }

    public static function migrationParent()
    {
        $model = self::find()->all();
        foreach ($model as $item) {
            if ((float)$item->parent > 0) {
                $param = ['parent' => $item->parent, 'child' => $item->id];
                $p = ParameterChild::findOne($param);
                if ($p == null) {
                    $p = new ParameterChild($param);
                    if ($p->save()) {
                        echo 'save';
                    } else {
                        echo 'error';
                    }
                }
            }
        }

    }
}