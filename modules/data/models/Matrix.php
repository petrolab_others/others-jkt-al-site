<?php
/**
 * Created by
 * User: Wisard17
 * Date: 05/04/2019
 * Time: 02.17 PM
 */

namespace app\modules\data\models;


use app\modelsDB\TblWearFtirMatrix;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class Matrix
 * @package app\modules\data\models
 *
 *
 * @property \app\modules\data\models\ParameterHasMatrix[] $parameterHasMatrix
 * @property string $action
 * @property \yii\db\ActiveQuery $parameterHasMatrixs
 * @property string $id
 * @property string $allParameter
 */
class Matrix extends TblWearFtirMatrix
{

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            ['Matrix', 'unique'],
        ]);
    }

    public function getId()
    {
        return $this->Matrix;
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return Html::tag('div',
            Html::tag('a', 'Action <span class="caret"></span>', [
                'class' => 'btn btn-default dropdown-toggle', 'data-toggle' => 'dropdown', 'aria-expanded' => 'false'
            ]) . Html::tag('ul',
//                Html::tag('li', Html::tag('a', '<i class="fa fa-caret-square-o-right"></i> view',
//                    ['href' => Url::toRoute(['/data/matrix/view', 'id' => $this->Matrix])])) .
                Html::tag('li', Html::tag('a', '<i class="fa fa-edit"></i> Edit',
                    ['href' => Url::toRoute(['/data/matrix/edit', 'id' => $this->Matrix])])) .
                Html::tag('li', Html::tag('a', '<i class="fa fa-copy"></i> Duplicate',
                    ['href' => Url::toRoute(['/data/matrix/new', 'duplicate' => $this->id])])) .
                Html::tag('li', Html::tag('a', '<i class="fa fa-times"></i> Delete',
                    ['href' => Url::toRoute(['/data/matrix/del', 'id' => $this->Matrix]),
                        'data-method' => 'post',
                        'data-confirm' => 'Are you absolutely sure ? You will lose all the information about this user with this action.',
                    ]))
                , ['class' => 'dropdown-menu']), ['class' => 'btn-group']);
    }

    /** @var ParameterHasMatrix[] */
    public $_parameterHasMatrix;

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParameterHasMatrixs()
    {
        return $this->hasMany(ParameterHasMatrix::className(), ['matrix' => 'Matrix']);
    }

    /**
     * @return ParameterHasMatrix[]
     */
    public function getParameterHasMatrix()
    {
        if ($this->_parameterHasMatrix == null) {
            $this->_parameterHasMatrix = $this->parameterHasMatrixs;
        }
        return $this->_parameterHasMatrix;
    }

    public $_ap;

    public function getAllParameter()
    {
        if ($this->_ap == null) {
            $out = '';
            foreach ($this->parameterHasMatrix as $hasMatrix) {
                $out .= Html::tag('li', $hasMatrix->parameter->name . ' : ' . $hasMatrix->min . '-' . $hasMatrix->max);
            }
            $this->_ap = Html::tag('ul', $out);
        }
        return $this->_ap;
    }
}