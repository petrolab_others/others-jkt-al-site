<?php
/**
 * Created by
 * User: Wisard17
 * Date: 02/03/2019
 * Time: 10.20 AM
 */

namespace app\modules\data\models;

use Exception;
use Yii;
use yii\web\UploadedFile;

/**
 * Class FormData
 * @package app\modules\data\models
 *
 * @property bool $wizard
 */
class FormData extends DataAnalisa
{

    /** @var UploadedFile */
    public $pic;

    /** @var UploadedFile */
    public $blotter;
    public $ftir;
    public $ftir2;
    public $ptest1;
    public $ptest2;
    public $filtrat;
    public $fcs1, $fcs2, $fcs3;

    public function rules()
    {
        return array_merge(parent::rules(), [
            [['type_report_id',], 'required'],
            [['unitHourL','oilHourL','unitHour','oilHour'], 'safe'],
            // [['ftir', 'ftir2'], 'file', 'skipOnEmpty' => true,'maxSize'=>1024 * 1024 * 8, 'extensions'=> 'png, jpg, jpeg'],
        ]);
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'pic' => 'Upload MPC',
            'ftir' => 'FTIR file 1',
            'ftir2' => 'FTIR file 2',
            'fcs1' => 'Sequence I',
            'fcs2' => 'Sequence II',
            'fcs3' => 'Sequence III',
        ]);
    }

    public $_wiz = false;

    public function getWizard()
    {
        return $this->_wiz;
    }

    public function load($data, $formName = null)
    {
        if (parent::load($data, $formName)) {
            $this->pic = UploadedFile::getInstance($this, 'pic');
            $this->blotter = UploadedFile::getInstance($this, 'blotter');
            $this->ftir = UploadedFile::getInstance($this, 'ftir');
            $this->ftir2 = UploadedFile::getInstance($this, 'ftir2');
            $this->ptest1 = UploadedFile::getInstance($this, 'ptest1');
            $this->ptest2 = UploadedFile::getInstance($this, 'ptest2');
            $this->filtrat = UploadedFile::getInstance($this, 'filtrat');
            $this->fcs1 = UploadedFile::getInstance($this, 'fcs1');
            $this->fcs2 = UploadedFile::getInstance($this, 'fcs2');
            $this->fcs3 = UploadedFile::getInstance($this, 'fcs3');
            $unitL = $this->unitHourL == '01' ? '' : $this->unitHourL;
            $oilL = $this->oilHourL == '01' ? '' : $this->oilHourL;
            $this->hm_unit = $this->unitHour !== '' ? $this->unitHour . ' ' . $unitL : null;
            $this->hm_oil = $this->oilHour !== '' ? $this->oilHour . ' ' . $oilL :
                ($oilL == 'NEW OIL' ? $oilL : null);
            return true;
        }
        return false;
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->verify_date != '') {
                $this->addError('statusVerify', 'Data telah terkunci silahkan hubungi manager');
                return false;
            }
            if ($this->publish != '') {
                $this->addError('statusPublish', 'Data telah dipublish, silahkan unpublished terlebih dahulu');
                return false;
            }
            return true;
        }
        return false;
    }

    /**
     * @param bool $runValidation
     * @param null $attributeNames
     * @return bool
     * @throws Exception
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            if ($this->pic != null) {
                $filename = 'mpc_' . str_replace(['/', '\\', ':', '|'], '_', $this->lab_number) . '_' . time();
                if ($this->pic->saveAs($this->dirBerkas . "/$filename." . $this->pic->extension)) {
                    $this->mpc_pic = "$filename." . $this->pic->extension;
                }
            }
            if ($this->blotter != null) {
                $filename = 'bst_' . $this->rowId;
                if ($this->blotter->saveAs($this->dirBerkas . "/$filename.jpg")) {

                }
            }
            if ($this->ftir != null) {
                $filename = 'ffp_' . $this->rowId;
                if ($this->ftir->saveAs($this->dirBerkas . "/$filename.jpg")) {
                }
            }
            if ($this->ftir2 != null) {
                $filename = 'ffp2_' . $this->rowId;
                if ($this->ftir2->saveAs($this->dirBerkas . "/$filename.jpg")) {
                }
            }
            if ($this->ptest1 != null) {
                $filename = 'ptest1_' . $this->rowId;
                if ($this->ptest1->saveAs($this->dirBerkas . "/$filename.jpg")) {

                }
            }
            if ($this->ptest2 != null) {
                $filename = 'ptest2_' . $this->rowId;
                if ($this->ptest2->saveAs($this->dirBerkas . "/$filename.jpg")) {

                }
            }
            if ($this->filtrat != null) {
                $filename = 'filtrat_' . $this->rowId;
                if ($this->filtrat->saveAs($this->dirBerkas . "/$filename.jpg")) {

                }
            }
            if ($this->fcs1 != null) {
                $filename = 'fcs1_' . $this->rowId;
                if ($this->fcs1->saveAs($this->dirBerkas . "/$filename.jpg")) {
                }
            }
            if ($this->fcs2 != null) {
                $filename = 'fcs2_' . $this->rowId;
                if ($this->fcs2->saveAs($this->dirBerkas . "/$filename.jpg")) {
                }
            }
            if ($this->fcs3 != null) {
                $filename = 'fcs3_' . $this->rowId;
                if ($this->fcs3->saveAs($this->dirBerkas . "/$filename.jpg")) {
                }
            }
            if (parent::save($runValidation, $attributeNames)) {

                $transaction->commit();
                return true;
            }
            $transaction->rollBack();
            return false;
        } catch (Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
}
