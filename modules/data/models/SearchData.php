<?php
/**
 * Created by
 * User: Wisard17
 * Date: 02/03/2019
 * Time: 01.28 PM
 */

namespace app\modules\data\models;


use Yii;
use app\components\ArrayHelper;
use yii\db\Query;
use yii\helpers\Json;

/**
 * Class SearchData
 * @package app\modules\data\models
 */
class SearchData extends DataAnalisa
{
    /**
     * @var array
     */
    public $allField = [
        'id' => 'id',
        'branch_no' => 'branch_no',
        'CustomerID' => 'CustomerID',
        'lab_number' => 'lab_number',
        'UnitID' => 'UnitID',
        'type_report_id' => 'type_report_id',
        'sample_name' => 'sample_name',
        'sampling_date' => 'sampling_date',
        'received_date' => 'received_date',
        'start_analisa' => 'start_analisa',
        'end_analisa' => 'end_analisa',
        'report_date' => 'report_date',
        'publish' => 'publish',
        'eval_code' => 'eval_code',
        'followup' => 'followup',
        'recom1' => 'recom1',
        'recom2' => 'recom2',
        'spesifikasi_id' => 'spesifikasi_id',
        'appearance' => 'appearance',
        'appearance_code' => 'appearance_code',
        'water_content' => 'water_content',
        'water_content_code' => 'water_content_code',
        'flash_point' => 'flash_point',
        'flash_point_code' => 'flash_point_code',
        'pour_point' => 'pour_point',
        'pour_point_code' => 'pour_point_code',
        'density' => 'density',
        'density_code' => 'density_code',
        'metal_ca' => 'metal_ca',
        'metal_mg' => 'metal_mg',
        'metal_zn' => 'metal_zn',
        'metal_p' => 'metal_p',
        'metal_ca_code' => 'metal_ca_code',
        'metal_mg_code' => 'metal_mg_code',
        'metal_zn_code' => 'metal_zn_code',
        'metal_p_code' => 'metal_p_code',
        'visc_40' => 'visc_40',
        'visc_100' => 'visc_100',
        'visc_index' => 'visc_index',
        'visc_40_code' => 'visc_40_code',
        'visc_100_code' => 'visc_100_code',
        'visc_index_code' => 'visc_index_code',
        'tan' => 'tan',
        'tan_code' => 'tan_code',

        'status' => 'eval_code',
        'customerName' => 'branch_no',
        'sampleDate' => 'sampling_date',
        'receiveDate' => 'received_date',
        'reportDate' => 'report_date',
        'reportType' => 'type_report_id',
        'unitNo' => 'UnitID',

    ];

    /**
     * @param $params
     * @return string
     */
    public function ordering($params)
    {
        $ncol = isset($params['order'][0]['column']) ? $params['order'][0]['column'] : 0;
        $col = (isset($params['columns'][$ncol]) && array_key_exists($params['columns'][$ncol]['data'], $this->allField)) ?
            $this->allField[$params['columns'][$ncol]['data']] : '';
        $argg = isset($params['order'][0]['dir']) ? $params['order'][0]['dir'] : 'asc';
        $table = self::tableName();

        if (isset($params['columns'][$ncol]['data']) && $params['columns'][$ncol]['data'] === 'customerName') {
            $col = 'branch';
            $table = 'tbl_branch';
        }

        if (isset($params['columns'][$ncol]['data']) && $params['columns'][$ncol]['data'] === 'reportType') {
            $col = 'name';
            $table = 'type_report';
        }

        if (isset($params['columns'][$ncol]['data']) && $params['columns'][$ncol]['data'] === 'unitNo') {
            $col = 'UnitNo';
            $table = 'tbl_unit';
        }

        return $col !== '' ? "$table.$col $argg " : '';
    }


    public $allData;

    public $currentData;

    /**
     * @param Query $query
     * @return int
     */
    public function calcAllData($query)
    {
        return $this->allData === null ? $query->count() : $this->allData;
    }

    /**
     * @param Query $query
     * @param $params
     * @return Query
     */
    public function defaultFilterByUser($query, $params = '')
    {
        $user = Yii::$app->user->identity;

        if ($user->ruleAccess === -1) {
            return $query;
        }

        if ($user->ruleAccess === 2 || $user->ruleAccess === 3) {
            $query->andWhere(['in', self::tableName() . '.CustomerID', $user->getAllId()]);
        }

        if ($user->ruleAccess === 5) {
            $query->andWhere(['in', self::tableName() . '.CustomerID', $user->getAllId()]);
        }

        return $query;
    }

    /**
     * @param $params
     * @param bool $export
     * @param bool $join
     * @return $this|\yii\db\ActiveQuery|Query
     */
    public function searchData($params, $export = false, $join = true)
    {

        $odr = $this->ordering($params);

        $query = self::find()->joinWith(['customer', 'typeReport', 'unit']);

        if (!$join) {
            $query = self::find();
        }

        $query = $this->defaultFilterByUser($query, $params);

        $query->orderBy($odr);

        $start = isset($params['start']) ? $params['start'] : 0;
        $lang = isset($params['length']) ? $params['length'] : 10;

        $this->allData = $this->calcAllData($query);

        if (isset($params['r_start']) && $params['r_start'] != '') {
            $query->andFilterWhere(['>=', '`' . self::tableName() . '`.received_date' , date('Y-m-d H:i:s',strtotime($params['r_start']))]);
            if (isset($params['r_end']) && $params['r_end'] != '') {
                $query->andFilterWhere(['<=', '`' . self::tableName() . '`.received_date' , date('Y-m-d H:i:s',strtotime($params['r_end']))]);
            }
        }

        $fltr = '';
        if (isset($params['columns'])) {
            foreach ($params['columns'] as $col) {
                if (isset($params['search']) && $params['search']['value'] !== '') {
                    $lst[] = $col['data'];
                    if (array_key_exists($col['data'], $this->allField) &&
                        !array_key_exists($col['data'], $lst) && $this->allField[$col['data']] != '') {
                        $fltr .= $fltr !== '' ? ' or ' : '';
                        $fltr .= ' `' . self::tableName() . '`.' . $this->allField[$col['data']] . " like '%" . $params['search']['value'] . "%' ";
                    }
                    if ($col['data'] === 'customerName') {
                        $fltr .= $fltr !== '' ? ' or ' : '';
                        $fltr .= ' `tbl_branch`.branch ' . " like '%" . $params['search']['value'] . "%' ";
                    }
                    if ($col['data'] === 'reportType') {
                        $fltr .= $fltr !== '' ? ' or ' : '';
                        $fltr .= ' `type_report`.name ' . " like '%" . $params['search']['value'] . "%' ";
                    }
                    if ($col['data'] === 'unitNo') {
                        $fltr .= $fltr !== '' ? ' or ' : '';
                        $fltr .= ' `tbl_unit`.UnitNo ' . " like '%" . $params['search']['value'] . "%' ";
                    }
                }
                if ($col['searchable'] === 'true' && $col['search']['value'] !== '' &&
                    array_key_exists($col['data'], $this->allField) && $this->allField[$col['data']] != '') {
                    if ($col['data'] === 'customerName') {
                        $query->andFilterWhere(['like', '`tbl_branch`.branch', $col['search']['value']]);
                    } elseif ($col['data'] === 'reportType') {
                        $query->andFilterWhere(['like', '`type_report`.name', $col['search']['value']]);
                    } elseif ($col['data'] === 'unitNo') {
                        $query->andFilterWhere(['like', '`tbl_unit`.UnitNo', $col['search']['value']]);
                    } else {
                        $query->andFilterWhere(['like', '`' . self::tableName() . '`.' . $this->allField[$col['data']], $col['search']['value']]);
                    }
                }
            }
        }

        $query->andWhere($fltr);

        $this->load($params);

        if ($export) {
            return $query;
        }

        $this->currentData = $query->count();

        $query->limit($lang)->offset($start);
        return $query;
    }

    /**
     * @param $params
     * @return mixed
     */
    public function searchDataTable($params)
    {
        $data = $this->searchData($params);
        return Json::encode([
            'draw' => isset ($params['draw']) ? (int)$params['draw'] : 0,
            'recordsTotal' => (int)$this->allData,
            'recordsFiltered' => (int)$this->currentData,
            'data' => $this->renderData($data, $params),
        ]);
    }

    /**
     * @param $query Query
     * @param $params
     * @return array
     */
    public function renderData($query, $params)
    {
        $out = [];

        /** @var self $model */
        foreach ($query->all() as $model) {
            $out[] = array_merge(ArrayHelper::toArray($model), [
                'action' => $model->action,
                'rowId' => $model->rowId,
                'status' => $model->status,
                'statusPublish' => $model->statusPublish,
                'reportType' => $model->reportType,
                'sampleDate' => $model->sampleDate,
                'receiveDate' => $model->receiveDate,
                'reportDate' => $model->reportDate,
                'customerName' => $model->customerName,
                'unitNo' => $model->unitNo,
                'statusVerify' => $model->statusVerify,
            ]);
        }
        return $out;
    }
}