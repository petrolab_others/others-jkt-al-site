<?php
/**
 * Created by
 * User: Wisard17
 * Date: 25/02/2019
 * Time: 10.25 AM
 */

namespace app\modules\data\models;


use app\components\ArrayHelper;
use Yii;
use app\components\Migration;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/**
 * Class FormParameter
 * @package app\modules\data\models
 *
 *
 */
class FormReference extends Reference
{

    /**
     * @param bool $runValidation
     * @param null $attributeNames
     * @return bool
     * @throws \Throwable
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {

            if (parent::save($runValidation, $attributeNames)) {
                foreach ($this->parameterHasReference as $item) {
                    if ($item->actionId == 'del') {
                        $item->delete();
                    } else {
                        $item->reference_id = $this->id;
                        if (!$item->save()) {
                            $this->addError('parameterHasReference', 'kesalahan pada  :' . ArrayHelper::toString($item->errors));
                            $transaction->rollBack();
                            return false;
                        }
                    }
                }
                $transaction->commit();
                return true;
            }
            $transaction->rollBack();
            return false;
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    /**
     * @param array $data
     * @param null|string $formName
     * @return bool
     */
    public function load($data, $formName = null)
    {
        $data = $data == null ? [] : $data;
        $ld = parent::load($data, $formName);
        if (isset($data['ParameterHasReference'])) {
            foreach ($data['ParameterHasReference'] as $idx => $datum) {
                $pca = explode('_', $idx);
                $referenceId = isset($pca[0]) ? $pca[0] : 0;
                $parameterId = isset($pca[1]) ? $pca[1] : 0;
                $item = ParameterHasReference::findOne(['reference_id' => $referenceId, 'parameter_id' => $parameterId]);
                if ($item == null) {
                    $item = new ParameterHasReference($datum);
                }
                $item->load(['Data' => $datum], 'Data');
                $item->actionId = isset($datum['actionId']) ? $datum['actionId'] : null;
                $this->_parameterHasReference[$idx] = $item;
            }
        }
        return $ld;
    }


    /**
     * @param \yii\widgets\ActiveForm $form
     * @return string
     */
    public function renderDetail($form)
    {
        $out = '<thead><tr>
                    <th class="text-center">action</th>
                    <th class="text-center" >Name</th>
                    <th class="text-center" >Value</th>
                    </tr></thead><tbody>';
        foreach ($this->parameterHasReference as $idx => $child) {
            $child->idName = $child->id == '_' ? 'newrow_' . $idx : $child->id;
            $out .= Html::tag('tr',
                Html::tag('td', $child->action) .
                Html::tag('td', $form->field($child, 'parameter_id', ['template' => '<label class="">{input}</label>{error}'])
                    ->dropDownList($child->parameter_id == '' ? [] :
                        [$child->parameter_id => $child->parameter->namePParent], ['class' => 'form-control select2ajax', 'style' => '100%',
                        'prompt' => '- Select Parameter -', 'data-url' => Url::toRoute(['/data/parameter/list-parameters']),
                    ])) .
                Html::tag('td', $form->field($child, 'value', ['template' => '<label class="">{input}</label>{error}']))
                , ['data-id' => $child->idName]
            );
        }

        return Html::tag('table', $out . '</tbody>', ['class' => 'table']);
    }

    /**
     * @param int $rowTo
     * @return array
     */
    public static function renderRowAjax($rowTo = 0)
    {
        $form = ActiveForm::begin();
        $child = new ParameterHasReference();
        $child->idName = 'newrow_' . $rowTo;

        $out = Html::tag('tr',
            Html::tag('td', $child->action) .
            Html::tag('td', $form->field($child, 'parameter_id', ['template' => '<label class="">{input}</label>{error}'])
                ->dropDownList($child->parameter_id == '' ? [] :
                    [$child->parameter_id => $child->parameter->namePParent], ['class' => 'form-control select2ajax', 'style' => '100%',
                    'prompt' => '- Select Parameter -', 'data-url' => Url::toRoute(['/data/parameter/list-parameters']),
                ])) .
            Html::tag('td', $form->field($child, 'value', ['template' => '<label class="">{input}</label>{error}']))
            , ['data-id' => $child->idName]
        );

        return [
            'data' => $out,
            'jsAdd' => ArrayHelper::toArray($form->attributes),
        ];
    }
}