<?php
/**
 * Created by
 * User: Wisard17
 * Date: 23/03/2019
 * Time: 01.07 PM
 */

namespace app\modules\data\models;

use app\components\ArrayHelper;
use app\components\Excel;
use yii\web\UploadedFile;

/**
 * Class FormUploadData
 * @package app\modules\data\models
 */
class FormUploadData extends DataAnalisa
{

    public $uploadFile;


    /**
     * @param UploadedFile|null $image
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function upload(UploadedFile $image = null)
    {
        $transaction = $this::getDb()->beginTransaction();
        try {
            $excel = new Excel();
            if ($image != null) {
                $excel = $excel::renderData($image->tempName);
                $arrExcel = $excel->getActiveSheet()->toArray();
                $countInput = 0;
                $countError = 0;
                $header = [];
                $idxLabNo = '';
                $l = [
                    'branch_no' => 'cust_id', 'CustomerID' => 'attention_id', 'report_number' => 'reportnumber',
                    'sample_name' => 'samplename', 'sampling_date' => 'samplingdate', 'received_date' => 'receiveddate',
                    'start_analisa' => 'startanalisa', 'end_analisa' => 'endanalisa', 'report_date' => 'reportdate',
                    'req_order' => 'reqorder', 'hm_unit' => 'hoursonunit', 'hm_oil' => 'hoursonoil'
                ];
                foreach ($arrExcel as $i => $item) {
                    if ($i == 0) {
                        foreach ($item as $key => $col) {
                            $col = str_replace([' ', '	', "\n"], '', $col);
                            if ($this->hasAttribute($col)) {
                                if ($col == 'lab_number') {
                                    $idxLabNo = $key;
                                } else {
                                    $header[$key] = $col;
                                }
                            } else {
                                $c2 = $this->indexOf($l, str_replace(' ', '', $col));
                                if ($c2) {
                                    if ($col == 'lab_number') {
                                        $idxLabNo = $key;
                                    } else {
                                        $header[$key] = $c2;
                                    }
                                } else {
                                    $this->dataMessage .= "Kolom $col tidak terdaftar dalam table DataBase <br>";
                                }
                            }
                        }
                        if ($idxLabNo === '') {
                            $this->dataMessage .= "File Excel tidak memiliki Kolom <strong>lab_number</strong> sebagai identifikasi data <br>";
                            return false;
                        }
                    } else {
                        $labNo = isset($item[$idxLabNo]) ? $item[$idxLabNo] : '';
                        if ($labNo == '') {
                            $this->dataMessage .= 'baris ke ' . ($i + 1) . ' lab number kosong <br>';
                            continue;
                        }
                        $report = self::findOne(['lab_number' => $labNo]);
                        if ($report == null) {
                            $report = new self(['lab_number' => $labNo]);
                        }
                        foreach ($header as $k => $h) {
                            if ($h == '') {
                                continue;
                            }
                            if ($report->hasAttribute($h)) {
                                if (in_array($h, ['sampling_date', 'received_date', 'report_date', 'publish'], true)){
                                    if (is_numeric($item[$k])) {
                                        /** @noinspection SummerTimeUnsafeTimeManipulationInspection */
                                        $time = ($item[$k] - 25569) * 86400;
                                        $report->$h = date('Y-m-d',  $time);
                                    } else {
                                        $report->$h = date('Y-m-d', strtotime($item[$k]));
                                    }
                                } else {
                                    $report->$h = (string)$item[$k];
                                }
                            }
                        }
                        if ($report->save()) {
                            $countInput++;
                        } else {
                            $countError++;
                            $this->dataMessage .= "lab number : $item[$idxLabNo] error dengan " . ArrayHelper::toString($report->errors) . ' <br>';
                        }
                    }
                }

                $this->status_save = $countInput > 0 ? 'success' : 'error';
                $this->message = "Upload berhasil dengan <br><br> Jumlah Success : $countInput <br>" .
                    ($countError > 0 ? "Jumlah Error : $countError <br>" : '') . '<br><hr>';
                $transaction->commit();
                return true;
            }
            $this->status_save = 'error';
            $this->message = 'Data Tidak disimpan, Coba lagi.<br>' . ArrayHelper::toString($this->errors);

            $transaction->rollBack();
            return false;
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    /**
     * @param array $arr
     * @param $keyword
     * @return bool|int|string
     */
    protected function indexOf(array $arr, $keyword)
    {
        foreach ($arr as $idx => $item) {
            if (stripos($item, $keyword) !== false) {
                return $idx;
                break;
            }
        }
        return false;
    }

    public $message = '';

    public $status_save = '';

    public $class_message = '';

    public $dataMessage = '';
}