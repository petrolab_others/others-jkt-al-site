<?php
/**
 * Created by
 * User: Wisard17
 * Date: 13/03/2019
 * Time: 10.18 AM
 */

namespace app\modules\data\models;


use app\components\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class TypeReport
 * @package app\modules\data\models
 *
 * @property string $action
 * @property \app\modules\data\models\Parameters[] $parametersList
 * @property string $tempLabNo
 */
class TypeReport extends \app\modelsDB\TypeReport
{

    public function rules()
    {
        return array_merge(parent::rules(), [
            ['name', 'unique'],
        ]);
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'mpc' => 'Fitur MPC',
            'chart' => 'Show Chart',
            'type_view_id' => 'Category View',
        ]);
    }

    public function getTempLabNo()
    {
        return $this->lab_no_temp == '' ? '{count}/O/L/{y}' : $this->lab_no_temp;
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return Html::tag('div',
            Html::tag('a', 'Action <span class="caret"></span>', [
                'class' => 'btn btn-default dropdown-toggle', 'data-toggle' => 'dropdown', 'aria-expanded' => 'false'
            ]) . Html::tag('ul',
//                Html::tag('li', Html::tag('a', '<i class="fa fa-caret-square-o-right"></i> view',
//                    ['href' => Url::toRoute(['/data/report-type/view', 'id' => $this->id])])) .
                Html::tag('li', Html::tag('a', '<i class="fa fa-edit"></i> Edit',
                    ['href' => Url::toRoute(['/data/report-type/edit', 'id' => $this->id])])) .
                Html::tag('li', Html::tag('a', '<i class="fa fa-copy"></i> Duplicate',
                    ['href' => Url::toRoute(['/data/report-type/new', 'duplicate' => $this->id])])) .
                Html::tag('li', Html::tag('a', '<i class="fa fa-times"></i> Delete',
                    ['href' => Url::toRoute(['/data/report-type/del', 'id' => $this->id]),
                        'data-method' => 'post',
                        'data-confirm' => 'Are you absolutely sure ? You will lose all the information about this user with this action.',
                    ]))
                , ['class' => 'dropdown-menu']), ['class' => 'btn-group']);
    }

    public $_parameter_list;

    /**
     * @return Parameters[]
     */
    public function getParametersList()
    {
        if ($this->_parameter_list == null) {
            $this->_parameter_list = Parameters::find()->innerJoinWith('parameterHasTypeReports')
                ->where([ParameterHasTypeReport::tableName() . '.type_report_id' => $this->id])
                ->andWhere('parameter.parent < 1 or parameter.parent is null')->andWhere('parameter.active = 1')
                ->orderBy('order')->all();
        }
        return $this->_parameter_list;
    }

    public function listView()
    {
        return ArrayHelper::map(TypeView::find()->all(), 'id', 'name');
    }
}