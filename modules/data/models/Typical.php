<?php
/**
 * Created by
 * User: Wisard17
 * Date: 25/02/2019
 * Time: 10.15 AM
 */

namespace app\modules\data\models;


use Yii;
use yii\helpers\Html;

/**
 * Class Typical
 * @package app\modules\data\models
 *
 * @property string $idName
 * @property Parameters $parameter
 * @property string $action
 */
class Typical extends \app\modelsDB\Typical
{

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'condition1' => Yii::t('app', 'N'),
            'condition2' => Yii::t('app', 'B'),
            'condition3' => Yii::t('app', 'C'),
            'condition4' => Yii::t('app', 'D'),
            'condition5' => Yii::t('app', 'E'),
        ]);

    }

    /**
     * @return string
     */
    public function getAction()
    {
        return Html::tag('div',
            Html::tag('a', 'Action <span class="caret"></span>', [
                'class' => 'btn btn-default dropdown-toggle', 'data-toggle' => 'dropdown', 'aria-expanded' => 'false'
            ]) . Html::tag('ul',
                Html::tag('li', '<a data-action="delate_row">Delete</a>')
                , ['class' => 'dropdown-menu']), ['class' => 'btn-group']);
    }

    /** @var string */
    public $_idName;

    /**
     * @return string
     */
    public function getIdName()
    {
        return $this->_idName;
    }

    /**
     * @param string $idName
     */
    public function setIdName($idName)
    {
        $this->_idName = $idName;
    }

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function formName()
    {
        if ($this->_idName == '') {
            return parent::formName() . '[new]';
        }
        return parent::formName() . "[$this->idName]";
    }

    /** @var  string|int */
    public $actionId;

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParameter()
    {
        return $this->hasOne(Parameters::className(), ['id' => 'new_oil_parameter_id']);
    }

}