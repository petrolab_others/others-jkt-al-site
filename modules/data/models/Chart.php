<?php
/**
 * Created by
 * User: Wisard17
 * Date: 08/04/2019
 * Time: 10.20 AM
 */

namespace app\modules\data\models;

use Yii;
use app\components\XYChart;
use app\helpers\ChartDir;

/**
 * Class Chart
 * @package app\modules\data\models
 */
class Chart extends ChartDir
{
    public function makeChart($width, $height, $title, $category, $data, $scale = 0){

        $c = new XYChart($width, $height, 0xffffff, -1, 0);

        $c->setPlotArea(30, 45, $width-50, $height-80, 0xffffff, -1, -1, 0xc0c0c0, -1);
        $c->addTitle2(Top, $title, "arialbd.ttf", 14, 0xffffff, 0x31319c);
        $c->xAxis->setLabels($category);
        $c->xAxis->setLabelStyle("Arial Bold", 8, "0xcc6600");
        $layer = $c->addLineLayer();

        if ($scale > 0) {
            $c->yAxis->setLinearScale(0, $scale);
        }

        $legendObj = $c->addLegend(30, 14, false, '4', 12);
        $legendObj->setBackground(Transparent);

        foreach ($data as $idx => $datum) {
            $dataSetObj = $layer->addDataSet($datum['dataset'], -1, $datum['label']);
            $dataSetObj->setDataSymbol((string)($idx+1), 12);
        }

        $vfile = $this->dirBerkas . '/img_' . strtolower(str_replace(' ', '_',
                preg_replace('/[^A-Za-z0-9\_]/', '', $title))) . '.png';

        $c->makeChart2('PNG');
        $c->makeChart($vfile);

        $im = imagecreatefrompng($vfile);
        $im2 = imagecrop($im, ['x' => 0, 'y' => 0, 'width' => imagesx($im), 'height' => imagesy($im) - 9]);
        if ($im2 !== FALSE) {
            imagepng($im2, $vfile);
            imagedestroy($im2);
        }
        imagedestroy($im);

        return $vfile;
    }

    public function makeChartLagen($width, $height, $title, $category, $data){

        $c = new XYChart($width, $height, 0xffffff, -1, 0);

        $c->setPlotArea(30, 45, $width-180, $height-80, 0xffffff, -1, -1, 0xc0c0c0, -1);
        $c->addTitle2(Top, $title, "arialbd.ttf", 14, 0xffffff, 0x31319c);
        $c->xAxis->setLabels($category);
        $c->xAxis->setLabelStyle("Arial Bold", 8, "0xcc6600");
        $layer = $c->addLineLayer();

        $legendObj = $c->addLegend($width, 20, false, '4', 10);
        $legendObj->setSize(100,$height-20);
        $legendObj->setBackground(Transparent);
        $legendObj->setAlignment(TopRight);

        foreach ($data as $idx => $datum) {
            $dataSetObj = $layer->addDataSet($datum['dataset'], -1, $datum['label']);
            $dataSetObj->setDataSymbol((string)mt_rand(1,15), 12);
        }

        $vfile = $this->dirBerkas . '/img_' . strtolower(str_replace(' ', '_',
                preg_replace('/[^A-Za-z0-9\_]/', '', $title))) . '.png';

        $c->makeChart2('PNG');
        $c->makeChart($vfile);

        $im = imagecreatefrompng($vfile);
        $im2 = imagecrop($im, ['x' => 0, 'y' => 0, 'width' => imagesx($im), 'height' => imagesy($im) - 9]);
        if ($im2 !== FALSE) {
            imagepng($im2, $vfile);
            imagedestroy($im2);
        }
        imagedestroy($im);

        return $vfile;
    }
}