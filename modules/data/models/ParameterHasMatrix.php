<?php
/**
 * Created by
 * User: Wisard17
 * Date: 05/04/2019
 * Time: 02.18 PM
 */

namespace app\modules\data\models;


use yii\helpers\Html;

/**
 * Class ParameterHasMatrix
 * @package app\modules\data\models
 *
 * @property string $idName
 * @property string $action
 * @property string $id
 * @property Parameters $parameter
 */
class ParameterHasMatrix extends \app\modelsDB\ParameterHasMatrix
{

    public function rules()
    {
        return array_merge(parent::rules(), [
            [['parameter_id', 'matrix'], 'unique', 'targetAttribute' => ['parameter_id', 'matrix']],
        ]);
    }

    public function getId()
    {
        return $this->matrix . '_' . $this->parameter_id;
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return Html::tag('div',
            Html::tag('a', 'Action <span class="caret"></span>', [
                'class' => 'btn btn-default dropdown-toggle', 'data-toggle' => 'dropdown', 'aria-expanded' => 'false'
            ]) . Html::tag('ul',
                Html::tag('li', '<a data-action="delate_row">Delete</a>')
                , ['class' => 'dropdown-menu']), ['class' => 'btn-group']);
    }

    /** @var string */
    public $_idName;

    /**
     * @return string
     */
    public function getIdName()
    {
        return $this->_idName;
    }

    /**
     * @param string $idName
     */
    public function setIdName($idName)
    {
        $this->_idName = $idName;
    }

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function formName()
    {
        if ($this->_idName == '') {
            return parent::formName() . '[new]';
        }
        return parent::formName() . "[$this->idName]";
    }

    /** @var  string|int */
    public $actionId;

    /**
     * @return \yii\db\ActiveQuery|Parameters
     */
    public function getParameter()
    {
        return $this->hasOne(Parameters::className(), ['id' => 'parameter_id']);
    }
}