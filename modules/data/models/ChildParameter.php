<?php
/**
 * Created by
 * User: Wisard17
 * Date: 28/02/2019
 * Time: 02.32 PM
 */

namespace app\modules\data\models;


use yii\helpers\Html;

/**
 * Class ChildParameter
 * @package app\modules\data\models
 *
 * @property string $idName
 */
class ChildParameter extends Parameters
{

    /**
     * @return string
     */
    public function getAction()
    {
        return Html::tag('div',
            Html::tag('a', 'Action <span class="caret"></span>', [
                'class' => 'btn btn-default dropdown-toggle', 'data-toggle' => 'dropdown', 'aria-expanded' => 'false'
            ]) . Html::tag('ul',
                Html::tag('li', '<a data-action="delate_row">Delete</a>')
                , ['class' => 'dropdown-menu']), ['class' => 'btn-group']);
    }

    /** @var string */
    public $_idName;

    /**
     * @return string
     */
    public function getIdName()
    {
        return $this->_idName;
    }

    /**
     * @param string $idName
     */
    public function setIdName($idName)
    {
        $this->_idName = $idName;
    }

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function formName()
    {
        if ($this->_idName == '') {
            return parent::formName() . '[new]';
        }
        return parent::formName() . "[$this->idName]";
    }

    /** @var  string|int */
    public $actionId;

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->col_name = FormParameter::generateColumnName($this->name . $this->method);
            }
            return true;
        }
        return false;
    }
}