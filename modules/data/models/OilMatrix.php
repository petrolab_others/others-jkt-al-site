<?php
/**
 * Created by
 * User: Wisard17
 * Date: 06/04/2019
 * Time: 01.26 PM
 */

namespace app\modules\data\models;


use app\modelsDB\TblOilMatrix;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class OilMatrix
 * @package app\modules\data\models
 *
 * @property mixed $allParameter
 * @property mixed $id
 * @property \yii\db\ActiveQuery $parameterHasOilMatrixs
 * @property \app\modules\data\models\ParameterHasOilMatrix[] $parameterHasOilMatrix
 * @property string $action
 */
class OilMatrix extends TblOilMatrix
{

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            ['OIL_MATRIX', 'unique'],
        ]);
    }

    public function getId()
    {
        return $this->OIL_MATRIX;
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return Html::tag('div',
            Html::tag('a', 'Action <span class="caret"></span>', [
                'class' => 'btn btn-default dropdown-toggle', 'data-toggle' => 'dropdown', 'aria-expanded' => 'false'
            ]) . Html::tag('ul',
//                Html::tag('li', Html::tag('a', '<i class="fa fa-caret-square-o-right"></i> view',
//                    ['href' => Url::toRoute(['/data/oil-matrix/view', 'id' => $this->OIL_MATRIX])])) .
                Html::tag('li', Html::tag('a', '<i class="fa fa-edit"></i> Edit',
                    ['href' => Url::toRoute(['/data/oil-matrix/edit', 'id' => $this->OIL_MATRIX])])) .
                Html::tag('li', Html::tag('a', '<i class="fa fa-copy"></i> Duplicate',
                    ['href' => Url::toRoute(['/data/oil-matrix/new', 'duplicate' => $this->id])])) .
                Html::tag('li', Html::tag('a', '<i class="fa fa-times"></i> Delete',
                    ['href' => Url::toRoute(['/data/oil-matrix/del', 'id' => $this->OIL_MATRIX]),
                        'data-method' => 'post',
                        'data-confirm' => 'Are you absolutely sure ? You will lose all the information about this user with this action.',
                    ]))
                , ['class' => 'dropdown-menu']), ['class' => 'btn-group']);
    }

    /** @var ParameterHasOilMatrix[] */
    public $_parameterHasOilMatrix;

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParameterHasOilMatrixs()
    {
        return $this->hasMany(ParameterHasOilMatrix::className(), ['OIL_MATRIX' => 'OIL_MATRIX']);
    }

    /**
     * @return ParameterHasOilMatrix[]
     */
    public function getParameterHasOilMatrix()
    {
        if ($this->_parameterHasOilMatrix == null) {
            $this->_parameterHasOilMatrix = $this->parameterHasOilMatrixs;
        }
        return $this->_parameterHasOilMatrix;
    }

    public $_ap;

    public function getAllParameter()
    {
        if ($this->_ap == null) {
            $out = '';
            foreach ($this->parameterHasOilMatrix as $hasMatrix) {
                $out .= Html::tag('li', $hasMatrix->parameter->name . ' : ' . $hasMatrix->min . '-' . $hasMatrix->max);
            }
            $this->_ap = Html::tag('ul', $out);
        }
        return $this->_ap;
    }
}