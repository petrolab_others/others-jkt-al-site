<?php
/**
 * Created by
 * User: Wisard17
 * Date: 28/11/2018
 * Time: 09.37 AM
 */

namespace app\modules\data\models;


use app\components\ArrayHelper;
use app\components\Excel;


/**
 * Class FormDownloadExcel
 * @package app\modules\master\models
 *
 * @property array $propertySelect
 * @property mixed $temp_parameter
 * @property \app\modules\data\models\Parameters[] $params
 * @property array $parameterSelect
 */
class FormDownloadExcel extends SearchData
{

    public $code = false;

    public $csv = false;

    public $property = [
        'lab_number',
        'unitNo',
        'unitLocation',
        'componentName',
//        'type_report_id',
        'sample_name',
        'sampling_date',
        'received_date',
        'report_date',
        'report_number',
        'hm_unit',
        'hm_oil',
        'followup',
        'recom1',
        'recom2',
    ];

    public $parameter = [];

    public $_temp_parameter;

    public $temp_property = [
        'customerName',
        'forCustomer',
        'lab_number',
        'UnitID',
        'ComponentID',
        'unitNo',
        'unitLocation',
        'componentName',
        'type_report_id',
        'sample_name',
        'sampling_date',
        'received_date',
        'start_analisa',
        'end_analisa',
        'report_date',
        'publish',
        'eval_code',
        'report_number',
        'req_order',
        'hm_unit',
        'hm_oil',
        'followup',
        'recom1',
        'recom2',
        'spesifikasi_id',
        'reference_id',
        'OIL_MATRIX',
        'matrix'
    ];

    public function getParameterSelect()
    {
        $out = [];
        foreach ($this->temp_parameter as $param => $item) {
            $out[$param] = $this->getAttributeLabel($param);
        }
        return $out;
    }

    public function getPropertySelect()
    {
        $out = [];
        foreach ($this->temp_property as $param) {
            if ($param == 'lab_number') {
                $out[$param] = $param;
            } else {
                $out[$param] = $this->getAttributeLabel($param);
            }
        }
        return $out;
    }

    public function rules()
    {
        return array_merge(parent::rules(), [
            [['parameter', 'code', 'property', 'csv'], 'safe']
        ]);
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'code' => 'Download with parameter code',
            'csv' => 'Change to CSV format',
        ]);
    }

    /**
     * Initializes the view.
     */
    public function init()
    {
        parent::init();

        if ($this->property == null) {
            $this->property = $this->temp_property;
        }
    }

    public $for_customer = 1;

    /**
     * @param null $params
     * @param string $fileName
     * @param bool $headerOnly
     * @return string
     * @throws \PHPExcel_Exception
     */
    public function generateExcel($params = null, $fileName = '', $headerOnly = false)
    {
        $countFilter = 0;
        if (isset($params['columns'])) {
            foreach ($params['columns'] as $col) {
                if (isset($params['search']) && $params['search']['value'] !== '') {
                    $countFilter++;
                }
                if ($col['searchable'] === 'true' && $col['search']['value'] !== '') {
                    $countFilter++;
                }
            }
        }

        $query = $this->searchData($params, true);

        $data = $query->all();

        $allType = array_unique(ArrayHelper::getColumn($data, 'type_report_id'));

        if (count($allType) > 0) {
            $pt = ParameterHasTypeReport::find()->where(['in', 'type_report_id', $allType])->all();
            $this->allParamByType = array_unique(ArrayHelper::getColumn($pt,'parameter_id'));
        }

        if ($this->parameter == null) {
            $this->parameter = array_keys($this->temp_parameter);
        }

        $e = new Excel();
        $p = $e->newExcel();
        $row = 2;
        $col = 'A';
        $c = [];
        $labelProperty = $this->propertySelect;
        $labelParameter = $this->parameterSelect;
        if ($headerOnly) {
            $header = array_keys($this->attributes);
            $header = array_replace($header, [1 => 'cust_id', 2 => 'attention_id']);
            unset($header[0]);
            foreach ($header as $item) {
                if ($item == 'mpc_pic') {
                    continue;
                }
                $p->getActiveSheet()->setCellValue($col . 1, $item);
                $col++;
            }
            $e->download($p, $fileName);
            return '';
        }
        foreach ($data as $record) {
            foreach ($this->property as $value) {
                if (!isset($c[$value])) {
                    $c[$value] = $col;
                    $p->getActiveSheet()->setCellValue($c[$value] . 1,
                        isset($labelProperty[$value]) ? $labelProperty[$value] : $value);
                    $col++;
                }
                $p->getActiveSheet()->setCellValue($c[$value] . $row, $record->$value);
//                    $record->hasAttribute($value) ? $record->$value : '');
            }

            foreach ($this->parameter as $value) {
                $code = isset($this->temp_parameter[$value]) ? $this->temp_parameter[$value] : '';
                if ($code == 'file') {
                    continue;
                }
                if (!isset($c[$value])) {
                    $c[$value] = $col;
                    $p->getActiveSheet()->setCellValue($c[$value] . 1,
                        $this->for_customer == 1 && isset($labelParameter[$value]) ? $labelParameter[$value] : $value);
                    $col++;
                    if ($this->code && $code != '') {
                        $c[$code] = $col;
                        $p->getActiveSheet()->setCellValue($c[$code] . 1, $code);
                        $col++;
                    }
                }
                $p->getActiveSheet()->setCellValue($c[$value] . $row,
                    $record->hasAttribute($value) ? $record->$value : '');
                if ($this->code && $code != '') {
                    $p->getActiveSheet()->setCellValue($c[$code] . $row,
                        $record->hasAttribute($value) ? $record->$code : '');
                }
            }
            $row++;
        }
        if ($fileName == '') {
            $fileName = 'Export_excel_' . date('d-m-Y_H-i-s');
        }
        if ($this->csv) {
            $e->downloadCsv($p, $fileName);
            return '';
        }
        $e->download($p, $fileName);
        return '';
    }

    /**
     * @return array
     */
    public function getTemp_parameter()
    {
        if ($this->_temp_parameter == null) {
            foreach ($this->params as $parameter) {
                if ($parameter->col_name != '') {
                    $this->_temp_parameter[$parameter->col_name] = $parameter->col_name . '_code';
                }
            }
            if ($this->_temp_parameter == null) {
                $this->_temp_parameter = [];
            }
        }
        return $this->_temp_parameter;
    }

    public $_params;

    public function getAttributeLabel($attribute)
    {
        if (isset($this->params[$attribute])) {
            return $this->params[$attribute]->name;
        }
        return parent::getAttributeLabel($attribute);
    }

    public $allParamByType;

    /**
     * @return Parameters[]
     */
    public function getParams()
    {
        if ($this->_params == null) {
            $p = Parameters::find()->where('parameter.active = 1')
                ->indexBy('col_name');
            if ($this->allParamByType !== null) {
                $p->andWhere(['in', 'id', $this->allParamByType]);
            }
            foreach ($p->all() as $item) {
                $this->_params[$item->col_name] = $item;
                foreach ($item->child as $childParameter) {
                    $this->_params[$childParameter->col_name] = $childParameter;
                }
            }
        }
        return $this->_params;
    }

}