<?php
/**
 * Created by
 * User: Wisard17
 * Date: 25/02/2019
 * Time: 10.25 AM
 */

namespace app\modules\data\models;


use app\components\ArrayHelper;
use Yii;
use app\components\Migration;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * Class FormParameter
 * @package app\modules\data\models
 *
 *
 * @property ChildParameter[] $childList
 */
class FormParameter extends Parameters
{

    /**
     * @param bool $runValidation
     * @param null $attributeNames
     * @return bool
     * @throws \yii\db\Exception
     * @throws \Exception
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            if ($this->isNewRecord) {
                $this->col_name = self::generateColumnName($this->name . $this->method);
            }
            if (count($this->childList) > 1) {
                $this->parent = 0;
            }
            if (parent::save($runValidation, $attributeNames)) {
                foreach ($this->childList as $item) {
                    $item->parent = $this->id;
                    if ($item->actionId == 'del') {
                        $item->active = 0;
                    }

                    if (!$item->save()) {
                        $this->addError('child', 'kesalahan pada child :' . ArrayHelper::toString($item->errors));
                        $transaction->rollBack();
                        return false;
                    }

                    $param = ['parent' => $item->parent, 'child' => $item->id];
                    $p = ParameterChild::findOne($param);
                    if ($p == null) {
                        $p = new ParameterChild($param);
                        if ($p->save()) {

                        }
                    }
                }
                $transaction->commit();
                return true;
            }
            $transaction->rollBack();
            return false;
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    public $_childList = [];

    /**
     * @param array $data
     * @param null|string $formName
     * @return bool
     */
    public function load($data, $formName = null)
    {
        $data = $data == null ? [] : $data;
        $ld = parent::load($data, $formName);
        if (isset($data['ChildParameter'])) {
            foreach ($data['ChildParameter'] as $idx => $datum) {
                if (!is_numeric($idx)) {
                    $this->_childList[$idx] = new ChildParameter($datum);
                } else {
                    $item = ChildParameter::findOne(['id' => $idx]);
                    if ($item == null) {
                        $item = new ChildParameter($datum);
                    }
                    $item->load(['Data' => $datum], 'Data');
                    $item->actionId = isset($datum['actionId']) ? $datum['actionId'] : null;
                    $this->_childList[$idx] = $item;
                }
            }
        }
        return $ld;
    }


    /**
     * @param \yii\widgets\ActiveForm $form
     * @return string
     */
    public function renderDetail($form)
    {
        $out = '<thead><tr>
                    <th class="text-center">action</th>
                    <th class="text-center" >Name</th>
                    <th class="text-center" >Unit</th>
                    <th class="text-center" >Method</th>
                    <th class="text-center" >Col Name</th>
                    </tr></thead><tbody>';
        foreach ($this->childList as $idx => $child) {
            $child->idName = $child->id == null ? 'newrow_' . $idx : $child->id;
            $out .= Html::tag('tr',
                Html::tag('td', $child->action) .
                Html::tag('td', $form->field($child, 'name', ['template' => '<label class="">{input}</label>{error}'])) .
                Html::tag('td', $form->field($child, 'unit', ['template' => '<label class="">{input}</label>{error}'])) .
                Html::tag('td', $form->field($child, 'method', ['template' => '<label class="">{input}</label>{error}'])) .
                Html::tag('td', $form->field($child, 'col_name', ['template' => '<label class="">{input}</label>{error}'])->textInput([
                    'readonly' => 'readonly'
                ]))
                , ['data-id' => $child->idName]
            );
        }

        return Html::tag('table', $out . '</tbody>', ['class' => 'table']);
    }

    /**
     * @param int $rowTo
     * @return array
     */
    public static function renderRowAjax($rowTo = 0)
    {
        $form = ActiveForm::begin();
        $child = new ChildParameter();
        $child->idName = 'newrow_' . $rowTo;

        $out = Html::tag('tr',
            Html::tag('td', $child->action) .
            Html::tag('td', $form->field($child, 'name', ['template' => '<label class="">{input}</label>{error}'])) .
            Html::tag('td', $form->field($child, 'unit', ['template' => '<label class="">{input}</label>{error}'])) .
            Html::tag('td', $form->field($child, 'method', ['template' => '<label class="">{input}</label>{error}'])) .
            Html::tag('td', $form->field($child, 'col_name', ['template' => '<label class="">{input}</label>{error}']))
            , ['data-id' => $child->idName]
        );

        return [
            'data' => $out,
            'jsAdd' => ArrayHelper::toArray($form->attributes),
        ];
    }


    /**
     * @param $name
     * @return string|string[]|null
     */
    public static function generateColumnName($name)
    {
        $migration = new Migration();
        $name = strtolower($name);
        $name = $migration->clean($name);
        if ((int)$name[0] > 0) {
            $name = 'col_' . $name;
        }
        if ($migration->columnExists(DataAnalisa::tableName(), $name)) {
//            $a = self::find()->where(['like', 'col_name', $name])->count();
//            $a /= 2;
//            $num = str_pad((int)$a, 2, '0', STR_PAD_LEFT);
            return $name;
        }
//        $migration->addColumn(DataAnalisa::tableName(), $name, 'VARCHAR(10)');
//        $migration->addColumn(DataAnalisa::tableName(), $name . '_code', 'VARCHAR(2)');
        return $name;
    }

    /**
     * @return ChildParameter[]
     */
    public function getChildList()
    {
        if ($this->_childList == null) {
            $this->_childList = $this->child;
        }
        return $this->_childList;
    }
}