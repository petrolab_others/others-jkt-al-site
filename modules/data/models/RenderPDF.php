<?php
/**
 * Created by
 * User: Wisard17
 * Date: 19/03/2019
 * Time: 01.49 PM
 */

namespace app\modules\data\models;

/**
 * Class RenderPDF
 * @package app\modules\data\models
 *
 * @property int $maxCol
 * @property string $type
 */
class RenderPDF extends DataAnalisa
{
    public $draft = false;

    public $viewMode = 'pdf';

    public $show_all_param = false;

    public $mpc_only = false;

    public function ttdMeneger($site)
    {
        $listSite = [
            'J' => 'Endriastuti, S.Si',
            'B' => 'Reko Sayogo',
        ];
        return isset($listSite[$site]) ? $listSite[$site] : '';
    }

    public function getType()
    {
        return 'fuel';
    }

    public function nameType()
    {
        return $this->typeReport == null ? 'OIL ANALYSIS REPORT' : $this->typeReport->header_name;
    }

    public function getMaxCol()
    {
        $out = 4;
        if ($this->isHistory) {
            $out +=5;
        } else {
            $out++;
        }
        if ($this->typeReport->reference == 1) {
            $out++;
        }
        if ((string)$this->typeReport->limit_type == '1') {
            $out += 2;
        }
        if ((string)$this->typeReport->limit_type == '2') {
            $out++;
        }
        return $out;
    }

    public function chart1()
    {
        $ch = new Chart();
        $category = [];
        $store = [];
        $data = [];
        foreach ($this->history as $idx => $item) {
            $category[$idx] = $item->sampleDate;
            $store[''][] = (string)$item->visc_40 === '' ? NoValue : (float)$item->visc_40;
        }
        foreach ($store as $label => $value) {
            $data = [['label' => $label, 'dataset' => $value]];
        }
        return $ch->makeChart(600,200, 'Viscosity Kin at  40°C',$category, $data);
    }

    public function chart2()
    {
        $ch = new Chart();
        $w_m = Parameters::findAll([20,28]);
        $category = [];
        $store = [];
        $data = [];
        foreach ($this->history as $idx => $item) {
            $category[$idx] = $item->sampleDate;
            foreach ($w_m as $value) {
                foreach ($value->child as $child) {
                    $col_name = $child->col_name;
                    $store[$child->name][] = (string)$item->$col_name === '' ? NoValue : (float)$item->$col_name;
                }
            }
        }
        foreach ($store as $label => $value) {
            $data[] = ['label' => $label, 'dataset' => $value];
        }
        return $ch->makeChartLagen(600,200, 'Wear Metals, Contaminants (ppm)',$category, $data);
    }

    public function chart3()
    {
        $ch = new Chart();
        $category = [];
        $store = [];
        $data = [];
        foreach ($this->history as $idx => $item) {
            $category[$idx] = $item->sampleDate;
            $store[''][] = (string)$item->water_content === '' ? NoValue : (float)$item->water_content;
        }
        foreach ($store as $label => $value) {
            $data = [['label' => $label, 'dataset' => $value]];
        }
        return $ch->makeChart(600,200, 'Water Content (ppm)', $category, $data);
    }

    public function chart4()
    {
        $ch = new Chart();
        $category = [];
        $store = [];
        $data = [];
        foreach ($this->history as $idx => $item) {
            $category[$idx] = $item->sampleDate;
            $store[''][] = (string)$item->tan === '' ? NoValue : (float)$item->tan;
        }
        foreach ($store as $label => $value) {
            $data = [['label' => $label, 'dataset' => $value]];
        }
        return $ch->makeChart(600,200, 'Total Acid Number (TAN)',$category, $data);
    }

    public function chart5()
    {
        $ch = new Chart();
        $category = [];
        $store = [];
        $data = [];
        foreach ($this->history as $idx => $item) {
            $date = $item->sampleDate == NULL ? $item->received_date : $item->sampleDate;
            $category[$idx] = date('Y-m-d', strtotime($date));
            foreach($item->dataAnalisaHasParameters as $x){
                if($x->col_name == 'visc_40'){
                    $store[''][] = (string)$x->value === '' ? NoValue : (float)$x->value;
                }
            }
        }
        foreach ($store as $label => $value) {
            $data = [['label' => $label, 'dataset' => $value]];
        }
        return $ch->makeChart(600,200, 'Viscosity Kin at  40°C',$category, $data);
    }

    public function chart6()
    {
        $ch = new Chart();
        $category = [];
        $store = [];
        $data = [];
        foreach ($this->history as $idx => $item) {
            $date = $item->sampleDate == NULL ? $item->received_date : $item->sampleDate;
            $category[$idx] = date('Y-m-d', strtotime($date));
            foreach($item->dataAnalisaHasParameters as $x){
                if($x->col_name == 'visc_100'){
                    $store[''][] = (string)$x->value === '' ? NoValue : (float)$x->value;
                }
            }
        }
        foreach ($store as $label => $value) {
            $data = [['label' => $label, 'dataset' => $value]];
        }
        return $ch->makeChart(600,200, 'Viscosity Kin at  100°C',$category, $data);
    }

    public function chartMpc()
    {
        $ch = new Chart();
        $category = [];
        $store = [];
        $data = [];
        foreach ($this->history as $idx => $item) {
            $category[$idx] = $item->lab_number;
            $store[''][] = (string)$item->mpc === '' ? NoValue : (float)$item->mpc;
        }
        foreach ($store as $label => $value) {
            $data = [['label' => $label, 'dataset' => $value]];
        }
        return $ch->makeChart(600,200, 'MPC Trending',$category, $data, 100);
    }
}
