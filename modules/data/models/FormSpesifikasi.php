<?php
/**
 * Created by
 * User: Wisard17
 * Date: 05/04/2019
 * Time: 01.46 PM
 */

namespace app\modules\data\models;


use app\components\ArrayHelper;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class FormSpesifikasi
 * @package app\modules\data\models
 */
class FormSpesifikasi extends Spesifikasi
{


    /**
     * @param bool $runValidation
     * @param null $attributeNames
     * @return bool
     * @throws \Throwable
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {

            if (parent::save($runValidation, $attributeNames)) {
                foreach ($this->typical as $item) {

                    if ($item->actionId == 'del') {
                        $item->delete();
                        continue;
                    }
                    $item->spesifikasi_id = $this->id;
                    if (!$item->save()) {
                        $this->addError('typical', 'kesalahan pada  :' . ArrayHelper::toString($item->errors));
                        $transaction->rollBack();
                        return false;
                    }
                }
                $transaction->commit();
                return true;
            }
            $transaction->rollBack();
            return false;
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    /**
     * @param array $data
     * @param null|string $formName
     * @return bool
     */
    public function load($data, $formName = null)
    {
        $data = $data == null ? [] : $data;
        $ld = parent::load($data, $formName);
        if (isset($data['Typical'])) {
            foreach ($data['Typical'] as $idx => $datum) {
                $item = Typical::findOne($idx);
                if ($item == null) {
                    $item = new Typical($datum);
                }
                $item->load(['Data' => $datum], 'Data');
                $item->actionId = isset($datum['actionId']) ? $datum['actionId'] : null;
                $this->_typical[$idx] = $item;
            }
        }
        return $ld;
    }


    /**
     * @param \yii\widgets\ActiveForm $form
     * @return string
     */
    public function renderDetail($form)
    {
        $out = '<thead><tr>
                    <th class="text-center">action</th>
                    <th class="text-center" >Name</th>
                    <th class="text-center" >Value</th>
                    <th class="text-center" >Condition Update Code</th>
                    </tr></thead><tbody>';
        foreach ($this->typical as $idx => $child) {
            $child->idName = $child->id == '' ? 'newrow_' . $idx : $child->id;
            $out .= Html::tag('tr',
                Html::tag('td', $child->action) .
                Html::tag('td', $form->field($child, 'new_oil_parameter_id', ['template' => '<label class="">{input}</label>{error}'])
                    ->dropDownList($child->new_oil_parameter_id == '' ? [] :
                        [$child->new_oil_parameter_id => $child->parameter->name], ['class' => 'form-control select2ajax', 'style' => '100%',
                        'prompt' => '- Select Parameter -', 'data-url' => Url::toRoute(['/data/parameter/list-parameters']),
                    ])) .
                Html::tag('td', $form->field($child, 'value', ['template' => '<label class="">{input}</label>{error}'])) .
                Html::tag('td', $form->field($child, 'condition2') . $form->field($child, 'condition3') .
                    $form->field($child, 'condition4'))
                , ['data-id' => $child->idName]
            );
        }

        return Html::tag('table', $out . '</tbody>', ['class' => 'table']);
    }

    /**
     * @param int $rowTo
     * @return array
     */
    public static function renderRowAjax($rowTo = 0)
    {
        $form = ActiveForm::begin();
        $child = new Typical();
        $child->idName = 'newrow_' . $rowTo;

        $out = Html::tag('tr',
            Html::tag('td', $child->action) .
            Html::tag('td', $form->field($child, 'new_oil_parameter_id', ['template' => '<label class="">{input}</label>{error}'])
                ->dropDownList($child->new_oil_parameter_id == '' ? [] :
                    [$child->new_oil_parameter_id => $child->parameter->name], ['class' => 'form-control select2ajax', 'style' => '100%',
                    'prompt' => '- Select Parameter -', 'data-url' => Url::toRoute(['/data/parameter/list-parameters']),
                ])) .
            Html::tag('td', $form->field($child, 'value', ['template' => '<label class="">{input}</label>{error}'])) .
            Html::tag('td', $form->field($child, 'condition2') . $form->field($child, 'condition3') .
                $form->field($child, 'condition4'))
            , ['data-id' => $child->idName]
        );

        return [
            'data' => $out,
            'jsAdd' => ArrayHelper::toArray($form->attributes),
        ];
    }
}