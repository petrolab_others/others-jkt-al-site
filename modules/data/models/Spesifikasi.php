<?php
/**
 * Created by
 * User: Wisard17
 * Date: 25/02/2019
 * Time: 10.16 AM
 */

namespace app\modules\data\models;


use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class Spesifikasi
 * @package app\modules\data\models
 *
 * @property string $allParameter
 * @property Typical[] $typical
 * @property Typical[] $typicals
 * @property string $action
 */
class Spesifikasi extends \app\modelsDB\Spesifikasi
{
    /**
     * @return string
     */
    public function getAction()
    {
        return Html::tag('div',
            Html::tag('a', 'Action <span class="caret"></span>', [
                'class' => 'btn btn-default dropdown-toggle', 'data-toggle' => 'dropdown', 'aria-expanded' => 'false'
            ]) . Html::tag('ul',
//                Html::tag('li', Html::tag('a', '<i class="fa fa-caret-square-o-right"></i> view',
//                    ['href' => Url::toRoute(['/data/spesifikasi/view', 'id' => $this->id])])) .
                Html::tag('li', Html::tag('a', '<i class="fa fa-edit"></i> Edit',
                    ['href' => Url::toRoute(['/data/spesifikasi/edit', 'id' => $this->id])])) .
                Html::tag('li', Html::tag('a', '<i class="fa fa-copy"></i> Duplicate',
                    ['href' => Url::toRoute(['/data/spesifikasi/new', 'duplicate' => $this->id])])) .
                Html::tag('li', Html::tag('a', '<i class="fa fa-times"></i> Delete',
                    ['href' => Url::toRoute(['/data/spesifikasi/del', 'id' => $this->id]),
                        'data-method' => 'post',
                        'data-confirm' => 'Are you absolutely sure ? You will lose all the information about this user with this action.',
                    ]))
                , ['class' => 'dropdown-menu']), ['class' => 'btn-group']);
    }

    /** @var Typical[] */
    public $_typical;


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTypicals()
    {
        return $this->hasMany(Typical::className(), ['spesifikasi_id' => 'id']);
    }

    /**
     * @return Typical[]
     */
    public function getTypical()
    {
        if ($this->_typical == null) {
            $this->_typical = $this->typicals;
        }
        return $this->_typical;
    }

    public $_ap;

    public function getAllParameter()
    {
        if ($this->_ap == null) {
            $out = '';
            foreach ($this->typical as $tp) {
                $out .= Html::tag('li', $tp->parameter->name . ' : ' . $tp->value);
            }
            $this->_ap = Html::tag('ul', $out);
        }
        return $this->_ap;
    }
}