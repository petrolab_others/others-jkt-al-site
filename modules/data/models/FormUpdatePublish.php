<?php
/**
 * Created by
 * User: Wisard17
 * Date: 26/03/2019
 * Time: 11.14 AM
 */

namespace app\modules\data\models;

use app\components\ArrayHelper;
use Curl\Curl;
use yii\helpers\Json;

/**
 * Class FormUpdatePublish
 * @package app\modules\data\models
 */
class FormUpdatePublish extends SearchData
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['publish'], 'string'],
        ];
    }

    /**
     * @param bool $runValidation
     * @param null $attributeNames
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        $transaction = $this::getDb()->beginTransaction();
        try {
            $this->publish = $this->publish === '' ? null : $this->publish;
            $err = $this->errors;
            if (parent::save($runValidation, $attributeNames) && count($err) < 1) {

                $this->status_save = 'success';
                $this->message = 'Berhasil Disimpan';
                $transaction->commit();
                return true;
            }

            $this->status_save = 'error';
            $this->message = 'Data Tidak disimpan, Coba lagi.<br>' . ArrayHelper::toString($this->errors);

            $transaction->rollBack();
            return false;
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch(\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    /**
     * @var string
     */
    public $message = '';

    /**
     * @var string
     */
    public $status_save = '';
    /**
     * @var array
     */
    public $dataApi=[];
    /**
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function changeState()
    {
        $user = \Yii::$app->user->identity;
        if (in_array($user->ruleAccess, [2, 3], true)) {
            $this->addError('publish', 'this user does not have permission ');
            $this->message = 'Data Tidak disimpan, Coba lagi.<br>' . ArrayHelper::toString($this->errors);
            return false;
        }
        if ($this->verify_date == '' && $this->publish == '') {
            $this->addError('publish', 'data belum diverifikasi');
            $this->message = 'Data Tidak disimpan, Coba lagi.<br>' . ArrayHelper::toString($this->errors);
            return false;
        }
        $this->publish = $this->publish == '' ? date('Y-m-d H:i:s') : null;
        return $this->save();
    }

    /**
     * @param $params
     * @return false
     */
    public function updateStatus($params){
        $query = (new self)->searchData($params, true);

        if (isset($params['lab_no'])) {
            $query->andWhere(['lab_number' => $params['lab_no']]);
        }
        if ($query->where == '') {
            $this->message = 'Filter First';
            return false;
        }
        $query->andWhere("lab_number like '%F%'");
//        $query->andWhere("tbl_branch.branch like='%pama%'");
        $data = $query->all();
        if ($data == null) {
            $this->message = 'No Data Found';
            return false;
        }
        if (!$this->saveApi($data)) {
            $this->message = 'error to import data';
            return false;
        }else{
            $this->message='Success  upload  data PAMA  sebanyak  '.$this->rowCount;
            return true;
        }

    }

    /**
     * @var string
     */
    private  $url_api = 'https://pama.petrolab.co.id/api_erp';
    private $rowCount=0;
    public function saveApi($data)
    {
        $api=[];

        foreach ($data as $record){
            $temp=[];
            $this->rowCount++;
            $record->publish=1;
            foreach ($this->ListProperty() as $prop => $value){
                $temp=array_merge($temp,[$value=>$record->$prop]);
            }
            foreach($record->temp_value() as $col){
                if(isset($this->colByParam()[$col->col_name])){
                    $col_name=$this->colByParam()[$col->col_name];
                    $col_code=$col_name.'_code';
                    $temp=array_merge($temp,[$col_name=>$col->value,$col_code=>$col->code]);
                }
            }
            $api['data'][] = $temp;
        }
        if ($api == []) {
            return true;
        }
        $c = new Curl();
        $c->setOpt(CURLOPT_SSL_VERIFYPEER, false);
        $c->setTimeout(10000);
        $c->post($this->url_api . '/data-upload/fuel', ['all' => Json::encode($api)]);
        if ($c->error) {

            return false;
        }
        if (isset($c->response->sv)) {
            $this->dataApi = [
                'save' => ArrayHelper::toArray($c->response->sv),
            ];
            // $dataStatus = ArrayHelper::toArray($c->response->sv);
        }

        return true;
    }

    /**
     * @return string[]
     */

private function ListProperty()
{
    return [
        'id'=>'id',
        'lab_number'=>'lab_number',
        'sampling_date'=>'sampling_date',
        'received_date'=>'received_date',
        'report_date'=>'report_date',
        'type_report_id'=>'type_report_id',
        'spesifikasi_id'=>'spesifikasi_id',
        'sample_name'=>'type',
        'customerName'=>'branch',
        'CustomerID'=>'customer_id',
        'forCustomer'=>'name',
        'address'=>'address',
        'unitNo'=>'eng_sn',
        'componentName'=>'eng_type',
        'brand'=>'eng_builder',
        'unitLocation'=>'eng_location',
        'report_number'=>'report_number',
        'req_order'=>'req_order',
        'eval_code'=>'eval_code',
        'recom1'=>'recommended1',
        'publish'=>'publish',
    ];
}

    /**
     * @return string[]
     */
    private  function colByParam(){
        return [
            'appearance'=>'appearance',
            '_density_at_15c'=>'density',
            'tan'=>'total_acid_number',
            'flash_point__pmcc_'=>'flash_point',
            'visc_40'=>'kinematic_viscosity',
            'water_content'=>'water_content',
            'water_content_by_distillation'=>'water_content',
            'ash_content'=>'ash_content',
            'pour_point'=>'pour_point',
            'cetane_index'=>'cetane_index',
            'conradson_carbon_residue_on_10_distillate_residue'=>'conradson_carbon',
            'distillation_recovery_basis_00'=>'',
            'ibp00'=>'distillation_ibp',
            '_5__vol00'=>'distillation_5',
            'col_10_vol00'=>'distillation_10',
            'col_20_vol'=>'distillation_20',
            'col_30_vol'=>'distillation_30',
            'col_40_vol'=>'distillation_40',
            'col_50_vol'=>'distillation_50',
            'col_60_vol'=>'distillation_60',
            'col_70_vol'=>'distillation_70',
            'col_80_vol'=>'distillation_80',
            'col_90_vol'=>'distillation_90',
            'col_95_vol'=>'distillation_95',
            'ep'=>'distillation_ep',
            'recovery__vol'=>'distillation_recovery',
            'residue__vol'=>'distillation_residue',
            'loss__vol'=>'distillation_loss',
            'recovery_at_300c'=>'distillation_recovery_300',
            'total_sulphur_content_'=>'sulphur_content',
            'sulphur_s_wtastm_d518518'=>'sulphur_content',
            'sulphur_content'=>'sulphur_content',
            'sulphur_content_00'=>'sulphur_content',
            'sulfur_s'=>'sulphur_content',
            'fe_iron00'=>'fe_iron',
            'cu_copper00'=>'cu_copper',
            'al_alumunium01'=>'alumunium',
            'cr_chromium'=>'cr_chromium',
            'ni_nickel'=>'ni_nickel',
            'sn_tin'=>'sn_tin',
            'pb_lead00'=>'pb_lead',
            'si_silicon'=>'silicon',
            'ag_silver_'=>'ag_silver',
            'ti_titanium_'=>'ti_titanium',
            'mo_molybdenum_'=>'mo_molybdenum',
            'zn_zinc'=>'zn_zinc',
            'zinc_zn'=>'zn_zinc',
            'sodium_naastm_d_518513e1'=>'sodium',
            'calcium_ca'=>'calcium_ca',
            'phosphor_p'=>'phosphor_p',
            'boron_b_sup'=>'boron_b',
            'potassium_k_sup'=>'potassium_k',
            'vanadium_v_sup'=>'vanadium_v',
            'barium_ba_sup'=>'barium_ba',
            'manganese_mn_sup'=>'manganese_mn',
            'cadmium_cd'=>'cadmium_cd',
            'magnessium_mg'=>'magnessium_mg',
            'mg_magnessium'=>'magnessium_mg',
            'sediment'=>'sediment',
            'fame_content'=>'fame_content',
            'oxidation_stability_rancimat'=>'rancimat',
            'lubricity_by_hfrr'=>'lubricity_of_diesel',
            'colour_astm_'=>'colour',
            'solid_particles_particle_size_'=>'',
            'col_4_m'=>'particles_4',
            'col_6_m'=>'particles_6',
            'col_14_m'=>'particles_14',
            'iso_code__4406'=>'iso_4406',
            'corrosion_copper_strip_3h50c'=>'copper_corrosion',
            'total_glycerol'=>'total_glyceride',
            'total_glycerol_sni_7182sni_71822015'=>'total_glyceride',
            'mono_glycerinegc'=>'monoglyceride',
            'monoglyceride'=>'monoglyceride',
            'partikulat_astm_d_6217'=>'particulat',
            'metal_content'=>'',
             'microbiological_growthastm_d_7463'=>'microbiological'
        ];
    }

}