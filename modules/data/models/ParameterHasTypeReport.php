<?php
/**
 * Created by
 * User: Wisard17
 * Date: 19/03/2019
 * Time: 01.53 PM
 */

namespace app\modules\data\models;

/**
 * Class ParameterHasTypeReport
 * @package app\modules\data\models
 *
 * @property Parameters $parameters
 */
class ParameterHasTypeReport extends \app\modelsDB\ParameterHasTypeReport
{

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParameters()
    {
        return $this->hasOne(Parameters::className(), ['id' => 'parameter_id']);
    }
}