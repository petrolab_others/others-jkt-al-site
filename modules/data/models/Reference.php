<?php
/**
 * Created by
 * User: Wisard17
 * Date: 25/02/2019
 * Time: 10.10 AM
 */

namespace app\modules\data\models;


use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class Reference
 * @package app\modules\data\models
 *
 * @property \app\modules\data\models\ParameterHasReference[] $parameterHasReference
 * @property mixed $allParameter
 * @property string $action
 */
class Reference extends \app\modelsDB\Reference
{

    public function rules()
    {
        return array_merge(parent::rules(), [
            ['name', 'unique'],
            ['name', 'required'],
        ]);
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'allParameter' => 'Parameter',
        ]);
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return Html::tag('div',
            Html::tag('a', 'Action <span class="caret"></span>', [
                'class' => 'btn btn-default dropdown-toggle', 'data-toggle' => 'dropdown', 'aria-expanded' => 'false'
            ]) . Html::tag('ul',
//                Html::tag('li', Html::tag('a', '<i class="fa fa-caret-square-o-right"></i> view',
//                    ['href' => Url::toRoute(['/data/reference/view', 'id' => $this->id])])) .
                Html::tag('li', Html::tag('a', '<i class="fa fa-edit"></i> Edit',
                    ['href' => Url::toRoute(['/data/reference/edit', 'id' => $this->id])])) .
                Html::tag('li', Html::tag('a', '<i class="fa fa-copy"></i> Duplicate',
                    ['href' => Url::toRoute(['/data/reference/new', 'duplicate' => $this->id])])) .
                Html::tag('li', Html::tag('a', '<i class="fa fa-times"></i> Delete',
                    ['href' => Url::toRoute(['/data/reference/del', 'id' => $this->id]),
                        'data-method' => 'post',
                        'data-confirm' => 'Are you absolutely sure ? You will lose all the information about this user with this action.',
                    ]))
                , ['class' => 'dropdown-menu']), ['class' => 'btn-group']);
    }

    /** @var ParameterHasReference[] */
    public $_parameterHasReference;

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParameterHasReferences()
    {
        return $this->hasMany(ParameterHasReference::className(), ['reference_id' => 'id']);
    }

    /**
     * @return ParameterHasReference[]
     */
    public function getParameterHasReference()
    {
        if ($this->_parameterHasReference == null) {
            $this->_parameterHasReference = $this->parameterHasReferences;
        }
        return $this->_parameterHasReference;
    }

    public $_ap;

    public function getAllParameter()
    {
        if ($this->_ap == null) {
            $out = '';
            foreach ($this->parameterHasReference as $hasReference) {
                $out .= Html::tag('li', $hasReference->parameter->name . ' : ' . $hasReference->value);
            }
            $this->_ap = Html::tag('ul', $out);
        }
        return $this->_ap;
    }

}