<?php
/**
 * Created by
 * User: Wisard17
 * Date: 01/04/2019
 * Time: 10.10 AM
 */

namespace app\modules\data\models;


use app\components\ArrayHelper;
use Yii;
use yii\helpers\Html;

/**
 * Class FormTypeReport
 * @package app\modules\data\models
 */
class FormTypeReport extends TypeReport
{
    public $list_parameter = [];

    public $all_param = [];

    public $parameter_del_list = [];

    public function rules()
    {
        return array_merge(parent::rules(), [
            [['all_param', 'parameter_del_list'], 'each', 'rule' => ['safe']]
        ]);
    }

    /**
     * @param \yii\widgets\ActiveForm $form
     * @return string
     */
    public function renderDetail($form)
    {
        $out = '';
        foreach ($this->parametersList as $idx => $parameter) {
            $out .= Html::tag('li', Html::tag('p', $parameter->name) .
                Html::activeHiddenInput($this, 'all_param[]',  ['value' => $parameter->id]).
                Html::tag('span', 'x', ['class' => 'badge', 'style' => 'left: 3px;', 'data-action' => 'del_param'])
                , ['class' => 'btn btn-default', 'data-id' => $parameter->id]);
        }

        return $out;
    }

    public static function renderRowAjax($id)
    {
        $parameter = Parameters::findOne($id);
        if ($parameter == null) {
            return '';
        }
        return Html::tag('li', Html::tag('p', $parameter->name) .
            Html::activeHiddenInput(new self(), 'all_param[]',  ['value' => $parameter->id]).
            Html::tag('span', 'x', ['class' => 'badge', 'style' => 'left: 3px;', 'data-action' => 'del_param'])
            , ['class' => 'btn btn-default', 'data-id' => $parameter->id]);
    }

    public function load($data, $formName = null)
    {
//        $this->list_parameter = isset($data['list_parameter']) ? $data['list_parameter'] : '';
        $this->_del_param = isset($data['parameter_del_list']) ? $data['parameter_del_list'] : [];
        if (parent::load($data, $formName)) {
            $this->_parameter_list = Parameters::findAll($this->all_param);
            return true;
        }
        return false;
    }

    public $_del_param;

    /**
     * @param bool $runValidation
     * @param null $attributeNames
     * @return bool
     * @throws \Exception
     * @throws \Throwable
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            if (parent::save($runValidation, $attributeNames)) {
                foreach ($this->parameter_del_list as $del_id) {
                    $del = ParameterHasTypeReport::findOne(['type_report_id' => $this->id, 'parameter_id' => $del_id]);
                    if ($del != null) {
                        $del->delete();
                    }
                }
                foreach ($this->all_param as $order => $param_id) {
                    $item = ParameterHasTypeReport::findOne(['type_report_id' => $this->id, 'parameter_id' => $param_id]);
                    if ($item == null) {
                        $item = new ParameterHasTypeReport(['type_report_id' => $this->id, 'parameter_id' => $param_id]);
                    }
                    $item->order = $order;

                    if (!$item->save()) {
                        $this->addError('list_parameter', 'kesalahan pada child :' . ArrayHelper::toString($item->errors));
                        $transaction->rollBack();
                        return false;
                    }
                }

                $transaction->commit();
                return true;
            }
            $transaction->rollBack();
            return false;
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
}