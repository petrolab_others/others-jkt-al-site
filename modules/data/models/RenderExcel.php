<?php
/**
 * Created by
 * User     : Wisard17
 * Date     : 31/10/2019
 * Time     : 11.39 AM
 * File Name: RenderExcel.php
 **/

namespace app\modules\data\models;


use app\components\Excel;
use yii\helpers\Html;

/**
 * Class RenderExcel
 * @package app\modules\data\models
 */
class RenderExcel extends DataAnalisa
{

    /**
     * @param string $fileName
     * @return string
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     * @throws \PHPExcel_Writer_Exception
     */
    public function generateExcel($fileName = '')
    {
        $e = new Excel();
        $p = $e->newExcel();
        $row = 2;
        $col = 'A';

        $listHeader = [
            ['customerName', 'unitNo'],
            ['address', 'componentName'],
            ['forCustomer', 'OIL_MATRIX'],
            ['unitLocation', 'matrix'],
        ];

        if ((string)$this->typeReport->limit_type == '2') {
            $listHeader[2][1] = 'sample_name';
            $listHeader[3][1] = 'typicalType';
        }

        if ($this->req_order != '') {
            $listHeader[4][0] = 'blank';
            $listHeader[4][1] = 'req_order';
        }

        foreach ($listHeader as $item) {
            foreach ($item as $value) {
                if ($value == 'blank') {
                    continue;
                }
                $p->getActiveSheet()->setCellValue($col . $row, $this->getAttributeLabel($value));
                $col++;
                $p->getActiveSheet()->setCellValue($col . $row,  $this->$value);
                $col++;
                $col++;
            }
            $col = 'A';
            $row++;
        }
        $row++;

        $p->getActiveSheet()->setCellValue($col . $row, 'Test Detail');
        $row++;

        $col_header = ['lab_number', 'sampleDate', 'receiveDate', 'analysisDate', 'reportDate', 'hm_oil', 'hm_unit', 'sample_name'];

        foreach ($col_header as $value) {
            $p->getActiveSheet()->setCellValue($col . $row, $this->getAttributeLabel($value));
            $col++;
            $p->getActiveSheet()->setCellValue($col . $row, $this->$value);
            $col = 'A';
            $row++;
        }
        $row++;
        $p->getActiveSheet()->setCellValue($col . $row, 'No');
        $col++;
        $p->getActiveSheet()->setCellValue($col . $row, 'Parameter');
        $col++;
        $p->getActiveSheet()->setCellValue($col . $row, 'Unit');
        $col++;
        $p->getActiveSheet()->setCellValue($col . $row, 'Method');
        $col++;
        $p->getActiveSheet()->setCellValue($col . $row, 'Result');
        $col= 'A';
        $row++;
        $i = 1;
        foreach ($this->renderParameter() as $category => $value) {
            if ((string)$value->parameter->parent == '0') {
                $p->getActiveSheet()->setCellValue($col . $row, $i);
                $col++;
                $p->getActiveSheet()->setCellValue($col . $row, $value->parameter->name);
                $col++;
                $p->getActiveSheet()->setCellValue($col . $row, $value->parameter->unit);
                $col++;
                $p->getActiveSheet()->setCellValue($col . $row, $value->parameter->method);
                $col = 'A';
                $row++;
                foreach ($value->parameters->child as $item) {
                    $c = $item->col_name;
                    if (!$this->hasAttribute($c)) {
                        continue;
                    }
                    if ($this->$c == '' ) {
                        continue;
                    }
                    $col++;
                    $p->getActiveSheet()->setCellValue($col . $row, $this->getAttributeLabel($c));
                    $col++;
                    $p->getActiveSheet()->setCellValue($col . $row, $this->getAttributeUnit($c));
                    $col++;
                    $p->getActiveSheet()->setCellValue($col . $row, $this->getAttributeMethod($c));
                    $col++;
                    $p->getActiveSheet()->setCellValue($col . $row, html_entity_decode($this->$c));
                    $col = 'A';
                    $row++;
                }
            } else {
                $c = $value->parameter->col_name;
                if (!$this->hasAttribute($c)) {
                    continue;
                }
                $p->getActiveSheet()->setCellValue($col . $row, $i);
                $col++;
                $p->getActiveSheet()->setCellValue($col . $row, $this->getAttributeLabel($c));
                $col++;
                $p->getActiveSheet()->setCellValue($col . $row, $this->getAttributeUnit($c));
                $col++;
                $p->getActiveSheet()->setCellValue($col . $row, $this->getAttributeMethod($c));
                $col++;
                $p->getActiveSheet()->setCellValue($col . $row, html_entity_decode($this->$c))    ;
                $col = 'A';
                $row++;
            }
            $i++;
        }
        if ($this->mpcShow) {
            $c = 'mpc';
            $p->getActiveSheet()->setCellValue($col . $row, $i);
            $col++;
            $p->getActiveSheet()->setCellValue($col . $row, $this->getAttributeLabel($c));
            $col++;
            $p->getActiveSheet()->setCellValue($col . $row, $this->getAttributeUnit($c));
            $col++;
            $p->getActiveSheet()->setCellValue($col . $row, $this->getAttributeMethod($c));
            $col++;
            $p->getActiveSheet()->setCellValue($col . $row, html_entity_decode($this->$c))    ;
            $col = 'A';
            $row++;
        }

        if ($fileName == '') {
            $fileName = 'Report_excel_' . date('d-m-Y_H-i-s');
        }
        $e->download($p, $fileName);
        return '';
    }
}