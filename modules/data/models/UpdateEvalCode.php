<?php


namespace app\modules\data\models;


use app\components\ArrayHelper;
use Yii;

/**
 * Class UpdateEvalCode
 * @package app\modules\data\models
 */
class UpdateEvalCode extends SearchData
{

    public $updateEvalCode;

    /**
     * @param $params
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function updateCode($params)
    {
        $query = (new self)->searchData($params, true);

        if (isset($params['lab_no'])) {
            $query->andWhere(['lab_number' => $params['lab_no']]);
        }

        if ($query->where == '') {
            $this->message = 'Filter First';
            return false;
        }

        $data = $query->all();
        if ($data == null) {
            $this->message = 'No Data Found';
            return false;
        }
        $statusSuccees = 0;
        $out = true;
        $msg = '';
        foreach ($data as $report) {
            if ($report->updateEvalCode()) {
                $this->data[$report->rowId] = $report->status;
                $statusSuccees++;
            } else {
                $out = false;
                $msg .=
                    "Lab No $report->Lab_No error : " . ArrayHelper::toString($report->errors) . '<br>';
            }
        }
        $this->message .= $statusSuccees > 1 ? "Success diupdate : $statusSuccees <hr>" : '';
        $this->message .= $msg;
        return $out;
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function updateEvalCode()
    {
        if ($this->publish != '') {
            $this->addError('updateEvalCode', 'Data telah dipublish');
            return false;
        }

        $evalCode = '';
        foreach ($this->parameters as $item) {
            $parameter = $item->col_name;
            $code = $item->col_name . '_code';

            $value = $this->hasAttribute($parameter) ? $this->$parameter : '';
            $value_code = null;
            if ($value != '') {
                $limit = $this->renderLimit($item);
                if (count($limit) == 6) {
                    $minB = isset($limit['Bmin']) ? $limit['Bmin'] : '';
                    $minC = isset($limit['Cmin']) ? $limit['Cmin'] : '';
                    $minD = isset($limit['Dmin']) ? $limit['Dmin'] : '';
                    $maxB = isset($limit['Bmax']) ? $limit['Bmax'] : '';
                    $maxC = isset($limit['Cmax']) ? $limit['Cmax'] : '';
                    $maxD = isset($limit['Dmax']) ? $limit['Dmax'] : '';
                    if (($minB != '') &&
                        ($minC != '') &&
                        ($maxB != '') &&
                        ($maxC != '') ) {
                        $NilaiViscocity = '';
                        If (($maxB > $value) && ($value > $minB)) {
                            $NilaiViscocity = '';
                        }

                        If (($minB >= $value) && ($value > $minC)) {
                            $NilaiViscocity = 'BL';
                            $attention = 1;
                        }
                        if ($minD != '') {
                            If (($minC >= $value) && ($value > $minD)) {
                                $NilaiViscocity = 'CL';
                                $urgent = 1;
                            }
                            If ($minD >= $value) {
                                $NilaiViscocity = 'DL';
                                $urgent = 1;
                            }
                        }

                        If (($maxB <= $value) && ($value < $maxC)) {
                            $NilaiViscocity = 'BH';
                            $attention = 1;
                        }
                        if ($maxD != '') {
                            If (($maxC <= $value) && ($value < $maxD)) {
                                $NilaiViscocity = 'CH';
                                $urgent = 1;
                            }
                            If ($maxD <= $value) {
                                $NilaiViscocity = 'DH';
                                $urgent = 1;
                            }
                        }
                        $value_code = strlen($NilaiViscocity) > 1 ? $NilaiViscocity[0] : $NilaiViscocity;
                    }
                }
                if (count($limit) == 3) {
                    $B = isset($limit['B']) ? $limit['B'] : '';
                    $C = isset($limit['C']) ? $limit['C'] : '';
                    $D = isset($limit['D']) ? $limit['D'] : '';
                    if (($B != '') && ($C != '')) {
                        if ($B > $C) {
                            if ($value <= $B) {
                                $value_code = 'B';
                            }
                            if ($value <= $C) {
                                $value_code = 'C';
                            }
                            if ($D != '') {
                                if ($value <= $D) {
                                    $value_code = 'D';
                                }
                            }
                        } else {
                            if ($value >= $B) {
                                $value_code = 'B';
                            }
                            if ($value >= $C) {
                                $value_code = 'C';
                            }
                            if (($D != '') && $value >= $D) {
                                $value_code = 'D';
                            }
                        }
                    }
                }
            }
            if ($this->hasAttribute($code)) {
                $this->$code = $value_code;
                if (strlen($value_code) > 1) {
                    $evalCode = $value_code[0] > $evalCode ? $value_code[0] : $evalCode;
                } else {
                    $evalCode = $value_code > $evalCode ? $value_code : $evalCode;
                }
            }
        }

        $this->eval_code = $evalCode == '' ? 'N' : $evalCode;
        return $this->save();
    }

    public $message = '';

    public $status_save = '';

    public $class_message = '';

    public $data = [];

    public function save($runValidation = true, $attributeNames = null)
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            if (parent::save($runValidation, $attributeNames)) {
                $transaction->commit();
                return true;
            }
            $transaction->rollBack();
            return false;
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
}