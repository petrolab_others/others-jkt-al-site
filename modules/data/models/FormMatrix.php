<?php
/**
 * Created by
 * User: Wisard17
 * Date: 12/04/2019
 * Time: 04.12 PM
 */

namespace app\modules\data\models;


use app\components\ArrayHelper;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;

/**
 * Class FormMatrix
 * @package app\modules\data\models
 */
class FormMatrix extends Matrix
{
    /**
     * @param bool $runValidation
     * @param null $attributeNames
     * @return bool
     * @throws \Throwable
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {

            if (parent::save($runValidation, $attributeNames)) {
                foreach ($this->parameterHasMatrix as $item) {
                    if ($item->actionId == 'del') {
                        $item->delete();
                    } else {
                        $item->matrix = $this->Matrix;
                        if (!$item->save()) {
                            $this->addError('parameterHasMatrix', 'kesalahan pada  :' . ArrayHelper::toString($item->errors));
                            $transaction->rollBack();
                            return false;
                        }
                    }
                }
                $transaction->commit();
                return true;
            }
            $transaction->rollBack();
            return false;
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    /**
     * @param array $data
     * @param null|string $formName
     * @return bool
     */
    public function load($data, $formName = null)
    {
        $data = $data == null ? [] : $data;
        $ld = parent::load($data, $formName);
        if (isset($data['ParameterHasMatrix'])) {
            foreach ($data['ParameterHasMatrix'] as $idx => $datum) {
                $pca = explode('_', $idx);
                $matrix = isset($pca[0]) ? $pca[0] : 0;
                $parameterId = isset($pca[1]) ? $pca[1] : 0;
                $item = ParameterHasMatrix::findOne(['matrix' => $matrix, 'parameter_id' => $parameterId]);
                if ($item == null) {
                    $item = new ParameterHasMatrix($datum);
                }
                $item->load(['Data' => $datum], 'Data');
                $item->actionId = isset($datum['actionId']) ? $datum['actionId'] : null;
                $this->_parameterHasMatrix[$idx] = $item;
            }
        }
        return $ld;
    }


    /**
     * @param \yii\widgets\ActiveForm $form
     * @return string
     */
    public function renderDetail($form)
    {
        $out = '<thead><tr>
                    <th class="text-center">action</th>
                    <th class="text-center" >Name</th>
                    <th class="text-center" >Show PDF</th>
                    <th class="text-center" >Condition Update Code</th>                   
                    </tr></thead><tbody>';
        foreach ($this->parameterHasMatrix as $idx => $child) {
            $child->idName = $child->id == '_' ? 'newrow_' . $idx : $child->id;
            $out .= Html::tag('tr',
                Html::tag('td', $child->action) .
                Html::tag('td', $form->field($child, 'parameter_id', ['template' => '<label class="">{input}</label>{error}'])
                    ->dropDownList($child->parameter_id == '' ? [] :
                        [$child->parameter_id => $child->parameter->namePParent], ['class' => 'form-control select2ajax', 'style' => '100%',
                        'prompt' => '- Select Parameter -', 'data-url' => Url::toRoute(['/data/parameter/list-parameters']),
                    ])) .
                Html::tag('td', $form->field($child, 'min') .
                    $form->field($child, 'max')) .
                Html::tag('td', $form->field($child, 'B') . $form->field($child, 'C') . $form->field($child, 'D'))
                , ['data-id' => $child->idName]
            );
        }

        return Html::tag('table', $out . '</tbody>', ['class' => 'table']);
    }

    /**
     * @param int $rowTo
     * @return array
     */
    public static function renderRowAjax($rowTo = 0)
    {
        $form = ActiveForm::begin([
            'layout' => 'horizontal',
            'fieldConfig' => [
                'template' => "{label}\n<div class=\"col-lg-8\">{input}\n{error}</div>",
                'labelOptions' => ['class' => 'col-lg-4 control-label'],
            ],
        ]);
        $child = new ParameterHasMatrix();
        $child->idName = 'newrow_' . $rowTo;

        $out =  Html::tag('tr',
            Html::tag('td', $child->action) .
            Html::tag('td', $form->field($child, 'parameter_id', ['template' => '<label class="">{input}</label>{error}'])
                ->dropDownList($child->parameter_id == '' ? [] :
                    [$child->parameter_id => $child->parameter->namePParent], ['class' => 'form-control select2ajax', 'style' => '100%',
                    'prompt' => '- Select Parameter -', 'data-url' => Url::toRoute(['/data/parameter/list-parameters']),
                ])) .
            Html::tag('td', $form->field($child, 'min') .
                $form->field($child, 'max')) .
            Html::tag('td', $form->field($child, 'B') . $form->field($child, 'C') . $form->field($child, 'D'))
            , ['data-id' => $child->idName]
        );

        return [
            'data' => $out,
            'jsAdd' => ArrayHelper::toArray($form->attributes),
        ];
    }
}