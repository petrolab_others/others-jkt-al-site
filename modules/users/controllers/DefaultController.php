<?php

namespace app\modules\users\controllers;

use app\modules\reports\models\Customer;
use app\modules\users\models\FormUser;
use app\modules\users\models\RuleAccess;
use app\modules\users\models\UserLevel;
use conquer\select2\Select2Action;
use Yii;
use app\modules\users\models\search\SearchUser;
use app\models\AccessControl;
use yii\db\ActiveQuery;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotAcceptableHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Default controller for the `users` module
 */
class DefaultController extends Controller
{

    public function actions()
    {
        return [
            'list-rule' => [
                'class' => Select2Action::className(),
                'dataCallback' => [$this, 'dataRule'],
            ],
            'list-customer' => [
                'class' => Select2Action::className(),
                'dataCallback' => [$this, 'dataCustomer'],
            ],
        ];
    }

    /**
     * @param $q
     * @return array
     */
    public function dataRule($q)
    {
        $query = UserLevel::find();

        $out = [];

        if ($q != '') {
            $query->andWhere(['like', 'User_Level_Name', $q]);
        }

        $query->limit(20)->distinct();

        foreach ($query->all() as $r) {
            $out[] = [
                'id' => $r->User_Level_ID,
                'text' => $r->User_Level_Name,
            ];
        }

        return [
            'results' => $out,
        ];
    }

    /**
     * @param $q
     * @return array[]
     */
    public function dataCustomer($q)
    {
        $query = Customer::find();

        $out = [];

        if ($q != '') {
            $query->andWhere(['like', 'Name', $q]);
        }

        $query->limit(100)->distinct();

        foreach ($query->all() as $r) {
            $out[] = [
                'id' => $r->CustomerID,
                'text' => $r->CustomerID . ' = ' . $r->Name,
            ];
        }

        return [
            'results' => $out,
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new SearchUser();
        $request = Yii::$app->request->queryParams;

        if (Yii::$app->request->isAjax) {
            return $searchModel->searchDataTable($request);
        }

        return $this->render('index');
    }

    /**
     * @param $username
     * @return string
     * @throws NotAcceptableHttpException
     * @throws NotFoundHttpException
     */
    public  function actionProfile($username)
    {
        $model = $this->findModel($username);

        return $this->render('profile', [
            'model' => $model
        ]);
    }

    /**
     * @param $username
     * @return array|string|Response
     * @throws NotAcceptableHttpException
     * @throws NotFoundHttpException
     */
    public  function actionEdit($username)
    {
        $model = $this->findModel($username);

        $post = Yii::$app->request->post();

        if (Yii::$app->request->isAjax && $model->load($post)) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('edit', [
                'model' => $model,
            ]);
        }

        if ($model->load($post) && $model->save()) {
            return $this->redirect(['/users/default/profile', 'username'=> $model->username]);
        }

        return $this->render('edit', [
            'model' => $model,
        ]);
    }

    /**
     * @return array|string|Response
     */
    public  function actionAdd()
    {
        $model = new FormUser();

        $post = Yii::$app->request->post();

        if (Yii::$app->request->isAjax && $model->load($post)) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }

        if ($model->load($post) && $model->save()) {
            return $this->redirect(['/users/default/profile', 'username'=> $model->username]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the Orders model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $username
     * @return FormUser the loaded model
     * @throws NotFoundHttpException|NotAcceptableHttpException if the model cannot be found
     */
    protected function findModel($username)
    {
        if (($model = FormUser::findOne(['Username' => $username])) !== null) {
            if (!RuleAccess::accessSelfProfile($username))
                throw new NotAcceptableHttpException('The requested page not allowed.');
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
