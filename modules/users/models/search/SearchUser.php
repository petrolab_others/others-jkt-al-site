<?php
/**
 * Created by
 * User: Wisard17
 * Date: 17/11/2017
 * Time: 09.21 AM
 */

namespace app\modules\users\models\search;


use app\modules\users\models\User;
use yii\base\Model;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * Class SearchUser
 * @package app\modules\users\models\search
 */
class SearchUser extends User
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Username', 'Password', 'First_Name', 'Last_Name', 'Email', 'User_Level', 'Report_To', 'Activated',
                'Locked', 'Profile', 'Current_URL', 'Theme', 'Menu_Horizontal', 'Table_Width_Style', 'Scroll_Table_Width',
                'Scroll_Table_Height', 'Rows_Vertical_Align_Top', 'Language', 'Redirect_To_Last_Visited_Page_After_Login',
                'Font_Name', 'Font_Size',
            ], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public $allField = [
        'Username' => 'Username',
        'Password' => 'Password',
        'First_Name' => 'First_Name',
        'Last_Name' => 'Last_Name',
        'Email' => 'Email',
        'User_Level' => 'User_Level',
        'Report_To' => 'Report_To',
        'Activated' => 'Activated',
        'Locked' => 'Locked',
        'Profile' => 'Profile',
        'Current_URL' => 'Current_URL',
        'Theme' => 'Theme',
        'Menu_Horizontal' => 'Menu_Horizontal',
        'Table_Width_Style' => 'Table_Width_Style',
        'Scroll_Table_Width' => 'Scroll_Table_Width',
        'Scroll_Table_Height' => 'Scroll_Table_Height',
        'Rows_Vertical_Align_Top' => 'Rows_Vertical_Align_Top',
        'Language' => 'Language',
        'Redirect_To_Last_Visited_Page_After_Login' => 'Redirect_To_Last_Visited_Page_After_Login',
        'Font_Name' => 'Font_Name',
        'Font_Size' => 'Font_Size',


    ];

    public function ordering($params)
    {
        $ncol = isset($params['order'][0]['column']) ? $params['order'][0]['column'] : 0;
        $col = (isset($params['columns'][$ncol]) && array_key_exists($params['columns'][$ncol]['data'], $this->allField)) ?
            $this->allField[$params['columns'][$ncol]['data']] : '';
        $argg = isset($params['order'][0]['dir']) ? $params['order'][0]['dir'] : 'asc';
        $table = self::tableName();

        return $col !== '' ? "$table.$col $argg " : '';
    }


    public $allData;

    public $currentData;

    /**
     * @param Query $query
     * @return int
     */
    public function calcAllData($query)
    {
        return $this->allData == null ? $query->count() : $this->allData;
    }

    /**
     * @param Query $query
     * @param $params
     * @return Query
     */
    public function defaultFilterByUser($query, $params)
    {


        return $query;
    }

    /**
     * @param $params
     * @param bool $export
     * @param bool $join
     * @return $this|\yii\db\ActiveQuery|Query
     */
    public function searchData($params, $export = false, $join = true)
    {

        $odr = $this->ordering($params);

        $query = self::find();

        if (!$join)
            $query = self::find();

        $query = $this->defaultFilterByUser($query, $params);

        $query->orderBy($odr);

        $start = isset($params['start']) ? $params['start'] : 0;
        $lang = isset($params['length']) ? $params['length'] : 10;

        $this->allData = $this->calcAllData($query);

        $fltr = '';
        $fltr2 = '';

        if (isset($params['columns']))
            foreach ($params['columns'] as $col) {
                if (isset($params['search']) && $params['search']['value'] != '') {
                    $lst[] = $col['data'];
                    if (array_key_exists($col['data'], $this->allField) &&
                        !array_key_exists($col['data'], $lst) && $this->allField[$col['data']] != '') {
                        $fltr .= $fltr == '' ? '' : ' or ';
                        if ($this->allField[$col['data']] == 'Customer_ID') {
                            $fltr .= ' `customers`.Customer_Name ' . " like '%" . $params['search']['value'] . "%' ";
                        } else {
                            $fltr .= self::tableName() . '.' . $this->allField[$col['data']] . " like '%" . $params['search']['value'] . "%' ";
                        }
                    }
                }
                if ($col['searchable'] == 'true' && $col['search']['value'] != '' &&
                    array_key_exists($col['data'], $this->allField) && $this->allField[$col['data']] != '') {
                    $fltr2 .= $fltr2 == '' ? '' : ' and ';
                    $fltr2 .= ' ' . $this->allField[$col['data']] . " like '%" . $col['search']['value'] . "%' ";
                    $query->andFilterWhere(['like', self::tableName() . '.' . $this->allField[$col['data']], $col['search']['value']]);
                }

            }

        $query->andWhere($fltr);


        $this->load($params);

        if ($export) {
            return $query;
        }

        $this->currentData = $query->count();

        $query->limit($lang)->offset($start);
        return $query;
    }

    /**
     * @param $params
     * @return mixed
     */
    public function searchDataTable($params)
    {
        $data = $this->searchData($params);
        return Json::encode([
            "draw" => isset ($params['draw']) ? intval($params['draw']) : 0,
            "recordsTotal" => intval($this->allData),
            "recordsFiltered" => intval($this->currentData),
            "data" => $this->renderData($data, $params),
        ]);
    }

    /**
     * @param $query Query
     * @param $params
     * @return array
     */
    public function renderData($query, $params)
    {
        $out = [];

        /** @var self $model */
        foreach ($query->all() as $model) {

            $out[] = array_merge(ArrayHelper::toArray($model), [
                'action' => $model->action,
                'ruleName' => $model->ruleName,
            ]);

        }
        return $out;
    }

}