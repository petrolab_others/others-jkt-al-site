<?php
/**
 * Created by
 * User: Wisard17
 * Date: 20/11/2017
 * Time: 03.35 PM
 */

namespace app\modules\users\models;

use Yii;
use yii\web\NotAcceptableHttpException;

/**
 * Class FormUser
 * @package app\modules\users\models
 */
class FormUser extends User
{

    public $reset_password;

    public function rules()
    {
        return array_merge([
            [['Username', 'Password'], 'required'],
            [['customer_id', 'User_Level', 'Report_To', 'Scroll_Table_Width', 'Scroll_Table_Height'], 'integer'],
            [['Activated', 'Locked', 'Profile', 'Current_URL', 'Menu_Horizontal', 'Table_Width_Style', 'Rows_Vertical_Align_Top', 'Redirect_To_Last_Visited_Page_After_Login'], 'string'],
            [['Username', 'First_Name', 'Last_Name', 'Font_Name'], 'string', 'max' => 50],
            [['Password'], 'string', 'max' => 64],
            [['Email'], 'string', 'max' => 100],

            [['Username', 'Email'], 'unique'],
            ['reset_password', 'safe']
        ]);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $user = Yii::$app->user->identity;
            if (!RuleAccess::accessSelfProfile($user->username)) {
                throw new NotAcceptableHttpException('The requested page not allowed.');
            }

            if ($user->ruleAccess == 4) {
                if(!($this->username == $user->username || in_array($this->User_Level, [2,3,5]))) {
                    throw new NotAcceptableHttpException('The requested page not allowed.');
                }
            }

            if ($this->customer_id == '') {
                $this->customer_id = 0;
            }
            if ($this->isNewRecord) {
                $this->setPassword($this->Password);
                $this->authKey;
            }
            if ($this->reset_password != '') {
                $this->setPassword($this->reset_password);
            }

            return true;
        }
        return false;
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        if (parent::save($runValidation, $attributeNames)) {
            $this->authKey;
            if ($this->additionUser != null) {
                $this->additionUser->save();
            }
            return true;
        }
        return false;
    }
}