<?php
/**
 * Created by
 * User: Wisard17
 * Date: 17/11/2017
 * Time: 10.16 AM
 */

namespace app\modules\users\models;


use app\modules\customer\models\Customer;


/**
 * Class UsersHasCustomers
 * @package app\modules\users\models
 *
 * @property \app\modules\customer\models\Customer $customer
 */
class UsersHasCustomers extends \app\modelsDB\UsersHasCustomers
{
    /**
     * @return \yii\db\ActiveQuery|Customer
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['Customer_ID' => 'customers_Customer_ID']);
    }
}