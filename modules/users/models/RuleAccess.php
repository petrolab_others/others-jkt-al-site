<?php
/**
 * Created by
 * User: Wisard17
 * Date: 25/11/2017
 * Time: 07.40 PM
 */

namespace app\modules\users\models;


use Yii;

/**
 * Class RuleAccess
 * @package app\modules\users\models
 */
class RuleAccess
{

    public static function accessSelfProfile($username)
    {
        $user = Yii::$app->user->identity;

        if ($user->ruleAccess == -1)
            return true;

        if ($user->ruleAccess == 4)
            return true;

        if ($user->username == 'admin')
            return true;

        return $username == $user->username;
    }
}