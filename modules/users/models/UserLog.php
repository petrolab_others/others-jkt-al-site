<?php
/**
 * Created by
 * User: Wisard17
 * Date: 27/04/2018
 * Time: 09.46 AM
 */

namespace app\modules\users\models;


use app\components\ArrayHelper;
use Yii;
use yii\base\Model;
use yii\helpers\Json;

/**
 * Class UserLog
 * @package app\models
 *
 * @property mixed $log
 */
class UserLog extends Model
{
    public $username;
    public $ip_login;
    public $session_id;
    public $date_login;
    public $date_logout;

    /**
     * UserLog constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);
    }

    /**
     * @param array $condition
     * @return self
     */
    public static function findOne($condition)
    {
        $model = new self($condition);
        $file = $model->openFile();
        if (!$file) {
            return null;
        }
        $content = file_get_contents($file);
        Yii::configure($model,Json::decode($content));
        return $model;
    }

    public $log;

    public function filename()
    {
        $st = str_replace(array('/', '\\', '<', '>', ':', '?', '|'), '_', $this->session_id);
        return str_replace(':','_', $this->ip_login) . "_" . $st . '.json';
    }

    /**
     * @return string
     */
    public function openFile()
    {
        $dir = $this->createFolder();
        $file = $this->filename();
        if (!file_exists("$dir/$file")) {
            return false;
        }
        return "$dir/$file";
    }

    /**
     * @return bool|string
     */
    public function createFolder()
    {
        $dir = Yii::getAlias('@logfile');
        $folders = [date('Y-m-d'), $this->username];
        foreach ($folders as $folder) {
            if (!file_exists("$dir/$folder") && !mkdir("$dir/$folder", 0777, true) && !is_dir("$dir/$folder")) {
                throw new \RuntimeException(sprintf('Directory "%s" was not created', "$dir/$folder"));
            }
            $dir = "$dir/$folder";
        }
        return $dir;
    }

    /**
     * @param $json
     * @param $fileName
     * @return bool|int
     */
    public function saveJsonToFile($json, $fileName)
    {
        $fp = fopen($fileName, 'w+');
        $check = fwrite($fp, $json);
        fclose($fp);
        return $check;
    }

    /**
     * @return bool
     */
    public function save()
    {
        $dir = $this->createFolder();
        $file = $this->filename();
        return $this->saveJsonToFile(Json::encode(ArrayHelper::toArray($this)),"$dir/$file") !== false;
    }
}