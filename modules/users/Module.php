<?php

namespace app\modules\users;


use app\components\ArrayHelper;
use app\models\AccessControl;
use Yii;
use yii\base\ActionEvent;
use yii\base\BootstrapInterface;
use yii\base\Event;
use yii\web\Application;
use yii\web\Controller;

/**
 * users module definition class
 */
class Module extends \yii\base\Module implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\users\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Bootstrap method to be called during application bootstrap stage.
     * @param \yii\base\Application $app the application currently running
     */
    public function bootstrap($app)
    {
        if ($app instanceof Application) {
            $app->getUrlManager()->addRules([
                ['class' => 'yii\web\UrlRule', 'pattern' => $this->id, 'route' => $this->id . '/default/index'],
                ['class' => 'yii\web\UrlRule', 'pattern' => $this->id . '/<username:\w+>', 'route' => $this->id . '/default/profile'],
                ['class' => 'yii\web\UrlRule', 'pattern' => $this->id . '/edit/<username:\w+>', 'route' => $this->id . '/default/edit'],
                ['class' => 'yii\web\UrlRule', 'pattern' => $this->id . '/<controller:[\w\-]+>/<action:[\w\-]+>', 'route' => $this->id . '/<controller>/<action>'],
            ], false);
        }

        Event::on(Controller::className(), Controller::EVENT_AFTER_ACTION, function (ActionEvent $event) {
            if (!Yii::$app->user->isGuest) {
                $user = Yii::$app->user->identity;
                $user->log(
                    $event->action->controller->module->id . '/' .
                    $event->action->controller->id . '/' .
                    $event->action->id . '(' . ArrayHelper::toString(Yii::$app->request->post()) . ')');
            }
        });

    }
}
