<?php
/**
 * Created by
 * User: Wisard17
 * Date: 10/5/2017
 * Time: 9:06 PM
 */

use app\smartadmin\Alert;
use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = Yii::t('app', 'Users Meneger');
$this->params['breadcrumbs'][] = $this->title;

?>


<div class="row">

    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-6">
        <h1 class="page-title txt-color-blueDark"><i class="fa-fw fa fa-handshake-o "></i>
            <?= $this->title ?>
        </h1>
    </div>


    <!-- right side of the page with the sparkline graphs -->
    <!-- col -->
    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-6">
        <!-- sparks -->
        <ul id="sparks">
            <li class="sparks-info .bg-color-teal">
                <?= Html::a('<i class="fa fa-plus-circle"></i>
                    Add', Url::toRoute(['/users/default/add']), ['class' => 'btn btn-primary']) ?>
            </li>

            <li class="sparks-info">
                <h5> Site Orders <span class="txt-color-greenDark"><i class="fa fa-shopping-cart"></i>&nbsp;2447</span>
                </h5>
                <!--                <div class="sparkline txt-color-greenDark hidden-mobile hidden-md hidden-sm">-->
                <!--                    110,150,300,130,400,240,220,310,220,300, 270, 210-->
                <!--                </div>-->
            </li>
        </ul>
        <!-- end sparks -->
    </div>
    <!-- end col -->

</div>
<!-- end row -->

<!--
	The ID "widget-grid" will start to initialize all widgets below
	You do not need to use widgets if you dont want to. Simply remove
	the <section></section> and you can use wells or panels instead
	-->

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <?= Alert::widget() ?>

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget well" id="wid-id-0">
                <!-- widget options:
                    usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                    data-widget-colorbutton="false"
                    data-widget-editbutton="false"
                    data-widget-togglebutton="false"
                    data-widget-deletebutton="false"
                    data-widget-fullscreenbutton="false"
                    data-widget-custombutton="false"
                    data-widget-collapsed="true"
                    data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-comments"></i> </span>
                    <h2>Widget Title </h2>

                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->
                        <input class="form-control" type="text">
                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body no-padding">
                        <div class="ibox-content" id="uploadspase">

                            <?= \app\components\DataTable::widget([
                                'columns' => [
                                    'action',
                                    'Username',
                                    'ruleName',
                                    'First_Name',
                                    'Last_Name',
                                    'Email',
                                ],
                                'options' => [
                                    'order' => "[1, 'ASC']",
                                ],
                                'model' => new \app\modules\users\models\User(),
                            ]) ?>
                        </div>
                        <!-- end widget content -->

                    </div>
                    <!-- end widget div -->

                </div>
                <!-- end widget -->

        </article>
        <!-- WIDGET END -->

    </div>

    <!-- end row -->

    <!-- row -->

    <div class="row">

        <!-- a blank row to get started -->
        <div class="col-sm-12">
            <!-- your contents here -->
        </div>

    </div>

    <!-- end row -->

</section>
