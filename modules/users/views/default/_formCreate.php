<?php
/**
 * Created by
 * User: Wisard17
 * Date: 10/16/2017
 * Time: 9:59 AM
 */

use app\smartadmin\ActiveForm;
use conquer\select2\Select2Asset;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\modules\users\models\FormUser */
/* @var $form ActiveForm */
?>
<div class="add-_form">

    <?php $form = ActiveForm::begin([
        'id' => 'user-form',
//        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}<label class='input'>{input}</label>{error}",
//            'labelOptions' => ['class' => 'label  '],
            'inputOptions' => ['class' => 'form-control'],
        ],
        'options' => [
            'class' => 'smart-form',
        ],
        'enableAjaxValidation' => Yii::$app->request->isAjax,

    ]); ?>

    <header>
        Lengkapi field di bawah ini.
    </header>

    <fieldset>
        <div class="row">
            <section class="col col-6">
                <?= $form->field($model, 'Username') ?>
            </section>
            <section class="col col-6">
                <?= $form->field($model, 'Password') ?>
            </section>
        </div>

    </fieldset>

    <fieldset>
        <div class="row">
            <section class="col col-6">
                <?= $form->field($model, 'First_Name') ?>
            </section>
            <section class="col col-6">
                <?= $form->field($model, 'Last_Name') ?>
            </section>
        </div>

    </fieldset>

    <fieldset>
        <div class="row">
            <section class="col col-6">
                <?= $form->field($model, 'User_Level')
                    ->dropDownList($model->usersLevel == null ? [] : [$model->User_Level => $model->usersLevel->User_Level_Name],
                        ['data-url' => Url::to(['/users/default/list-rule']), 'class' => 'form-control select2ajax']) ?>
            </section>
            <section class="col col-6">
                <?= $form->field($model, 'customer_id')->dropDownList($model->customer == null ? [] : [$model->customer_id => $model->customer->Name],
                    ['data-url' => Url::to(['/users/default/list-customer']), 'class' => 'form-control select2ajax']) ?>
            </section>
        </div>

    </fieldset>

    <fieldset>
        <div class="row">
            <section class="col col-6">
                <?= $form->field($model, 'Email') ?>
            </section>


        </div>
    </fieldset>

    <footer>
        <?= Html::submitButton(Yii::t('app', $model->isNewRecord ? 'Submit ' : 'Save '),
            ['class' => 'btn btn-primary']) ?>
    </footer>

    <?php ActiveForm::end(); ?>

</div>


<?php
Select2Asset::register($this);

$jsCode = <<< JS
$(function () {
    var form = $('#user-form');
    let par = function () {
        return {
            ajax: {
                url: function (a) {
                    return this.attr('data-url');
                }
                
            }
        }
    };
    form.find('.select2ajax').select2(par());
});

JS;


$this->registerJs($jsCode, 4, 'form-user');