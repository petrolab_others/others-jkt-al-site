<?php
/**
 * Created by
 * User: Wisard17
 * Date: 10/16/2017
 * Time: 10:11 AM
 */


/* @var $this yii\web\View */
/* @var $model \app\modules\users\models\FormUser */

use yii\helpers\Url;

$this->title = Yii::t('app', 'Edit Profile');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => Url::toRoute(['/users'])];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">

    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-6">
        <h1 class="page-title txt-color-blueDark"><i class="fa-fw fa fa-cube"></i>
            <?= $this->title ?>
        </h1>
    </div>


    <!-- right side of the page with the sparkline graphs -->
    <!-- col -->
    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-6">
        <!-- sparks -->
        <ul id="sparks">

            <li class="sparks-info">
                <h5> My Income <span class="txt-color-blue">$47,171</span></h5>
                <!--                <div class="sparkline txt-color-blue hidden-mobile hidden-md hidden-sm">-->
                <!--                    1300, 1877, 2500, 2577, 2000, 2100, 3000, 2700, 3631, 2471, 2700, 3631, 2471-->
                <!--                </div>-->
            </li>
            <li class="sparks-info">
                <h5> Site Traffic <span class="txt-color-purple"><i class="fa fa-arrow-circle-up"
                                                                    data-rel="bootstrap-tooltip" title="Increased"></i>&nbsp;45%</span>
                </h5>
                <!--                <div class="sparkline txt-color-purple hidden-mobile hidden-md hidden-sm">-->
                <!--                    110,150,300,130,400,240,220,310,220,300, 270, 210-->
                <!--                </div>-->
            </li>
            <li class="sparks-info">
                <h5> Site Orders <span class="txt-color-greenDark"><i class="fa fa-shopping-cart"></i>&nbsp;2447</span>
                </h5>
                <!--                <div class="sparkline txt-color-greenDark hidden-mobile hidden-md hidden-sm">-->
                <!--                    110,150,300,130,400,240,220,310,220,300, 270, 210-->
                <!--                </div>-->
            </li>
        </ul>
        <!-- end sparks -->
    </div>
    <!-- end col -->

</div>

<section id="widget-grid" class="">

    <!-- START ROW -->
    <div class="row">

        <!-- NEW COL START -->
        <article class="col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false"
                 data-widget-custombutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                    <h2>Form  </h2>

                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body ">
                        <?= $this->render('_form', [
                            'model' => $model,
                        ]) ?>
                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

        </article>
        <!-- END COL -->

    </div>

    <!-- END ROW-->

</section>
<!-- end widget grid -->
