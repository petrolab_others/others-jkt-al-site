<?php
/**
 * Created by
 * User: Wisard17
 * Date: 20/11/2017
 * Time: 10.00 AM
 */


/**
 * @var $model \app\modules\users\models\User
 * @var $this yii\web\View
 *
 */


use yii\helpers\Url;

$this->title = Yii::t('app', '' . $model->First_Name . '');
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="row">

    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-6">
        <h1 class="page-title txt-color-blueDark"><i class="fa-fw fa fa-user-circle "></i>
            <?= $this->title ?>
        </h1>
    </div>


    <!-- right side of the page with the sparkline graphs -->
    <!-- col -->
    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-6">
        <!-- sparks -->
        <ul id="sparks">
            <li class="sparks-info">
                <h5> My Income <span class="txt-color-blue">$47,171</span></h5>
                <!--                <div class="sparkline txt-color-blue hidden-mobile hidden-md hidden-sm">-->
                <!--                    1300, 1877, 2500, 2577, 2000, 2100, 3000, 2700, 3631, 2471, 2700, 3631, 2471-->
                <!--                </div>-->
            </li>
            <li class="sparks-info">
                <h5> Site Traffic <span class="txt-color-purple"><i class="fa fa-arrow-circle-up"
                                                                    data-rel="bootstrap-tooltip" title="Increased"></i>&nbsp;45%</span>
                </h5>
                <!--                <div class="sparkline txt-color-purple hidden-mobile hidden-md hidden-sm">-->
                <!--                    110,150,300,130,400,240,220,310,220,300, 270, 210-->
                <!--                </div>-->
            </li>
            <li class="sparks-info">
                <h5> Site Orders <span class="txt-color-greenDark"><i class="fa fa-shopping-cart"></i>&nbsp;2447</span>
                </h5>
                <!--                <div class="sparkline txt-color-greenDark hidden-mobile hidden-md hidden-sm">-->
                <!--                    110,150,300,130,400,240,220,310,220,300, 270, 210-->
                <!--                </div>-->
            </li>
        </ul>
        <!-- end sparks -->
    </div>
    <!-- end col -->

</div>

<section id="widget-grid" class="well well-light well-sm no-margin no-padding">

    <!-- row -->
    <div class="row">



        <div class="col-sm-12">

            <div class="row">

                <div class="col-sm-3 profile-pic">

                    <div class="padding-10">
                        <h4 class="font-md"><strong><?= ''?></strong>
                            <br>
                            <small><!-- ssss--></small></h4>
                        <br>
                        <h4 class="font-md"><strong><?= ''?></strong>
                            <br>
                            <small><!-- ssss--> </small></h4>
                    </div>
                </div>
                <div class="col-sm-6">
                    <h1> <span class="semi-bold"><?= $model->First_Name . ' ' . $model->Last_Name?></span>
                        <br>
                        <small>  <?= $model->ruleName?></small></h1>

                    <ul class="list-unstyled">
                        <li>
                            <p class="text-muted">
                                <i class="fa fa-phone"></i> Phone :&nbsp;&nbsp;<?= ''?>
                            </p>
                        </li>
                        <li>
                            <p class="text-muted">
                                <i class="fa fa-envelope"></i> Email :&nbsp;&nbsp;<?= $model->Email?>
                            </p>
                        </li>
                        <li>
                            <p class="text-muted">
                                <i class="fa fa-fax"></i> Fax :&nbsp;&nbsp;<?= ''?>
                            </p>
                        </li>
                        <li>
                            <p class="text-muted">
                                <i class="fa fa-home"></i>&nbsp;Address :&nbsp;<?= ''?>
                            </p>
                        </li>
                    </ul>
                    <br>
                    <p class="font-md">
                        <i>detail</i>
                    </p>
                    <ul class="list-unstyled">
                        <li>
                            <p class="text-muted">
                                <i class="fa fa-phone"></i>&nbsp;Phone : <?= ''?>
                            </p>
                        </li>

                        <li>
                            <p class="text-muted">
                                <i class="fa fa-fax"></i> Fax :&nbsp;<?= ''?>
                            </p>
                        </li>
                        <li>
                            <p class="text-muted">
                                <i class="fa fa-home"></i>&nbsp;Address :&nbsp;<?= ''?>
                            </p>
                        </li>
                    </ul>

                    <br>
                    <a href="javascript:void(0);" class="btn btn-default btn-xs"><i class="fa fa-envelope-o"></i> Send Message</a>
                    <br>
                    <br>

                </div>
                <div class="col-sm-3">
                    <h1><small>Contact</small></h1>
                    <?= ''?>
                    <ul class="list-inline friends-list">
                        <!--                        <li><img src="img/avatars/1.png" alt="friend-1">-->
                        <!--                        </li>-->
                        <!--                        <li><img src="img/avatars/2.png" alt="friend-2">-->
                        <!--                        </li>-->
                        <!--                        <li><img src="img/avatars/3.png" alt="friend-3">-->
                        <!--                        </li>-->
                        <!--                        <li><img src="img/avatars/4.png" alt="friend-4">-->
                        <!--                        </li>-->
                        <!--                        <li><img src="img/avatars/5.png" alt="friend-5">-->
                        <!--                        </li>-->
                        <!--                        <li><img src="img/avatars/male.png" alt="friend-6">-->
                        <!--                        </li>-->
                        <!--                        <li>-->
                        <!--                            <a href="javascript:void(0);">413 more</a>-->
                        <!--                        </li>-->
                    </ul>



                </div>

            </div>
            <div class="row">
                <div class="col-sm-4"></div>
                <div class="col-sm-8 ">

                    <div class="btn-group">
                        <a href="<?= Url::toRoute(['/users/default/edit', 'username' => $model->username]) ?>"
                           class="btn btn-sm btn-primary"> <i class="fa fa-edit"></i> Edit </a>
                    </div>

                    <div class="btn-group">
                        <a href="<?= Url::toRoute(['/users/add/del', 'id' => $model->username]) ?>"
                           class="btn btn-sm btn-danger" data-method='post'> <iclass="fa fa-close"></i> Delete </a>
                    </div>

                </div>
            </div>
            <br>
            <br>
        </div>

    </div>

    <!-- end row -->

</section>
